package appsoft.erp.gaikwad.activities.activitys;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.extras.TodaysDate;
import appsoft.erp.gaikwad.activities.modals.ResponseModel;
import appsoft.erp.gaikwad.activities.modals.StudentsName;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import appsoft.erp.gaikwad.activities.volley_singleton.NetworkStatus;

public class FeedbackActivity extends AppCompatActivity implements NetworkStatus{

    private Spinner studentName;
    private TextInputEditText description;
    private String todaysDate;
    private CoordinatorLayout layout;
    private String studentNameString = "";
    private SessionManagement session;
    private List<StudentsName> studentsList = null;
    private int studentId = 0;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback_layout);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        session = new SessionManagement(this);
        initialiseData();

        if (isNetworkAvailable()) {
            setStudentsToList();
        } else {
            final Snackbar snackbar = Snackbar.make(layout, getResources().getString(R.string.no_connection), Snackbar.LENGTH_INDEFINITE);
            snackbar.setActionTextColor(getResources().getColor(R.color.colorDarkRedBlood));
            snackbar.setAction("RETRY", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isNetworkAvailable()) {
                        snackbar.show();
                    } else {
                        setStudentsToList();
                    }
                }
            });
            snackbar.show();
        }

        studentName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StudentsName name = (StudentsName) parent.getSelectedItem();

                studentId = Integer.parseInt(name.getStudentId());

                studentNameString = name.toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setStudentsToList(){
        ApiInterface stuInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<StudentsName>> call = stuInterface.getAllStudent(session.getUserId());
        call.enqueue(new Callback<List<StudentsName>>() {
            @Override
            public void onResponse(Call<List<StudentsName>> call, retrofit2.Response<List<StudentsName>> response) {

                studentsList = response.body();

                try {
                    ArrayAdapter<StudentsName> classAdapter = new ArrayAdapter<StudentsName>(getApplicationContext(), R.layout.text_filter_view, studentsList);
                    classAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
                    studentName.setSelection(0);
                    studentName.setPrompt("Student Name");
                    studentName.setAdapter(classAdapter);
                } catch (NullPointerException npe) {
                    showPopUp();
                    Log.i("DEBUG", "Null Pointer Exception Occur: " + npe.toString());
                } catch (Exception exp) {
                    Log.i("DEBUG", "Exception Occur: " + exp.toString());
                }
            }

            @Override
            public void onFailure(Call<List<StudentsName>> call, Throwable t) {
                showPopUp();
                Log.i("DEBUG", "Failure Occur: "+t.toString());
            }
        });
    }

    private void showPopUp(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Suggestion");
        builder.setCancelable(false);
        builder.setMessage("No Student Available...");
        builder.setNeutralButton("Go Back", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        Dialog dialog = builder.create();
        dialog.show();
    }


    @SuppressLint("RestrictedApi")
    private void initialiseData() {
        studentName = findViewById(R.id.spinnerStudent);
        description = findViewById(R.id.description);
        todaysDate = TodaysDate.getTodaysDate();
        layout = findViewById(R.id.feedbackLayout);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }
    }


    public void addFeedBack(View view){

        if (isAllFieldFilled()){
            if (isNetworkAvailable()){
                ApiInterface in = ClientAPI.getRetrofit().create(ApiInterface.class);
                Call<ResponseModel> call = in.addFeedback(studentId, description.getText().toString(), session.getUserId());
                call.enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                        ResponseModel res = response.body();
                        try {
                            Toast toast = Toast.makeText(getApplicationContext(), "" + res.getRes(), Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            refreshView();
                        }catch (Exception exp){
                            Log.i("DEBUG", "Exception Occur: "+exp.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {
                        Log.i("DEBUG", "Failure Occur: "+t.getMessage());
                    }
                });
            }
        }else{
            Toast.makeText(this, "Check all fields are filled or not.", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isAllFieldFilled() {

        if (description.getText().toString().trim().isEmpty()){
            return false;
        }else if (studentNameString.isEmpty()){
            return false;
        }else{
            return true;
        }
    }

    @Override
    public boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        return networkInfo!=null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.finish();
            return true;
        }else{
            return super.onOptionsItemSelected(item);
        }
    }

    private void refreshView(){
        description.setText("");
    }
}
