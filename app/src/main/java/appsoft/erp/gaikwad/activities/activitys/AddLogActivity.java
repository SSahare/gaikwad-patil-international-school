package appsoft.erp.gaikwad.activities.activitys;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import appsoft.erp.gaikwad.activities.helper.AddActionHelper;
import appsoft.erp.gaikwad.activities.interfaces.AddActionInterface;
import retrofit2.Call;
import retrofit2.Callback;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.extras.DatePickerFragment;
import appsoft.erp.gaikwad.activities.extras.TodaysDate;
import appsoft.erp.gaikwad.activities.modals.ResponseModel;
import appsoft.erp.gaikwad.activities.modals.Subject;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import appsoft.erp.gaikwad.activities.volley_singleton.NetworkStatus;

public class AddLogActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener
        , NetworkStatus{

    private Spinner classname,section, subject;
    private TextView logDate;
    private EditText description;
    private CoordinatorLayout layout;
    private static int flag_class = 0;
    private static int flag_section = 0;
    private String classNameString = "";
    private String sectionNameString = "";
    private String subjectNameString = "";
    private SessionManagement session;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_log_layout);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        session = new SessionManagement(getApplicationContext());
        initialiseData();

        if (isNetworkAvailable()) {
            setClassNames();
        } else {
            final Snackbar snackbar = Snackbar.make(layout, getResources().getString(R.string.no_connection), Snackbar.LENGTH_INDEFINITE);
            snackbar.setActionTextColor(getResources().getColor(R.color.colorDarkRedBlood));
            snackbar.setAction("RETRY", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isNetworkAvailable()) {
                        snackbar.show();
                    }else {
                        setClassNames();
                    }
                }
            });
            snackbar.show();
        }

        classname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               /* if (position>0){

                    if (position > 0 && position < 4){
                        subjectNameString="Pre-Primary Sub";
                    }
                    flag_class = 1;
                    classNameString = (String) parent.getSelectedItem();
                }*/

                if (position>0){
                    classNameString = classname.getItemAtPosition(position).toString();
                    AddActionHelper.getSection(getApplicationContext(), classNameString, new AddActionInterface() {
                        @Override
                        public void getDataListener(List<String> list) {
                            ArrayAdapter<String> secAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.text_filter_view, list);
                            secAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
                            section.setAdapter(secAdapter);
                        }
                    });
                }else {
                    classNameString = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        section.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                /*if (position>0){
                    flag_section = 1;
                    sectionNameString = (String) parent.getSelectedItem();
                }*/


                if (position > 0) {
                    sectionNameString = section.getItemAtPosition(position).toString();
                    AddActionHelper.getSubjects(getApplicationContext(), classNameString, sectionNameString, new AddActionInterface(){
                        @Override
                        public void getDataListener(List<String> list) {
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.text_filter_view, list);
                            adapter.setDropDownViewResource(R.layout.text_drop_down_theme);
                            subject.setAdapter(adapter);
                        }
                    });
                }else {
                    sectionNameString = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        subject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               /* if (flag_class == 1) {
                    subjectNameString = (String) parent.getSelectedItem();
                }*/

                if (position>0){
                    subjectNameString = (String) parent.getSelectedItem();
                }else {
                    subjectNameString = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /*private void getSubjectTeacherDetail() {


        ApiInterface i = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<Subject>> call = i.getStudentDetail(new SessionManagement(getApplicationContext()).getUserId());
        call.enqueue(new Callback<List<Subject>>() {
            @Override
            public void onResponse(Call<List<Subject>> call, Response<List<Subject>> response) {
                if (response.isSuccessful()) {
                    List<Subject> myClass = response.body();
                    parseData(myClass);
                } else {
                    Toast.makeText(AddLogActivity.this, "Oops, class not found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Subject>> call, Throwable t) {
                Log.i("DEBUG", "onFailure: " + t.getMessage());
                Toast.makeText(AddLogActivity.this, "Server failure: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
*/
   /* private void parseData(List<Subject> myClass) {
        // Filter data here
        Set<String> classes = new HashSet<>();
        Set<String> subjects = new HashSet<>();
        Set<String> sections = new HashSet<>();

        try {
            for (Subject sub : myClass) {
                classes.add(sub.getClass_());
                subjects.add(sub.getSubject());
                sections.add(sub.getSection());
            }
        }catch (NullPointerException npe){npe.printStackTrace();}

        List<String> clslist = new ArrayList<>();
        clslist.add("Select class");
        clslist.addAll(classes);
        setArrayAdapterToClasses(clslist);

        List<String> sublist = new ArrayList<>();
        sublist.add("Select subject");
        sublist.addAll(subjects);
        setArrayAdapterToSubject(sublist);

        List<String> seclist = new ArrayList<>();
        seclist.add("Select section");
        seclist.addAll(sections);
        setArrayAdapterToSection(seclist);
    }
*/
    private void setArrayAdapterToSection(List<String> sectionList) {
        try {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.text_filter_view, sectionList);
            adapter.setDropDownViewResource(R.layout.text_drop_down_theme);
            section.setAdapter(adapter);

        } catch (NullPointerException npe) {
            Log.i("DEBUG", "No subject");
        } catch (Exception exp) {
            Log.i("DEBUG", "Exception");
        }
    }
    private void setArrayAdapterToSubject(List<String> subjectList) {
        try {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.text_filter_view, subjectList);
            adapter.setDropDownViewResource(R.layout.text_drop_down_theme);
            subject.setAdapter(adapter);

        } catch (NullPointerException npe) {
            Log.i("DEBUG", "No subject");
        } catch (Exception exp) {
            Log.i("DEBUG", "Exception");
        }
    }

    private void setArrayAdapterToClasses(List<String> classList) {
        try {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.text_filter_view, classList);
            adapter.setDropDownViewResource(R.layout.text_drop_down_theme);
            classname.setAdapter(adapter);

        } catch (NullPointerException npe) {
            Log.i("DEBUG", "No subject");
        } catch (Exception exp) {
            Log.i("DEBUG", "Exception");
        }
    }

//    private void getClassOfStudent() {
//        ApiInterface i = ClientAPI.getRetrofit().create(ApiInterface.class);
//        Call<List<Class>> call = i.getStudentClass(new SessionManagement(getApplicationContext()).getUserId());
//        call.enqueue(new Callback<List<Class>>() {
//            @Override
//            public void onResponse(Call<List<Class>> call, Response<List<Class>> response) {
//                if (response.isSuccessful()){
//                    List<Class> myClass = response.body();
//                    String myclass = myClass;
//                    getSubjectNamesFromServer(myclass);
//                }else {
//                    Toast.makeText(AddLogActivity.this, "Oops, class not found", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<Class>> call, Throwable t) {
//                Toast.makeText(AddLogActivity.this, "Server failure: "+t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

    private void setArrayAdapterTosubject(List<String> subjectList){
        try{
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.text_filter_view, subjectList);
            adapter.setDropDownViewResource(R.layout.text_drop_down_theme);
            subject.setAdapter(adapter);
        }catch (NullPointerException npe){
            Log.i("DEBUG", "No subject");
        }catch (Exception exp){
            Log.i("DEBUG", "Exception");
        }
    }

    private void getSubjectNamesFromServer(String str_sub) {
        ApiInterface apiInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<Subject>> call = apiInterface.getAllSubject(str_sub);
        call.enqueue(new Callback<List<Subject>>() {
            @Override
            public void onResponse(Call<List<Subject>> call, retrofit2.Response<List<Subject>> response) {
                if (response.isSuccessful()){
                    try {
                        List<Subject> sub_all = response.body();
                        List<String> sub_str = new ArrayList<>();
                        for (Subject s : sub_all){
                            sub_str.add(s.getSubject());
                        }
                        setArrayAdapterTosubject(sub_str);

                    }catch (NullPointerException npe) {
                        Log.i("DEBUG", "Subject Null Pointer Exception");
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Subject>> call, Throwable t) {
                Log.i("DEBUG", "Loading subject: "+t.toString());
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void initialiseData() {
        classname = findViewById(R.id.spinnerClass);
        section = findViewById(R.id.spinnerSection);
        subject = findViewById(R.id.spinnerSubject);
        logDate = findViewById(R.id.logDate);
        description = findViewById(R.id.description);
        layout = findViewById(R.id.logLayout);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        setClassNames();
//        setSectionName();

        logDate.setText(TodaysDate.getTodaysDate());

        logDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callDatePicker(view);
            }
        });
    }

    private void setClassNames() {
        /*ArrayAdapter<CharSequence> classAdapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.list_of_class, R.layout.text_filter_view);
        classAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
        classname.setAdapter(classAdapter);*/

        AddActionHelper.getClasses(getApplicationContext(), new AddActionInterface() {
            @Override
            public void getDataListener(List<String> list) {
                if (list!=null){
                    ArrayAdapter<String> arrayAdapter =new ArrayAdapter<>(getApplicationContext(),R.layout.text_filter_view,list);
                    arrayAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
                    classname.setAdapter(arrayAdapter);
                }else {
                    Toast.makeText(AddLogActivity.this, "Subject is not assigned to you", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

  /*  private void setSectionName(){
        ArrayAdapter<CharSequence> classAdapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.list_of_section, R.layout.text_filter_view);
        classAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
        section.setAdapter(classAdapter);
    }*/

    private void callDatePicker(View view){
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.show(getFragmentManager(),"DatePick");
    }


    public void addLog(View view){
        if (isAllFieldFilled()){
            if(isNetworkAvailable()){
                ApiInterface in = ClientAPI.getRetrofit().create(ApiInterface.class);
                Call<ResponseModel> call = in.addSubjectDairy(
                        session.getUserId(),
                        logDate.getText().toString(),
                        classNameString,
                        sectionNameString,
                        subjectNameString,
                        description.getText().toString()
                );

                call.enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call, retrofit2.Response<ResponseModel> response) {
                        ResponseModel res = response.body();
                        Toast toast = Toast.makeText(getApplicationContext(), ""+res.getRes(), Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        refreshView();
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {

                    }
                });
            }
        }else{
            Toast.makeText(this, "Check all fields are filled or not.", Toast.LENGTH_SHORT).show();
        }
    }

    private void refreshView(){
        setClassNames();
//        setSectionName();
        description.setText("");
    }

    private boolean isAllFieldFilled() {

        if (subjectNameString.isEmpty()){
            return false;
        }else if (classNameString.isEmpty()){
            return false;
        }else if (description.getText().toString().trim().isEmpty()){
            return false;
        }else if (sectionNameString.isEmpty()){
            return false;
        }else if (logDate.getText().toString().trim().isEmpty()){
            return false;
        }else {
            return true;
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        Calendar calendar = new GregorianCalendar(year,month,day);
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.ENGLISH);
        logDate.setText(dateFormat.format(calendar.getTime()));
    }

    @Override
    public boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        return networkInfo!=null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.finish();
            return true;
        }else{
            return super.onOptionsItemSelected(item);
        }
    }
}
