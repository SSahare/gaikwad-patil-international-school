package appsoft.erp.gaikwad.activities.open_results;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import appsoft.erp.gaikwad.R;

public class ParentLogDetailActivity  extends AppCompatActivity {
    TextView tv_log_Subject,tv_Log_Description;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parent_log_activity);
//        tv_LogDate = findViewById(R.id.tv_LogDate);
        tv_log_Subject=findViewById(R.id.tv_log_Subject);
        tv_Log_Description=findViewById(R.id.tv_Log_Description);


        tv_log_Subject.setText(getIntent().getStringExtra("logsubject"));
        tv_Log_Description.setText(getIntent().getStringExtra("logdescriptiion"));//logdescriptiion

        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Log Detail");

           // getSupportActionBar().setTitle(getIntent().getStringExtra("assignmentsubject"));
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}



