package appsoft.erp.gaikwad.activities.teacher_profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeacherAddressActivity extends AppCompatActivity {
    private TeacherProfileModel profileModel;
    private SessionManagement sessionManagement;
    TextView localaddress, permanentaddress;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teacher_address_detail);
        sessionManagement=new SessionManagement(getApplicationContext());
        localaddress=findViewById(R.id.tv_address);
        permanentaddress=findViewById(R.id.tv_permanent_Address);
        getDataFromServer();


        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Address Detail");
        }
    }

    private void getDataFromServer() {

        ApiInterface in = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<TeacherProfileModel> call = in.getTProfileData(sessionManagement.getUserId());
        call.enqueue(new Callback<TeacherProfileModel>() {
            @Override
            public void onResponse(Call<TeacherProfileModel> call, Response<TeacherProfileModel> response) {
            profileModel  = response.body();

//                if (profileModel.getEducation().size() != 0){
//                    setEducationalDetails();
//                }
//
//                setBasicDetails();
                setAddressDetails();
            }

            @Override
            public void onFailure(Call<TeacherProfileModel> call, Throwable t) {
                Log.i("DEBUG", "Error Ocurred");
            }
        });
    }

    private void setAddressDetails() {
        localaddress.setText(profileModel.getBasic().getLocal());
        permanentaddress.setText(profileModel.getBasic().getPermanent());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
}
}
