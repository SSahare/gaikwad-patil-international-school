package appsoft.erp.gaikwad.activities.rest_api_config;

import java.util.List;
import appsoft.erp.gaikwad.activities.modals.ByDayModel;
import appsoft.erp.gaikwad.activities.modals.ByMonthModel;
import appsoft.erp.gaikwad.activities.modals.ByStudentModel;
import appsoft.erp.gaikwad.activities.modals.Class;
import appsoft.erp.gaikwad.activities.modals.ClassTeacherDetail;
import appsoft.erp.gaikwad.activities.modals.MonthlyDetailAttendanceModel;
import appsoft.erp.gaikwad.activities.modals.ReportModel;
import appsoft.erp.gaikwad.activities.modals.Section;
import appsoft.erp.gaikwad.activities.modals.TeacherDashboardModel;
import appsoft.erp.gaikwad.activities.modals.TodaysSchedule;
import appsoft.erp.gaikwad.activities.teacher_profile.TeacherProfileModel;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import appsoft.erp.gaikwad.activities.modals.Assignments;
import appsoft.erp.gaikwad.activities.modals.Attendance;
import appsoft.erp.gaikwad.activities.modals.Circular;
import appsoft.erp.gaikwad.activities.modals.ClassTimeTable;
import appsoft.erp.gaikwad.activities.modals.Event;
import appsoft.erp.gaikwad.activities.modals.Feedback;
import appsoft.erp.gaikwad.activities.modals.Fees;
import appsoft.erp.gaikwad.activities.modals.FeesRecord;
import appsoft.erp.gaikwad.activities.modals.Homework;
import appsoft.erp.gaikwad.activities.modals.Leave;
import appsoft.erp.gaikwad.activities.modals.ListAttendance;
import appsoft.erp.gaikwad.activities.modals.Log;
import appsoft.erp.gaikwad.activities.modals.LoginModel;
import appsoft.erp.gaikwad.activities.modals.News;
import appsoft.erp.gaikwad.activities.modals.ResponseModel;
import appsoft.erp.gaikwad.activities.modals.Students;
import appsoft.erp.gaikwad.activities.modals.StudentsName;
import appsoft.erp.gaikwad.activities.modals.Subject;
import appsoft.erp.gaikwad.activities.modals.UserProfileModel;
import retrofit2.http.Streaming;

public interface ApiInterface {

    // -------------------------------------- //
    // -------------Login Panel------------- //
    // -------------------------------------- //

    @GET("{receipt_name}")
    @Streaming
    Call<ResponseBody> downloadFile(@Path("receipt_name") String receipt_name);


    @FormUrlEncoded
    @POST("login/force_password_change")
    Call<Boolean> getForceFulResponse(@Field("username") String username, @Field("password_new") String password, @Field("password_conf_pass" )String conf_password);

    @FormUrlEncoded
    @POST("login/check_user")
    Call<LoginModel> getLoginCredentials(@Field("username") String username, @Field("password") String password);

    // -------------------------------------- //
    // -------------Common Panel------------- //
    // -------------------------------------- //

    @GET("parents/news")
    Call<List<News>> getNewsList();

    @GET("parents/circulars")
    Call<List<Circular>> getCircularList();

    @GET("parents/view_event")
    Call<List<Event>> getEventList();

    // -------------------------------------- //
    // -------------Parent Panel------------- //
    // -------------------------------------- //

    @GET("parents/get_class_student/{student_id}")
    Call<List<Class>> getStudentClass(@Path("student_id") int student_id);

    @GET("parents/assignment/{student_id}")
    Call<List<Assignments>> getStudentAssignmentList(@Path("student_id") int student_id);

    @GET("parents/homework/{student_id}")
    Call<List<Homework>> getStudentHomeworkList(@Path("student_id") int student_id);

    @GET("parents/view_daily_log/{student_id}")
    Call<List<Log>> getStudentLogList(@Path("student_id") int student_id);

    @GET("parents/feedback/{student_id}")
    Call<List<Feedback>> getStudentFeedbackList(@Path("student_id") int student_id);

    @GET("parents/attendance_for_dashboard/{student_id}")
    Call<Attendance> getDashboardAttendance(@Path("student_id") int student_id);

    @GET("parents/attendance/{student_id}")
    Call<List<ListAttendance>> getAttendance(@Path("student_id") int student_id);

    @GET("parents/fees_record/{student_id}")
    Call<Fees> getStudentFeesRecord(@Path("student_id") int student_id);

    @GET("parents/fees_record_log/{student_id}")
    Call<List<FeesRecord>> getStudentFeesLog(@Path("student_id") int student_id);

    @GET("parents/enroll_for_event/{event_id}/{student_id}")
    Call<String> getEnrollment(@Path("event_id") int event_id, @Path("student_id") int student_id);

    @GET("parents/day_wise_time_table/{student_id}/{day}")
    Call<List<ClassTimeTable>> getClassTimeTable(@Path("student_id") int student_id, @Path("day") String day);

    @GET("parents/profile/{student_id}")
    Call<UserProfileModel> getProfileData(@Path("student_id") int student_id);

    @FormUrlEncoded
    @POST("parents/report_card")
    Call<List<ReportModel>> getReport(@Field("student_id") int student_id, @Field("exam_name") String exam_name);


    @FormUrlEncoded
    @POST("parents/get_attendace_record_month")
    Call<List<MonthlyDetailAttendanceModel>> getAttendanceDetail(@Field("user_id") int user_id, @Field("date") String date);


    // --------------------------------------- //
    // -------------Teacher Panel------------- //
    // --------------------------------------- //

    @GET("parents/subjects/{class}")
    Call<List<Subject>> getAllSubject(@Path("class") String myclass);

    @GET("teacher/subject_teacher/{teacher_id}")
    Call<List<Subject>> getStudentDetail(@Path("teacher_id") int student_id);

    @GET("teacher/enroll_for_event/{event_id}/{teacher_id}")
    Call<String> getEnrollmentTeacher(@Path("event_id") int event_id, @Path("teacher_id") int teacher_id);

    @GET("teacher/view_assignment/{teacher_id}")
    Call<List<Assignments>> getAssignmentList(@Path("teacher_id") int teacher_id);

    @GET("teacher/view_homework/{teacher_id}")
    Call<List<Homework>> getHomeworkList(@Path("teacher_id") int teacher_id);

    @GET("teacher/view_daily_log/{teacher_id}")
    Call<List<Log>> getLogList(@Path("teacher_id") int teacher_id);

    @GET("teacher/view_feedback/{teacher_id}")
    Call<List<Feedback>> getFeedbackList(@Path("teacher_id") int teacher_id);

    @FormUrlEncoded
    @POST("teacher/class_time_table")
    Call<List<ClassTimeTable>> getTeacherTimeTable(@Field("tt_class") String tt_class, @Field("section") String section, @Field("day") String day);


    // Add Activity Pre requirements

    @GET("teacher/assigned_classes/{teacher_id}")
    Call<List<Class>> getAssignedClasses(@Path("teacher_id") int teacher_id);

    @GET("teacher/assigned_class_section/{teacher_id}/{class_name}")
    Call<List<Section>> getAssignedClassSection(@Path("teacher_id")int teacher_id, @Path("class_name") String class_name);

    @GET("teacher/assigned_subjects/{teacher_id}/{class_name}/{section}")
    Call<List<Subject>> getAssignedSubjects(@Path("teacher_id") int teacher_id, @Path("class_name") String class_name, @Path("section") String section);

    @FormUrlEncoded
    @POST("teacher/add_assignment")
    Call<ResponseModel> addAssignment(
            @Field("teacher_id") int teacher_id,
            @Field("assignment_class") String assignment_class,
            @Field("section") String  section,
            @Field("submission_date") String  submission_date,
            @Field("subject") String  subject,
            @Field("title") String  title,
            @Field("description") String  description
            //@Part MultipartBody.Part assignment_file
    );

    @FormUrlEncoded
    @POST("teacher/add_homework")
    Call<ResponseModel> addHomework(
            @Field("teacher_id") int teacher_id,
            @Field("assignment_class") String assignment_class,
            @Field("section") String section,
            @Field("submission_date") String submission_date,
            @Field("subject") String subject,
            @Field("title") String title,
            @Field("description") String description
    );

    @FormUrlEncoded
    @POST("teacher/add_daily_log")
    Call<ResponseModel> addSubjectDairy(
            @Field("teacher_id") int teacher_id,
            @Field("log_date") String assignment_class,
            @Field("log_class") String section,
            @Field("log_section") String submission_date,
            @Field("log_subject") String subject,
            @Field("log_description") String title
    );

    @GET("teacher/feedback/{teacher_id}")
    Call<List<StudentsName>> getAllStudent(@Path("teacher_id") int teacher_id);

    @FormUrlEncoded
    @POST("teacher/add_feedback")
    Call<ResponseModel> addFeedback(
            @Field("student_id") int student_id,
            @Field("content") String content,
            @Field("teacher_id") int teacher_id
    );

    @GET("teacher/attendance/{teacher_id}")
    Call<Students> getAttendanceList(@Path("teacher_id") int teacher_id);

    @FormUrlEncoded
    @POST("teacher/apply_for_leave/{teacher_id}")
    Call<ResponseModel> applyForLeave(
            @Path("teacher_id") int teacher_id,
            @Field("leave_type") String leave_type,
            @Field("date_from") String date_from,
            @Field("date_to") String date_to,
            @Field("reason") String reason
    );

    @GET("teacher/leaves/{teacher_id}")
    Call<List<Leave>> getLeaves(@Path("teacher_id") int teacher_id);

    @GET("teacher/get_todays_leactures/{teacher_id}")
    Call<List<TodaysSchedule>>getTodaysSchedule(@Path("teacher_id") int teacher_id);


    @FormUrlEncoded
    @POST("teacher/mark_attendance")
    Call<ResponseModel> saveAttendance(
            @Field("teacher_id") int teacher_id,
                @Field("attendance_class") String attendance_class,
            @Field("class_section") String class_section,
            @Field("attendance_date") String attendance_date,
            @Field("attendance[]") List<String> studentsNames
    );

    @FormUrlEncoded
    @POST("teacher/view_teacher_profile")
    Call<TeacherProfileModel> getTProfileData(@Field("teacher_id") int teacher_id);

    @FormUrlEncoded
    @POST("teacher/class_teacher_details")
    Call<ClassTeacherDetail> getClassTeacherDetail(@Field("teacher_id") int teacher_id);

    @FormUrlEncoded
    @POST("teacher/view_student_attendance")
    Call<List<ByDayModel>> getDayAttendanceList(
            @Field("type") String type,
            @Field("class") String className,
            @Field("section") String section,
            @Field("date") String date);

    @FormUrlEncoded
    @POST("teacher/view_student_attendance")
    Call<List<ByMonthModel>> getMonthAttendanceList(
            @Field("type") String type,
            @Field("class") String className,
            @Field("section") String section,
            @Field("month") String month);

    @FormUrlEncoded
    @POST("teacher/view_student_attendance")
    Call<List<ByStudentModel>> getStudentAttendanceList(
            @Field("type") String type,
            @Field("class") String className,
            @Field("section") String section,
            @Field("student_id") String student_id);

    @FormUrlEncoded
    @POST("teacher/subject_teacher_details")
    Call<TeacherDashboardModel> getTeacherDetailDashboard(@Field("teacher_id") int teacher_id);
}