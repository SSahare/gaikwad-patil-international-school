package appsoft.erp.gaikwad.activities.parents_fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;

import com.github.demono.AutoScrollViewPager;

import java.util.ArrayList;
import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.extras.Constants;
import appsoft.erp.gaikwad.activities.fragments.CircularFragment;
import appsoft.erp.gaikwad.activities.fragments.DashboardModel;
import appsoft.erp.gaikwad.activities.fragments.EventFragment;
import appsoft.erp.gaikwad.activities.fragments.NewsFragment;
import appsoft.erp.gaikwad.activities.fragments.ViewAssignmentListFragment;
import appsoft.erp.gaikwad.activities.fragments.ViewHomeworkListFragment;
import appsoft.erp.gaikwad.activities.fragments.ViewLogFragment;
import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import appsoft.erp.gaikwad.activities.modals.Attendance;
import appsoft.erp.gaikwad.activities.pager_adapter.ImagePagerAdapter;
import appsoft.erp.gaikwad.activities.parent_activity.ReportActivity;
import appsoft.erp.gaikwad.activities.parent_adapters.ParentDashdoardAdapter;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ParentDashboardFragment extends Fragment implements View.OnClickListener,ParentDashdoardAdapter.ParentDashbaordListerner ,OnTitleSetListener {
    private SessionManagement session;
    private Context context;
    RecyclerView parentrecycleview;
    private ImagePagerAdapter adapter;
    private AutoScrollViewPager autoScrollViewPager;

    DashboardModel mFlowerData;
    public ParentDashboardFragment(){ }
    public static OnTitleSetListener onTitleSetListener = null;
    public static ParentDashboardFragment getInstance(OnTitleSetListener onTitleSetListener2){
        onTitleSetListener =onTitleSetListener2;
        return new ParentDashboardFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {


         View view=inflater.inflate(R.layout.parent_dashboard_fragment,container,false);
         session = new SessionManagement(getContext());


        autoScrollViewPager=view.findViewById(R.id.autoscrollview);
        adapter= new ImagePagerAdapter(getContext());
        autoScrollViewPager.setAdapter(adapter);
        autoScrollViewPager.startAutoScroll();
        autoScrollViewPager.setCycle(true);
        autoScrollViewPager.setDirection(View.SCROLLBAR_POSITION_LEFT);
        autoScrollViewPager.setSlideInterval(1500);
        parentrecycleview=view.findViewById(R.id.rcv_Parent_Dashbaord);
        getparentdashboard();


        if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_PARENT)){
            getDashboardAttendance();
        }
        return view;

    }

    private void getDashboardAttendance() {

        ApiInterface attendApi = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<Attendance> call = attendApi.getDashboardAttendance(session.getUserId());
        Log.i("DEBUG", "Dash ID: "+session.getUserId());
        call.enqueue(new Callback<Attendance>() {
            @Override
            public void onResponse(Call<Attendance> call, Response<Attendance> response) throws NullPointerException{
                if (response.isSuccessful()) { // 200 to 300
                    Attendance obj = response.body();

                }
            }

            @Override
            public void onFailure(Call<Attendance> call, Throwable t) {
                Log.i("DEBUG", "Attendance Exp: "+t.toString());
            }
        });
    }

    private boolean context() {
         return true;
    }

    private void getparentdashboard() {
        final GridLayoutManager mGridLayoutManager = new GridLayoutManager(getActivity(), 3);
        parentrecycleview.setLayoutManager(mGridLayoutManager);
        final List<DashboardModel> mFlowerList = new ArrayList<>();
        mFlowerData=new DashboardModel("Attendence",R.drawable.large_parent_attendence_icon,R.drawable.attendence_gradient);
        mFlowerList.add(mFlowerData);

        mFlowerData=new DashboardModel("Homework", R.drawable.largehomeworkicon, R.drawable.homework_gradient);
        mFlowerList.add(mFlowerData);


        mFlowerData=new DashboardModel("Assignment", R.drawable.largeassignmenticon, R.drawable.assignment_gradient);
        mFlowerList.add(mFlowerData);

       mFlowerData=new DashboardModel("Report Card", R.drawable.ic_report, R.drawable.report_card_gradeint);
       mFlowerList.add(mFlowerData);

        mFlowerData=new DashboardModel("Circular", R.drawable.largecircularicon, R.drawable.circular_gradient);
        mFlowerList.add(mFlowerData);



        mFlowerData=new DashboardModel("News", R.drawable.largenewsicon, R.drawable.news_gradeint);
        mFlowerList.add(mFlowerData);



        mFlowerData=new DashboardModel("Time Table", R.drawable.largetimetableicon, R.drawable.timetable_gradeint);
        mFlowerList.add(mFlowerData);

        mFlowerData=new DashboardModel("Subject Diary", R.drawable.ic_subject_dairy, R.drawable.subject_dairy_gradeint);
        mFlowerList.add(mFlowerData);


        mFlowerData=new DashboardModel("Event", R.drawable.largeeventicon, R.drawable.event_gradeint);
        mFlowerList.add(mFlowerData);


        ParentDashdoardAdapter parentDashdoardAdapter = new ParentDashdoardAdapter(getContext(),mFlowerList,this);
        parentrecycleview.setAdapter(parentDashdoardAdapter);
    }

    @Override
    public void getGridPosition(int position) {


         if (position == 0){

             getFragmentManager().beginTransaction().replace(R.id.frameForFragments,ParentAttendanceFragment.getInstance(this)).addToBackStack(null).commit();
         }

         if (position == 1){
             getFragmentManager().beginTransaction().replace(R.id.frameForFragments,ViewHomeworkListFragment.getInstance(this)).addToBackStack(null).commit();
         }


        if (position == 2){

            getFragmentManager().beginTransaction().replace(R.id.frameForFragments, ViewAssignmentListFragment.getInstance(this)).addToBackStack(null).commit();

        }
        if (position == 3){


//            getFragmentManager().beginTransaction().replace(R.id.frameForFragments,ParentReportCard.getInstance(this)).addToBackStack(null).commit();

             Intent intent=new Intent(getContext(),ReportActivity.class);
             startActivity(intent);


        }
        if (position == 4){

            getFragmentManager().beginTransaction().replace(R.id.frameForFragments, CircularFragment.getInstance(this)).addToBackStack(null).commit();

        }

        if (position == 5){

            getFragmentManager().beginTransaction().replace(R.id.frameForFragments, NewsFragment.getInstance(this)).addToBackStack(null).commit();

        }
        if (position == 6){

            getFragmentManager().beginTransaction().replace(R.id.frameForFragments, Parent_Time_Table.getInstance(this)).addToBackStack(null).commit();


//            Intent intent=new Intent(getContext(),ParentClassTimeTable.class);
//
//            startActivity(intent);


        }

        if (position == 7){

            getFragmentManager().beginTransaction().replace(R.id.frameForFragments, ViewLogFragment.getInstance(this)).addToBackStack(null).commit();

        }
        if (position == 8){

            getFragmentManager().beginTransaction().replace(R.id.frameForFragments, EventFragment.getInstance(this)).addToBackStack(null).commit();

        }

    }

    @Override
    public  void  setOnTitleText(String titleText) {
        onTitleSetListener.setOnTitleText(titleText);
    }

    @Override
    public void onClick(View view) {

    }
}
