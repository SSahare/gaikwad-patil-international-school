package appsoft.erp.gaikwad.activities.activitys;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.ybq.android.spinkit.style.Circle;

import appsoft.erp.gaikwad.activities.fragments.TimeTable_Fragment;
import appsoft.erp.gaikwad.activities.fragments.ViewAllotmentFragment;
import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import appsoft.erp.gaikwad.activities.parents_fragments.ExamTimeTableFragment;
import appsoft.erp.gaikwad.activities.parents_fragments.FeesFragment;
import appsoft.erp.gaikwad.activities.parents_fragments.ParentDashboardFragment;
import appsoft.erp.gaikwad.activities.parents_fragments.Parent_Time_Table;
import appsoft.erp.gaikwad.activities.profile.ProfileFragment;
import appsoft.erp.gaikwad.activities.teacher_profile.TeacherProfileFragment;
import de.hdodenhof.circleimageview.CircleImageView;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.extras.Constants;
import appsoft.erp.gaikwad.activities.fragments.CircularFragment;
import appsoft.erp.gaikwad.activities.fragments.DashboardFragment;
import appsoft.erp.gaikwad.activities.fragments.EventFragment;
import appsoft.erp.gaikwad.activities.fragments.NewsFragment;
import appsoft.erp.gaikwad.activities.fragments.ViewAssignmentListFragment;
import appsoft.erp.gaikwad.activities.fragments.ViewFeedbackListFragment;
import appsoft.erp.gaikwad.activities.fragments.ViewHomeworkListFragment;
import appsoft.erp.gaikwad.activities.fragments.ViewLogFragment;
import appsoft.erp.gaikwad.activities.parent_activity.ReportActivity;
import appsoft.erp.gaikwad.activities.parents_fragments.ParentAttendanceFragment;
import appsoft.erp.gaikwad.activities.session.SessionManagement;

import static appsoft.erp.gaikwad.R.id.parent_timetable;
import static appsoft.erp.gaikwad.activities.extras.Constants.ATTACHMENT_BASE_URL;
import static appsoft.erp.gaikwad.activities.extras.Constants.STUDENT_SUB_URL;
import static appsoft.erp.gaikwad.activities.extras.Constants.TEACHER_SUB_URL;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
        , DashboardFragment.OnCardListener, OnTitleSetListener {

    String imageUrl = "";
    private CircleImageView profileImage;
    public TextView profileType, profileName;
    private Toolbar toolbar;
    private DashboardFragment mDashboardFragment;
    private ParentDashboardFragment pparentDashboardFragment;
    private static final int CALENDER_PERMISSION = 100;
    public static boolean IS_CALENDER_PERMISSION = false;
    private SessionManagement session;
    private MenuItem menuItemSelected;
    DrawerLayout drawer;
    private BottomNavigationView bottom_navigation;
    private static final String SELECTED_ITEM = "selected_item";

    ProgressBar progressBar;
    private int mMenuItemSelected;
    private ImageButton ib_home, ib_schedule, ib_timetable, ib_profile, ib_event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.home_activity);
        profileImage= findViewById(R.id.profileImage);


        session = new SessionManagement(getApplicationContext());
        createLoginSession();
        session.checkLogin();
        loadFragment(DashboardFragment.getInstance(this));
        parentloadFragment(ParentDashboardFragment.getInstance(this));
         drawer = findViewById(R.id.drawer_layout);
        bottom_navigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        progressBar = findViewById(R.id.progress_HomeActivity);
        Circle circle = new Circle();
        progressBar.setIndeterminateDrawable(circle);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String role = session.getUserRole();

        if (role.equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
            bottom_navigation.inflateMenu(R.menu.bottom_navigation_menu);
            bottom_navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {

                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {


                    switch (item.getItemId()) {

                        case R.id.ib_home:
                            toolbar.setTitle("Dashboard");
                        getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments,new DashboardFragment()).addToBackStack(null).commit();
                        return true;
                        case R.id.ib_schedule:
                            getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments,ViewAllotmentFragment.getInstance(HomeActivity.this)).addToBackStack(null).commit();

                            return true;

                        case R.id.ib_time_table:

                            getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments,TimeTable_Fragment.getInstance(HomeActivity.this)).addToBackStack(null).commit();

                            return true;

                        case R.id.ib_events:
                            getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, EventFragment.getInstance(HomeActivity.this)).addToBackStack(null).commit();
                            return true;

                        case R.id.teacher_profile:

                            getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments,TeacherProfileFragment.getInstance(HomeActivity.this)).addToBackStack(null).commit();

                            return true;
                      /*  default:
                            getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments,new DashboardFragment()).addToBackStack(null).commit();
*/
                    }
                        return false;
                }
            });

        } else if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_PARENT)) {

            bottom_navigation.inflateMenu(R.menu.parent_bottom_navigation_menu);
            bottom_navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                    bottom_navigation.getMenu().getItem(0);

                    switch (item.getItemId()) {

                        case R.id.parent_ib_home:

                            toolbar.setTitle("DashBoard");
                            getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, new ParentDashboardFragment()).addToBackStack(null).commit();
                            return true;
                        case R.id.parent_fees:
                            getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments,FeesFragment.getInstance(HomeActivity.this)).addToBackStack(null).commit();

                            return true;

                        case parent_timetable:

                            getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, Parent_Time_Table.getInstance(HomeActivity.this)).addToBackStack(null).commit();

                                return true;

                        case R.id.parent_event:

                                getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, EventFragment.getInstance(HomeActivity.this)).addToBackStack(null).commit();

                            return true;
                            case R.id.parent_profile:

                                getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, ProfileFragment.getInstance(HomeActivity.this)).addToBackStack(null).commit();

                            return true;

                    }



                    return false;
                }

            });


        }


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        toolbar.setTitle("Dashboard");

        if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_PARENT)) {

            getSupportFragmentManager().beginTransaction().add(R.id.frameForFragments, (ParentDashboardFragment) pattachedDashboard()).addToBackStack(null).commit();
        }
        if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER)) {

            getSupportFragmentManager().beginTransaction().add(R.id.frameForFragments, (DashboardFragment) attachedDashboard()).addToBackStack(null).commit();

        }

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        setNavigationalDrawer();
        navigationView.setNavigationItemSelectedListener(this);
    }


    private void parentloadFragment(ParentDashboardFragment parentDashboardFragment) {


        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameForFragments, parentDashboardFragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    private void loadFragment(DashboardFragment dashboardFragment) {

        if (dashboardFragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frameForFragments, dashboardFragment).addToBackStack(null)
                    .commit();

        }

    }




    private void createLoginSession() {
        Intent i = getIntent();
        if (i != null) {
            String fName = i.getStringExtra("KEY_FirstName");
            String lName = i.getStringExtra("KEY_LastName");
            String userRole = i.getStringExtra("KEY_Role");
            int userId = i.getIntExtra("KEY_ID", 0);
            String imagePathName = i.getStringExtra("KEY_ImagePath");

            if (userRole.trim().equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
                imageUrl = ATTACHMENT_BASE_URL + TEACHER_SUB_URL + imagePathName;
                //setProfileImage(teacherImageUrl);
            }
            if (userRole.trim().equals(Constants.LOGIN_TYPE_AS_PARENT)) {
                imageUrl = ATTACHMENT_BASE_URL + STUDENT_SUB_URL + imagePathName;
                //setProfileImage(studentImageUrl);
            }

            session.createLoginSession(fName, lName, userId, imageUrl, userRole, imagePathName);
        }
    }

    private Object pattachedDashboard() {

        if (pparentDashboardFragment == null) {
            pparentDashboardFragment = ParentDashboardFragment.getInstance(this);
        }
        return pparentDashboardFragment;

    }




    private void setNavigationalDrawer() {


        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);


        profileName = headerView.findViewById(R.id.profileName);
        profileImage = headerView.findViewById(R.id.profileImage);
        profileType = headerView.findViewById(R.id.profileType);
        RelativeLayout layout = headerView.findViewById(R.id.nav_header_layout);


        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (session.getUserRole().equalsIgnoreCase(Constants.LOGIN_TYPE_AS_PARENT)) {

                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments,ProfileFragment.getInstance(HomeActivity.this)).addToBackStack(null).commit();
                    drawer.closeDrawers();

                } else if (session.getUserRole().equalsIgnoreCase(Constants.LOGIN_TYPE_AS_TEACHER)) {
                    drawer.closeDrawers();
                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments,TeacherProfileFragment.getInstance(HomeActivity.this)).addToBackStack(null).commit();

                }
            }
        });


        profileName.setText(session.getUserName());
        profileType.setText(session.getUserRole() + session.getUserId());
        setProfileImage(session.getImagePath());

        if (session.getUserRole().trim().equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
            navigationView.inflateMenu(R.menu.teacher_menu_list);
        }
        if (session.getUserRole().trim().equals(Constants.LOGIN_TYPE_AS_PARENT)) {
            navigationView.inflateMenu(R.menu.parent_menu_list);
        }
    }

    private void setProfileImage(String imageUrl) {

        if (session.getImageLastPath().isEmpty() || session.getImageLastPath().equals(null) || session.getImageLastPath() == "") {
            try {
                Glide.with(getApplicationContext())
                        .load(getResources().getDrawable(R.drawable.person))
                        .override(100, 100)
                        .fitCenter()
                        .into(profileImage);
            } catch (Exception e) {
            }
        } else {
            try {
                Glide.with(getApplicationContext())
                        .load(imageUrl)
                        .override(100, 100)
                        .fitCenter()
                        .into(profileImage);
            } catch (Exception e) {
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER)){
            bottom_navigation.getMenu().findItem(R.id.ib_home).setChecked(true);
        }
        if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_PARENT)){
            bottom_navigation.getMenu().findItem(R.id.parent_ib_home).setChecked(true);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(HomeActivity.this, Manifest.permission.WRITE_CALENDAR)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                builder.setTitle("Calender Permission");
                builder.setMessage("This app needs calender permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.WRITE_CALENDAR}, CALENDER_PERMISSION);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.WRITE_CALENDAR}, CALENDER_PERMISSION);
            }
        } else {
            IS_CALENDER_PERMISSION = true;
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.GOTO_LOGIN_PAGE_REQ) {
            // Logout User From Here
            Toast.makeText(this, "Logout Successfully..", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void onBackPressed() {


//        if (bottom_navigation.equals(Constants.LOGIN_TYPE_AS_PARENT)) {
//            bottom_navigation.inflateMenu(R.menu.bottom_navigation_menu);
//            if (m)
//            getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments,new ParentDashboardFragment()).commit();
//        }



        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        int count = getSupportFragmentManager().getBackStackEntryCount();
        SessionManagement sessionManagement = new SessionManagement(getApplicationContext());


        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }

        if (count > 1){
            if (sessionManagement.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
                Toast.makeText(this, "Press Back One More Time To Exit", Toast.LENGTH_SHORT).show();
                Log.i("TAG", "" + getSupportFragmentManager().getBackStackEntryCount());
                toolbar.setTitle("Dashboard");
                getSupportFragmentManager().popBackStack(0, 0);
                getSupportFragmentManager().beginTransaction().add(R.id.frameForFragments, new DashboardFragment()).commit();
                bottom_navigation.getMenu().findItem(R.id.ib_home).setChecked(true);

            }else if (sessionManagement.getUserRole().equals(Constants.LOGIN_TYPE_AS_PARENT)){
                Toast.makeText(this, "Press Back One More Time To Exit", Toast.LENGTH_SHORT).show();
                Log.i("TAG", "" + getSupportFragmentManager().getBackStackEntryCount());
                toolbar.setTitle("Dashboard");
                getSupportFragmentManager().popBackStack(0, 0);
                getSupportFragmentManager().beginTransaction().add(R.id.frameForFragments, new ParentDashboardFragment()).commit();
                bottom_navigation.getMenu().findItem(R.id.parent_ib_home).setChecked(true);
            }
        }else {
            //session.logoutUser();
            finish();
        }


    }


    private Object attachedDashboard() {
        if (mDashboardFragment == null) {
            mDashboardFragment = new DashboardFragment();
        }

        return mDashboardFragment;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.logout) {
            //session.checkLogin();
            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivityForResult(i, Constants.GOTO_LOGIN_PAGE_REQ);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        if ((session.getUserRole()).equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
            switch (item.getItemId()) {

                case R.id.dashboard:
                    toolbar.setTitle("Dashboard");
                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, (DashboardFragment) attachedDashboard()).addToBackStack(null).commit();
                    break;

                case R.id.view_attendance_t:

                    Intent viewAttendanceIntent = new Intent(HomeActivity.this, ViewAttendanceActivity.class);
                    startActivity(viewAttendanceIntent);
                    break;


                case R.id.view_assignment_t:

                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, ViewAssignmentListFragment.getInstance(this)).addToBackStack(null).commit();
                    break;


                case R.id.view_homework_t:

                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, ViewHomeworkListFragment.getInstance(this)).addToBackStack(null).commit();
                    break;

                case R.id.view_daily_log:

                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, ViewLogFragment.getInstance(this)).addToBackStack(null).commit();
                    break;

                case R.id.class_time_table:

                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, TimeTable_Fragment.getInstance(this)).addToBackStack(null).commit();

                    break;


                case R.id.news:
                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, NewsFragment.getInstance(this)).addToBackStack(null).commit();
                    break;

                case R.id.circular:

                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, CircularFragment.getInstance(this)).addToBackStack(null).commit();
                    break;

                case R.id.view_feedback_t:

                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, ViewFeedbackListFragment.getInstance(this)).addToBackStack(null).commit();
                    break;

                case R.id.leave_manager:
                    Intent leaveIntent = new Intent(HomeActivity.this, LeaveManagerAct.class);
                    startActivity(leaveIntent);
                    return true;

                case R.id.event:

                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, EventFragment.getInstance(this)).addToBackStack(null).commit();
                    break;
            }
        }
        if ((session.getUserRole()).equals(Constants.LOGIN_TYPE_AS_PARENT)) {

            switch (item.getItemId()) {

                case R.id.dashboard:
                    toolbar.setTitle("Dashboard");
                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, (ParentDashboardFragment) pattachedDashboard()).addToBackStack(null).commit();
                    break;

                case R.id.attendance:
                    toolbar.setTitle("Attendance");
                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments,ParentAttendanceFragment.getInstance(this)).addToBackStack(null).commit();
                    break;

                case R.id.class_time_table:

                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments,Parent_Time_Table.getInstance(this)).addToBackStack(null).commit();

                    break;

                case R.id.exam_time_table:
                     Toast.makeText(this, "Work in progress", Toast.LENGTH_SHORT).show();

//                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, ExamTimeTableFragment.getInstance(this)).addToBackStack(null).commit();
                    break;


                case R.id.report_card:


                    Intent repIntent = new Intent(HomeActivity.this, ReportActivity.class);
                    startActivity(repIntent);
                    break;

                case R.id.assignment:
                    toolbar.setTitle("Assignments");
                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, ViewAssignmentListFragment.getInstance(this)).addToBackStack(null).commit();
                    break;

                case R.id.homework:
                    toolbar.setTitle("Homeworks");
                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, ViewHomeworkListFragment.getInstance(this)).addToBackStack(null).commit();
                    break;

                case R.id.sub_dairy:
                    toolbar.setTitle("Subject Dairy");
                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, ViewLogFragment.getInstance(this)).addToBackStack(null).commit();
                    break;

                case R.id.news:
                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, NewsFragment.getInstance(this)).addToBackStack(null).commit();
                    break;

                case R.id.circular:
                    toolbar.setTitle("Circulars");
                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, CircularFragment.getInstance(this)).addToBackStack(null).commit();
                    break;

                case R.id.fees_record:
                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, FeesFragment.getInstance(this)).addToBackStack(null).commit();

                    break;

                case R.id.feedback:
                    toolbar.setTitle("Feedback");
                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, ViewFeedbackListFragment.getInstance(this)).addToBackStack(null).commit();
                    break;

                case R.id.event:
                    toolbar.setTitle("Events");
                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, EventFragment.getInstance(this)).addToBackStack(null).commit();
                    break;
            }
        }
        return true;
    }

    @Override
    public void setOnCardClickListener(String cardName) {

    }

    @Override
    public void setOnTitleText(String titleText) {
        toolbar.setTitle(titleText);
    }




}