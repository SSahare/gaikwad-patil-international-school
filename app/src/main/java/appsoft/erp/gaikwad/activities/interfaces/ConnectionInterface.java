package appsoft.erp.gaikwad.activities.interfaces;

public interface ConnectionInterface {
    void ViewsWhenConnected();
    void ViewsWhenNotConnected();
    void ViewsWhenNoDataFound();
}
