package appsoft.erp.gaikwad.activities.modals;

//-----------------------------------vishal.rana.tp.activities.modals.Fees.java-----------------------------------
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Fees implements Serializable{
    @SerializedName("total_school_fees")
    @Expose
    private int totalSchoolFees;

    @SerializedName("total_fees_paid")
    @Expose
    private int totalFeesPaid;

    @SerializedName("balance_fees")
    @Expose
    private int balancedFee;

    public Fees(){}

    public Fees(int totalSchoolFees, int totalFeesPaid, int balancedFee) {
        this.totalSchoolFees = totalSchoolFees;
        this.totalFeesPaid = totalFeesPaid;
        this.balancedFee = balancedFee;
    }

    public int getTotalSchoolFees() { return totalSchoolFees; }

    public int getTotalFeesPaid() {
        return totalFeesPaid;
    }

    public int getBalancedFee(){ return balancedFee;}
}
