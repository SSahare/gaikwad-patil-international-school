package appsoft.erp.gaikwad.activities.open_results;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import appsoft.erp.gaikwad.R;

public class LogDetailActivity extends AppCompatActivity {
TextView  tv_logClass,tv_LogSection,tv_LogDate,tv_LogDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_detail);

        tv_logClass=findViewById(R.id.tv_logClass);
        tv_LogSection=findViewById(R.id.tv_LogSection);
        tv_LogDate=findViewById(R.id.tv_LogDate);
        tv_LogDescription=findViewById(R.id.tv_LogDescription);


        tv_logClass.setText("Class :"+getIntent().getStringExtra("logclass"));
        tv_LogSection.setText("Section :"+getIntent().getStringExtra("logsection"));
        tv_LogDate.setText("Date :"+getIntent().getStringExtra("logdate"));
        tv_LogDescription.setText(getIntent().getStringExtra("logdescriptiion"));

        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);

            getSupportActionBar().setTitle(getIntent().getStringExtra("logsubject"));
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
