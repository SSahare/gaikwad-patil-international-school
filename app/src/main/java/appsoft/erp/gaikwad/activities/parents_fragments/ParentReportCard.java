package appsoft.erp.gaikwad.activities.parents_fragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;

import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import appsoft.erp.gaikwad.activities.modals.Class;
import appsoft.erp.gaikwad.activities.modals.ReportModel;
import appsoft.erp.gaikwad.activities.parent_adapters.ReportAdapter;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ParentReportCard extends Fragment {

    private Spinner examinations;
    private Button btn_ok;
    ProgressDialog progressDoalog;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mManager;
    private String examName = "";
    private static OnTitleSetListener onTitleSetListener=null;


    public ParentReportCard(){}


    public static ParentReportCard getInstance(OnTitleSetListener onTitleSetListener1){
        onTitleSetListener=onTitleSetListener1;
        return new ParentReportCard();
    }


    @Override
    public void onResume() {
        super.onResume();
        setTitle();
    }

    private void setTitle() {
        onTitleSetListener.setOnTitleText("Time Table");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.activity_report,container,false);
        examinations = view.findViewById(R.id.spinnerExam);
        recyclerView =  view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        mManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mManager);

        getStudentClass();




        examinations.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                examName = (String) parent.getSelectedItem();
                fatchResultOfStudent();
            }



            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        return view;



    }

    private void fatchResultOfStudent() {
        SessionManagement session = new SessionManagement(getContext());
        ApiInterface i = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<ReportModel>> call = i.getReport(session.getUserId(), examName);
        call.enqueue(new Callback<List<ReportModel>>() {
            @Override
            public void onResponse(Call<List<ReportModel>> call, Response<List<ReportModel>> response) {
                List<ReportModel> reportModelList = response.body();
                try {
                    if (reportModelList.size() == 0) {
                        ReportAdapter adapter = new ReportAdapter(reportModelList);
                        recyclerView.setAdapter(adapter);
                        Toast toast = Toast.makeText(getContext(), "Oops, No Report List available", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    } else {
                        ReportAdapter adapter = new ReportAdapter(reportModelList);
                        recyclerView.setAdapter(adapter);
                    }
                } catch (NullPointerException npe) {
                    Toast toast = Toast.makeText(getContext(), "Oops, No Report List available", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }

            @Override
            public void onFailure(Call<List<ReportModel>> call, Throwable t) {

            }
        });
    }


            private void getStudentClass() {


                SessionManagement session = new SessionManagement(getContext());
                reportprogressbar();
                ApiInterface apiInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
                /* TODO: Generate errors remove after testing */
                Call<Class> call = null;//apiInterface.getStudentClass(session.getUserId());
                call.enqueue(new Callback<Class>() {
                    @Override
                    public void onResponse(Call<Class> call, Response<Class> response) {
                        try {
                            Class className = response.body();
                            String c_name = className.getClasses();
                            float c_number = getClassNumber(c_name);
                            setExamSpineers(c_number);
                            Log.i("DEBUG", "Class: " + c_number);

                        } catch (NullPointerException npe) {
                            Log.i("DEBUG", "Exc: " + npe.toString());
                        }
                        progressDoalog.dismiss();

                    }

                    @Override
                    public void onFailure(Call<Class> call, Throwable t) {
                        progressDoalog.dismiss();
                        Log.i("DEBUG", "Failure Exception: " + t.toString());
                    }
                });
            }

            private float getClassNumber(String c_name) {
                if (c_name.equalsIgnoreCase("nursery")) {
                    return 0.1f;
                } else if (c_name.equalsIgnoreCase("KG1")) {
                    return 0.2f;
                } else if (c_name.equalsIgnoreCase("KG2")) {
                    return 0.3f;
                } else {
                    String chr = c_name.substring(0, 1);
                    return Float.parseFloat(chr);
                }
            }

            private void reportprogressbar() {


                progressDoalog = new ProgressDialog(getContext());
                progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDoalog.setCancelable(true);
                progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                progressDoalog.getWindow().setGravity(Gravity.CENTER);
                progressDoalog.setMessage("Please Wait...");
                progressDoalog.setIndeterminate(true);
                Circle wave = new Circle();
                progressDoalog.setIndeterminateDrawable(wave);
                progressDoalog.show();
            }


            private void setExamSpineers(float c_number) {
                if (c_number <= 5 && c_number > 0) {
                    ArrayAdapter<CharSequence> examAdapter = ArrayAdapter.createFromResource(getContext(), R.array.report_examination, R.layout.text_filter_view);
                    examAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
                    examinations.setAdapter(examAdapter);
                } else if (c_number > 5 && c_number <= 12) {
                    ArrayAdapter<CharSequence> examAdapter = ArrayAdapter.createFromResource(getContext(), R.array.report_evalution, R.layout.text_filter_view);
                    examAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
                    examinations.setAdapter(examAdapter);
                } else {
                    Toast.makeText(getContext(), "Class is not valid", Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public boolean onOptionsItemSelected(MenuItem item) {


                if (item.getItemId() == android.R.id.home) {

                    return true;
                } else if (item.getItemId() == R.id.downloadReportcard) {
                    Toast.makeText(getContext(), "Report generation on process. Please, try next time.", Toast.LENGTH_SHORT).show();
                    return true;
                }
                return false;

            }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.download_menu, menu);
//        return super.onCreateOptionsMenu(menu);
//    }



}
