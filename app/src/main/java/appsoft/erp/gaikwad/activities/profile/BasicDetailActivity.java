package appsoft.erp.gaikwad.activities.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.UserProfileModel;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BasicDetailActivity extends AppCompatActivity {

    TextView name,gender,dob,aadharnumber,contactnumber,emailaddress;
//    Animation uptodown;
    private SessionManagement session;
    UserProfileModel model;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.basic_detail_activity);
//        uptodown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.uptodown);

        session = new SessionManagement(getApplicationContext());
        name = findViewById(R.id.tv_name);
//        name.setAnimation(uptodown);
        gender=findViewById(R.id.tv_Gender);
//        gender.setAnimation(uptodown);
        dob=findViewById(R.id.tv_DOB);
//        dob.setAnimation(uptodown);
        aadharnumber=findViewById(R.id.tv_aadhar_Number);
//        aadharnumber.setAnimation(uptodown);
        contactnumber=findViewById(R.id.tv_Contact_Number);
//        contactnumber.setAnimation(uptodown);
        emailaddress=findViewById(R.id.tv_Email_Address);
//        emailaddress.setAnimation(uptodown);
        getDataFromServer();



        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
           getSupportActionBar().setTitle("Basic Detail");
        }
    }

    private void getDataFromServer() {


        ApiInterface in = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<UserProfileModel> call = in.getProfileData(session.getUserId());
        call.enqueue(new Callback<UserProfileModel>() {
            @Override
            public void onResponse(Call<UserProfileModel> call, Response<UserProfileModel> response) {
                model = response.body();

                setBasicDetails();;

            }


            @Override
            public void onFailure(Call<UserProfileModel> call, Throwable t) {

                Log.i("DEBUG", "Error Ocurred");
            }
        });
    }

    private void setBasicDetails() {

        name.setText(model.getFirstName()+" "+model.getMiddleName()+" "+model.getLastName());
        gender.setText(model.getGender());
        dob.setText(model.getDateOfBirth());
        aadharnumber.setText(model.getAdharNo());
        contactnumber.setText(model.getParentNumber());
        emailaddress.setText(model.getParentEmail());



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
