package appsoft.erp.gaikwad.activities.parent_activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;

import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.ReportModel;
import appsoft.erp.gaikwad.activities.parent_adapters.ReportAdapter;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportActivity extends AppCompatActivity {

    private Spinner examinations;
    ProgressDialog progressDoalog;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mManager;
    private String examName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        btn_ok = findViewById(R.id.buttonReport);
        examinations = findViewById(R.id.spinnerExam);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        mManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mManager);

        // getStudentClass();
        // Report card
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Report not found");
        builder.setMessage("Report card is not present for this user");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        builder.show();

//        btn_ok.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                fatchResultOfStudent();
//            }
//        });

        //////////////////////////////////
        //   Evolution kg1 to 5th      //
        //   Exams     6th to 12th     //
        /////////////////////////////////

       /* classes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0){
                    examFlag = 0;
                } else if (position >= 1 && position <= 8) {
                    examFlag = 1;
                }else {
                    examFlag = 2;
                }
                setExaminations(examFlag);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        examinations.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                examName = (String) parent.getSelectedItem();
                fatchResultOfStudent();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    /*private void setClass() {
        ArrayAdapter<CharSequence> termAdapter = ArrayAdapter.createFromResource(this, R.array.list_of_class, R.layout.text_filter_view);
        termAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
        classes.setAdapter(termAdapter);
    }*/

   /* private void setExaminations(int examFlag){
        if (examFlag == 1) {
            ArrayAdapter<CharSequence> examAdapter = ArrayAdapter.createFromResource(this, R.array.report_evalution, R.layout.text_filter_view);
            examAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
            examinations.setAdapter(examAdapter);
        }else if (examFlag == 2){
            ArrayAdapter<CharSequence> examAdapter = ArrayAdapter.createFromResource(this, R.array.report_examination, R.layout.text_filter_view);
            examAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
            examinations.setAdapter(examAdapter);
        }else if(examFlag == 0){
            examinations.setAdapter(null);
        }
    }
*/
    private void fatchResultOfStudent() {
        SessionManagement session = new SessionManagement(getApplicationContext());
        ApiInterface i = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<ReportModel>> call = i.getReport(session.getUserId(), examName);
        call.enqueue(new Callback<List<ReportModel>>() {
            @Override
            public void onResponse(@NonNull Call<List<ReportModel>> call, Response<List<ReportModel>> response) {

                List<ReportModel> reportModelList = response.body();
                try{
                    if (reportModelList.size() == 0){
                        ReportAdapter adapter = new ReportAdapter(reportModelList);
                        recyclerView.setAdapter(adapter);
                        Toast toast = Toast.makeText(getApplicationContext(), "Oops, No Report List available", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER,0,0);
                        toast.show();
                    }else{
                        ReportAdapter adapter = new ReportAdapter(reportModelList);
                        recyclerView.setAdapter(adapter);
                    }
                }catch (NullPointerException npe){
                    Toast toast = Toast.makeText(getApplicationContext(), "Oops, No Report List available", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER,0,0);
                    toast.show();
                }
            }

            @Override
            public void onFailure(Call<List<ReportModel>> call, Throwable t) {
                Toast toast = Toast.makeText(getApplicationContext(), "Unable to load report data...", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER,0,0);
                toast.show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.download_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
            return true;
        }else if (item.getItemId() == R.id.downloadReportcard){
            Toast.makeText(this, "Report generation on process. Please, try next time.", Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    private void getStudentClass(){
//        SessionManagement session = new SessionManagement(getApplicationContext());
//        reportprogressbar();
//        ApiInterface apiInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
//        Call<Class> call = apiInterface.getStudentClass(session.getUserId());
//        call.enqueue(new Callback<Class>() {
//            @Override
//            public void onResponse(Call<Class> call, Response<Class> response) {
//                try {
//                    Class className = response.body();
//                    String c_name = className.getClasses();
//                    float c_number = getClassNumber(c_name);
//                    setExamSpineers(c_number);
//                    Log.i("DEBUG", "Class: "+c_number);
//
//                }catch (NullPointerException npe){
//                    Log.i("DEBUG", "Exc: "+npe.toString());
//                }
//                progressDoalog.dismiss();
//
//            }
//
//            @Override
//            public void onFailure(Call<Class> call, Throwable t) {
//                progressDoalog.dismiss();
//                Log.i("DEBUG", "Failure Exception: "+t.toString());
//            }
//        });
    }

    private void reportprogressbar() {

        progressDoalog = new ProgressDialog(this);
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
        progressDoalog.setMessage("Please Wait...");
        progressDoalog.setIndeterminate(true);
        Circle wave=new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();
    }

    private void setExamSpineers(float c_number) {
        if (c_number <= 5 && c_number > 0){
            ArrayAdapter<CharSequence> examAdapter = ArrayAdapter.createFromResource(this, R.array.report_evalution, R.layout.text_filter_view);
            examAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
            examinations.setAdapter(examAdapter);
        }else if(c_number >6 && c_number <= 12){
            ArrayAdapter<CharSequence> examAdapter = ArrayAdapter.createFromResource(this, R.array.report_examination, R.layout.text_filter_view);
            examAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
            examinations.setAdapter(examAdapter);
        }else{
            Toast.makeText(this, "Class is not valid", Toast.LENGTH_SHORT).show();
        }
    }

    private float getClassNumber(String c_name) {
        if(c_name.equalsIgnoreCase("nursery")){
            return 0.1f;
        }else if (c_name.equalsIgnoreCase("KG1")){
            return 0.2f;
        }else if(c_name.equalsIgnoreCase("KG2")){
            return 0.3f;
        }else{
            String chr = c_name.substring(0,1);
            return Float.parseFloat(chr);
        }
    }
}