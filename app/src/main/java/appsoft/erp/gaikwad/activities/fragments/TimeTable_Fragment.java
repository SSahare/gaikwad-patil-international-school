package appsoft.erp.gaikwad.activities.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.adapters.TimeTableAdapter;
import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import appsoft.erp.gaikwad.activities.modals.ClassTimeTable;
import appsoft.erp.gaikwad.activities.network_broadcast.ConnectionDetector;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.volley_singleton.NetworkStatus;
import retrofit2.Call;
import retrofit2.Callback;

public class TimeTable_Fragment extends Fragment implements NetworkStatus {

    private Spinner classname, section, day;
    private Button ok;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mManager;
    private LinearLayout layout;
    private String classNameString = "";
    private String sectionNameString = "";
    private String dayNameString = "";
    ProgressDialog progressDoalog;
    private ConstraintLayout constraintLayout;
    private LinearLayout dropDownLayout, dateTextLayout;
    private ImageView noDataFound;


    private List<ClassTimeTable> list = null;
    public static OnTitleSetListener onTitleSetListener=null;

    public TimeTable_Fragment() {
    }
    public static TimeTable_Fragment getInstance(OnTitleSetListener onTitleSetListener1){
        onTitleSetListener=onTitleSetListener1;
        return new TimeTable_Fragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle();
    }

    private void setTitle() {
        onTitleSetListener.setOnTitleText("Time Table");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_class_time_table, container, false);

        classname = view.findViewById(R.id.spinnerClass);
        section = view.findViewById(R.id.spinnerSection);
        day = view.findViewById(R.id.spinnerDay);
        recyclerView = view.findViewById(R.id.recyclerView);
        mManager = new LinearLayoutManager(getContext());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(mManager);

        constraintLayout =view.findViewById(R.id.no_internet_layout);
        noDataFound = view.findViewById(R.id.no_data_found_image);
        dateTextLayout = view.findViewById(R.id.date_layout);
        dropDownLayout = view.findViewById(R.id.dropdown_layout);
        Button tryAgain = view.findViewById(R.id.try_again);

        if (ConnectionDetector.isConnected(getContext())){
            viewsWhenConnected();
            loadTimeTable();
        }else {
            viewsWhenNotConnected();
        }
        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectionDetector.isConnected(getContext())){
                    viewsWhenConnected();
                    loadTimeTable();
                }
            }
        });
        setClassNames();
        setSectionName();
        setDays();



        classname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    classNameString = (String) parent.getSelectedItem();
                    loadTimeTable();
                    progressDoalog.dismiss();
                } else {
                    TimeTableAdapter adapter = new TimeTableAdapter(list);
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        section.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    sectionNameString = (String) parent.getSelectedItem();
                    loadTimeTable();
                    progressDoalog.dismiss();
                } else {
                    TimeTableAdapter adapter = new TimeTableAdapter(list);
                    recyclerView.setAdapter(adapter);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        day.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {

                    dayNameString = (String) parent.getSelectedItem();
                    loadTimeTable();
                    progressDoalog.dismiss();
                } else {
                    TimeTableAdapter adapter = new TimeTableAdapter(list);
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        return view;

    }
    private void viewsWhenConnected() {
        recyclerView.setVisibility(View.VISIBLE);
        constraintLayout.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);

    }

    private void viewsWhenNotConnected() {
        recyclerView.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.VISIBLE);
        noDataFound.setVisibility(View.GONE);

    }
    private void viewsWhenNoDataFound() {
        recyclerView.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.GONE);
        noDataFound.setVisibility(View.VISIBLE);

    }


    private void setClassNames() {
        ArrayAdapter<CharSequence> classAdapter = ArrayAdapter.createFromResource(getContext(), R.array.list_of_class, R.layout.text_filter_view);
        classAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
        classname.setAdapter(classAdapter);
    }

    private void setSectionName() {
        ArrayAdapter<CharSequence> classAdapter = ArrayAdapter.createFromResource(getContext(), R.array.list_of_section, R.layout.text_filter_view);
        classAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
        section.setAdapter(classAdapter);
    }

    private void setDays() {

        ArrayAdapter daysAdapter = ArrayAdapter.createFromResource(getContext(), R.array.list_of_days, R.layout.text_filter_view);
        daysAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
        day.setAdapter(daysAdapter);
    }

    private void loadTimeTable() {

        timetableprogressbar();
        if (isAllFieldFilled()){
            if(ConnectionDetector.isConnected(getContext())) {

                ApiInterface ttInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
                Call<List<ClassTimeTable>> call = ttInterface.getTeacherTimeTable(classNameString, sectionNameString, dayNameString);

                call.enqueue(new Callback<List<ClassTimeTable>>() {
                    @Override
                    public void onResponse(@NotNull Call<List<appsoft.erp.gaikwad.activities.modals.ClassTimeTable>> call, @NotNull retrofit2.Response<List<appsoft.erp.gaikwad.activities.modals.ClassTimeTable>> response) {
                        if (response.isSuccessful()) {
                            list = response.body();
                            try {
                                if (list == null || list.size() == 0) {
                                    viewsWhenNoDataFound();
                                    TimeTableAdapter adapter = new TimeTableAdapter(list);
                                    recyclerView.setAdapter(adapter);
                                    Toast.makeText(getContext(), R.string.class_tt_not_found, Toast.LENGTH_SHORT).show();
                                    return;
                                } else {
                                    viewsWhenConnected();
                                    TimeTableAdapter adapter = new TimeTableAdapter(list);
                                    recyclerView.setAdapter(adapter);
                                    progressDoalog.dismiss();
                                }

                            } catch (Resources.NotFoundException npe) {
                                npe.printStackTrace();
                            }
                            progressDoalog.dismiss();
                        }
                        progressDoalog.dismiss();
                    }

                    @Override
                    public void onFailure(@NotNull Call<List<ClassTimeTable>> call, @NotNull Throwable t) {
                        progressDoalog.dismiss();
                        Toast.makeText(getContext(), "Something wrong with server...", Toast.LENGTH_LONG).show();
                    }
                });
            }else {
                Toast.makeText(getContext(), R.string.no_connection, Toast.LENGTH_SHORT).show();
            }


        }
        progressDoalog.dismiss();
    }

    private void timetableprogressbar() {

        progressDoalog = new ProgressDialog(getContext());
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
        progressDoalog.setMessage("Please Wait...");
//        progressDoalog.getWindow().setGravity(LinearLayout.LayoutParams.MATCH_PARENT LinearLayout.LayoutParams.MATCH_PARENT);
        progressDoalog.setIndeterminate(true);
        Circle wave = new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();
    }


    private boolean isAllFieldFilled() {
        if (classNameString.isEmpty() || sectionNameString.isEmpty() || dayNameString.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean isNetworkAvailable() {


        ConnectivityManager conMgr =  (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        return netInfo != null;

    }
}
