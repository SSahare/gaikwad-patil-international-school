package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ByDayModel {

    @SerializedName("student_id")
    @Expose
    private String studentId;

    @SerializedName("student_name")
    @Expose
    private String studentName;

    @SerializedName("att")
    @Expose
    private String att;

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getAtt() {
        return att;
    }

    public void setAtt(String att) {
        this.att = att;
    }

}
