package appsoft.erp.gaikwad.activities.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hzn.lib.EasyTransition;
import com.hzn.lib.EasyTransitionOptions;

import java.util.ArrayList;
import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.extras.Constants;
import appsoft.erp.gaikwad.activities.modals.Circular;
import appsoft.erp.gaikwad.activities.open_results.CircularDetailActivity;


public class CircularAdapter extends RecyclerView.Adapter<CircularAdapter.CircularViewHolder>{

    private List<Circular> circularList;
    private Context context;
    private Circular circularObj;

    public CircularAdapter(Context context,List<Circular> circularList){
        this.circularList = circularList;
        this.context = context;
    }

    @Override
    public CircularViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.circular_card,parent,false);
        return new CircularViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CircularViewHolder holder, int position) {
        circularObj = circularList.get(position);
        String dateStr = circularObj.getDateOfPublish();
        String[] arrOfDateStr = dateStr.split("-");

        holder.givenDateMonth.setText(arrOfDateStr[0]);
        holder.givenDateDay.setText(arrOfDateStr[1]);
        holder.givenDateYear.setText(arrOfDateStr[2]);
        holder.title.setText(circularObj.getCircularTitle());

    }

    @Override
    public int getItemCount() {
        return circularList.size();
    }

    public void filter(List<Circular> list){
        circularList = new ArrayList<>();
        circularList.addAll(list);
        notifyDataSetChanged();
    }

    class CircularViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView title,description,publishDate;
        TextView  givenDateMonth, givenDateDay, givenDateYear ;
        public CircularViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            title = itemView.findViewById(R.id.circularTitle);
//            description = itemView.findViewById(R.id.circularDescription);
//            publishDate = itemView.findViewById(R.id.publishDate);
            givenDateMonth = itemView.findViewById(R.id.dateofGivenMonth);
            givenDateDay = itemView.findViewById(R.id.dateofGivenDay);
            givenDateYear = itemView.findViewById(R.id.dateofGivenYear);
        }

        @Override
        public void onClick(View view) {


            Pair[] pair=new Pair[4];
            pair[0]=new Pair(title,"cv_title");
            pair[1]=new Pair(description,"cv_description");

            Intent cirIntent;
            int pos = getAdapterPosition();
            Circular circularObj = circularList.get(pos);

            cirIntent= new Intent(context, CircularDetailActivity.class);


            cirIntent.putExtra("title",circularObj.getCircularTitle());
            cirIntent.putExtra("date",circularObj.getDateOfPublish());
            cirIntent.putExtra("desc",circularObj.getCircularDescription());
            cirIntent.putExtra("flag", Constants.CIRCULAR_FLAG);

            EasyTransitionOptions options = EasyTransitionOptions.makeTransitionOptions((Activity)context);
            EasyTransition.startActivity(cirIntent,options);
//            context.startActivity(cirIntent,options);
        }
    }
}
