package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.SerializedName;

public class Report {
    @SerializedName("subject")
    private String subject;
    @SerializedName("marks")
    private String marks;

    public Report(String subject, String marks) {
        this.subject = subject;
        this.marks = marks;
    }

    public String getSubject() {
        return subject;
    }

    public String getMarks() {
        return marks;
    }
}
