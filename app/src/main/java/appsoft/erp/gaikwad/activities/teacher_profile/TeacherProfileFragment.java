package appsoft.erp.gaikwad.activities.teacher_profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import de.hdodenhof.circleimageview.CircleImageView;

public class TeacherProfileFragment  extends Fragment implements OnTitleSetListener {
    private  LinearLayout linearbasic,lineareducational,linearaddress;
    private  TextView teachername,teacherid;
    CircleImageView teacherprofile;
    private static OnTitleSetListener onTitleSetListener = null;
    private SessionManagement sessionManagement;


    public TeacherProfileFragment(){}

    public  static TeacherProfileFragment getInstance(OnTitleSetListener onTitleSetListener1){
          onTitleSetListener= onTitleSetListener1;
        return new TeacherProfileFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        settitle();
    }

    private void settitle() {

        onTitleSetListener.setOnTitleText("Profile");
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.teacher_profile_activity,container,false);
        sessionManagement = new SessionManagement(getContext());

        linearbasic=view.findViewById(R.id.linearlayout_basic_detail);

        linearbasic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),TeacherBasicDetailActivity.class);
                startActivity(intent);
            }
        });
        linearaddress=view.findViewById(R.id.linearlayout_address_detail);

        linearaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),TeacherAddressActivity.class);
                startActivity(intent);
            }
        });
        lineareducational=view.findViewById(R.id.linearlayout_educational_detail);

        lineareducational.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),TecherEducationalActivity.class);
                startActivity(intent);
            }
        });

        teachername=view.findViewById(R.id.teacherName);
        teacherid=view.findViewById(R.id.teacherId);
        teacherprofile=view.findViewById(R.id.teacherProfilePhoto);

        teachername.setText(sessionManagement.getUserName());
        teacherid.setText(String.valueOf(sessionManagement.getUserId()));
        Log.i("DEBUG", sessionManagement.getImagePath());
        if (sessionManagement.getImageLastPath().isEmpty() || sessionManagement.getImageLastPath().equals(null) || sessionManagement.getImageLastPath() == ""){
            Glide.with(this)
                    .load("")
                    .placeholder(getResources().getDrawable(R.drawable.person))
                    .fitCenter()
                    .into(teacherprofile);
        }else {
            Glide.with(this)
                    .load(sessionManagement.getImagePath())
                    .fitCenter()
                    .into(teacherprofile);
        }



        return view;
    }

    @Override
    public void setOnTitleText(String titleText) {
        onTitleSetListener.setOnTitleText("Profile");
    }
}
