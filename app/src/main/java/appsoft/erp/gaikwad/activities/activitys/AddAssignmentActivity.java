package appsoft.erp.gaikwad.activities.activitys;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import appsoft.erp.gaikwad.activities.helper.AddActionHelper;
import appsoft.erp.gaikwad.activities.interfaces.AddActionInterface;
import retrofit2.Call;
import retrofit2.Callback;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.extras.DatePickerFragment;
import appsoft.erp.gaikwad.activities.extras.TodaysDate;
import appsoft.erp.gaikwad.activities.modals.ResponseModel;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import appsoft.erp.gaikwad.activities.volley_singleton.NetworkStatus;

import static appsoft.erp.gaikwad.activities.extras.Constants.EXTERNAL_FILE_PERMISSION_REQUEST;

public class AddAssignmentActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener
        , NetworkStatus, View.OnClickListener {

    private Spinner classname, section, subject;
    private EditText title, description;
    private TextView submissionDate;
    private String todaysDate;
    private CoordinatorLayout layout;
    private static String fPath = "";
    private TextView filename;
    private Button fileUpload;

    private String classNameString = "";
    private String sectionNameString = "";
    private String subjectNameString = "";
    private SessionManagement session;

    // First we need to find classes for teacher
    // Load subjects according to selected class

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_assignment_layout);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        session = new SessionManagement(this);
        initialiseData();

        if (isNetworkAvailable()) {
            setClassNames();
        } else {
            final Snackbar snackbar = Snackbar.make(layout, getResources().getString(R.string.no_connection), Snackbar.LENGTH_INDEFINITE);
            snackbar.setActionTextColor(getResources().getColor(R.color.colorDarkRedBlood));
            snackbar.setAction("RETRY", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isNetworkAvailable()) {
                        snackbar.show();
                        setClassNames();
                    }
                }
            });
            snackbar.show();
        }

        classname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0){
                    classNameString = classname.getItemAtPosition(position).toString();
                    AddActionHelper.getSection(getApplicationContext() ,classNameString, new AddActionInterface() {
                        @Override
                        public void getDataListener(List<String> list) {
                            ArrayAdapter<String> secAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.text_filter_view, list);
                            secAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
                            section.setAdapter(secAdapter);
                        }


                    });

                }else {

                    classNameString = "";
                      }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        section.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0){
                    sectionNameString = section.getItemAtPosition(position).toString();
                    AddActionHelper.getSubjects(getApplicationContext(),classNameString, sectionNameString, new AddActionInterface() {
                        @Override
                        public void getDataListener(List<String> list) {
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.text_filter_view, list);
                            adapter.setDropDownViewResource(R.layout.text_drop_down_theme);
                            subject.setAdapter(adapter);
                        }

                    });
                }else {
                    sectionNameString = "";
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        subject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Log.i("DEBUG", view.toString());
                    subjectNameString = (String) parent.getSelectedItem();
                }else {
                    subjectNameString = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

//    private void getSubjectTeacherDetail() {
//        ApiInterface i = ClientAPI.getRetrofit().create(ApiInterface.class);
//        Call<List<Subject>> call = i.getStudentDetail(new SessionManagement(getApplicationContext()).getUserId());
//        call.enqueue(new Callback<List<Subject>>() {
//            @Override
//            public void onResponse(Call<List<Subject>> call, Response<List<Subject>> response) {
//                if (response.isSuccessful()) {
//                    List<Subject> myClass = response.body();
//                    parseData(myClass);
//                } else {
//                    Toast.makeText(AddAssignmentActivity.this, "Oops, class not found", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<Subject>> call, Throwable t) {
//                Log.i("DEBUG", "onFailure: " + t.getMessage());
//                Toast.makeText(AddAssignmentActivity.this, "Server failure: " + t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

//    private void parseData(List<Subject> myClass) {
//        // Filter data here
//        Set<String> classes = new HashSet<>();
//        Set<String> subjects = new HashSet<>();
//        Set<String> sections = new HashSet<>();
//
//        try {
//            for (Subject sub : myClass) {
//                classes.add(sub.getClass_());
//                subjects.add(sub.getSubject());
//                sections.add(sub.getSection());
//            }
//        }catch (NullPointerException npe){npe.printStackTrace();}
//
//        List<String> clslist = new ArrayList<>();
//        clslist.add("Select class");
//        clslist.addAll(classes);
//        setArrayAdapterToClasses(clslist);
//
//        List<String> sublist = new ArrayList<>();
//        sublist.add("Select subject");
//        sublist.addAll(subjects);
//        setArrayAdapterToSubject(sublist);
//
//        List<String> seclist = new ArrayList<>();
//        seclist.add("Select section");
//        seclist.addAll(sections);
//        setArrayAdapterToSection(seclist);
//    }

//    private void setArrayAdapterToClasses(List<String> classList) {
//        try {
//            ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.text_filter_view, classList);
//            adapter.setDropDownViewResource(R.layout.text_drop_down_theme);
//            classname.setAdapter(adapter);
//
//        } catch (NullPointerException npe) {
//            Log.i("DEBUG", "No subject");
//        } catch (Exception exp) {
//            Log.i("DEBUG", "Exception");
//        }
//    }
//
//    private void setArrayAdapterToSubject(List<String> subjectList) {
//        try {
//            ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.text_filter_view, subjectList);
//            adapter.setDropDownViewResource(R.layout.text_drop_down_theme);
//            subject.setAdapter(adapter);
//
//        } catch (NullPointerException npe) {
//            Log.i("DEBUG", "No subject");
//        } catch (Exception exp) {
//            Log.i("DEBUG", "Exception");
//        }
//    }
//
//    private void setArrayAdapterToSection(List<String> sectionList) {
//        try {
//            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.text_filter_view, sectionList);
//            adapter.setDropDownViewResource(R.layout.text_drop_down_theme);
//            section.setAdapter(adapter);
//
//        } catch (NullPointerException npe) {
//            Log.i("DEBUG", "No subject");
//        } catch (Exception exp) {
//            Log.i("DEBUG", "Exception");
//        }
//    }

    @SuppressLint("RestrictedApi")
    private void initialiseData() {
        classname = findViewById(R.id.spinnerClass);
        section = findViewById(R.id.spinnerSection);
        subject = findViewById(R.id.spinnerSubject);
        description = findViewById(R.id.description);
        title = findViewById(R.id.assignmentTitle);
        submissionDate = findViewById(R.id.dateOfSubmission);
        filename = findViewById(R.id.filename);
        layout = findViewById(R.id.assignmentLayout);
        fileUpload = findViewById(R.id.chooseFileButton);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        setTodaysDate();
        submissionDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callDatePicker(view);
            }
        });

    }
        private void setClassNames() {
            AddActionHelper.getClasses(getApplicationContext(), new AddActionInterface() {
                @Override
                public void getDataListener(List<String> list) {
                    if (list != null){
                        ArrayAdapter<String> classAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.text_filter_view, list);
                        classAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
                        classname.setAdapter(classAdapter);
                    }else {
                        Toast.makeText(getApplicationContext(), "Subject not assigned to you.", Toast.LENGTH_SHORT).show();
                    }
                }


            });

        }


    private void callDatePicker(View view) {
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.show(getFragmentManager(), "DatePick");
    }

    public void addAssignment(View view) {
        if (isAllFieldFilled() && isNetworkAvailable()) {


            ApiInterface assignInter = ClientAPI.getRetrofit().create(ApiInterface.class);

            Call<ResponseModel> call = assignInter.addAssignment(
                    session.getUserId(),
                    classNameString,
                    sectionNameString,
                    submissionDate.getText().toString(),
                    subjectNameString,
                    title.getText().toString(),
                    description.getText().toString()
                    //file
            );

            call.enqueue(new Callback<ResponseModel>() {
                @Override
                public void onResponse(Call<ResponseModel> call, retrofit2.Response<ResponseModel> response) {
                    ResponseModel res = response.body();
                    try {
                        Toast toast = Toast.makeText(getApplicationContext(), "" + res.getRes(), Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        refreshView();
                    } catch (Exception exp) {
                        Log.i("DEBUG", "Assignment Catch: " + exp.toString());
                    }
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    Log.i("DEBUG", "Assignment Failure: " + t.toString());
                }
            });
        } else {
            Toast.makeText(this, "Please, fill all the required field or check internet connection.", Toast.LENGTH_LONG).show();
        }
    }

    private void refreshView() {
        setClassNames();
        description.setText("");
        title.setText("");
        submissionDate.setText("");
        filename.setText("");
    }


    public void uploadFile(){

        if (getFilePermission()){
            String[] mimeTypes = {"application/pdf","application/msword","image/*","application/vnd.openxmlformats-officedocument.wordprocessingml.document"};
            // Here we upload assignment file
            Intent fileIntent = new Intent(Intent.ACTION_GET_CONTENT);

            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.KITKAT){
                fileIntent.setType("*/*");
                fileIntent.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);
            }else {
                String mimeTypesStr = "";
                for (String mimeType : mimeTypes) {
                    mimeTypesStr += mimeType + "|";
                }
                fileIntent.setType(mimeTypesStr.substring(0,mimeTypesStr.length() - 1));
            }

            fileIntent.addCategory(Intent.CATEGORY_OPENABLE);
            Intent chooserIntent = Intent.createChooser(fileIntent,"File");
            startActivityForResult(chooserIntent,101);
        }else{
            AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
            builder.setTitle("Suggestion");
            builder.setMessage("Please, accept the permission to upload assignment");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }
    }


    private boolean getFilePermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if( ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_FILE_PERMISSION_REQUEST);
            }

            return true;
        }
        return true;
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == EXTERNAL_FILE_PERMISSION_REQUEST && (grantResults[0] == PackageManager.PERMISSION_GRANTED)){
            uploadFile();
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_FILE_PERMISSION_REQUEST);
            }
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 101 && resultCode==RESULT_OK){

            if (data != null){
                final Uri fileUri = data.getData();
                fPath = fileUri.toString();
                Log.i("DEBUG","path: "+fPath);
                String onlyFileName = fPath.substring(fPath.lastIndexOf("%")+3);
                filename.setText(onlyFileName);
                Toast.makeText(this, fPath, Toast.LENGTH_LONG).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private boolean isAllFieldFilled() {

        if (title.getText().toString().trim().isEmpty()) {
            return false;
        } else if (description.getText().toString().trim().isEmpty()) {
            return false;
        } else if (submissionDate.getText().toString().trim().isEmpty()) {
            return false;
        }else if (sectionNameString.trim().isEmpty()){
            return false;
        }else if (subjectNameString.trim().isEmpty()){
            return false;
        }else{
            return true;
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        Calendar calendar = new GregorianCalendar(year, month, day);
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.ENGLISH);
        submissionDate.setText(dateFormat.format(calendar.getTime()));
    }

    private void setTodaysDate() {
        todaysDate = TodaysDate.getTodaysDate();
    }

    @Override
    public boolean isNetworkAvailable() {
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        return netInfo != null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        uploadFile();
    }
}



