package appsoft.erp.gaikwad.activities.fragments;

 public class DashboardModel {
     public int getDrawable() {
         return drawable;
     }

     public void setDrawable(int drawable) {
         this.drawable = drawable;
     }

     public int drawable;

     public int getColor() {
         return color;
     }

     public void setColor(int color) {
         this.color = color;
     }

     public int color;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImg_id() {
        return img_id;
    }

    public DashboardModel(String name, int img_id) {
        this.name = name;
        this.img_id = img_id;
    }

     public DashboardModel(String name, int img_id, int drawable) {
         this.name = name;
         this.img_id = img_id;
         this.drawable=drawable;
     }


     public void setImg_id(int img_id) {
        this.img_id = img_id;
    }

    private String name;
    private  int img_id;
}

