package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Feedback {

    @SerializedName("sr_no")
    private String sr_no;

    @SerializedName("student_id")
    private String student_id;

    @SerializedName("teacher_id")
    private String teacher_id;

    @SerializedName("date")
    private String date;

    @SerializedName("content")
    private String feedback;

    @SerializedName("first_name")
    private String first_name;

    @SerializedName("last_name")
    private String last_name;

    public Feedback(){}

    public Feedback(String sr_no, String student_id, String teacher_id, String date, String feedback) {
        this.sr_no = sr_no;
        this.student_id = student_id;
        this.teacher_id = teacher_id;
        this.date = date;
        this.feedback = feedback;
    }

    public Feedback(String sr_no, String student_id, String teacher_id, String date, String feedback, String first_name, String last_name) {
        this.sr_no = sr_no;
        this.student_id = student_id;
        this.teacher_id = teacher_id;
        this.date = date;
        this.feedback = feedback;
        this.first_name = first_name;
        this.last_name = last_name;
    }

    public Feedback(String date, String feedback) {
        this.date = date;
        this.feedback = feedback;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getDate() {
        String outputDateStr = "";
        try {
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outputFormat = new SimpleDateFormat("MMM-dd-yyyy");
//            Date date = inputFormat.parse(date);
            Date datedate=inputFormat.parse(date);
            outputDateStr = outputFormat.format(datedate);
        }catch (Exception exc){
            return date;
        }
        return outputDateStr;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    /*public String getStudentName() {
        return studentName;
    }*/

    /*public void setStudentName(String studentName) {
        this.studentName = studentName;
    }*/

    public String getSr_no() {
        return sr_no;
    }

    public String getStudent_id() {
        return student_id;
    }

    public String getTeacher_id() {
        return teacher_id;
    }
}
