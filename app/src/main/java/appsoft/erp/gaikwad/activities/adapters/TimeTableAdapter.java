package appsoft.erp.gaikwad.activities.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.ClassTimeTable;

public class TimeTableAdapter extends RecyclerView.Adapter<TimeTableAdapter.ViewTimeTable> {

    private List<ClassTimeTable> classTimeTablesList;
    private ClassTimeTable object;

    public TimeTableAdapter(List<ClassTimeTable> classTimeTablesList){
        this.classTimeTablesList = classTimeTablesList;
    }

    @Override
    public ViewTimeTable onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.time_table_card,parent,false);
        return new ViewTimeTable(view);
    }

    @Override
    public void onBindViewHolder(ViewTimeTable holder, int position) {
        object = classTimeTablesList.get(position);
        holder.timing.setText(object.getTime());
        holder.subject.setText(object.getSubject());
    }

    @Override
    public int getItemCount() {
        try {
            Log.i("DEBUG", "List of class TT size: "+classTimeTablesList.size());
            return classTimeTablesList.size();
        }catch (NullPointerException npe){
            return 0;
        }
    }

    class ViewTimeTable extends RecyclerView.ViewHolder{

        private TextView timing,subject;

        public ViewTimeTable(View itemView) {
            super(itemView);

            timing = itemView.findViewById(R.id.timing);
            subject = itemView.findViewById(R.id.subject);
        }
    }
}
