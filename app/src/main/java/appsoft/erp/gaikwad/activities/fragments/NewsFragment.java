package appsoft.erp.gaikwad.activities.fragments;


import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.github.ybq.android.spinkit.style.Circle;

import java.util.ArrayList;
import java.util.List;

import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import appsoft.erp.gaikwad.activities.network_broadcast.ConnectionDetector;
import retrofit2.Call;
import retrofit2.Callback;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.adapters.NewsAdapter;
import appsoft.erp.gaikwad.activities.modals.News;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.search_package.SearchColor;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends Fragment  {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager manager;
//    private List<News> newsList = null;
    private  List<News> newsList;
    private SearchView searchView;
    SwipeRefreshLayout swipeRefreshLayout;
    ProgressDialog progressDoalog;
    private NewsAdapter adapter;
    private ImageView dataNotFound;

    ConstraintLayout constraintLayout;
    Button tryAgain;
    private ImageView noDataFound;
    private LinearLayout dropDownLayout,dateTextLayout;
    private static OnTitleSetListener onTitleSetListener = null;

    public NewsFragment() {
        // Required empty public constructor
    }

    public static NewsFragment getInstance(OnTitleSetListener onTitleSetListener1) {
        // Required empty public constructor
        onTitleSetListener = onTitleSetListener1;
        return new NewsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle();
    }

    private void setTitle() {

        onTitleSetListener.setOnTitleText("News");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setTitle();

        View view = inflater.inflate(R.layout.news_fragment, container, false);
        int resId = R.anim.layout_animation_fall_down;
        recyclerView = view.findViewById(R.id.newsRecycler);
        recyclerView.setHasFixedSize(true);

        swipeRefreshLayout=view.findViewById(R.id.swiperefreshlayout);
//        dateTextLayout= view.findViewById(R.id.date_layout);
        dataNotFound = view.findViewById(R.id.no_data_found_image);
        constraintLayout = view.findViewById(R.id.no_internet_layout);
//        dropDownLayout = view.findViewById(R.id.dropdown_layout);
        tryAgain = view.findViewById(R.id.try_again);
        manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNewsFromServer();
                swipeRefreshLayout.setRefreshing(false);
            }
        });



        setTitle();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)

            if (ConnectionDetector.isConnected(getContext())){
                viewsWhenConnected();
                getNewsFromServer();
            }
            else {
                viewsWhenNotConnected();
            }

            tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConnectionDetector.isConnected(getContext())){

                    viewsWhenConnected();
                    getNewsFromServer();
                }
                else {
                    Toast.makeText(getActivity().getApplicationContext(), R.string.no_connection, Toast.LENGTH_SHORT).show();
                }
            }
        });

        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(),resId);
        recyclerView.setLayoutAnimation(animation);

        return view;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search_menu, menu);
        final MenuItem searchMenuItem = menu.findItem(R.id.isync_search);
        SearchColor.changeSearchViewColor(searchView);
        searchView = SearchColor.getSearchView(searchMenuItem);
        final MenuItem  additem=menu.findItem(R.id.isync_add);
        additem.setVisible(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()){
                    searchView.setIconified(true);
                }
                searchMenuItem.collapseActionView();

                Log.i("vishal.rana.tp", "Submit Query: "+query+" "+query.length());

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                try {
                    final List<News> filterModeList = filter(newsList, newText);
                    adapter.filteredList(filterModeList);
                }catch (NullPointerException npe){}
                return false;
            }
        });
    }

    private List<News> filter(List<News> list, String query){

        query = query.toLowerCase();
        try {

            final List<News> filterList = new ArrayList();
            for (News obj : list) {

                final String titleText = obj.getNewsTitle().toLowerCase();

                final String dateText = obj.getDateOfPublish().toLowerCase();

                if (titleText.contains(query) || dateText.contains(query)) {

                    filterList.add(obj);
                }
            }
            return filterList;
        }catch (NullPointerException npe){ }
        return null;
    }

    private void getNewsFromServer(){
        newsprogressbar();
        ApiInterface newsApi = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<News>> call = newsApi.getNewsList();
//        final List<News> nlist = new ArrayList<>();
        call.enqueue(new Callback<List<News>>() {
            @Override
            public void onResponse(Call<List<News>> call, retrofit2.Response<List<News>> response){
                if (response.isSuccessful()) {
                    newsList = response.body();
                    try {
                        if (null == newsList || newsList.size() == 0)
                        {
                            viewsWhenNoDataFound();
                            return;
                        } else {
                            viewsWhenConnected();
                            adapter = new NewsAdapter(getContext(), newsList);
                            recyclerView.setAdapter(adapter);
                        }

                    }catch (NullPointerException npe){

                        Log.i("DEBUG", "News are empty");
                    }
                    catch (Exception exp){ Log.i("DEBUG", ""+exp.toString());
                    }
                }
                progressDoalog.dismiss();
            }

            @Override
            public void onFailure(Call<List<News>> call, Throwable t) {
                viewsWhenNoDataFound();
                Log.i("DEBUG", "News Dash Expsn: "+t.toString());
                progressDoalog.dismiss();
            }
        });
    }




    private void viewsWhenConnected() {

        recyclerView.setVisibility(View.VISIBLE);
        constraintLayout.setVisibility(View.GONE);
        dataNotFound.setVisibility(View.GONE);

    }

    private void viewsWhenNotConnected() {

        recyclerView.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.VISIBLE);
        dataNotFound.setVisibility(View.GONE);

    }

    private void viewsWhenNoDataFound() {

        recyclerView.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.GONE);
        dataNotFound.setVisibility(View.VISIBLE);

    }

    private void newsprogressbar() {

        progressDoalog = new ProgressDialog(getActivity());
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.setMessage("Please Wait...");
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
        progressDoalog.setIndeterminate(true);
        Circle wave=new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();

    }



}
