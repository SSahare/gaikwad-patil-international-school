package appsoft.erp.gaikwad.activities.teacher_profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeacherBasicDetailActivity extends AppCompatActivity {
    private SessionManagement sessionManagement;
    private TeacherProfileModel profileModel;

    TextView name, gender, degination, dob, conctact, alternate_cont, email, alternate_email, expereince, bloodgroup, medicaldetail, appoitmentdate, adharnumber;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.teacher_basic_detail);
        sessionManagement = new SessionManagement(getApplicationContext());
        name = findViewById(R.id.name);
        gender = findViewById(R.id.gender);
        degination = findViewById(R.id.designation);
        dob = findViewById(R.id.dob);
        conctact = findViewById(R.id.contact);
        alternate_cont = findViewById(R.id.alternatecontact);
        email = findViewById(R.id.email);
        alternate_email = findViewById(R.id.alternateemail);
        expereince = findViewById(R.id.expereince);
        bloodgroup = findViewById(R.id.bloodgroup);
        medicaldetail = findViewById(R.id.medicaldetail);
        appoitmentdate = findViewById(R.id.apppointment_date);
        adharnumber = findViewById(R.id.tv_aadhar_Number);

        getDataFromServer();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Basic Detail");
        }


    }

    private void getDataFromServer() {

        ApiInterface in = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<TeacherProfileModel> call = in.getTProfileData(sessionManagement.getUserId());
        call.enqueue(new Callback<TeacherProfileModel>() {
            @Override
            public void onResponse(Call<TeacherProfileModel> call, Response<TeacherProfileModel> response) {
                profileModel = response.body();

//                if (profileModel.getEducation().size() != 0){
//                    setEducationalDetails();
//                }

                setBasicDetails();
//                setAddressDetails();
            }

            @Override
            public void onFailure(Call<TeacherProfileModel> call, Throwable t) {
                Log.i("DEBUG", "Error Ocurred");
            }
        });
    }

    private void setBasicDetails() {
        name.setText(profileModel.getBasic().getFirstName() + " " + profileModel.getBasic().getMiddleName() + " " + profileModel.getBasic().getLastName());
        gender.setText(profileModel.getBasic().getGender());
        degination.setText(profileModel.getBasic().getDesignation());
        dob.setText( profileModel.getBasic().getDateOfBirth());
        conctact.setText(profileModel.getBasic().getMobileNo());
        alternate_cont.setText( profileModel.getBasic().getAlternateNo());
        email.setText(profileModel.getBasic().getEmail());
        alternate_email.setText( profileModel.getBasic().getAlternateEmail());
        appoitmentdate.setText( profileModel.getBasic().getAppointmentDate());
        expereince.setText(profileModel.getBasic().getExperience());
        bloodgroup.setText( profileModel.getBasic().getBloodGroup());
        medicaldetail.setText( profileModel.getBasic().getMedicalHistory());
        adharnumber.setText( profileModel.getBasic().getAdharNumber());

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}

//  Basic_detail.add("Name: "+profileModel.getBasic().getFirstName()+" "+profileModel.getBasic().getMiddleName()+" "+profileModel.getBasic().getLastName());
//          Basic_detail.add("Gender: "+profileModel.getBasic().getGender());
//          Basic_detail.add("Designation: "+profileModel.getBasic().getDesignation());
//          Basic_detail.add("Date of Birth: "+profileModel.getBasic().getDateOfBirth());
//          Basic_detail.add("Aadhar No.: "+profileModel.getBasic().getAdharNumber());
//          Basic_detail.add("Contact: "+profileModel.getBasic().getMobileNo());
//          Basic_detail.add("Alternate Contact: "+profileModel.getBasic().getAlternateNo());
//          Basic_detail.add("Email: "+profileModel.getBasic().getEmail());
//          Basic_detail.add("Alternate Email: "+profileModel.getBasic().getAlternateEmail());
//          Basic_detail.add("Appointment Date: "+profileModel.getBasic().getAppointmentDate());
//          Basic_detail.add("Experience: "+profileModel.getBasic().getExperience());
//          Basic_detail.add("Blood Group: "+profileModel.getBasic().getBloodGroup());
//          Basic_detail.add("Medical Details: "+profileModel.getBasic().getMedicalHistory());