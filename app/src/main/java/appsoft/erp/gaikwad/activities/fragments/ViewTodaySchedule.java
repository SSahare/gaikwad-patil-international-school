package appsoft.erp.gaikwad.activities.fragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;

import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.TodaysSchedule;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewTodaySchedule extends Fragment {
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    TodaysScheduleAdapter todaysScheduleAdapter;
    private List<TodaysSchedule> todaysScheduleList;
    ProgressDialog progressDoalog;



    public ViewTodaySchedule() { }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_todayschedule, container, false);
        recyclerView = view.findViewById(R.id.recycleViewtodaychedule);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);

        getTeacherTodaysSchedule();
        return view;

    }

    private void getTeacherTodaysSchedule() {
        todaysschedule();
        SessionManagement sessionManagement = new SessionManagement(getContext());
        ApiInterface apiInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<TodaysSchedule>> call = apiInterface.getTodaysSchedule(sessionManagement.getUserId());
        call.enqueue(new Callback<List<TodaysSchedule>>() {
            @Override
            public void onResponse(Call<List<TodaysSchedule>> call, Response<List<TodaysSchedule>> response) {
                todaysScheduleList = response.body();
                todaysScheduleAdapter = new TodaysScheduleAdapter(todaysScheduleList);
                recyclerView.setAdapter(todaysScheduleAdapter);
                progressDoalog.dismiss();

            }

            @Override
            public void onFailure(Call<List<TodaysSchedule>> call, Throwable t) {
                Toast.makeText(getContext(), "Server Error", Toast.LENGTH_SHORT).show();

            }
        });



    }

    private void todaysschedule() {
        progressDoalog = new ProgressDialog(getActivity());
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.setMessage("Please Wait...");
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
//        progressDoalog.getWindow().setGravity(LinearLayout.LayoutParams.MATCH_PARENT LinearLayout.LayoutParams.MATCH_PARENT);
        progressDoalog.setIndeterminate(true);
        Circle wave=new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();
    }

    private class TodaysScheduleAdapter extends RecyclerView.Adapter<TodaysScheduleAdapter.ViewHolder> {
        List<TodaysSchedule> todaysSchedules;
        TodaysSchedule todaysSchedule;


        public TodaysScheduleAdapter(List<TodaysSchedule> todaysSchedules) {
            this.todaysSchedules = todaysSchedules;
        }

        @NonNull
        @Override
        public TodaysScheduleAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view= LayoutInflater.from(getContext()).inflate(R.layout.todayscard,parent,false);

            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull TodaysScheduleAdapter.ViewHolder holder, int position) {
            todaysSchedule = todaysScheduleList.get(position);
            holder.textviewclass.setText(todaysSchedule.getTtclass());
            holder.textviewsection.setText(todaysSchedule.getSection());
            holder.textviewsubject.setText(todaysSchedule.getSubject());
            holder.textviewtime.setText(todaysSchedule.getTime());

        }

        @Override
        public int getItemCount() {
            return todaysScheduleList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView textviewclass,textviewsection,textviewsubject,textviewtime;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                textviewclass = itemView.findViewById(R.id.dashboard_class);
                textviewsection = itemView.findViewById(R.id.dashboard_section);
                textviewsubject = itemView.findViewById(R.id.dashboard_subject);
                textviewtime = itemView.findViewById(R.id.dashboardTime);
            }
        }
    }

}
