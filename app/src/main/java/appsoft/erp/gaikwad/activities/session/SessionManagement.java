package appsoft.erp.gaikwad.activities.session;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import appsoft.erp.gaikwad.activities.activitys.LoginActivity;

public class SessionManagement {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_FILE_NAME = "session_prefs";

    // All Shared Preferences Keys USER DATA
    private static final String IS_LOGIN = "isLoggedIn";

    public static final String KEY_FIRST_NAME = "_fname";
    public static final String KEY_LAST_NAME = "_lname";
    public static final String KEY_ID = "id";
    public static final String KEY_LAST_IMAGE_PATH = "_image_last";
    public static final String KEY_IMAGE_PATH = "_image";
    public static final String KEY_ROLE = "_role";

    public SessionManagement(Context context){
        this.context = context;
        pref = context.getSharedPreferences(PREF_FILE_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     * */
    public void createLoginSession(String fname, String lname, int id, String imagePath, String role, String imageLastPath){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        editor.putString(KEY_FIRST_NAME, fname);
        editor.putString(KEY_LAST_NAME, lname);
        editor.putInt(KEY_ID, id);
        editor.putString(KEY_IMAGE_PATH, imagePath);
        editor.putString(KEY_ROLE, role);
        editor.putString(KEY_LAST_IMAGE_PATH, imageLastPath);
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public void checkLogin(){
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            context.startActivity(i);
        }

    }

    public String getUserName(){
        String fullName = pref.getString(KEY_FIRST_NAME, null)+" "+pref.getString(KEY_LAST_NAME, null);
        return fullName;
    }

    public int getUserId(){
        return pref.getInt(KEY_ID, 0);
    }

    public String getUserRole(){
        return pref.getString(KEY_ROLE, null);
    }

    public String getImagePath(){
        return pref.getString(KEY_IMAGE_PATH, null);
    }

    public String getImageLastPath(){
        return pref.getString(KEY_LAST_IMAGE_PATH, "");
    }

    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(context, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        context.startActivity(i);
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
}
