package appsoft.erp.gaikwad.activities.parent_adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.ReportModel;

public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.ReportHolder> {

    private List<ReportModel> reportList;
    private ReportModel report;
    public ReportAdapter(List<ReportModel> reportList){
        this.reportList = reportList;
    }

    @Override
    public ReportHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.report_card, parent, false);
        return new ReportHolder(view);
    }

    @Override
    public void onBindViewHolder(ReportHolder holder, int position) {
        report = reportList.get(position);
//        holder.marks.setText(report.getMarksObtained()+"/"+report.getOutOfMarks());
        holder.subjectName.setText(report.getSubject());
//        holder.reportCardResult.setText(report.getResult());
        holder.obtainedmarks.setText(report.getMarksObtained());
        holder.outofmarks.setText(report.getOutOfMarks());
    }

    @Override
    public int getItemCount() {
        try{
            if (reportList != null){
                return reportList.size();
            }
        }catch (NullPointerException npe){
            return 0;
        }
        return reportList.size();
    }

    class ReportHolder extends RecyclerView.ViewHolder{

        private TextView subjectName, outofmarks,obtainedmarks,reportCardResult;

        public ReportHolder(View itemView) {
            super(itemView);

            subjectName = itemView.findViewById(R.id.reportCardSubject);
            outofmarks = itemView.findViewById(R.id.report_out_of_marks);
            obtainedmarks = itemView.findViewById(R.id.reportcard_Obtained_Marks);
        }
    }
}
