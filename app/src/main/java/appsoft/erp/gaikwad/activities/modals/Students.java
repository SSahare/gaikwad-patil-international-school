package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Students {

    @SerializedName("class")
    @Expose
    private String _class;
    @SerializedName("section")
    @Expose
    private String section;
    @SerializedName("students_name")
    @Expose
    private List<StudentsName> studentsName = null;

    public String getClass_() {
        return _class;
    }

    public void setClass_(String _class) {
        this._class = _class;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public List<StudentsName> getStudentsName() {
        return studentsName;
    }

    public void setStudentsName(List<StudentsName> studentsName) {
        this.studentsName = studentsName;
    }
}
