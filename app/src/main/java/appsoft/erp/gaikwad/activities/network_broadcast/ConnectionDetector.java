package appsoft.erp.gaikwad.activities.network_broadcast;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionDetector {
    public static boolean isConnected(Context context){
        ConnectivityManager cm =(ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        assert cm !=null;
        NetworkInfo networkInfo =cm.getActiveNetworkInfo();
        return networkInfo !=null && networkInfo.isConnectedOrConnecting();
    }
}
