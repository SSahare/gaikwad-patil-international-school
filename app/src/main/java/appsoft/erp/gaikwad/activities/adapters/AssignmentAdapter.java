package appsoft.erp.gaikwad.activities.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.Assignments;
import appsoft.erp.gaikwad.activities.open_results.AssignmentDetailActivity;

public class AssignmentAdapter extends RecyclerView.Adapter<AssignmentAdapter.AssignmentViewHolder>{

    private List<Assignments> listOfAssignment;
    private Context context;
    private String dateText = "%s\n%s\n%s";


    public AssignmentAdapter(List<Assignments> listOfAssignment, Context context){
        this.listOfAssignment = listOfAssignment;
        this.context = context;
    }

    @Override
    public AssignmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.assignment_card,parent,false);
        return new AssignmentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AssignmentViewHolder holder, int position) {
        Assignments assignments;
        assignments = listOfAssignment.get(position);
            String datestring=assignments.getGivenDate();
            String[] arrofdate=datestring.split("-");
            holder.givenDateMonth.setText(arrofdate[0]);
            holder.givenDateDay.setText(arrofdate[1]);
            holder.givenDateYear.setText(arrofdate[2]);
        holder.title.setText(assignments.getTitle());
        holder.desc.setText(assignments.getDescription());
        holder.classname.setText(("("+assignments.getAssignmentClass()+" "+","+" "+assignments.getSection()+")"));
        holder.subject.setText("Subject : "+assignments.getSubject());
        if (assignments.getAttachment().isEmpty()||assignments.getAttachment().equals(null)){
        }


    }

    @Override
    public int getItemCount() {
        try {
            return listOfAssignment.size();
        }catch (NullPointerException npe){
            return 0;
        }
    }

    public void filteredList(List<Assignments> list){
        listOfAssignment = new ArrayList<>();
        listOfAssignment.addAll(list);
        notifyDataSetChanged();
    }

    class AssignmentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView givenDate, subDate,
                givenDateMonth, givenDateDay, givenDateYear,
                title, desc, classname, section, subject;
        private Button download;
        private ImageButton fileIcon;
        CardView card;

        private AssignmentViewHolder(View view) {
            super(view);
                view.setOnClickListener(this);

            givenDateMonth = itemView.findViewById(R.id.dateofGivenMonth);
            givenDateDay = itemView.findViewById(R.id.dateofGivenDay);
            givenDateYear = itemView.findViewById(R.id.dateofGivenYear);
            subDate = view.findViewById(R.id.dateOfSubmission);
            title = view.findViewById(R.id.assignmentTitle);
            desc = view.findViewById(R.id.description);
            classname = view.findViewById(R.id.classname);
            section = view.findViewById(R.id.section);
            card=view.findViewById(R.id.card);
            subject = view.findViewById(R.id.subject);

        }

        @Override
        public void onClick(View view) {
            int position=getAdapterPosition();
            Assignments assignments=listOfAssignment.get(position);
            Intent assignmentIntent=new Intent(context,AssignmentDetailActivity.class);
            assignmentIntent.putExtra("assignmenttitle",assignments.getTitle());
            assignmentIntent.putExtra("assignmentsubject",assignments.getSubject());
            assignmentIntent.putExtra("assignmentclass",assignments.getAssignmentClass());
            assignmentIntent.putExtra("assignmentsection",assignments.getSection());
            assignmentIntent.putExtra("assignmentsubmissiondate",assignments.getSubmissionDate());
            assignmentIntent.putExtra("assignmentdescription",assignments.getDescription());
            assignmentIntent.putExtra("assignmentdownload",assignments.getAttachment());
            context.startActivity(assignmentIntent);




        }
    }
}
