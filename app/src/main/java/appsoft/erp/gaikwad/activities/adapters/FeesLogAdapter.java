package appsoft.erp.gaikwad.activities.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.download.Downloader;
import appsoft.erp.gaikwad.activities.modals.FeesRecord;
import appsoft.erp.gaikwad.activities.pdf_viewer.ViewPdfActivity;

import static appsoft.erp.gaikwad.activities.extras.Constants.TEMP_PDF_SUB_URL;


public class FeesLogAdapter extends RecyclerView.Adapter<FeesLogAdapter.FeesHolder> {

    private Context context;
    private List<FeesRecord> feesList;
    private FeesRecord feesObj;
    //private int schoolFees = 0;

    public FeesLogAdapter(Context context, List<FeesRecord> feesList){
        this.context = context;
        this.feesList = feesList;
        //this.schoolFees = schoolFees;
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public FeesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fees_card, parent,false);
        return new FeesHolder(view);
    }

    @Override
    public void onBindViewHolder(FeesHolder holder, int position) {
        feesObj = feesList.get(position);
        String feesamount = feesObj.getFeesAmount();
        String feestransport = feesObj.getTransportationFees();
        holder.feesDate.setText(feesObj.getDate());
        holder.academicYear.setText(String.format("Academic Year(%s)", feesObj.getAcademicYear()));
        holder.fees.setText(feesObj.getFeesAmount());
        holder.srNo.setText(feesObj.getPaymentMode());
        holder.quarterName.setText(feesObj.getCashierId());
        if (feesObj.getTransportationFees() == null){
            holder.transport.setText("0");
        }
        else{
            holder.transport.setText(feesObj.getTransportationFees());
        }
        
        /*if (feesObj.getPdfFile().isEmpty()){
            holder.feeReceiptView.setVisibility(View.GONE);
            holder.feeReceiptDownload.setVisibility(View.GONE);
        }else {
            holder.feePending.setVisibility(View.GONE);
        }*/
    }

    @Override
    public int getItemCount() {
        return feesList == null? 0 : feesList.size();
    }

    class FeesHolder extends RecyclerView.ViewHolder{

//        private TextView academicYear, feesPaidDate, paidAmount, paymentMode,paymentTracationid,paymentcashier,paymentInstallation;

        private TextView academicYear, feesDate, srNo, quarterName, fees,transport;
        private Button feeReceiptView, feeReceiptDownload;
        FeesHolder(View itemView) {
            super(itemView);

            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
            academicYear = itemView.findViewById(R.id.academicYear);
            feesDate = itemView.findViewById(R.id.fees_date);
            srNo = itemView.findViewById(R.id.tv_sr_no);
            quarterName = itemView.findViewById(R.id.tv_quarter_name);
            fees =itemView.findViewById(R.id.tv_fees);
            feeReceiptDownload = itemView.findViewById(R.id.btn_download_fee_receipt);
            feeReceiptView = itemView.findViewById(R.id.btn_view_fee_receipt);
            transport = itemView.findViewById(R.id.tv_transport);

            feeReceiptView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Set View Receipt Here download or View Receipt
                    FeesRecord obj = feesList.get(getAdapterPosition());
                    String receiptName = obj.getPdfFile();
                    Intent pdf = new Intent(context, ViewPdfActivity.class);
                    pdf.putExtra("filename", receiptName);
                    context.startActivity(pdf);
                }
            });

            /*feePending.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "Either fees not paid or receipt not generated yet.", Toast.LENGTH_SHORT).show();
                }
            });*/

            feeReceiptDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Download Fees receipt here
                    FeesRecord obj = feesList.get(getAdapterPosition());
                    String  fee_receipt_name = obj.getPdfFile().trim(); // This is temp get really we need Q1 fees

                    if (fee_receipt_name.isEmpty()){
                        Toast.makeText(context, "Oops, file not found.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    String receiptUrl = String.format("%s%s",TEMP_PDF_SUB_URL, fee_receipt_name);
                    Log.i("DEBUG", "URL of receipt: "+receiptUrl);
                    Downloader downloader = new Downloader(receiptUrl, context);
                    downloader.downloadTask();
                }
            });
        }
    }
}
