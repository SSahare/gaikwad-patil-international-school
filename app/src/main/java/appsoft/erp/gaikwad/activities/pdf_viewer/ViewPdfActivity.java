package appsoft.erp.gaikwad.activities.pdf_viewer;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.github.ybq.android.spinkit.style.Circle;
import com.shockwave.pdfium.PdfDocument;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static appsoft.erp.gaikwad.activities.extras.Constants.TEMP_PDF_SUB_URL;

public class ViewPdfActivity extends AppCompatActivity  implements OnPageChangeListener, OnLoadCompleteListener {
    private PDFView pdfView;
    private static final String TAG = ViewPdfActivity.class.getSimpleName();
    Integer pageNumber = 0;
    private ProgressDialog progressDoalog;
    String pdfFileName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pdf);
        pdfView = findViewById(R.id.pdf_viewer_layout);
        pdfFileName = getIntent().getStringExtra("filename");
        if (pdfFileName.isEmpty()){
            createDialogBox();
        }else {
            displayFromInternet(pdfFileName);
        }
    }

    private void createDialogBox() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("File Not Found");
        builder.setMessage("File is not present on server, Login to web application and create receipt first.");
        builder.setPositiveButton(R.string.exit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                finish();
            }
        });
        builder.show();
    }

    private void displayFromInternet(String receiptName) {
        aissignmentprogressbar();
        Log.i("DEBUG", "File URL: "+String.format("%s%s", TEMP_PDF_SUB_URL, receiptName));
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(TEMP_PDF_SUB_URL)
                .build();

        ApiInterface in = retrofit.create(ApiInterface.class);
        Call<ResponseBody> call = in.downloadFile(receiptName);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                ResponseBody body = response.body();
                runData(body);
                //progressDoalog.dismiss();
            }
            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                if (progressDoalog != null){
                    progressDoalog.dismiss();
                }
            }
        });
    }

    private void runData(final ResponseBody body) {
        new Thread(){
            @Override
            public void run() {
                try {
                    InputStream iStream = new BufferedInputStream(Objects.requireNonNull(body).byteStream(), 1024 * 8);

                    //creating file name dynamically
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.US);
                    Date now = new Date();
                    pdfFileName = formatter.format(now) + ".pdf";

                    final File file = new File(getCacheDir(), pdfFileName);
                    FileOutputStream output = null;
                    output = new FileOutputStream(file);
                    final byte[] buffer = new byte[1024];
                    int size;
                    while ((size = iStream.read(buffer)) != -1) {
                        output.write(buffer, 0, size);
                    }
                    iStream.close();
                    output.close();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            OpenPdfActivity(file.getAbsolutePath());
                        }
                    });

                } catch (FileNotFoundException e) {
                    Log.i("DEBUG", "FileNotFoundException pdf: "+e.getMessage());
                } catch (IOException e) {
                    Log.i("DEBUG", "IO pdf: "+e.getMessage());
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }catch (Exception exp){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            createDialogBox();
                        }
                    });
                    Log.i("DEBUG", "Exception pdf: "+exp.getMessage());
                }
            }
        }.start();
    }

    private void OpenPdfActivity(String absolutePath) {

        progressDoalog.dismiss();
        pdfView.fromFile(new File(absolutePath))
                .defaultPage(pageNumber)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onLoad(this)
                .scrollHandle(new DefaultScrollHandle(this))
                .load();
    }

    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = pdfView.getDocumentMeta();
        printBookmarksTree(pdfView.getTableOfContents(), "-");
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
    }

    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {

            Log.e(TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));

            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }




    private void aissignmentprogressbar() {

        progressDoalog = new ProgressDialog(ViewPdfActivity.this);
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
        progressDoalog.setMessage("Generating PDF Please Wait...");
        progressDoalog.setIndeterminate(true);
        Circle wave=new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();

    }
}
