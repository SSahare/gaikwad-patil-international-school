package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attendance {
    /*@SerializedName("")
    private int month;
    @SerializedName("")
    private int year;*/
    @SerializedName("working_days")
    @Expose
    private int workingDays;
    @SerializedName("attendance_count")
    @Expose
    private int attendanceDays;
    @SerializedName("absent_days")
    @Expose
    private int absentDays;

   /* public Attendance(int month, int year, int w_days, int p_days, int a_days) {
        this.month = month;
        this.year = year;
        this.workingDays = w_days;
        this.attendanceDays = p_days;
        this.absentDays = a_days;
    }*/

    public Attendance( int w_days, int p_days, int a_days){
        this.workingDays = w_days;
        this.attendanceDays = p_days;
        this.absentDays = a_days;
    }

   /* public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }*/

    public int getW_days() {
        return workingDays;
    }

    public int getP_days() {
        return attendanceDays;
    }

    public int getA_days() {
        return absentDays;
    }
}
