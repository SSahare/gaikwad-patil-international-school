package appsoft.erp.gaikwad.activities.open_results;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import appsoft.erp.gaikwad.R;

public class ParentHomeworkDetailActivity  extends AppCompatActivity {

    TextView assignmenttitle,tv_AssignmentSubmissiondate,assignmentclass,assignmentsection,assignemntdescription,tv_ParentAssignment_Subject;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parent_homework_activty);

        assignmenttitle = findViewById(R.id.tv_AssignmentTitle);
        assignemntdescription = findViewById(R.id.tv_AssignmentDescription);
//        tv_AssignmentSubmissiondate = findViewById(R.id.tv_AssignmentSubmissiondate);
        tv_ParentAssignment_Subject = findViewById(R.id.tv_ParentAssignment_Subject);


        tv_ParentAssignment_Subject.setText(getIntent().getStringExtra("homeworksubject"));
        assignmenttitle.setText(getIntent().getStringExtra("homeworktitle"));
        assignemntdescription.setText(getIntent().getStringExtra("homeworkdescription"));
//        tv_AssignmentSubmissiondate.setText( getIntent().getStringExtra("homeworkdateofsubmission"));


        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

                getSupportActionBar().setTitle("Homework Detail");
          //  getSupportActionBar().setTitle(getIntent().getStringExtra("assignmentsubject"));
        }
    }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            if (item.getItemId() == android.R.id.home) {
                finish();
                return true;
            } else {
                return super.onOptionsItemSelected(item);
            }

    }

}
