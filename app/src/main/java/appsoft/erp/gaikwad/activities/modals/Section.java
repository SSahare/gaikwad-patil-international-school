package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Section {

        @SerializedName("section")
        @Expose
        private String section;

        public Section(String section){
            this.section = section;
        }

        public String getSection() {
            return section;
        }
}
