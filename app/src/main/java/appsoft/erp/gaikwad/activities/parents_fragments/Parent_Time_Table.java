package appsoft.erp.gaikwad.activities.parents_fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;

import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.adapters.TimeTableAdapter;
import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import appsoft.erp.gaikwad.activities.modals.ClassTimeTable;
import appsoft.erp.gaikwad.activities.network_broadcast.ConnectionDetector;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import appsoft.erp.gaikwad.activities.volley_singleton.NetworkStatus;
import retrofit2.Call;
import retrofit2.Callback;

public class Parent_Time_Table  extends Fragment  implements NetworkStatus {
    private Spinner days;
    private LinearLayout layout,layoutClass,layoutSection;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mManager;
    private String selectedDay ="";
    ProgressDialog progressDoalog;
    private ImageView noDataFound;
    private ConstraintLayout constraintLayout;
public static OnTitleSetListener onTitleSetListener=null;
    private List<ClassTimeTable> list = null;
    SessionManagement session = null;


      public Parent_Time_Table(){}

      public static Parent_Time_Table getInstance(OnTitleSetListener onTitleSetListener1){
          onTitleSetListener=onTitleSetListener1;
          return new Parent_Time_Table();
      }

    @Override
    public void onResume() {
        super.onResume();
        setTitle();
    }

    private void setTitle() {
          onTitleSetListener.setOnTitleText("Time Table");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

          View view=inflater.inflate(R.layout.activity_class_time_table,container,false);

        session = new SessionManagement(getContext());
        days = view.findViewById(R.id.spinnerDay);
//        layout = findViewById(R.id.timetableLayout);
        layoutClass = view.findViewById(R.id.layoutClass);
//        layoutSection = findViewById(R.id.layoutSection);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        mManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mManager);
        layoutClass.setVisibility(View.GONE);

        constraintLayout = view.findViewById(R.id.no_internet_layout);
        noDataFound = view.findViewById(R.id.no_data_found_image);
        Button tryAgain = view.findViewById(R.id.try_again);

        if (ConnectionDetector.isConnected(getContext())){
            setDays();
        }else{
            viewsWhenNotConnected();
        }


        /*if (isNetworkAvailable()){
            setDays();
        }else{
            final Snackbar snackbar = Snackbar.make(layout,getResources().getString(R.string.no_connection),Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(getResources().getColor(R.color.colorWhite));
            snackbar.setAction("RETRY", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isNetworkAvailable()){
                        snackbar.show();
                    }else {
                        setDays();
                    }
                }
            });
            snackbar.show();
        }*/

        days.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    return;
                }
                selectedDay = (String)parent.getSelectedItem();
                selectedDay = selectedDay.toLowerCase();
                loadTimeTable();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectionDetector.isConnected(getContext())){
                    setDays();
                    viewsWhenConnected();
                }else {
                    Toast.makeText(getContext(), "Opps! "+R.string.no_connection, Toast.LENGTH_SHORT).show();
                }
            }
        });
        return  view;
    }



    private void loadTimeTable() {

        timetableprogressbar();
        ApiInterface classInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<ClassTimeTable>> call = classInterface.getClassTimeTable(session.getUserId(), selectedDay);
        call.enqueue(new Callback<List<ClassTimeTable>>() {
            @Override
            public void onResponse(Call<List<ClassTimeTable>> call, retrofit2.Response<List<ClassTimeTable>> response){
                list = response.body();
                try{
//                    if (list.size() != 0) {
//                        TimeTableAdapter adapter = new TimeTableAdapter(list);
//                        recyclerView.setAdapter(adapter);
//                    }

                    if (list == null || list.isEmpty()) {
                        viewsWhenNoDataFound();
                    }
                    else  {
                        viewsWhenConnected();
                        TimeTableAdapter adapter = new TimeTableAdapter(list);
                        recyclerView.setAdapter(adapter);
                    }

                }catch (NullPointerException npe){
                    TimeTableAdapter adapter = new TimeTableAdapter(list);
                    recyclerView.setAdapter(adapter);
                    Toast.makeText(getContext(), "Time Table Not Available.", Toast.LENGTH_SHORT).show();

                }
                progressDoalog.dismiss();

            }



            @Override
            public void onFailure(Call<List<ClassTimeTable>> call, Throwable t) {

                TimeTableAdapter adapter = new TimeTableAdapter(list);
                recyclerView.setAdapter(adapter);
                progressDoalog.dismiss();
                t.printStackTrace();
            }
        });
    }



    private void viewsWhenNotConnected() {
        constraintLayout.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);
    }


    private void viewsWhenConnected() {
        constraintLayout.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        noDataFound.setVisibility(View.GONE);
    }


    private void viewsWhenNoDataFound() {
        constraintLayout.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        noDataFound.setVisibility(View.VISIBLE);
    }

    private void timetableprogressbar() {


        progressDoalog = new ProgressDialog(getContext());
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
        progressDoalog.setMessage("Please Wait...");
//        progressDoalog.getWindow().setGravity(LinearLayout.LayoutParams.MATCH_PARENT LinearLayout.LayoutParams.MATCH_PARENT);
        progressDoalog.setIndeterminate(true);
        Circle wave=new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();
    }


    private boolean isAllFieldFilled() {
        if (selectedDay.isEmpty()) {
            return false;
        } else {
            return true;
        }

    }
    private void setDays() {


        ArrayAdapter daysAdapter = ArrayAdapter.createFromResource(getContext(),R.array.list_of_days,R.layout.spinner_list);
//        daysAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        daysAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
        days.setAdapter(daysAdapter);
    }



    @Override
    public boolean isNetworkAvailable() {
        ConnectivityManager conMgr =  (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        return netInfo != null;
    }
    
    
}
