package appsoft.erp.gaikwad.activities.parents_fragments;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.adapters.ExamAdapter;
import appsoft.erp.gaikwad.activities.divider.SimpleItemDecoration;
import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import appsoft.erp.gaikwad.activities.modals.ExamModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExamTimeTableFragment extends Fragment {
    public static OnTitleSetListener onTitleSetListener=null;
    RecyclerView recyclerView;
    public ExamTimeTableFragment(){}

    public static ExamTimeTableFragment getInstance(OnTitleSetListener onTitleSetListener1){
        onTitleSetListener=onTitleSetListener1;
        return new ExamTimeTableFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        List<ExamModel> l = new ArrayList<>();
        l.add(new ExamModel("1.", "Hindi", "Monday", "12-09-2019", "11.00 am to 2.00 pm"));
        l.add(new ExamModel("2.", "Maths", "Thursday", "15-09-2019", "11.00 am to 2.00 pm"));
        l.add(new ExamModel("3.", "Geography", "Saturday", "17-09-2019", "9.00 am to 12.00 pm"));
        l.add(new ExamModel("4.", "History", "Tuesday", "19-09-2019", "11.00 am to 2.00 pm"));
        l.add(new ExamModel("5.", "Civics", "Friday", "22-09-2019", "11.00 am to 2.00 pm"));
        l.add(new ExamModel("6.", "English", "Monday", "25-09-2019", "11.00 am to 2.00 pm"));

        // Alert Box
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Dummy Time Table");
        builder.setMessage("This is an dummy time table, used for viewing purpose");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();

        if (l.isEmpty()){
            View view = LayoutInflater.from(getContext()).inflate(R.layout.data_empty_layout, container, false);
            return view;
        }else {
            View view = inflater.inflate(R.layout.fragment_exam_time_table, container, false);
            recyclerView = view.findViewById(R.id.recyclerExam);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            ExamAdapter adapter = new ExamAdapter(l);
            recyclerView.addItemDecoration(new SimpleItemDecoration(getContext()));
            recyclerView.setAdapter(adapter);
            return view;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle();
    }

    private void setTitle() {
        onTitleSetListener.setOnTitleText("Exam");
    }
}