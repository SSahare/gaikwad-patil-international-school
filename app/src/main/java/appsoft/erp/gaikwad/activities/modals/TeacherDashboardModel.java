package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TeacherDashboardModel {

    @SerializedName("class_teacher")
    @Expose
    private ClassTeacher classTeacher;
    @SerializedName("subject_teacher")
    @Expose
    private List<SubjectTeacher> subjectTeacher = null;

    public ClassTeacher getClassTeacher() {
        return classTeacher;
    }

    public void setClassTeacher(ClassTeacher classTeacher) {
        this.classTeacher = classTeacher;
    }

    public List<SubjectTeacher> getSubjectTeacher() {
        return subjectTeacher;
    }

    public void setSubjectTeacher(List<SubjectTeacher> subjectTeacher) {
        this.subjectTeacher = subjectTeacher;
    }

}
