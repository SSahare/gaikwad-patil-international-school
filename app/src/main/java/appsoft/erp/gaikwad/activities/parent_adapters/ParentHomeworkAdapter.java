package appsoft.erp.gaikwad.activities.parent_adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import appsoft.erp.gaikwad.activities.modals.Homework;
import appsoft.erp.gaikwad.activities.open_results.ParentHomeworkDetailActivity;

public class ParentHomeworkAdapter extends RecyclerView.Adapter<ParentHomeworkAdapter.ParentHomeworkViewHolder>  implements OnTitleSetListener {

    private List<Homework> homeworkList;
    private Context context;
    private String dateText = "%s\n%s\n%s";

    public ParentHomeworkAdapter(Context context, List<Homework> homeworkList){
        this.homeworkList = homeworkList;
        this.context = context;
    }

    @Override
    public ParentHomeworkViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.parent_homework_card,parent,false);

        return new ParentHomeworkViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(ParentHomeworkViewHolder holder, int position) {
         Homework homework;
        homework = homeworkList.get(position);
        String dateStr = homework.getDateOfGiven();
        String[] arrOfDateStr = dateStr.split("-");

        holder.givenDateMonth.setText(arrOfDateStr[0]);
        holder.givenDateDay.setText(arrOfDateStr[1]);
        holder.givenDateYear.setText(arrOfDateStr[2]);

//         holder.givenDate.setText("Given Date : "+homework.getDateOfGiven());
//         holder.submissionDate.setText("Submission Date : "+homework.getDateOfSubmission());
         holder.subject.setText("Subject : "+homework.getSubject());
         holder.title.setText(homework.getTitle());
         holder.desc.setText(homework.getDescription());
    }

    @Override
    public int getItemCount() {
        return homeworkList.size();
    }

    public void filter(List<Homework> list){
        homeworkList = new ArrayList<>();
        homeworkList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public void setOnTitleText(String titleText) {

    }

    class ParentHomeworkViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView givenDate,submissionDate,title,desc,subject,givenDateMonth, givenDateDay, givenDateYear;
        private Context context;

        private ParentHomeworkViewHolder(View itemView, Context context) {
            super(itemView);
            this.context = context;
            itemView.setOnClickListener(this);
            givenDateMonth = itemView.findViewById(R.id.dateofGivenMonth);
            givenDateDay = itemView.findViewById(R.id.dateofGivenDay);
            givenDateYear = itemView.findViewById(R.id.dateofGivenYear);
//            givenDate = itemView.findViewById(R.id.dateofGiven);
//            submissionDate = itemView.findViewById(R.id.dateOfSubmission);
            title = itemView.findViewById(R.id.assignmentTitle);
            subject = itemView.findViewById(R.id.subject);
            desc = itemView.findViewById(R.id.description);
        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            Homework homework= homeworkList.get(pos);
            Intent homeworkintent=new Intent(context,ParentHomeworkDetailActivity.class);
            homeworkintent.putExtra("homeworktitle",homework.getTitle());
            homeworkintent.putExtra("homeworkdescription",homework.getDescription());
            homeworkintent.putExtra("homeworksubject",homework.getSubject());
//            homeworkintent.putExtra("homeworksection",homework.getSection());
//            homeworkintent.putExtra("homeworkdateofsubmission",homework.getDateOfSubmission());
//            homeworkintent.putExtra("homeworkclass",homework.getClassname());
            context.startActivity(homeworkintent);

        }
    }
}
