package appsoft.erp.gaikwad.activities.parent_adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.ListAttendance;
import appsoft.erp.gaikwad.activities.parent_activity.MonthlyAttendanceActivity;
import appsoft.erp.gaikwad.activities.session.SessionManagement;

public class Parent_Attendance_Adapter extends RecyclerView.Adapter<Parent_Attendance_Adapter.AViewHolder>{

    private List<ListAttendance> attendanceList;
    private Context context;
    private ListAttendance attendance;

    public Parent_Attendance_Adapter(Context context, List<ListAttendance> attendanceList){
        this.attendanceList = attendanceList;
        this.context = context;
    }

    @Override
    public AViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.parent_attendance_card,parent,false);
        return new AViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AViewHolder holder, int position) {
        attendance = attendanceList.get(position);
        holder.month.setText(String.valueOf(attendance.getMonth()));
        holder.w_days.setText(String.valueOf(attendance.getWorkingDays()));
        holder.p_days.setText(String.valueOf(attendance.getPresentDays()));
        holder.a_days.setText(String.valueOf(attendance.getAbsentDays()));
    }

    @Override
    public int getItemCount() {
        return attendanceList.size();
    }

    class AViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView month,w_days,p_days,a_days;

        public AViewHolder(View itemView) {
            super(itemView);
            month = itemView.findViewById(R.id.month_attendance);
            w_days = itemView.findViewById(R.id.school_working_day);
            p_days = itemView.findViewById(R.id.school_present_day);
            a_days = itemView.findViewById(R.id.school_absent_day);
                itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            SessionManagement sessionManagement = new SessionManagement(context);
            ListAttendance list=attendanceList.get(getAdapterPosition());
            Intent intent = new Intent(context, MonthlyAttendanceActivity.class);
            intent.putExtra("month",list.getMonth());
            intent.putExtra("user_id", sessionManagement.getUserId());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }
}
