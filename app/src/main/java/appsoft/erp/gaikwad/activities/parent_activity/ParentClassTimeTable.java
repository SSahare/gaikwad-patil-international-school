package appsoft.erp.gaikwad.activities.parent_activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.adapters.TimeTableAdapter;
import appsoft.erp.gaikwad.activities.modals.ClassTimeTable;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import appsoft.erp.gaikwad.activities.volley_singleton.NetworkStatus;

public class ParentClassTimeTable extends AppCompatActivity implements NetworkStatus{

    private Spinner days;
    private LinearLayout layout,layoutClass,layoutSection;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mManager;
    private String selectedDay ="";
    ProgressDialog progressDoalog;

    private List<ClassTimeTable> list = null;
    SessionManagement session = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_time_table);

        session = new SessionManagement(getApplicationContext());
        days = findViewById(R.id.spinnerDay);
//        layout = findViewById(R.id.timetableLayout);
        layoutClass = findViewById(R.id.layoutClass);
//        layoutSection = findViewById(R.id.layoutSection);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        mManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mManager);

        layoutClass.setVisibility(View.GONE);
//        layoutSection.setVisibility(View.GONE);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (isNetworkAvailable()){
            setDays();
        }else{
            final Snackbar snackbar = Snackbar.make(layout,getResources().getString(R.string.no_connection),Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(getResources().getColor(R.color.colorWhite));
            snackbar.setAction("RETRY", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isNetworkAvailable()){
                        snackbar.show();
                    }else {
                        setDays();
                    }
                }
            });
            snackbar.show();
        }

        days.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){

                }
                selectedDay = (String)parent.getSelectedItem();
                selectedDay = selectedDay.toLowerCase();
                loadTimeTable();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void loadTimeTable(){
            timetableprogressbar();
        ApiInterface classInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<ClassTimeTable>> call = classInterface.getClassTimeTable(session.getUserId(), selectedDay);
        call.enqueue(new Callback<List<ClassTimeTable>>() {
            @Override
            public void onResponse(Call<List<ClassTimeTable>> call, retrofit2.Response<List<ClassTimeTable>> response){
                list = response.body();
                try{
                    if (list.size() != 0) {
                        TimeTableAdapter adapter = new TimeTableAdapter(list);
                        recyclerView.setAdapter(adapter);
                    }
                    else  {
                        TimeTableAdapter adapter = new TimeTableAdapter(list);
                        recyclerView.setAdapter(adapter);
                        Toast.makeText(ParentClassTimeTable.this, "Time Table Not Available.", Toast.LENGTH_SHORT).show();
            }

            }catch (NullPointerException npe){
                    TimeTableAdapter adapter = new TimeTableAdapter(list);
                    recyclerView.setAdapter(adapter);
                    Toast.makeText(ParentClassTimeTable.this, "Time Table Not Available.", Toast.LENGTH_SHORT).show();

                }
                progressDoalog.dismiss();

            }

            @Override
            public void onFailure(Call<List<ClassTimeTable>> call, Throwable t) {

                TimeTableAdapter adapter = new TimeTableAdapter(list);
                recyclerView.setAdapter(adapter);
                progressDoalog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void timetableprogressbar() {




        progressDoalog = new ProgressDialog(this);
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
        progressDoalog.setMessage("Please Wait...");
//        progressDoalog.getWindow().setGravity(LinearLayout.LayoutParams.MATCH_PARENT LinearLayout.LayoutParams.MATCH_PARENT);
        progressDoalog.setIndeterminate(true);
        Circle wave=new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();
    }


    private boolean isAllFieldFilled() {
        if (selectedDay.isEmpty()){
            return false;
        }else{
            return true;
        }
    }

    private void setDays() {
        ArrayAdapter daysAdapter = ArrayAdapter.createFromResource(getApplicationContext(),R.array.list_of_days,R.layout.spinner_list);
//        daysAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            daysAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
            days.setAdapter(daysAdapter);
    }

    @Override
    public boolean isNetworkAvailable() {
        ConnectivityManager conMgr =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        return netInfo != null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
            return true;
        }
        return false;
    }
}
