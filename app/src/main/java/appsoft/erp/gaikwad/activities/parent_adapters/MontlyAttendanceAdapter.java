package appsoft.erp.gaikwad.activities.parent_adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.MonthlyDetailAttendanceModel;


public class MontlyAttendanceAdapter extends RecyclerView.Adapter<MontlyAttendanceAdapter.ViewHolder> {

    Context context;
    private List<MonthlyDetailAttendanceModel> monthlyDetailAttendanceModelList;
    private MonthlyDetailAttendanceModel attendance;

    public MontlyAttendanceAdapter(Context context, List<MonthlyDetailAttendanceModel> monthlyDetailAttendanceModelList1){
        this.context = context;
        this.monthlyDetailAttendanceModelList = monthlyDetailAttendanceModelList1;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_month_attendance,parent,false);
        return  new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        attendance = monthlyDetailAttendanceModelList.get(position);
        holder.textViewAttendaceDate.setText(attendance.getDate());
        holder.textViewAttendanceStatus.setText(attendance.getAttendanceStatus());

    }

    @Override
    public int getItemCount() {
        return monthlyDetailAttendanceModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView textViewAttendaceDate,textViewAttendanceStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewAttendaceDate=itemView.findViewById(R.id.tv_attendancedate);
            textViewAttendanceStatus = itemView.findViewById(R.id.tv_attendancestatus);
        }
    }
}
