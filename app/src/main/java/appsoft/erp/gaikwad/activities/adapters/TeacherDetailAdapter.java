package appsoft.erp.gaikwad.activities.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.TeacherDetailCombineModel;

public class TeacherDetailAdapter extends RecyclerView.Adapter<TeacherDetailAdapter.TeacherDetailHolder> {

    private List<TeacherDetailCombineModel> list;
    private TeacherDetailCombineModel model;
    public TeacherDetailAdapter(List<TeacherDetailCombineModel> list){
        this.list = list;
    }

    @Override
    public TeacherDetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.teacher_dashboard_detail_card, parent, false);

        return new TeacherDetailHolder(view);
    }

    @Override
    public void onBindViewHolder(TeacherDetailHolder holder, int position) {

        model = list.get(position);

        holder.className.setText(model.getClassName());
        holder.sectionName.setText(model.getSectionName());
        holder.subjectName.setText(model.getSubjectName());
        holder.roleName.setText(model.getRole());

    }

    @Override
    public int getItemCount() {
        if (list != null)
        return list.size();
        else
        {return 0;}
    }

    class TeacherDetailHolder extends RecyclerView.ViewHolder{

        private TextView className, sectionName, subjectName, roleName;

        public TeacherDetailHolder(View itemView) {
            super(itemView);

            className = itemView.findViewById(R.id.dashboard_class);
            sectionName = itemView.findViewById(R.id.dashboard_section);
            subjectName = itemView.findViewById(R.id.dashboard_subject);
            roleName = itemView.findViewById(R.id.dashboard_role);
        }
    }
}
