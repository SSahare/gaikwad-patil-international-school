package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClassTeacher {

    @SerializedName("response")
    @Expose
    private Boolean response;
    @SerializedName("class_assigned")
    @Expose
    private String classAssigned;
    @SerializedName("section_assigned")
    @Expose
    private String sectionAssigned;

    public Boolean getResponse() {
        return response;
    }

    public void setResponse(Boolean response) {
        this.response = response;
    }

    public String getClassAssigned() {
        return classAssigned;
    }

    public void setClassAssigned(String classAssigned) {
        this.classAssigned = classAssigned;
    }

    public String getSectionAssigned() {
        return sectionAssigned;
    }

    public void setSectionAssigned(String sectionAssigned) {
        this.sectionAssigned = sectionAssigned;
    }


}
