package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.SerializedName;

import appsoft.erp.gaikwad.activities.date_util.DateFormatter;

public class News {

    @SerializedName("sr_no")
    private String serial_no;

    @SerializedName("headline")
    private String newsTitle;

    @SerializedName("content")
    private String newsDescription;

    @SerializedName("date")
    private String dateOfPublish;

    public News(String serial_no, String newsTitle, String newsDescription, String dateOfPublish) {
        this.serial_no = serial_no;
        this.newsTitle = newsTitle;
        this.dateOfPublish = dateOfPublish;
        this.newsDescription = newsDescription;
    }

    public String getSerial_no(){ return serial_no;}

    public String getNewsTitle() {
        return newsTitle;
    }

    public String getNewsDescription() {
        return newsDescription;
    }

    public String getDateOfPublish() {
        return DateFormatter.getStandardDateFormat(dateOfPublish);
    }
}