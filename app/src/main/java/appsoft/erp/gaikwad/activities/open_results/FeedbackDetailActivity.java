package appsoft.erp.gaikwad.activities.open_results;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import appsoft.erp.gaikwad.R;

public class FeedbackDetailActivity extends AppCompatActivity {
    TextView tv_FeedBackDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_detail);
        tv_FeedBackDetail=findViewById(R.id.tv_FeedBackDetail);

        tv_FeedBackDetail.setText(getIntent().getStringExtra("feedbackdestudent"));



        if (getSupportActionBar()!=null){
            getSupportActionBar().setTitle("Detail Feedback");
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);


        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
