package appsoft.erp.gaikwad.activities.search_package;

import android.graphics.Color;
import android.support.v7.widget.SearchView;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class SearchColor {

    private static SearchView searchView;
    public static void changeSearchViewColor(View view) {
        if (view != null){
            if (view instanceof TextView){
                ((TextView) view).setTextColor(Color.WHITE);
                return;
            }else if (view instanceof ViewGroup){
                ViewGroup vg = (ViewGroup) view;
                for (int i =0; i< vg.getChildCount(); i++){
                    changeSearchViewColor(vg.getChildAt(i));
                }
            }
        }
    }

    public static SearchView getSearchView(MenuItem searchMenuItem){
        searchView = (SearchView) searchMenuItem.getActionView();
        ((EditText)searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text))
                .setHintTextColor(Color.WHITE);
        return searchView;
    }
}
