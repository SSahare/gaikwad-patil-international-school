package appsoft.erp.gaikwad.activities.open_results;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import appsoft.erp.gaikwad.R;

public class NewsDetailActivity extends AppCompatActivity {

    private TextView title, publishDate;
    private TextView desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);

        setContentView(R.layout.activity_news);
        ImageButton imageButton=findViewById(R.id.imageButton);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                finishAfterTransition();

            }
            });



        title = (TextView) findViewById(R.id.newsTitle);
        publishDate = (TextView)findViewById(R.id.publishDate);
        desc = findViewById(R.id.description);
        title.setText(getIntent().getStringExtra("title"));
        publishDate.setText(getIntent().getStringExtra("date"));
        Document docs = Jsoup.parse(getIntent().getStringExtra("desc"));
        String description = docs.text();
        desc.setText(description);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
            return true;
        }else {
            return super.onOptionsItemSelected(item);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onSupportNavigateUp() {


        finishAfterTransition();

        return true;
    }

}