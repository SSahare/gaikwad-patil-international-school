package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.SerializedName;

public class LoginModel {

    private boolean logged_in;
    private String username;
    private String user_role;
    @SerializedName("user_id")
    private int user_id;
    private String first_name;
    private String last_name;
    private String profile_photo;
    private String error;
    @SerializedName("first_login")
    private boolean isFirstLogin;

    public boolean isFirstLogin() {
        return isFirstLogin;
    }

    public void setFirstLogin(boolean firstLogin) {
        isFirstLogin = firstLogin;
    }




    // Only Error String


    public boolean isLogged_in() {
        return logged_in;
    }

    public String getUsername() {
        return username;
    }

    public String getUser_role() {
        return user_role;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getProfile_photo() {
        return profile_photo;
    }

    public String getError() {
        return error;
    }

    public LoginModel(boolean logged_in, String username, String user_role, int user_id, String first_name, String last_name, String profile_photo) {
        this.logged_in = logged_in;
        this.username = username;
        this.user_role = user_role;
        this.user_id = user_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.profile_photo = profile_photo;
    }

    public LoginModel(String error, boolean logged_in){
        this.error = error;
        this.logged_in = logged_in;
    }

    @Override
    public String toString() {
        return first_name+" "+last_name+"\n"+user_role;
    }
}
