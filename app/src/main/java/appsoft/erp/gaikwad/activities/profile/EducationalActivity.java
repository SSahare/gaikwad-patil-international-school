package appsoft.erp.gaikwad.activities.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.UserProfileModel;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EducationalActivity extends AppCompatActivity {
    TextView registration,admission,addmissiondate,academicyear;

    private SessionManagement session;
    UserProfileModel model;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.educational_activity);
        session = new SessionManagement(getApplicationContext());

        registration=findViewById(R.id.tv_Registration_detail);
        admission=findViewById(R.id.tv_Addmission_Number);
        addmissiondate=findViewById(R.id.tv_Addmission_Date);
        academicyear=findViewById(R.id.tv_Academic_year);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Educational Detail");
        }
        getDataFromServer();
    }

    private void getDataFromServer() {


        ApiInterface in = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<UserProfileModel> call = in.getProfileData(session.getUserId());
        call.enqueue(new Callback<UserProfileModel>() {
            @Override
            public void onResponse(Call<UserProfileModel> call, Response<UserProfileModel> response) {
                model = response.body();

                setEducationalDetails();;

            }


            @Override
            public void onFailure(Call<UserProfileModel> call, Throwable t) {

                Log.i("DEBUG", "Error Ocurred");
            }
        });
    }

    private void setEducationalDetails() {

        registration.setText(model.getRegistrationNo());
        admission.setText(model.getAdmissionNumber());
        addmissiondate.setText(model.getAdmissionDate());
        academicyear.setText(model.getAcademicYear());

    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}

