package appsoft.erp.gaikwad.activities.activitys;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.leave_manager.ApplyForLeaveFragment;
import appsoft.erp.gaikwad.activities.leave_manager.LeaveStatusFragment;
import appsoft.erp.gaikwad.activities.leave_manager.LeavesHistoryFragment;
import appsoft.erp.gaikwad.activities.leave_manager.RemainingLeavesFragment;
import appsoft.erp.gaikwad.activities.volley_singleton.NetworkStatus;

public class LeaveManagerActivity extends AppCompatActivity implements NetworkStatus{

    private LinearLayout layout;
    private Toolbar toolbar;
    private ApplyForLeaveFragment applyForLeaveFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leave_manager_activity);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        initialiseData();

        applyForLeaveFragment = new ApplyForLeaveFragment();
        getFragmentManager().beginTransaction().add(R.id.leave_frame,applyForLeaveFragment).addToBackStack("BaseLeave").commit();

        if (isNetworkAvailable()){

        }else{
            final Snackbar snackbar = Snackbar.make(layout,getResources().getString(R.string.no_connection),Snackbar.LENGTH_INDEFINITE);
            snackbar.setActionTextColor(getResources().getColor(R.color.colorDarkRedBlood));
            snackbar.setAction("RETRY", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isNetworkAvailable()){
                        snackbar.show();
                    }
                }
            });
            snackbar.show();
        }
    }

    @SuppressLint("RestrictedApi")
    private void initialiseData() {
        layout = findViewById(R.id.leave_layout);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.leave_manager_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.apply){
            toolbar.setTitle("Leave manager");
            getFragmentManager().beginTransaction().add(R.id.leave_frame,new ApplyForLeaveFragment()).commit();
        }else if (id == R.id.remaining_leave){
            toolbar.setTitle("Remaining Leaves");
            getSupportFragmentManager().beginTransaction().add(R.id.leave_frame,new RemainingLeavesFragment()).commit();
        }else if (id == R.id.leave_status){
            toolbar.setTitle("Leave status");
            getSupportFragmentManager().beginTransaction().add(R.id.leave_frame,new LeaveStatusFragment()).commit();
        } else if(id == R.id.leave_history) {
            toolbar.setTitle("Leave History");
            getSupportFragmentManager().beginTransaction().add(R.id.leave_frame,new LeavesHistoryFragment()).commit();
        }else if (id == android.R.id.home){
            this.finish();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count>1){
            Toast.makeText(this, "Press Back One More Time To Exit", Toast.LENGTH_SHORT).show();
            Log.i("TAG",""+getSupportFragmentManager().getBackStackEntryCount());
            getSupportFragmentManager().popBackStack(0,0);
            getFragmentManager().beginTransaction().add(R.id.leave_frame,new ApplyForLeaveFragment()).commit();

        }if (count == 0){
            finish();
        }
        super.onBackPressed();
    }


    @Override
    public boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        return networkInfo!=null;
    }
}
