package appsoft.erp.gaikwad.activities.modals;

public class ExamModel {

    private String srNo;
    private String subject;
    private String day;
    private String date;
    private String time;

    public ExamModel(String srNo, String subject, String day, String date, String time) {
        this.srNo = srNo;
        this.subject = subject;
        this.day = day;
        this.date = date;
        this.time = time;
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
