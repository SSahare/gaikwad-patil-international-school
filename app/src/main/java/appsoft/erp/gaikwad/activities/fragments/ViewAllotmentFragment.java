package appsoft.erp.gaikwad.activities.fragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;

import java.util.ArrayList;
import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.adapters.TeacherDetailAdapter;
import appsoft.erp.gaikwad.activities.extras.Constants;
import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import appsoft.erp.gaikwad.activities.modals.ClassTeacher;
import appsoft.erp.gaikwad.activities.modals.SubjectTeacher;
import appsoft.erp.gaikwad.activities.modals.TeacherDashboardModel;
import appsoft.erp.gaikwad.activities.modals.TeacherDetailCombineModel;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAllotmentFragment extends android.support.v4.app.Fragment {
    RecyclerView dashboard_teacher_detail_recycler;
    private SessionManagement session;
    private static OnTitleSetListener onTitleSetListener = null;
    ProgressDialog progressDoalog;
    private ImageView noDataFound;
    private ConstraintLayout constraintLayout;
    FrameLayout frameLayout;

    public ViewAllotmentFragment(){

    }

    public static ViewAllotmentFragment getInstance(OnTitleSetListener onTitleSetListener1) {
        // Required empty public constructor
        onTitleSetListener = onTitleSetListener1;
        return new ViewAllotmentFragment();
    }


    @Override
    public void onResume() {
        super.onResume();
        setTitle();
    }

    private void setTitle() {
        onTitleSetListener.setOnTitleText("Allotment");
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        final View view=inflater.inflate(R.layout.allotment_layout,container,false);
        constraintLayout = view.findViewById(R.id.no_internet_layout);
//        frameLayout = view.findViewById(R.id.constraint_layout);
        noDataFound = view.findViewById(R.id.no_data_found_image);
        Button tryAgain = view.findViewById(R.id.try_again);
     dashboard_teacher_detail_recycler=view.findViewById(R.id.dashboard_teacher_detail_recycler);
        session = new SessionManagement(getContext());




     if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER))

     {
         getTeacherDetail(view);
     }
        return view;
    }


    

    private void getTeacherDetail(View view) {
            allotmentprogressbar();
        final List<TeacherDetailCombineModel> detail = new ArrayList<>();
        ApiInterface teacherApi = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<TeacherDashboardModel> call = teacherApi.getTeacherDetailDashboard(session.getUserId());
        call.enqueue(new Callback<TeacherDashboardModel>() {
            @Override
            public void onResponse(Call<TeacherDashboardModel> call, Response<TeacherDashboardModel> response) {

                TeacherDashboardModel teacherDashboardModel = response.body();
                try {
                    ClassTeacher classTeacher = teacherDashboardModel.getClassTeacher();
                    List<SubjectTeacher> subjectTeacher = teacherDashboardModel.getSubjectTeacher();
                    if (classTeacher.getResponse()){
                        detail.add(new TeacherDetailCombineModel(classTeacher.getClassAssigned(),
                                classTeacher.getSectionAssigned(),
                                "-",
                                "C . T"));
                    }
                    if (subjectTeacher.size() != 0){
                        for (SubjectTeacher data : subjectTeacher){
                            detail.add(new TeacherDetailCombineModel(data.getClass_(),
                                    data.getSection(),
                                    data.getSubject(),
                                    "S . T"));
                        }
                    }
                }

                catch (NullPointerException npe){
                    Log.i("DEBUG", "NPE Called");
                }catch (IllegalArgumentException iae){
                    Log.i("DEBUG", "Illegal Called");
                }catch (Exception exp){
                    Log.i("DEBUG", "Exception Called");
                }
                progressDoalog.dismiss();

                if (detail != null){


                    dashboard_teacher_detail_recycler.setHasFixedSize(true);
                    dashboard_teacher_detail_recycler.setLayoutManager(new LinearLayoutManager(getContext()));
                    try {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        }
                    }catch (NullPointerException npe){

                    }catch (Exception exp){

                    }

                    TeacherDetailAdapter adapter = new TeacherDetailAdapter(detail);
                    dashboard_teacher_detail_recycler.setAdapter(adapter);

                }
                progressDoalog.dismiss();

            }

            @Override
            public void onFailure(Call<TeacherDashboardModel> call, Throwable t) {

                Toast.makeText(getContext(), "Oops, Fail Teacher Detail", Toast.LENGTH_SHORT).show();
                progressDoalog.dismiss();
            }
        });


    }

    private void allotmentprogressbar() {

        progressDoalog = new ProgressDialog(getActivity());
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
        progressDoalog.setMessage("Please Wait...");
        progressDoalog.setIndeterminate(true);
        Circle wave=new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();
    }
}

