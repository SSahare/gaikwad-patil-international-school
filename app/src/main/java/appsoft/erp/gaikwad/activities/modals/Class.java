package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.SerializedName;

public class Class {
    @SerializedName("class")
    private String classes;

    public Class(String classes){
        this.classes = classes;
    }

    public String getClasses() {
        return classes;
    }
}
