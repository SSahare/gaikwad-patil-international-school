package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Log {

    @SerializedName("sr_no")
    @Expose
    private String srNo;
    @SerializedName("teacher_id")
    @Expose
    private String teacherId;
    @SerializedName("log_date")
    @Expose
    private String logDate;
    @SerializedName("log_class")
    @Expose
    private String logClass;
    @SerializedName("log_section")
    @Expose
    private String logSection;
    @SerializedName("log_subject")
    @Expose
    private String logSubject;
    @SerializedName("log_description")
    @Expose
    private String logDescription;

    /*
        "sr_no": "1",
        "teacher_id": "10005",
        "log_date": "2018-07-03",
        "log_class": "4th",
        "log_section": "A",
        "log_subject": "English",
        "log_description": "Quisque velit nisi, pretium ut lacinia in, elementum id enim."
     */

    public Log(String logDate, String classname, String section, String description, String logSubject) {
        this.logDate = logDate;
        this.logClass = classname;
        this.logSection = section;
        this.logDescription = description;
        this.logSubject = logSubject;
    }

    public Log(String logDate, String description, String logSubject) {
        this.logDate = logDate;
        this.logDescription = description;
        this.logSubject = logSubject;
    }

    public Log() {
    }

    public String getLogDate() {

        String outputDateStr = "";
        try {
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outputFormat = new SimpleDateFormat("MMM-dd-yyyy");
            java.util.Date date = inputFormat.parse(logDate);
            outputDateStr = outputFormat.format(date);
        }catch (Exception exc){
            return logDate;
        }
        return outputDateStr;

    }

    public void setLogDate(String logDate) {
        this.logDate = logDate;
    }

    public String getClassname() {
        return logClass;
    }

    public void setClassname(String classname) {
        this.logClass = classname;
    }

    public String getSection() {
        return logSection;
    }

    public void setSection(String section) {
        this.logSection = section;
    }

    public String getDescription() {
        return logDescription;
    }

    public void setDescription(String description) {
        this.logDescription = description;
    }

    public String getLogSubject() {
        return logSubject;
    }

    public void setLogSubject(String logSubject) {
        this.logSubject = logSubject;
    }
}
