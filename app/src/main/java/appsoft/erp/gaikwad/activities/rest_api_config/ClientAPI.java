package appsoft.erp.gaikwad.activities.rest_api_config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;

import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.TlsVersion;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ClientAPI {


    private static final String BASE_URL ="https://www.gpis-erp.in/app_rest/";
    private static final String USERNAME = "gpiserp2019";
    private static final String PASSWORD = "Kq.k-Va+h$73n~).";

    private static Retrofit retrofit = null;
    private static Gson gson = new GsonBuilder().setLenient().create();

    public static Retrofit getRetrofit() {
        SSLContext mSSLContext = null;
        try {
            mSSLContext = SSLContext.getInstance("TLSv1.2");
            mSSLContext.init(null, null, null);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                .tlsVersions(TlsVersion.TLS_1_2)
                .build();

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new BasicAuthInterceptor(USERNAME, PASSWORD))
                .connectTimeout(20000, TimeUnit.SECONDS)
                .readTimeout(20000, TimeUnit.SECONDS)
                .writeTimeout(20000, TimeUnit.SECONDS)
                .sslSocketFactory(new Tls12SocketFactory(mSSLContext.getSocketFactory()), new TlsX509TrustManager())
                .connectionSpecs(Collections.singletonList(spec))
                .build();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;
    }
}
