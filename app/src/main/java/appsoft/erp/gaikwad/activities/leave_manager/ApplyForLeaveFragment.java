package appsoft.erp.gaikwad.activities.leave_manager;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.extras.DatePickerFragment;

public class ApplyForLeaveFragment extends Fragment{

    private EditText reason;
    private TextView fromDate, toDate;
    private Spinner leaveType;
    private Button apply;
    private static int toggleDateFlag = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.apply_for_leave, container, false);
        fromDate = view.findViewById(R.id.dateFrom);
        toDate = view.findViewById(R.id.dateTo);
        leaveType = view.findViewById(R.id.leave_type);
        apply = view.findViewById(R.id.buttonApply);
        reason = view.findViewById(R.id.reasonForLeave);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(getActivity(), R.array.leave_types, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.spinner_list);
        leaveType.setAdapter(adapter);

        fromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callDatePickerFrom(view);
            }
        });

        toDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callDatePickerTo(view);
            }
        });
        return view;
    }

    private void callDatePickerFrom(View view){
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.show(getFragmentManager(), "datePicker");
        //fromDate.setText(fragment.getDateString());
    }

    private void callDatePickerTo(View view){
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.show(getFragmentManager(), "datePicker2");
        //toDate.setText(fragment.getDateString());
    }
    /*@Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        Calendar calendar = new GregorianCalendar(year,month,day);
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.ENGLISH);

        fromDate.setText(dateFormat.format(calendar.getTime()));
    }*/
}
