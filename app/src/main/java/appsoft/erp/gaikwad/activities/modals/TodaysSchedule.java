package appsoft.erp.gaikwad.activities.modals;

public class TodaysSchedule {

    private String ttclass;
    private String section;
    private String time;
    private String subject;

    public TodaysSchedule(String ttclass, String section, String time, String subject) {
        this.ttclass = ttclass;
        this.section = section;
        this.time = time;
        this.subject = subject;
    }

    public String getTtclass() {
        return ttclass;
    }

    public void setTtclass(String ttclass) {
        this.ttclass = ttclass;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

}
