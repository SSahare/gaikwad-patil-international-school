package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeesRecord {

    @SerializedName("transportation_fees")
    @Expose
    private String transportationFees;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("student_id")
    @Expose
    private String studentId;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("academic_year")
    @Expose
    private String academicYear;
    @SerializedName("cashier_id")
    @Expose
    private String cashierId;
    @SerializedName("fees_amount")
    @Expose
    private String feesAmount;
    @SerializedName("installment_no")
    @Expose
    private String installmentNo;
    @SerializedName("payment_mode")
    @Expose
    private String paymentMode;
    @SerializedName("transaction_no")
    @Expose
    private String transactionNo;
    @SerializedName("reciept_no")
    @Expose
    private String recieptNo;
    @SerializedName("fee_type")
    @Expose
    private String feeType;
    @SerializedName("admission_fees")
    @Expose
    private String admissionFees;
    @SerializedName("development_fees")
    @Expose
    private String developmentFees;
    @SerializedName("security_deposit")
    @Expose
    private String securityDeposit;
    @SerializedName("tution_fees")
    @Expose
    private String tutionFees;
    @SerializedName("term_fees")
    @Expose
    private String termFees;
    @SerializedName("sports_fees")
    @Expose
    private String sportsFees;
    @SerializedName("activity_fees")
    @Expose
    private String activityFees;
    @SerializedName("library_fees")
    @Expose
    private String libraryFees;
    @SerializedName("computer_fees")
    @Expose
    private String computerFees;
    @SerializedName("elearning")
    @Expose
    private String elearning;
    @SerializedName("pdf_file")
    @Expose
    private String pdfFile;

    public FeesRecord(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    public String getCashierId() {
        return cashierId;
    }

    public void setCashierId(String cashierId) {
        this.cashierId = cashierId;
    }

    public String getFeesAmount() {
        return feesAmount;
    }

    public void setFeesAmount(String feesAmount) {
        this.feesAmount = feesAmount;
    }

    public String getInstallmentNo() {
        return installmentNo;
    }

    public void setInstallmentNo(String installmentNo) {
        this.installmentNo = installmentNo;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public String getRecieptNo() {
        return recieptNo;
    }

    public void setRecieptNo(String recieptNo) {
        this.recieptNo = recieptNo;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getAdmissionFees() {
        return admissionFees;
    }

    public void setAdmissionFees(String admissionFees) {
        this.admissionFees = admissionFees;
    }

    public String getDevelopmentFees() {
        return developmentFees;
    }

    public void setDevelopmentFees(String developmentFees) {
        this.developmentFees = developmentFees;
    }

    public String getSecurityDeposit() {
        return securityDeposit;
    }

    public void setSecurityDeposit(String securityDeposit) {
        this.securityDeposit = securityDeposit;
    }

    public String getTutionFees() {
        return tutionFees;
    }

    public void setTutionFees(String tutionFees) {
        this.tutionFees = tutionFees;
    }

    public String getTermFees() {
        return termFees;
    }

    public void setTermFees(String termFees) {
        this.termFees = termFees;
    }

    public String getSportsFees() {
        return sportsFees;
    }

    public void setSportsFees(String sportsFees) {
        this.sportsFees = sportsFees;
    }

    public String getActivityFees() {
        return activityFees;
    }

    public void setActivityFees(String activityFees) {
        this.activityFees = activityFees;
    }

    public String getLibraryFees() {
        return libraryFees;
    }

    public void setLibraryFees(String libraryFees) {
        this.libraryFees = libraryFees;
    }

    public String getComputerFees() {
        return computerFees;
    }

    public void setComputerFees(String computerFees) {
        this.computerFees = computerFees;
    }

    public String getElearning() {
        return elearning;
    }

    public void setElearning(String elearning) {
        this.elearning = elearning;
    }

    public String getTransportationFees() {
        return transportationFees;
    }

    public void setTransportationFees(String transportationFees) {
        this.transportationFees = transportationFees;
    }


    public String getPdfFile() {
        if(pdfFile == null){
            return "";
        }
        return pdfFile;
    }

    public void setPdfFile(String pdfFile) {
        this.pdfFile = pdfFile;
    }
}
