package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Circular {

    @SerializedName("sr_no")
    private String serial_no;

    @SerializedName("title")
    private String circularTitle;

    @SerializedName("date")
    private String dateOfPublish;

    @SerializedName("content")
    private String circularDescription;

    public Circular(String serial_no, String circularTitle, String dateOfPublish, String circularDescription) {
        this.serial_no = serial_no;
        this.circularTitle = circularTitle;
        this.dateOfPublish = dateOfPublish;
        this.circularDescription = circularDescription;
    }

    public String getSerial_no(){ return serial_no;}

    public String getCircularTitle() {
        return circularTitle;
    }

    public String getDateOfPublish() {


        String outputDateStr = "";
        try {
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outputFormat = new SimpleDateFormat("MMM-dd-yyyy");
            Date date = inputFormat.parse(dateOfPublish);
            outputDateStr = outputFormat.format(date);
        }catch (Exception exc){
            return dateOfPublish;
        }
        return outputDateStr;

    }

    public String getCircularDescription() {
        return circularDescription;
    }
}
