package appsoft.erp.gaikwad.activities.activitys;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.divider.SimpleItemDecoration;
import appsoft.erp.gaikwad.activities.extras.DatePickerFragment;
import appsoft.erp.gaikwad.activities.extras.TodaysDate;
import appsoft.erp.gaikwad.activities.modals.SelectableStudents;
import appsoft.erp.gaikwad.activities.modals.Students;
import appsoft.erp.gaikwad.activities.modals.StudentsName;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import appsoft.erp.gaikwad.activities.volley_singleton.NetworkStatus;

public class AttendenceActivity extends AppCompatActivity implements NetworkStatus,
        DatePickerDialog.OnDateSetListener,
        AssignAlertDialogFragment.ResponseHandler,
        AttendanceHolder.OnItemSelectedListener{

    ProgressDialog progressDoalog;

    private RecyclerView recyclerView;
    private CoordinatorLayout layout;
    private SessionManagement session;
    private Students studentsList = null;
    List<StudentsName> selectedStudItems = new ArrayList<>();
    StudentsAttendanceAdapter adapter;
    private TextView class_info;
    private RecyclerView.LayoutManager mManager;
    private String classNameStr;
    private String sectionNameStr;
    private TextView todaysdate;
    List<StudentsName> list;

    public AttendanceHolder.OnItemSelectedListener mListerner = new AttendanceHolder.OnItemSelectedListener() {
        @Override
        public void onItemSelected(SelectableStudents item) {
                selectedStudItems.add(item);
                selectedStudItems = adapter.getSelectedItems();
                //Snackbar.make(recyclerView,"Total "+selectedItems.size()+" Labours are selected.",Snackbar.LENGTH_LONG).show();
        }
    };

    @Override
    public void onItemSelected(SelectableStudents item) {

    }

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attendance_layout);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        session = new SessionManagement(getApplicationContext());

        initialiseViews();

        if (!isNetworkAvailable()){
            final Snackbar snackbar = Snackbar.make(layout,getResources().getString(R.string.no_connection),Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(getResources().getColor(R.color.colorDarkRedBlood));
                    snackbar.setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!isNetworkAvailable()){
                                snackbar.show();
                            }
                        }
                    });
            snackbar.show();
        }else{
            loadStudents();
        }
    }

    @SuppressLint("RestrictedApi")
    private void initialiseViews() {

       todaysdate=findViewById(R.id.todaysDate);
        recyclerView = findViewById(R.id.studentsRecycler);
        layout = findViewById(R.id.attendanceLayout);
        class_info=findViewById(R.id.tv_class);
        recyclerView.setHasFixedSize(true);
        mManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mManager);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            recyclerView.addItemDecoration(new SimpleItemDecoration(this));

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        todaysdate.setText(TodaysDate.getTodaysDate());
        todaysdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callDatePicker(view);
            }
        });
    }

    private void callDatePicker(View view){
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.show(getFragmentManager(),"DatePick");
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        Calendar calendar = new GregorianCalendar(year,month,day);
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.ENGLISH);
        todaysdate.setText(dateFormat.format(calendar.getTime()));
    }

    public void saveAttendance(View view){

        AssignAlertDialogFragment dialogFragment = AssignAlertDialogFragment.newInstance();
        Bundle b = new Bundle();
        int listSize = 0;
        selectedStudItems = adapter.getSelectedItems();
        try {
            listSize = selectedStudItems.size();
        }catch (NullPointerException npe){
            Log.i("DEBUG", "Oops, no manpower selected");
        }catch (Exception ex){
            Log.i("DEBUG", "Exception Occurs");
        }
        b.putInt("size", listSize);
        b.putString("className", classNameStr);
        b.putString("sectionName", sectionNameStr);
        b.putParcelableArrayList("student_list", (ArrayList<? extends Parcelable>) selectedStudItems);
        b.putString("date", todaysdate.getText().toString());
        dialogFragment.setArguments(b);
        dialogFragment.show(getSupportFragmentManager(), "Assign Alert");
    }

    private boolean isAllFieldFill() {
        if (todaysdate.getText().toString().isEmpty()){
            return false;
        }else {
            return true;
        }
    }

    @Override
    public boolean isNetworkAvailable() {
        ConnectivityManager conMgr =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        return netInfo != null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
            return true;
        }else{
//            return super.onOptionsItemSelected(item);
            return false;
        }
    }

    public void loadStudents(){
        attendenceProgressBar();
        ApiInterface stuInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<Students> call = stuInterface.getAttendanceList(session.getUserId());
        call.enqueue(new Callback<Students>() {
            @Override
            public void onResponse(Call<Students> call, retrofit2.Response<Students> response) {

                try{
                studentsList = response.body();
                classNameStr = studentsList.getClass_();
                sectionNameStr = studentsList.getSection();

                class_info.setText("Class: "+classNameStr+" ("+sectionNameStr+")");
                list = studentsList.getStudentsName();
                //add all students added to selected stud
                //selectedStudItems.addAll(list);
                adapter = new StudentsAttendanceAdapter(mListerner, list, true);
                recyclerView.setAdapter(adapter);
                }catch (Exception exc){
                    Toast.makeText(AttendenceActivity.this, "Internal Failure", Toast.LENGTH_SHORT).show();
                }
                progressDoalog.dismiss();

            }

            @Override
            public void onFailure(Call<Students> call, Throwable t) {

                showPopUp();
                Log.i("DEBUG", "Failure Occur: "+t.toString());
                progressDoalog.dismiss();
            }
        });
    }

    private void attendenceProgressBar() {
        progressDoalog = new ProgressDialog(this);
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
        progressDoalog.setMessage("Please Wait...");
//        progressDoalog.getWindow().setGravity(LinearLayout.LayoutParams.MATCH_PARENT LinearLayout.LayoutParams.MATCH_PARENT);
        progressDoalog.setIndeterminate(true);
        Circle wave=new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();
    }

    private void showPopUp(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Suggestion");
        builder.setCancelable(false);
        builder.setMessage("Sorry, You Are Not Class Teacher\nCan't take attendance.");
        builder.setNeutralButton("Go Back", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                dialog.dismiss();
            }
        });
        Dialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void getResponseString(String response) {
        Toast toast = Toast.makeText(this, response, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0,0);
        toast.show();
    }
}