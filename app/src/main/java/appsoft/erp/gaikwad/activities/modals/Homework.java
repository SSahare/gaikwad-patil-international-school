package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import appsoft.erp.gaikwad.activities.date_util.DateFormatter;

public class Homework {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("teacher_id")
    @Expose
    private String teacherId;
    @SerializedName("homework_class")
    @Expose
    private String homeworkClass;
    @SerializedName("section")
    @Expose
    private String section;
    @SerializedName("given_date")
    @Expose
    private String givenDate;
    @SerializedName("submission_date")
    @Expose
    private String submissionDate;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;

    public Homework (){ }

    public Homework(String classname, String section, String subject, String description, String title, String dateOfGiven, String dateOfSubmission) {
        this.homeworkClass = classname;
        this.section = section;
        this.subject = subject;
        this.description = description;
        this.title = title;
        this.givenDate = dateOfGiven;
        this.submissionDate = dateOfSubmission;
    }

    public Homework(String subject, String description, String title, String dateOfGiven, String dateOfSubmission) {
        this.subject = subject;
        this.description = description;
        this.title = title;
        this.givenDate = dateOfGiven;
        this.submissionDate = dateOfSubmission;
    }

    public String getClassname() {
        return homeworkClass;
    }

    public void setClassname(String classname) {
        this.homeworkClass = classname;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateOfGiven(){
        String formattedDate = DateFormatter.getStandardDateFormat(givenDate);
        return formattedDate;
    }

    public void setDateOfGiven(String dateOfGiven) {
        this.givenDate = dateOfGiven;
    }

    public String getDateOfSubmission() {

        String formattedDate = DateFormatter.getStandardDateFormat(submissionDate);
        return formattedDate;
    }

    public void setDateOfSubmission(String dateOfSubmission) {
        this.submissionDate = dateOfSubmission;
    }
}
