package appsoft.erp.gaikwad.activities.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.Leave;

public class LeaveAdapter extends RecyclerView.Adapter<LeaveAdapter.LeaveHolder>{

    private List<Leave> leaveList = null;
    Leave leaveObj = null;

    public LeaveAdapter(List<Leave> leaveList){
        this.leaveList = leaveList;
    }

    @Override
    public LeaveHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.leave_status_card, parent, false);
        return new LeaveHolder(view);
    }

    @Override
    public void onBindViewHolder(LeaveHolder holder, int position) {

        leaveObj = leaveList.get(position);
        holder.leaveType.setText(leaveObj.getLeaveType());
        if (leaveObj.getStatus().equals("Pending")){
            holder.leaveStatus.setBackgroundColor(Color.YELLOW);
//            holder.leaveStatus.setBackgroundColor(R.drawable.registerbutton);

            holder.leaveStatus.setText(leaveObj.getStatus());
        }
       if(leaveObj.getStatus().equals("Approved")){
            holder.leaveStatus.setBackgroundColor(Color.GREEN);
           holder.leaveStatus.setText(leaveObj.getStatus());

        }
        if (leaveObj.getStatus().equals("Reject")){
            holder.leaveStatus.setBackgroundColor(Color.RED);

            holder.leaveStatus.setText(leaveObj.getStatus());

        }
        holder.reason.setText(leaveObj.getReason());
        holder.dateText.setText("From "+leaveObj.getDateFrom()+" "+" To "+leaveObj.getDateTo());
        holder.leaveDays.setText(leaveObj.getNoOfDays()+" Days");



    }

    @Override
    public int getItemCount() {
        return leaveList.size();
    }

    class LeaveHolder extends RecyclerView.ViewHolder{

        private TextView leaveStatus, leaveType, dateText, reason, leaveDays;

        public LeaveHolder(View itemView) {
            super(itemView);

            leaveType = itemView.findViewById(R.id.leaveType);
            leaveStatus = itemView.findViewById(R.id.leaveStatus);
            dateText = itemView.findViewById(R.id.toAndFromDate);
            reason = itemView.findViewById(R.id.reasonForLeave);
            leaveDays = itemView.findViewById(R.id.numberOfDays);
        }
    }
}
