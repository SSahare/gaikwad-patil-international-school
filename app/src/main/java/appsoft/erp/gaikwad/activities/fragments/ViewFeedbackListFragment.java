package appsoft.erp.gaikwad.activities.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;

import java.util.ArrayList;
import java.util.List;

import appsoft.erp.gaikwad.activities.activitys.FeedbackActivity;
import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import appsoft.erp.gaikwad.activities.network_broadcast.ConnectionDetector;
import retrofit2.Call;
import retrofit2.Callback;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.adapters.FeedbackAdapter;
import appsoft.erp.gaikwad.activities.extras.Constants;
import appsoft.erp.gaikwad.activities.modals.Feedback;
import appsoft.erp.gaikwad.activities.parent_adapters.ParentFeedbackAdapter;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.search_package.SearchColor;
import appsoft.erp.gaikwad.activities.session.SessionManagement;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewFeedbackListFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager manager;
    private List<Feedback> feedbackList = null;
    private SearchView searchView;
    private ParentFeedbackAdapter pAdapter;
    private FeedbackAdapter fAdapter;
    ProgressDialog progressDoalog;
    Animation uptodown;
    ConstraintLayout constraintLayout ;
    private ImageView dataNotFound;
    private ImageView noDataFound;

    SwipeRefreshLayout swipeRefreshLayout;
    private SessionManagement session;
    private static OnTitleSetListener onTitleSetListener = null;
    private LinearLayout dropDownLayout,dateTextLayout;

    public ViewFeedbackListFragment() { }
    public static ViewFeedbackListFragment getInstance(OnTitleSetListener onTitleSetListener1) {
        // Required empty public constructor
        onTitleSetListener = onTitleSetListener1;
        return new ViewFeedbackListFragment();
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Override
    public void onResume() {
        super.onResume();
        setTitle();
    }
    private void setTitle() {
        onTitleSetListener.setOnTitleText("Feedbacks");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.view_feedback_list_fragment, container, false);
//        int resId = R.anim.layout_animation_fall_down;
        session = new SessionManagement(getContext());
        recyclerView = view.findViewById(R.id.feedbackRecycler);
        recyclerView.setHasFixedSize(true);
        constraintLayout = view.findViewById(R.id.no_internet_layout);
        dropDownLayout = view.findViewById(R.id.dropdown_layout);
        noDataFound =view.findViewById(R.id.no_data_found_image);
        dataNotFound = view.findViewById(R.id.no_data_found_image);
        Button tryAgain = view.findViewById(R.id.try_again);
        uptodown = AnimationUtils.loadAnimation(getContext(), R.anim.uptodown);
        recyclerView.setAnimation(uptodown);
        manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        swipeRefreshLayout=view.findViewById(R.id.swiperefreshlayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_PARENT)){
                    getFeedbackFromServer();
                    swipeRefreshLayout.setRefreshing(false);
                }
                if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER)){
                    getFeedbackFromServer();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });


        if (ConnectionDetector.isConnected(getContext())){
            viewsWhenConnected();
            getFeedbackFromServer();
        }
        else {
            viewsWhenNotConnected();
        }

        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConnectionDetector.isConnected(getContext())){
                    getFeedbackFromServer();
                    viewsWhenConnected();
                }
            }
        });


        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search_menu, menu);
        final MenuItem searchMenuItem = menu.findItem(R.id.isync_search);
        SearchColor.changeSearchViewColor(searchView);
        searchView = SearchColor.getSearchView(searchMenuItem);
        if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
            final MenuItem additem = menu.findItem(R.id.isync_add);
            additem.setVisible(true);
            additem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    startActivity(new Intent(getActivity(), FeedbackActivity.class));
                    return false;
                }
            });
        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }

                searchMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                try {
                    final List<Feedback> list = filter(feedbackList, newText);
                    if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_PARENT)) {
                        pAdapter.filteredList(list);
                    }
                    if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
                        fAdapter.filteredList(list);
                    }
                }catch (NullPointerException npe){ }
                return false;
            }
        });
    }

    private List<Feedback> filter(List<Feedback> list, String query) {

        query = query.toLowerCase();
        final List<Feedback> fList = new ArrayList<>();
        try {
            for (Feedback obj : list) {
                final String dateText = obj.getDate().toLowerCase();
                final String firstname = obj.getFirst_name().toLowerCase();

                if (dateText.contains(query)||firstname.contains(query)) {
                    fList.add(obj);
                }
            }
            return fList;
        }catch (NullPointerException npe){

        }
        return null;
    }

    public void getFeedbackFromServer() {
        feedbackprogressdialog();
        ApiInterface apiInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<Feedback>> call;
        if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_PARENT)){
            call = apiInterface.getStudentFeedbackList(session.getUserId());
        }else{
            call = apiInterface.getFeedbackList(session.getUserId());
        }
        android.util.Log.i("DEBUG" , "ID : "+session.getUserId());
        call.enqueue(new Callback<List<Feedback>>() {
            @Override
            public void onResponse(Call<List<Feedback>> call, retrofit2.Response<List<Feedback>> response) {
                if (response.isSuccessful()){
                    feedbackList = response.body();
                    if (feedbackList== null ||feedbackList.size()==0){
                        viewsNoDataFound();                    }
                    try{
                        if (feedbackList != null) {
                            if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_PARENT)) {
                                pAdapter = new ParentFeedbackAdapter(getContext(),feedbackList);
                                recyclerView.setAdapter(pAdapter);
                                progressDoalog.dismiss();
                            }
                            if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
                                fAdapter = new FeedbackAdapter(getContext(), feedbackList);
                                recyclerView.setAdapter(fAdapter);
                                progressDoalog.dismiss();
                            }
                        } else {
                            Toast.makeText(getContext(), R.string.list_empty, Toast.LENGTH_SHORT).show();
                        }
                    }catch (NullPointerException npe){
                        android.util.Log.i("DEBUG", "Feedback Exception: "+npe.toString());
                    }
                    progressDoalog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<Feedback>> call, Throwable t) {
                android.util.Log.i("DEBUG", "Feedback Exception: "+t.toString());
                progressDoalog.dismiss();

            }
        });
    }



    private  void  viewsWhenConnected(){
        recyclerView.setVisibility(View.VISIBLE);
        constraintLayout.setVisibility(View.GONE);
        dataNotFound.setVisibility(View.GONE);

    }

    private void viewsWhenNotConnected() {

        recyclerView.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.VISIBLE);
        dataNotFound.setVisibility(View.GONE);
    }
    private void viewsNoDataFound() {
        recyclerView.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.GONE);
        dataNotFound.setVisibility(View.VISIBLE);
    }



    private void feedbackprogressdialog() {


        progressDoalog = new ProgressDialog(getActivity());
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.setMessage("Please Wait...");

        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
//        progressDoalog.getWindow().setGravity(LinearLayout.LayoutParams.MATCH_PARENT LinearLayout.LayoutParams.MATCH_PARENT);
        progressDoalog.setIndeterminate(true);
        Circle wave=new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();
    }
}