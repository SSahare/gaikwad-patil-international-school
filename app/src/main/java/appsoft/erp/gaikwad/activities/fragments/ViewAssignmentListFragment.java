package appsoft.erp.gaikwad.activities.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

import appsoft.erp.gaikwad.activities.activitys.AddAssignmentActivity;
import appsoft.erp.gaikwad.activities.date_util.DateFormatter;
import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import appsoft.erp.gaikwad.activities.network_broadcast.ConnectionDetector;
import retrofit2.Call;
import retrofit2.Callback;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.adapters.AssignmentAdapter;
import appsoft.erp.gaikwad.activities.extras.Constants;
import appsoft.erp.gaikwad.activities.modals.Assignments;
import appsoft.erp.gaikwad.activities.parent_adapters.ParentAssignmentAdapter;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.search_package.SearchColor;
import appsoft.erp.gaikwad.activities.session.SessionManagement;

import static appsoft.erp.gaikwad.activities.extras.Constants.LOGIN_TYPE_AS_PARENT;
import static appsoft.erp.gaikwad.activities.extras.Constants.LOGIN_TYPE_AS_TEACHER;

public class ViewAssignmentListFragment extends Fragment implements View.OnClickListener{

    private RecyclerView.LayoutManager assignmentLayoutManager;
    private RecyclerView recyclerView;
    private static List<Assignments> assignmentsList = new ArrayList<>();
    private List<Assignments> fList = new ArrayList<>();
    private SearchView searchView;
    private static ParentAssignmentAdapter pAdapter;
    ProgressDialog progressDoalog;
    Animation uptodown, downtoup;
    private static AssignmentAdapter aAdapter;
    private SessionManagement session;
    private AppCompatSpinner spinner;
    private static TextView dateFilterText;
    private ImageButton closeDate;
    private ImageButton closeSubject;
    SwipeRefreshLayout swipeRefreshLayout;
    private static String userRole = "";
    int refresh_count=0;
    ConstraintLayout constraintLayout;
    Button tryAgain;
    private ImageView noDataFound;
    private LinearLayout dropDownLayout,dateTextLayout;
    private static OnTitleSetListener onTitleSetListener = null;
    public ViewAssignmentListFragment() { }

    public static ViewAssignmentListFragment getInstance(OnTitleSetListener onTitleSetListener1) {
        // Required empty public constructor
        onTitleSetListener = onTitleSetListener1;
        return new ViewAssignmentListFragment();
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Override
    public void onResume() {
        super.onResume();
        setTitle();
    }

    private void setTitle() {
        onTitleSetListener.setOnTitleText("Assignments");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View assignmentView = inflater.inflate(R.layout.view_assignment_list_fragment, container, false);

        uptodown = AnimationUtils.loadAnimation(getContext(), R.anim.uptodown);
//        int resId = R.anim.layout_animation_fall_down;
        session = new SessionManagement(getContext());
        userRole = session.getUserRole();
        recyclerView = assignmentView.findViewById(R.id.assignmentRecycler);
        recyclerView.setHasFixedSize(true);
        assignmentLayoutManager = new LinearLayoutManager(getContext());
        spinner = assignmentView.findViewById(R.id.spinnerSubjectFilter);
        swipeRefreshLayout=assignmentView.findViewById(R.id.swiperefreshlayout);

        closeDate = assignmentView.findViewById(R.id.close_date);
//        closeSubject = assignmentView.findViewById(R.id.close_subject);
        dateFilterText = assignmentView.findViewById(R.id.dateFilterText);
        dateFilterText.setOnClickListener(this);
        recyclerView.setLayoutManager(assignmentLayoutManager);
        recyclerView.setAnimation(uptodown);
        dateTextLayout= assignmentView.findViewById(R.id.date_layout);
        noDataFound =assignmentView.findViewById(R.id.no_data_found_image);
        constraintLayout = assignmentView.findViewById(R.id.no_internet_layout);
        dropDownLayout = assignmentView.findViewById(R.id.dropdown_layout);
        tryAgain = assignmentView.findViewById(R.id.try_again);


       swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
           @Override
           public void onRefresh() {
               Log.i("DEBUG", "onRefresh called from SwipeRefreshLayout");

               if (session.getUserRole().equals(LOGIN_TYPE_AS_PARENT)) {
                   pAdapter = new ParentAssignmentAdapter(assignmentsList, getContext());
                   recyclerView.setAdapter(pAdapter);
                   swipeRefreshLayout.setRefreshing(false);

               }
               if (session.getUserRole().equals(LOGIN_TYPE_AS_TEACHER)){
                   aAdapter=new AssignmentAdapter(assignmentsList,getContext());
                   recyclerView.setAdapter(aAdapter);
                   getAssignmentListFromServer();
                   swipeRefreshLayout.setRefreshing(false);

               }
           }
       });

       if (ConnectionDetector.isConnected(getContext())){
            viewsWhenConnected();
           getAssignmentListFromServer();
       }
       else {
           viewsWhenNotConnected();
       }

       tryAgain.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               if (ConnectionDetector.isConnected(getContext())){
                   getAssignmentListFromServer();
                   viewsWhenConnected();
               }
           }
       });



        closeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateFilterText.setText("Date");
                if (userRole.equals(Constants.LOGIN_TYPE_AS_PARENT)) {
                    pAdapter.filteredList(assignmentsList);
                }
                if (userRole.equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
                    aAdapter.filteredList(assignmentsList);
                }
            }
        });

       /* closeSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                spinner.setSelection(0);
                if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_PARENT)) {
                    pAdapter.filteredList(assignmentsList);
                }
                if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
                    aAdapter.filteredList(assignmentsList);
                }
            }
        });*/
        return assignmentView;
    }



    private void viewsWhenConnected() {
        recyclerView.setVisibility(View.VISIBLE);
        constraintLayout.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);
        dateTextLayout.setVisibility(View.VISIBLE);
        dropDownLayout.setVisibility(View.VISIBLE);

    }

    private void viewsWhenNotConnected() {
        recyclerView.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.VISIBLE);
        noDataFound.setVisibility(View.GONE);
        dateTextLayout.setVisibility(View.GONE);
        dropDownLayout.setVisibility(View.GONE);
    }

    private void viewsNoDataFound() {
        recyclerView.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.GONE);
        noDataFound.setVisibility(View.VISIBLE);
        dateTextLayout.setVisibility(View.GONE);
        dropDownLayout.setVisibility(View.GONE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search_menu, menu);
        final MenuItem searchMenuItem = menu.findItem(R.id.isync_search);
        SearchColor.changeSearchViewColor(searchView);
        searchView = SearchColor.getSearchView(searchMenuItem);

        if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
            final MenuItem additem = menu.findItem(R.id.isync_add);
            additem.setVisible(true);
            additem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    startActivity(new Intent(getActivity(), AddAssignmentActivity.class));
                    return true;
                }
            });
        }



        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()){
                    searchView.setIconified(true);
                }
                Log.i("DEBUG", "Assign Submit Query: "+query+" "+query.length());
                searchMenuItem.collapseActionView();
                return false;
            }


            @Override
            public boolean onQueryTextChange(String newText){
                final List<Assignments> aList = filter(assignmentsList, newText);

                try {

                    if (session.getUserRole().equals(LOGIN_TYPE_AS_PARENT)) {
                        pAdapter.filteredList(aList);
                    }

                    if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
                        aAdapter.filteredList(aList);
                    }
                }catch (NullPointerException npe){
                    Log.i("DEBUG", "Empty List");
                }
                Log.i("vishal.rana.tp", "Assign filter list Size: "+aList.size());
                return false;
            }
        });
    }

    private List<Assignments> filter(List<Assignments> list, String query){
        query = query.toLowerCase();
        Log.i("vishal.rana.tp", "What is Query: "+query);
        final List<Assignments> filteredList = new ArrayList<>();
        try{
            for (Assignments obj: list){
                final String titleText = obj.getTitle().toLowerCase();
                final String givenText = obj.getGivenDate().toLowerCase();
                final String subText = obj.getSubject().toLowerCase();

                //add - in this envertedcommas
                Log.i("vishal.rana.tp", "Datas: "+titleText+""+givenText);
                if (titleText.contains(query) || givenText.contains(query) || subText.contains(query)){
                    filteredList.add(obj);
                    Log.i("vishal.rana.tp", "Size : "+filteredList.size());
                }
            }
        }catch (NullPointerException npe){
            Log.i("DEBUG", "No Assignment in the list");
        }catch (Exception exp){
            Log.i("DEBUG", "Other Exception occur");
        }
        return filteredList;
    }

    private void setFilterSpinner(List<Assignments> list) {
        HashSet<String> subjectSortedSet = new LinkedHashSet<>();
        subjectSortedSet.add("Subject");

        try {
            for (Assignments obj : list) {

                subjectSortedSet.add(obj.getSubject());

            }
            android.util.Log.i("DEBUG", "Subject Dairy :"+fList.size());
        }catch (NullPointerException npe){
            android.util.Log.i("DEBUG", "Subject Dairy is Empty");
        }
            try {


                if (subjectSortedSet != null) {
                    List<String> subList = new ArrayList<>();

                    subList.addAll(subjectSortedSet);

                    final ArrayAdapter<String> subjectAdapter = new ArrayAdapter<String>(getContext(), R.layout.text_filter_view, subList);
                    subjectAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(subjectAdapter);
                }

            }catch (NullPointerException np){
            np.printStackTrace();
            }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                dateFilterText.setText("Date");

                if (position != 0){
                    List<Assignments> finalSubList = subjectFilterList((String) parent.getSelectedItem());

                    if (userRole.equals(Constants.LOGIN_TYPE_AS_PARENT)) {

                        pAdapter.filteredList(finalSubList);

                    }
                    if (userRole.equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
                        aAdapter.filteredList(finalSubList);
                    }
                }else{
                    if (userRole.equals(Constants.LOGIN_TYPE_AS_PARENT)) {
                        pAdapter.filteredList(assignmentsList);
                    }
                    if (userRole.equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
                        aAdapter.filteredList(assignmentsList);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private List<Assignments> subjectFilterList(String sub){
        try {
            final List<Assignments> subFilterList = new ArrayList();
            for (Assignments aObj : assignmentsList) {
                if (aObj.getSubject().equals(sub)) {
                    subFilterList.add(aObj);
                }
            }
            return subFilterList;
        }catch (NullPointerException npe){}
        return null;
    }

    private void getAssignmentListFromServer(){
        aissignmentprogressbar();
        ApiInterface assignmentApi = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<Assignments>> call;
        if (session.getUserRole().equals(LOGIN_TYPE_AS_PARENT)){
            call = assignmentApi.getStudentAssignmentList(session.getUserId());
        }else {
            call = assignmentApi.getAssignmentList(session.getUserId());
        }

        call.enqueue(new Callback<List<Assignments>>() {
            @Override
            public void onResponse(Call<List<Assignments>> call, retrofit2.Response<List<Assignments>> response){
                if (response.isSuccessful()) {
                    assignmentsList = response.body();
                    if (assignmentsList == null ||assignmentsList.size()==0){
                        viewsNoDataFound();
                    }
                    else {
                        viewsWhenConnected();
                    }
                    fList.addAll(assignmentsList);
                    setFilterSpinner(fList);
                    try {
                            if (assignmentsList != null){
                                // here we assign if login as teacher then see teacher assignment adapter
                                if (session.getUserRole().equals(LOGIN_TYPE_AS_PARENT)) {
                                    pAdapter = new ParentAssignmentAdapter(assignmentsList, getContext());
                                    recyclerView.setAdapter(pAdapter);

                                }
                                if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER)){
                                    aAdapter = new AssignmentAdapter(assignmentsList, getContext());
                                    recyclerView.setAdapter(aAdapter);
                                }
                            }
                            else{
                                Toast.makeText(getContext(), R.string.list_empty, Toast.LENGTH_SHORT).show(); }
                        }catch (NullPointerException nullpointer){
                            Log.i("DEBUG", "Assignment List Empty");
                        }catch (Exception exp){
                        Log.i("DEBUG", "exception: "+exp.toString());
                    }
                    progressDoalog.dismiss();

                }
            }

            @Override
            public void onFailure(Call<List<Assignments>> call, Throwable t) {
                progressDoalog.dismiss();
                Log.i("DEBUG", "Assignment Expsn: "+t.toString());
            }
        });
    }



    private void aissignmentprogressbar() {

        progressDoalog = new ProgressDialog(getActivity());
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
        progressDoalog.setMessage("Please Wait...");
        progressDoalog.setIndeterminate(true);
        Circle wave=new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();
    }

    private void callDatePicker(){
        NewDateDialog fragment = new NewDateDialog();
        fragment.show(getFragmentManager() , "FilterDatePick");
    }

    @Override
    public void onClick(View v) {
        searchView.setQuery("", false);
        searchView.clearFocus();
        spinner.setSelection(0);
        callDatePicker();
    }

    @SuppressLint("ValidFragment")
    public static class NewDateDialog extends DialogFragment implements  DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();

            int y = c.get(Calendar.YEAR);
            int m = c.get(Calendar.MONTH);
            int d = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),this,y,m,d);

            return datePickerDialog;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            Calendar calendar = new GregorianCalendar(year,month,dayOfMonth);
            final DateFormat dateFormat = new SimpleDateFormat("MMM-dd-yyyy");
            dateFilterText.setText(dateFormat.format(calendar.getTime()));
            List<Assignments> finalSubList = filterListWithDate(dateFilterText.getText().toString().trim());

            if (userRole.equals(Constants.LOGIN_TYPE_AS_PARENT)) {
                pAdapter.filteredList(finalSubList);
            }
            if (userRole.equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
                aAdapter.filteredList(finalSubList);
            }


        }

        private List<Assignments> filterListWithDate(String dateText) {

            try {
                final List<Assignments> subFilterList = new ArrayList();

                for (Assignments obj: assignmentsList) {

                    String outputDateStr1 = obj.getGivenDate();
                    String outputDateStr2 = obj.getSubmissionDate();


                    if (DateFormatter.compareTwoDates(outputDateStr1, dateText)) {
                        subFilterList.add(obj);
                    }
                }
                return subFilterList;
            }catch (NullPointerException npe){}
            return null;


        }
    }

}