package appsoft.erp.gaikwad.activities.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.Feedback;
import appsoft.erp.gaikwad.activities.open_results.FeedbackDetailActivity;

public class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.FeedbackViewHolder>{

    private Context context;
    private List<Feedback> feedbackList;
    private Feedback feedbackObj;
    private String dateText = "%s\n%s\n%s";

    public FeedbackAdapter(Context context, List<Feedback> feedbackList){
        this.context = context;
        this.feedbackList = feedbackList;
        feedbackObj = new Feedback();
    }

    @Override
    public FeedbackViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.feedback_card,parent,false);
        return new FeedbackViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FeedbackViewHolder holder, int position) {

        feedbackObj = feedbackList.get(position);
        String datestr=feedbackObj.getDate();
        String[] arrofstr=datestr.split("-");
        holder.givenDateMonth.setText(arrofstr[0]);
        holder.givenDateDay.setText(arrofstr[1]);
        holder.givenDateYear.setText(arrofstr[2]);
        holder.studentName.setText(feedbackObj.getFirst_name()+" "+feedbackObj.getLast_name());
        holder.studentFeedback.setText(feedbackObj.getFeedback());
//        holder.date.setText(feedbackObj.getDate());


    }

    @Override
    public int getItemCount() {
        return feedbackList.size();
    }

    public void filteredList(List<Feedback> list){
        feedbackList = new ArrayList<>();
        feedbackList.addAll(list);
        notifyDataSetChanged();
    }

    class FeedbackViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView studentName, studentFeedback, date;
        TextView  givenDateMonth, givenDateDay, givenDateYear ;
        ImageView iv_icon;

        private FeedbackViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            givenDateMonth = itemView.findViewById(R.id.dateofGivenMonth);
            givenDateDay = itemView.findViewById(R.id.dateofGivenDay);
            givenDateYear = itemView.findViewById(R.id.dateofGivenYear);
            studentName = itemView.findViewById(R.id.studentName);
            studentFeedback = itemView.findViewById(R.id.studentsFeedback);
            iv_icon=itemView.findViewById(R.id.iv_icon);
//            date = itemView.findViewById(R.id.feedbackDate);
        }

        @Override
        public void onClick(View view) {

            int position= getAdapterPosition();
            Feedback feedback=feedbackList.get(position);
          Intent feedbackintent=new Intent(context,FeedbackDetailActivity.class);
          feedbackintent.putExtra("feedbackdestudent",feedback.getFeedback());
            context.startActivity(feedbackintent);


            // send data to new activity
        }
    }
}
