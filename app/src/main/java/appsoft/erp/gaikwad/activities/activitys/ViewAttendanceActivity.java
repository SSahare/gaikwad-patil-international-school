package appsoft.erp.gaikwad.activities.activitys;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.adapters.AttendanceByDayAdapter;
import appsoft.erp.gaikwad.activities.adapters.AttendanceByMonthAdapter;
import appsoft.erp.gaikwad.activities.adapters.AttendanceByStudentAdapter;
import appsoft.erp.gaikwad.activities.extras.DatePickerFragment;
import appsoft.erp.gaikwad.activities.modals.ByDayModel;
import appsoft.erp.gaikwad.activities.modals.ByMonthModel;
import appsoft.erp.gaikwad.activities.modals.ByStudentModel;
import appsoft.erp.gaikwad.activities.modals.ClassTeacherDetail;
import appsoft.erp.gaikwad.activities.modals.StudentsName;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAttendanceActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private TextView className;
    private Spinner selectionType;
    private LinearLayout layout;
    private LinearLayout view_attendance_layout;
    private TextView dateText;
    private Spinner spinner;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mManager;
    private int flag = 0;
    private int monthFlag = 0;
    private LinearLayout headerLayout;
    ProgressDialog progressDoalog;

    List<StudentsName> list;
    private String stId, monthName;
    private ClassTeacherDetail detail = new ClassTeacherDetail();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_attendance);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        headerLayout = findViewById(R.id.layoutListHeader);
//        view_attendance_layout = findViewById(R.id.view_attendance_layout);
        selectionType = findViewById(R.id.spinnerSelectionType);
        className = findViewById(R.id.textClassName);
        layout = findViewById(R.id.layoutSelectionView);
        recyclerView = findViewById(R.id.attendanceRecycler);
        recyclerView.setHasFixedSize(true);
        mManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mManager);

        getClassTeacherDetail();

        setSelectionType();
        selectionType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                layout.removeAllViews();
                if (position == 1) {
                    flag = 1;
                    dateText = getTextView();
                    layout.addView(dateText);
                    // add the color here
                    dateText.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callDatePicker(v);
                        }
                    });

                } else if (position == 2) {
                    flag = 2;
                    spinner = getSpinner();
                    layout.addView(spinner);

                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.month_year, R.layout.text_filter_view);
                    adapter.setDropDownViewResource(R.layout.text_drop_down_theme);
                    spinner.setAdapter(adapter);
                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position == 0) {
                                monthFlag = 0;
                            } else {
                                monthFlag = 1;
                                monthName = (String) parent.getSelectedItem();
                                headerLayout.setVisibility(View.VISIBLE);
                                setHeaders(2);
                                requestForByMonth();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else if (position == 3) {
                    flag = 3;
                    spinner = getSpinner();
                    layout.addView(spinner);

                    downloadStudents();
                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            StudentsName stName = list.get(position);
                            stId = stName.getStudentId();
                            headerLayout.setVisibility(View.VISIBLE);
                            setHeaders(3);
                            requestForByStudent();

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    /////////////////////////////////
                    // Download Students from here //
                    // And then set to the spinner //
                    /////////////////////////////////
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void downloadStudents() {
        SessionManagement sessionManagement = new SessionManagement(getApplicationContext());
        ApiInterface i = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<StudentsName>> call = i.getAllStudent(sessionManagement.getUserId());
        call.enqueue(new Callback<List<StudentsName>>() {
            @Override
            public void onResponse(Call<List<StudentsName>> call, Response<List<StudentsName>> response) {
                list = response.body();
                try {
                    ArrayAdapter<StudentsName> classAdapter = new ArrayAdapter<StudentsName>(getApplicationContext(), R.layout.text_filter_view, list);
                    classAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
                    spinner.setAdapter(classAdapter);
                    Log.i("DEBUG", "" + list.size());
                } catch (NullPointerException npe) {
                    Log.i("DEBUG", "Null Pointer Exception Occur: " + npe.toString());
                } catch (Exception exp) {
                    Log.i("DEBUG", "Exception Occur: " + exp.toString());
                }
            }

            @Override
            public void onFailure(Call<List<StudentsName>> call, Throwable t) {
                Log.i("DEBUG", "Failure Occur: " + t.toString());
            }
        });
    }

    private void showPopUp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Suggestion");
        builder.setCancelable(false);
        builder.setMessage("Oops, You can't see attendance");
        builder.setNeutralButton("Go Back", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                dialog.dismiss();
            }
        });
        Dialog dialog = builder.create();
        dialog.show();
    }

    private void callDatePicker(View view) {
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.show(getFragmentManager(), "DatePick");

    }

    private void setSelectionType() {
        ArrayAdapter<CharSequence> selAdapter = ArrayAdapter.createFromResource(this, R.array.attendance_type_array, R.layout.text_filter_view);
        selAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
        selectionType.setAdapter(selAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        else{
            return false;
        }

    }


    private void setHeaders(int i) {
        TextView heading1 = findViewById(R.id.textHead1);
        TextView heading2 = findViewById(R.id.textHead2);
        TextView heading3 = findViewById(R.id.textHead3);
        TextView heading4 = findViewById(R.id.textHead4);

        switch (i) {
            case 1:
                //view_attendance_layout.removeView(recyclerView);
                heading1.setText("Student Name");
                heading1.setTextColor(getResources().getColor(R.color.orange));
                heading2.setVisibility(View.GONE);
                heading2.setTextColor(getResources().getColor(R.color.orange));
                heading3.setText("Status");
                heading3.setTextColor(getResources().getColor(R.color.orange));
                heading3.setGravity(Gravity.CENTER);
                heading4.setVisibility(View.GONE);

                break;
            case 2:
                //view_attendance_layout.removeView(recyclerView);
                heading2.setVisibility(View.VISIBLE);
                heading1.setText("Student Name");
                heading1.setTextColor(getResources().getColor(R.color.orange));
                heading2.setText("Working Days");
                heading2.setTextColor(getResources().getColor(R.color.orange));
                heading3.setText("Present Days");
                heading3.setTextColor(getResources().getColor(R.color.orange));
                heading3.setGravity(Gravity.CENTER);
                heading4.setText("Percent");
                heading4.setVisibility(View.VISIBLE);
                heading4.setTextColor(getResources().getColor(R.color.orange));
                heading4.setGravity(Gravity.CENTER);
                break;
            case 3:
                heading2.setVisibility(View.VISIBLE);
                heading1.setText("Month");
                heading1.setTextColor(getResources().getColor(R.color.orange));
                heading2.setText("Working Days");
                heading2.setTextColor(getResources().getColor(R.color.orange));
                heading3.setText("Present Days");
                heading3.setTextColor(getResources().getColor(R.color.orange));
                heading3.setGravity(Gravity.CENTER);
                heading4.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = new GregorianCalendar(year, month, dayOfMonth);
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.ENGLISH);
        dateText.setText(dateFormat.format(calendar.getTime()));
        headerLayout.setVisibility(View.VISIBLE);
        setHeaders(1);
        requestForByDate();
    }

    private void requestForByDate() {
        dateProgressBar();

        if (dateText.getText().toString().trim().isEmpty()) {
            Log.i("DEBUG", "By Date Called If Empty");
            Toast.makeText(this, "Please, select date...", Toast.LENGTH_SHORT).show();
            AttendanceByDayAdapter ad = new AttendanceByDayAdapter(null);
            recyclerView.setAdapter(ad);
            recyclerView.removeAllViews();


        } else {
            Log.i("DEBUG", "By Date Called Not Empty");
            // Request for date Wise Atteandance
            ApiInterface i = ClientAPI.getRetrofit().create(ApiInterface.class);
            Call<List<ByDayModel>> call = i.getDayAttendanceList("date", detail.getClassAssigned(), detail.getSectionAssigned(), dateText.getText().toString());

            call.enqueue(new Callback<List<ByDayModel>>() {
                @Override
                public void onResponse(Call<List<ByDayModel>> call, Response<List<ByDayModel>> response) {
                    List<ByDayModel> listDay = response.body();



                    try {
                        Log.i("DEBUG", "Api Called");
                        if (listDay.size() == 0) {
                            Toast t = Toast.makeText(getApplicationContext(), "No attendance list found for current date", Toast.LENGTH_SHORT);
                            t.setGravity(0, 0, Gravity.CENTER);
                            t.show();
                            AttendanceByDayAdapter ad = new AttendanceByDayAdapter(listDay);




                        } else {
                            progressDoalog.dismiss();

                            Log.i("DEBUG", "Attendance called");
                            AttendanceByDayAdapter ad = new AttendanceByDayAdapter(listDay);
                            recyclerView.setAdapter(ad);



                        }
                    } catch (NullPointerException npe) {
                        Log.i("DEBUG", npe.toString());
                    } catch (Exception exp) {
                        Log.i("DEBUG", exp.toString());
                    }

                    progressDoalog.dismiss();

                }

                @Override
                public void onFailure(Call<List<ByDayModel>> call, Throwable t) {
                    progressDoalog.dismiss();
                    Log.i("DEBUG", t.toString());
                }
            });
        }
    }

    private void dateProgressBar() {
        progressDoalog = new ProgressDialog(this);
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
        progressDoalog.setMessage("Please Wait...");
//        progressDoalog.getWindow().setGravity(LinearLayout.LayoutParams.MATCH_PARENT LinearLayout.LayoutParams.MATCH_PARENT);
        progressDoalog.setIndeterminate(true);
        Circle wave=new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();
    }

    private void requestForByMonth() {
        dateProgressBar();
        if (monthFlag == 0) {
            Toast.makeText(this, "Please, select month...", Toast.LENGTH_SHORT).show();
            AttendanceByMonthAdapter am = new AttendanceByMonthAdapter(null);
            recyclerView.setAdapter(am);
            recyclerView.removeAllViews();
        } else {
            // Request for date Wise Atteandance
            ApiInterface i = ClientAPI.getRetrofit().create(ApiInterface.class);
            Call<List<ByMonthModel>> call = i.getMonthAttendanceList("month", detail.getClassAssigned(), detail.getSectionAssigned(), "01-" + monthName);
            call.enqueue(new Callback<List<ByMonthModel>>() {
                @Override
                public void onResponse(Call<List<ByMonthModel>> call, Response<List<ByMonthModel>> response) {

                    List<ByMonthModel> monthList = response.body();

                    try {
                        if (monthList.size() == 0) {
                            Toast t = Toast.makeText(getApplicationContext(), "No attendance list found for current date", Toast.LENGTH_SHORT);
                            t.setGravity(0, 0, Gravity.CENTER);
                            t.show();
                        }
                        callAdapter(monthList);

                    } catch (NullPointerException npe) {
                        Log.i("DEBUG", npe.toString());
                        callAdapter(monthList);
                    }
                    progressDoalog.dismiss();
                }

                @Override
                public void onFailure(Call<List<ByMonthModel>> call, Throwable t) {
                    progressDoalog.dismiss();
                    callAdapter(null);
                    Toast.makeText(ViewAttendanceActivity.this, "Failed to load..", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void callAdapter(List<ByMonthModel> list) {
        AttendanceByMonthAdapter ad = new AttendanceByMonthAdapter(list);
        recyclerView.setAdapter(ad);
    }

    private void requestForByStudent() {
        dateProgressBar();
        if (stId.equals("")) {
            Toast.makeText(this, "Please, select student...", Toast.LENGTH_SHORT).show();
            AttendanceByStudentAdapter am = new AttendanceByStudentAdapter(null,getApplicationContext(),stId);
            recyclerView.setAdapter(am);
        } else {
            // Request for date Wise Atteandance
            ApiInterface i = ClientAPI.getRetrofit().create(ApiInterface.class);
            Log.i("DEBUGC", stId + " " + detail.getClassAssigned() + " " + detail.getSectionAssigned());

            Call<List<ByStudentModel>> call = i.getStudentAttendanceList("student", detail.getClassAssigned(), detail.getSectionAssigned(), stId);
            call.enqueue(new Callback<List<ByStudentModel>>() {
                @Override
                public void onResponse(Call<List<ByStudentModel>> call, Response<List<ByStudentModel>> response) {
                    List<ByStudentModel> list = response.body();

                    try {
                        if (list.size() == 0) {
                            Toast t = Toast.makeText(getApplicationContext(), "No attendance list found for given student", Toast.LENGTH_SHORT);
                            t.setGravity(0, 0, Gravity.CENTER);
                            t.show();
//
                        } else {
                            AttendanceByStudentAdapter am = new AttendanceByStudentAdapter(list,getApplicationContext(),stId);
                            recyclerView.setAdapter(am);


                        }
                    } catch (Exception e) {
                        Log.i("DEBUG", "Exception Occurs");
                    }
                    progressDoalog.dismiss();

                }

                @Override
                public void onFailure(Call<List<ByStudentModel>> call, Throwable t) {
                    progressDoalog.dismiss();
                    Log.i("DEBUG", "Failure Occurs: " + t.toString());
                }
            });
        }
    }


    // add here the match parent spinner width
    private Spinner getSpinner() {
        Spinner studentSpinner = new Spinner(getApplicationContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
//        params.setMarginStart(2);
//        params.setMarginEnd(5);
        studentSpinner.setLayoutParams(params);
        studentSpinner.setGravity(Gravity.START);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){

        }

        return studentSpinner;
    }

    private TextView getTextView() {
        TextView newDateText = new TextView(getApplicationContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        newDateText.setLayoutParams(params);
        newDateText.setHint("Select Date");

        newDateText.setGravity(Gravity.START);
        params.setMarginStart(5);
        params.setMarginEnd(5);
        newDateText.setHintTextColor(Color.parseColor("#ffffff"));
        newDateText.setTextSize(16);
        newDateText.setTextColor(Color.parseColor("#ffffff"));
        return newDateText;
    }

    private void getClassTeacherDetail() {
        SessionManagement sessionManagement = new SessionManagement(getApplicationContext());
        ApiInterface in = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<ClassTeacherDetail> call = in.getClassTeacherDetail(sessionManagement.getUserId());
        call.enqueue(new Callback<ClassTeacherDetail>() {
            @Override
            public void onResponse(Call<ClassTeacherDetail> call, Response<ClassTeacherDetail> response) {
                detail = response.body();
                try {
                    if (detail.getResponse()) {
                        className.setText("Class: " + detail.getClassAssigned() + " (" + detail.getSectionAssigned() + ")");
                    } else {
                        showPopUp();
                    }
                } catch (Exception e) {
                    showPopUp();
                }
            }

            @Override
            public void onFailure(Call<ClassTeacherDetail> call, Throwable t) {
                Toast.makeText(ViewAttendanceActivity.this, "Server Failure Occur", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.search_menu,menu);

        final MenuItem  additem=menu.findItem(R.id.isync_add);
        additem.setVisible(true);
        additem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Intent intent=new Intent(ViewAttendanceActivity.this,AttendenceActivity.class);
                startActivity(intent);
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
}
