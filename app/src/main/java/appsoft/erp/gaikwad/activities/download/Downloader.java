package appsoft.erp.gaikwad.activities.download;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.util.Log;


public class Downloader{

    DownloadManager downloadManager;
    String stringUrl;
    Context context;

    public Downloader(String stringUrl, Context context){
        this.context = context;
        this.stringUrl = stringUrl;
        downloadManager = (DownloadManager)context.getSystemService(Context.DOWNLOAD_SERVICE);
    }

    public void downloadTask(){
//        Uri uri = Uri.parse(stringUrl.trim());
//        DownloadManager.Request request = new DownloadManager.Request(uri);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(stringUrl));
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        Long reference = downloadManager.enqueue(request);
        Log.i("DEBUG", "Reference ID: "+reference);
    }
}
