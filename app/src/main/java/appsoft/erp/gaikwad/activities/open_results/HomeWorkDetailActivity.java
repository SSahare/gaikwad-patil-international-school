package appsoft.erp.gaikwad.activities.open_results;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import appsoft.erp.gaikwad.R;

public class HomeWorkDetailActivity extends AppCompatActivity {
    TextView homeworktitle, homeworkclass, homeworksection, homeworkdescription, homeworksubmission;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_work_detail);



        homeworkclass = findViewById(R.id.tv_HomeworkClass);
        homeworksection = findViewById(R.id.tv_Homeworksection);
//        homeworksubmission = findViewById(R.id.tv_HomeworkSubmissiondate);
        homeworkdescription = findViewById(R.id.tv_HomeworkDescription);
        homeworktitle = findViewById(R.id.tv_homeworktitle);


        homeworkclass.setText("class :"+getIntent().getStringExtra("homeworkclass"));
        homeworksection.setText("section :"+getIntent().getStringExtra("homeworksection"));
//        homeworksubmission.setText("Submission Date :"+getIntent().getStringExtra("homeworkdateofsubmission"));
        homeworkdescription.setText(getIntent().getStringExtra("homeworkdescription"));
        homeworktitle.setText(getIntent().getStringExtra("homeworktitle"));


        if (getSupportActionBar() != null) {

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setTitle((getIntent().getStringExtra(String.valueOf(homeworksubject))));
            getSupportActionBar().setTitle(getIntent().getStringExtra("homeworksubject"));
//            getSupportActionBar().setCustomView(Integer.parseInt("Detail Homework"+getIntent().getStringExtra("homeworksubject")));

        }


    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
    }

