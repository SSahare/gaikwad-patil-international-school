package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserProfileModel {

    @SerializedName("student_id")
    @Expose
    private String studentId = "";

    @SerializedName("first_name")
    @Expose
    private String firstName = "";

    @SerializedName("middle_name")
    @Expose
    private String middleName = "";

    @SerializedName("last_name")
    @Expose
    private String lastName = "";

    @SerializedName("gender")
    @Expose
    private String gender = "";

    @SerializedName("date_of_birth")
    @Expose
    private String dateOfBirth = "";

    @SerializedName("parent_number")
    @Expose
    private String parentNumber = "";

    @SerializedName("parent_email")
    @Expose
    private String parentEmail = "";

    @SerializedName("adhar_no")
    @Expose
    private String adharNo = "";

    @SerializedName("profile_photo")
    @Expose
    private Object profilePhoto = "";

    @SerializedName("family_photo")
    @Expose
    private Object familyPhoto = "";

    @SerializedName("registration_no")
    @Expose
    private String registrationNo = "";

    @SerializedName("admission_number")
    @Expose
    private String admissionNumber = "";

    @SerializedName("academic_year")
    @Expose
    private String academicYear = "";

    @SerializedName("admission_date")
    @Expose
    private String admissionDate = "";

    @SerializedName("st_class")
    @Expose
    private String stClass = "";

    @SerializedName("section")
    @Expose
    private String section = "";

    @SerializedName("local_address")
    @Expose
    private String localAddress = "";

    @SerializedName("permanent_address")
    @Expose
    private String permanentAddress = "";

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getParentNumber() {
        return parentNumber;
    }

    public void setParentNumber(String parentNumber) {
        this.parentNumber = parentNumber;
    }

    public String getParentEmail() {
        return parentEmail;
    }

    public void setParentEmail(String parentEmail) {
        this.parentEmail = parentEmail;
    }

    public String getAdharNo() {
        return adharNo;
    }

    public void setAdharNo(String adharNo) {
        this.adharNo = adharNo;
    }

    public Object getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(Object profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public Object getFamilyPhoto() {
        return familyPhoto;
    }

    public void setFamilyPhoto(Object familyPhoto) {
        this.familyPhoto = familyPhoto;
    }

    public String getRegistrationNo() {
        return registrationNo;
    }

    public void setRegistrationNo(String registrationNo) {
        this.registrationNo = registrationNo;
    }

    public String getAdmissionNumber() {
        return admissionNumber;
    }

    public void setAdmissionNumber(String admissionNumber) {
        this.admissionNumber = admissionNumber;
    }

    public String getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    public String getAdmissionDate() {
        return admissionDate;
    }

    public void setAdmissionDate(String admissionDate) {
        this.admissionDate = admissionDate;
    }

    public String getStClass() {
        return stClass;
    }

    public void setStClass(String stClass) {
        this.stClass = stClass;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getLocalAddress() {
        return localAddress;
    }

    public void setLocalAddress(String $localAddress) {
        this.localAddress = $localAddress;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }
}