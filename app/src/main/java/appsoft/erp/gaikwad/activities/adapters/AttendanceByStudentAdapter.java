package appsoft.erp.gaikwad.activities.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.ByStudentModel;
import appsoft.erp.gaikwad.activities.parent_activity.MonthlyAttendanceActivity;

public class AttendanceByStudentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ByStudentModel byStudObj;
    private List<ByStudentModel> byStudModelList;
    Context context;
    String studentId;

    public AttendanceByStudentAdapter(List<ByStudentModel> byStudModelList,Context context ,String studentId) {
        this.byStudModelList = byStudModelList;
        this.context=context;
        this.studentId =studentId;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = inflater.inflate(R.layout.by_student_card, parent, false);
            return new AttendanceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            byStudObj = byStudModelList.get(position);
            Log.i("DEBUGC", byStudObj.getMonth() + " " + byStudObj.getYear() + " ");
            ((AttendanceViewHolder) holder).monthName.setText(byStudObj.getMonth() + ", " + byStudObj.getYear());
            ((AttendanceViewHolder) holder).workingDays.setText(byStudObj.getWorkingDay());
            ((AttendanceViewHolder) holder).presentDays.setText(byStudObj.getPresentDay());


    }

    @Override
    public int getItemCount() {
        try{
            return byStudModelList.size();
        }catch (NullPointerException exc){
            Log.i("DEBUG", exc.toString());
            return 0;
        }
    }

    class AttendanceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView monthName, presentDays, workingDays;
        public AttendanceViewHolder(View itemView) {
            super(itemView);

            monthName = itemView.findViewById(R.id.attendanceMonth);
            presentDays = itemView.findViewById(R.id.studentPresentDays);
            workingDays = itemView.findViewById(R.id.studentWorkingDays);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, MonthlyAttendanceActivity.class);
            ByStudentModel obj = byStudModelList.get(getAdapterPosition());
            intent.putExtra("month", String.format("%s, %s", obj.getMonth(), obj.getYear()));
            intent.putExtra("user_id", Integer.parseInt(studentId));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }
}