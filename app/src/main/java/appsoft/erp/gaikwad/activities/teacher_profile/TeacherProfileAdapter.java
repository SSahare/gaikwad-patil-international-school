package appsoft.erp.gaikwad.activities.teacher_profile;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import appsoft.erp.gaikwad.R;

public class TeacherProfileAdapter extends BaseExpandableListAdapter {

    private List<String> heading_titles;
    private HashMap<String,List<String>> header_contents;
    private Context ctx;

    public TeacherProfileAdapter(List<String> heading_titles, HashMap<String, List<String>> header_contents, Context ctx) {
        this.heading_titles = heading_titles;
        this.header_contents = header_contents;
        this.ctx = ctx;
    }

    @Override
    public int getGroupCount() {
        return heading_titles.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return header_contents.get(heading_titles.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return heading_titles.get(i);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return header_contents.get(heading_titles.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String title = (String)this.getGroup(groupPosition);
        if(convertView == null )
        {
            LayoutInflater layoutInflater=(LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=layoutInflater.inflate(R.layout.profile_list_view_layout,null);
        }
        TextView textView=  convertView.findViewById(R.id.headingItem);
        textView.setTypeface(null, Typeface.BOLD);
        textView.setText(title);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        String title=(String)this.getChild(groupPosition,childPosition);
        if(convertView == null)
        {
            LayoutInflater layoutInflater=(LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.my_profile_list_content,null);
        }
        TextView textView= (TextView) convertView.findViewById(R.id.childItem);
        textView.setText(title);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
