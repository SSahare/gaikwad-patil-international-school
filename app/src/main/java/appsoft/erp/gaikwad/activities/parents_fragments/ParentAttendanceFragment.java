package appsoft.erp.gaikwad.activities.parents_fragments;


import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;

import java.util.List;

import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import appsoft.erp.gaikwad.activities.network_broadcast.ConnectionDetector;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.ListAttendance;
import appsoft.erp.gaikwad.activities.parent_adapters.Parent_Attendance_Adapter;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;

/**
 * A simple {@link Fragment} subclass.
 */
public class ParentAttendanceFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mManager;
    private List<ListAttendance> attendanceList = null;
    private SessionManagement session;
    ProgressDialog progressDoalog;
    private Parent_Attendance_Adapter adapter;
    private static OnTitleSetListener onTitleSetListener=null ;
    private ConstraintLayout constraintLayout;
    private ImageView noDataFound;
    SwipeRefreshLayout swipeRefreshLayout;

    public ParentAttendanceFragment() { }

    public static ParentAttendanceFragment getInstance(OnTitleSetListener onTitleSetListener1) {
        // Required empty public constructor
        onTitleSetListener = onTitleSetListener1;
        return new ParentAttendanceFragment();
    }


    @Override
    public void onResume() {
        super.onResume();
        setTitle();
    }

    private void setTitle() {

        onTitleSetListener.setOnTitleText("Attendance");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.parents_attendance_fragment, container, false);

        session = new SessionManagement(getContext());
        mRecyclerView = view.findViewById(R.id.parentAttendanceRecycler);
        mRecyclerView.setHasFixedSize(true);
        mManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mManager);
        constraintLayout = view.findViewById(R.id.no_internet_layout);
        Button tryAgain = view.findViewById(R.id.try_again);
        noDataFound = view.findViewById(R.id.no_data_found_image);
        swipeRefreshLayout =view.findViewById(R.id.swiperefreshlayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDataFromServer();
                swipeRefreshLayout.setRefreshing(false);


            }
        });



        if (ConnectionDetector.isConnected(getContext())){
            viewsWhenConnected();
            getDataFromServer();
        }else {
            viewsWhenNotConnected();
        }

        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectionDetector.isConnected(getContext())){
                    viewsWhenConnected();
                    getDataFromServer();
                }else {
                    viewsWhenNotConnected();
                }
            }
        });
//        getDataFromServer();
        return view;
    }


    private void viewsWhenConnected(){
        constraintLayout.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
        noDataFound.setVisibility(View.GONE);
    }

    private void viewsWhenNotConnected(){
        constraintLayout.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);
    }

    private void dataEmpty(){
        mRecyclerView.setVisibility(View.GONE);
        noDataFound.setVisibility(View.VISIBLE);
        constraintLayout.setVisibility(View.GONE);
    }


    private void getDataFromServer(){
        attendenceprogressbar();
        ApiInterface apiInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<ListAttendance>> call = apiInterface.getAttendance(session.getUserId());
        call.enqueue(new Callback<List<ListAttendance>>() {
            @Override
            public void onResponse(Call<List<ListAttendance>> call, Response<List<ListAttendance>> response) {
                attendanceList = response.body();
                try {

                    if (attendanceList == null || attendanceList.size() == 0) {
                        dataEmpty();
                        progressDoalog.dismiss();
                    }else {
                        adapter = new Parent_Attendance_Adapter(getContext(), attendanceList);
                        mRecyclerView.setAdapter(adapter);
                        progressDoalog.dismiss();
                    }
                }catch (NullPointerException npe){
                    Log.i("DEBUG", "Attendance are empty");
                }catch (Exception exp){ Log.i("DEBUG", ""+exp.toString());}
                progressDoalog.dismiss();

            }


            @Override
            public void onFailure(Call<List<ListAttendance>> call, Throwable t) {
                Log.i("DEBUG", "News Dash Expsn: "+t.toString());
                progressDoalog.dismiss();
                Toast.makeText(getContext(), "Fail to load data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void attendenceprogressbar() {

        progressDoalog = new ProgressDialog(getActivity());
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
        progressDoalog.setMessage("Please Wait...");
        progressDoalog.setIndeterminate(true);
        Circle wave=new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();
    }
}
