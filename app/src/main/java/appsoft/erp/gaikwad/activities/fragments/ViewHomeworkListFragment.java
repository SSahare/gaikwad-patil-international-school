package appsoft.erp.gaikwad.activities.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

import appsoft.erp.gaikwad.activities.activitys.AddHomeworkActivity;
import appsoft.erp.gaikwad.activities.date_util.DateFormatter;
import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import appsoft.erp.gaikwad.activities.network_broadcast.ConnectionDetector;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.adapters.HomeworkAdapter;
import appsoft.erp.gaikwad.activities.extras.Constants;
import appsoft.erp.gaikwad.activities.modals.Homework;
import appsoft.erp.gaikwad.activities.parent_adapters.ParentHomeworkAdapter;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.search_package.SearchColor;
import appsoft.erp.gaikwad.activities.session.SessionManagement;

public class ViewHomeworkListFragment extends Fragment implements View.OnClickListener{

    private RecyclerView.LayoutManager assignmentLayoutManager;
    private RecyclerView recyclerView;
    private static List<Homework> homeworkList = null;
    private List<Homework> fList = new ArrayList<>();
    private static ParentHomeworkAdapter aAdapter;
    private static HomeworkAdapter hAdapter;
    private SearchView searchView;
    private SessionManagement session;
    Animation uptodown, downtoup;
    ProgressDialog progressDoalog;
    private AppCompatSpinner spinner;
    private static TextView dateFilterText;
    private ImageButton closeDate;
    private ImageButton closeSubject;
    ProgressBar progressBar;
    ConstraintLayout constraintLayout ;
    private ImageView dataNotFound;
    private ImageView noDataFound;
    private static String userRole = "";
    SwipeRefreshLayout swipeRefreshLayout;
    private static OnTitleSetListener onTitleSetListener = null;
    private LinearLayout dropDownLayout,dateTextLayout;

    public ViewHomeworkListFragment() { }
    public static ViewHomeworkListFragment getInstance(OnTitleSetListener onTitleSetListener1) {
        // Required empty public constructor
        onTitleSetListener = onTitleSetListener1;
        return new ViewHomeworkListFragment();
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Override
    public void onResume() {
        super.onResume();
        setTitle();
    }
    private void setTitle() {
        onTitleSetListener.setOnTitleText("Homeworks");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View homeworkView = inflater.inflate(R.layout.view_homework_list_fragment, container, false);
        session = new SessionManagement(getContext());
        userRole = session.getUserRole();

        uptodown = AnimationUtils.loadAnimation(getContext(), R.anim.uptodown);
        recyclerView = homeworkView.findViewById(R.id.homeworkRecycler);
        recyclerView.setHasFixedSize(true);
        swipeRefreshLayout=homeworkView.findViewById(R.id.swiperefreshlayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_PARENT)){
                    getHomeworkListFromServer();
                    swipeRefreshLayout.setRefreshing(false);
                }
                if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER)){
                    getHomeworkListFromServer();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
        assignmentLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(assignmentLayoutManager);
        spinner = homeworkView.findViewById(R.id.spinnerSubjectFilter);
//        progressBar=homeworkView.findViewById(R.id.homework_progress_bar);
        recyclerView.setAnimation(uptodown);
//
        closeDate = homeworkView.findViewById(R.id.close_date);
//        closeSubject = homeworkView.findViewById(R.id.close_subject);
        dateFilterText = homeworkView.findViewById(R.id.dateFilterText);
        dateFilterText.setOnClickListener(this);

        dataNotFound =homeworkView.findViewById(R.id.no_data_found_image);
        constraintLayout = homeworkView.findViewById(R.id.no_internet_layout);
        dropDownLayout = homeworkView.findViewById(R.id.dropdown_layout);
        dateTextLayout=homeworkView.findViewById(R.id.date_layout);
        noDataFound =homeworkView.findViewById(R.id.no_data_found_image);
        Button tryAgain = homeworkView.findViewById(R.id.try_again);



        closeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateFilterText.setText("");
                if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_PARENT)) {
                    aAdapter.filter(homeworkList);
                }
                if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
                    hAdapter.filter(homeworkList);
                }
            }
        });
//
//        closeSubject.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                spinner.setSelection(0);
//                if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_PARENT)) {
//                    progressBar.setVisibility(View.GONE);
//                    aAdapter.filter(homeworkList);
//                }
//                if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
//                    progressBar.setVisibility(View.GONE);
//                    hAdapter.filter(homeworkList);
//                }
//            }
//        });


        if (ConnectionDetector.isConnected(getContext())){
            viewsWhenConnected();
            getHomeworkListFromServer();
        }
        else {
            viewsWhenNotConnected();
        }

        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConnectionDetector.isConnected(getContext())){
                    getHomeworkListFromServer();
                    viewsWhenConnected();
                }
            }
        });


        return homeworkView;
    }


    private  void  viewsWhenConnected(){

        recyclerView.setVisibility(View.VISIBLE);
        constraintLayout.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);
        dateTextLayout.setVisibility(View.VISIBLE);
        dropDownLayout.setVisibility(View.VISIBLE);

    }

    private void viewsWhenNotConnected() {

        recyclerView.setVisibility(View.GONE);
        dataNotFound.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.VISIBLE);
        dateTextLayout.setVisibility(View.GONE);
        dropDownLayout.setVisibility(View.GONE);
    }
    private void viewsNoDataFound() {
        recyclerView.setVisibility(View.GONE);
        dataNotFound.setVisibility(View.VISIBLE);
        constraintLayout.setVisibility(View.GONE);
        dateTextLayout.setVisibility(View.GONE);
        dropDownLayout.setVisibility(View.GONE);
    }

    private void homeworkprogressbar() {


        progressDoalog = new ProgressDialog(getActivity());
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
        progressDoalog.setMessage("Please Wait...");

        progressDoalog.setIndeterminate(true);
        Circle wave=new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search_menu, menu);
        final MenuItem searchMenuItem = menu.findItem(R.id.isync_search);
        SearchColor.changeSearchViewColor(searchView);
        searchView = SearchColor.getSearchView(searchMenuItem);
        if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
            final MenuItem additem = menu.findItem(R.id.isync_add);
            additem.setVisible(true);
            additem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    startActivity(new Intent(getActivity(), AddHomeworkActivity.class));
                    return true;
                }
            });
        }


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()){
                    searchView.setIconified(true);
                }

                searchMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                final List<Homework> filterModeList = filter(homeworkList, newText);
                try {
                    if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_PARENT)) {
                        aAdapter.filter(filterModeList);
                    }
                    if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
                        hAdapter.filter(filterModeList);
                    }
                }catch (NullPointerException npe){
                    Log.i("DEBUG", "Empty List");
                }
                return false;
            }
        });
    }

    private List<Homework> filter(List<Homework> list, String query){

        query = query.toLowerCase();
        final List<Homework> filteredList = new ArrayList<>();
        try {
            for (Homework obj : list) {
                final String titleText = obj.getTitle().toLowerCase();
                final String subText = obj.getSubject().toLowerCase();
                final String dateText = obj.getDateOfGiven().toLowerCase();
                if (titleText.contains(query) || subText.contains(query) || dateText.contains(query)) {
                    filteredList.add(obj);
                }
            }
        }catch (NullPointerException npe){
            Log.i("DEBUG", "No Assignment in the list");
        }catch (Exception exp){
            Log.i("DEBUG", "Other Exception occur");
        }
        return filteredList;
    }

    private void setFilterSpinner(List<Homework> list) {
        HashSet<String> subjectSortedSet = new LinkedHashSet<>();
        subjectSortedSet.add("Subject");
        try {
            for (Homework obj : list) {
                subjectSortedSet.add(obj.getSubject());
            }
            android.util.Log.i("DEBUG", "Subject Dairy :"+fList.size());
        }catch (NullPointerException npe){
            android.util.Log.i("DEBUG", "Subject Dairy is Empty");
        }

        try {
            if (subjectSortedSet != null) {
                List<String> subList = new ArrayList<>();
                subList.addAll(subjectSortedSet);
                final ArrayAdapter<String> subjectAdapter = new ArrayAdapter<String>(getContext(), R.layout.text_filter_view, subList);
                subjectAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(subjectAdapter);
            }
        }catch (NullPointerException p){
            p.printStackTrace();
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                dateFilterText.setText("");

                if (position != 0){
                    List<Homework> finalSubList = subjectFilterList((String) parent.getSelectedItem());

                    if (userRole.equals(Constants.LOGIN_TYPE_AS_PARENT)) {
                        aAdapter.filter(finalSubList);
                    }
                    if (userRole.equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
                        hAdapter.filter(finalSubList);
                    }
                }else{
                    if (userRole.equals(Constants.LOGIN_TYPE_AS_PARENT)) {
                        aAdapter.filter(homeworkList);
                    }
                    if (userRole.equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
                        hAdapter.filter(homeworkList);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private List<Homework> subjectFilterList(String sub){
        try {
            final List<Homework> subFilterList = new ArrayList();
            for (Homework hObj : homeworkList) {
                if (hObj.getSubject().equals(sub)) {
                    subFilterList.add(hObj);
                }
            }
            return subFilterList;
        }catch (NullPointerException npe){}
        return null;
    }

    private void getHomeworkListFromServer(){
        homeworkprogressbar();
        ApiInterface homeApi = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<Homework>> call;

        if (userRole.equals(Constants.LOGIN_TYPE_AS_PARENT)){
            call = homeApi.getStudentHomeworkList(session.getUserId());
        }else {
            call = homeApi.getHomeworkList(session.getUserId());
        }
        call.enqueue(new Callback<List<Homework>>() {
            @Override
            public void onResponse(Call<List<Homework>> call, Response<List<Homework>> response) throws NullPointerException{
                if (response.isSuccessful()) {
                    homeworkList = response.body();
                    if (homeworkList==null||homeworkList.size()==0){
                        viewsNoDataFound();
                    }else {
                        viewsWhenConnected();
                    }
                    progressDoalog.dismiss();
                    fList.addAll(homeworkList);
                        setFilterSpinner(fList);
                        try{
                            if (homeworkList != null){
                                if (userRole.equals(Constants.LOGIN_TYPE_AS_PARENT)) {

//                                    progressDoalog.dismiss();
                                    aAdapter = new ParentHomeworkAdapter(getContext(), homeworkList);
                                    recyclerView.setAdapter(aAdapter);
                                }
                                if (userRole.equals(Constants.LOGIN_TYPE_AS_TEACHER)){
//                                    progressBar.setVisibility(View.GONE);
                                    hAdapter = new HomeworkAdapter(getContext(),homeworkList);
                                    recyclerView.setAdapter(hAdapter);
                                    progressDoalog.dismiss();

                                }
                            }else {
                                progressDoalog.dismiss();
//                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(getContext(), R.string.list_empty, Toast.LENGTH_SHORT).show();
                            }
                        }catch (NullPointerException npe){
                            Log.i("DEBUG", "Homework List Empty");
                        }catch (Exception exc){
                            Log.i("DEBUG", "Homework List Exception");
                        }
                    }
                progressDoalog.dismiss();
                }

            @Override
            public void onFailure(Call<List<Homework>> call, Throwable t) {
//                progressBar.setVisibility(View.GONE);
//                progressBar=null;
                progressDoalog.dismiss();
                Log.i("DEBUG", "Homework Expsn: "+t.toString());
            }
        });
    }

    private void callDatePicker(){
        NewDateDialog fragment = new NewDateDialog();
        fragment.show(getFragmentManager() , "FilterDatePick");
    }

    @Override
    public void onClick(View v) {
        searchView.setQuery("", false);
        searchView.clearFocus();
        spinner.setSelection(0);
        callDatePicker();
    }

    @SuppressLint("ValidFragment")
    public static class NewDateDialog extends DialogFragment implements  DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int y = c.get(Calendar.YEAR);
            int m = c.get(Calendar.MONTH);
            int d = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),this,y,m,d);

            return datePickerDialog;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

            Calendar calendar = new GregorianCalendar(year,month,dayOfMonth);
            final DateFormat dateFormat = new SimpleDateFormat("MMM-dd-yyyy");
            dateFilterText.setText(dateFormat.format(calendar.getTime()));
            List<Homework> finalSubList = filterListWithDate(dateFilterText.getText().toString().trim());

            if (userRole.equals(Constants.LOGIN_TYPE_AS_PARENT)) {
                aAdapter.filter(finalSubList);
            }
            if (userRole.equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
                hAdapter.filter(finalSubList);
            }
        }

        private List<Homework> filterListWithDate(String dateText) {

            try {
                final List<Homework> subFilterList = new ArrayList();

                for (Homework obj : homeworkList) {

                    String outputDateStr1 = obj.getDateOfGiven();
                    String outputDateStr2 = obj.getDateOfSubmission();

//                    if (DateFormatter.compareTwoDates(outputDateStr1, dateText) || DateFormatter.compareTwoDates(outputDateStr2, dateText)) {
//                        subFilterList.add(obj);
//                    }


                    if (DateFormatter.compareTwoDates(outputDateStr1, dateText)) {
                        subFilterList.add(obj);
                    }
                }
                return subFilterList;
            }catch (NullPointerException npe){}
            return null;
        }
    }
}
