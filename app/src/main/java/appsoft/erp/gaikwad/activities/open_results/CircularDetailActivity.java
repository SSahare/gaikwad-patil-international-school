package appsoft.erp.gaikwad.activities.open_results;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.hzn.lib.EasyTransition;

import appsoft.erp.gaikwad.R;

public class CircularDetailActivity extends AppCompatActivity {
    TextView tv_CircularTitle,tv_CircularDescription;
    Animation uptodown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_circular_detail);
        tv_CircularTitle=findViewById(R.id.tv_CircularTitle);
        tv_CircularDescription=findViewById(R.id.tv_CircularDescription);
        uptodown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.uptodown);

//        EasyTransition.enter( CircularDetailActivity.this);

        tv_CircularTitle.setText(getIntent().getStringExtra("title"));
        tv_CircularDescription.setText(getIntent().getStringExtra("desc"));

        tv_CircularDescription.setAnimation(uptodown);
        tv_CircularTitle.setAnimation(uptodown);

        if (getSupportActionBar() != null) {

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(" Circular Detail");

        }

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            EasyTransition.exit(CircularDetailActivity.this);
            finish();
            return true;
        }else {
            return super.onOptionsItemSelected(item);
        }
    }



    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onSupportNavigateUp() {


        finishAfterTransition();

        return true;
    }
}
