package appsoft.erp.gaikwad.activities.open_results;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import appsoft.erp.gaikwad.R;

public class ParentDetailFeedbackActivity extends AppCompatActivity {
    TextView tv_Parent_FeedBackDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_detail_feedback);
        tv_Parent_FeedBackDetail=findViewById(R.id.tv_Parent_FeedBackDetail);
        tv_Parent_FeedBackDetail.setText(getIntent().getStringExtra("feedbackdestudent"));



        if (getSupportActionBar()!=null){
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Feedback Detail");
        }







    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }

    }
}
