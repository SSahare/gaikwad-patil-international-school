package appsoft.erp.gaikwad.activities.leave_manager;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.annotations.SerializedName;

import appsoft.erp.gaikwad.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RemainingLeavesFragment extends Fragment {
    private TextView typeCL,typePL,typeSL;
    @SerializedName("")
    private String stypeCL;
    @SerializedName("")
    private String stypeSL;
    @SerializedName("")
    private String stypePL;
    public RemainingLeavesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view =  inflater.inflate(R.layout.remaining_leaves, container, false);

        typeCL = view.findViewById(R.id.clRemainingDays);
        typePL = view.findViewById(R.id.plRemainingDays);
        typeSL = view.findViewById(R.id.slRemainingDays);

        getInfoFromServer();
        return view;
    }

    private void getInfoFromServer() {
        // get info and set to text views
        // and set the text here
        // eg. CL   7
        //     SL   5
        //     PL   2
    }
}
