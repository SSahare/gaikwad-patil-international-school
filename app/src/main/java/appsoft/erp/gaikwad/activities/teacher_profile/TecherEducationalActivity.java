package appsoft.erp.gaikwad.activities.teacher_profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.widget.TextView;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TecherEducationalActivity extends AppCompatActivity {
    private SessionManagement sessionManagement;
    private TeacherProfileModel profileModel;
    Animation uptodown, downtoup;
    TextView tvclass, stream, institutename, boarduniversity, passingyear, percentage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teacher_educational_detail);
        sessionManagement = new SessionManagement(getApplicationContext());

        tvclass = findViewById(R.id.classname);
        stream = findViewById(R.id.stream);
        institutename = findViewById(R.id.institutte_name);
        boarduniversity = findViewById(R.id.university);
        passingyear = findViewById(R.id.passingyear);
        percentage = findViewById(R.id.percentage);

        getDataFromServer();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Educational Detail");
        }

    }

    private void getDataFromServer() {


        ApiInterface in = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<TeacherProfileModel> call = in.getTProfileData(sessionManagement.getUserId());
        call.enqueue(new Callback<TeacherProfileModel>() {
            @Override
            public void onResponse(Call<TeacherProfileModel> call, Response<TeacherProfileModel> response) {
                profileModel = response.body();

                if (profileModel.getEducation().size() != 0) {
                    setEducationalDetails();
                }

            }

            @Override
            public void onFailure(Call<TeacherProfileModel> call, Throwable t) {
                Log.i("DEBUG", "Error Ocurred");
            }
        });
    }

    private void setEducationalDetails() {
        tvclass.setText(profileModel.getEducation().get(0).getClass_());
        stream.setText(profileModel.getEducation().get(0).getStream());
        institutename.setText(profileModel.getEducation().get(0).getInstituteName());
        boarduniversity.setText(profileModel.getEducation().get(0).getBoardUniversity());
        passingyear.setText(profileModel.getEducation().get(0).getPassingYear());
        percentage.setText(profileModel.getEducation().get(0).getPercentage());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}


