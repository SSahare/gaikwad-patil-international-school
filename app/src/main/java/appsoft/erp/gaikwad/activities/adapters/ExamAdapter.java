package appsoft.erp.gaikwad.activities.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Random;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.ExamModel;

public class ExamAdapter extends RecyclerView.Adapter<ExamAdapter.ExamHolder> {

    List<ExamModel> list;
    Integer[] colors = {R.color.colorDarkRedBlood,
            R.color.colorDarkAmber,
            R.color.colorDarkIndigo,
            R.color.colorDarkLime,
            R.color.colorDarkPurple,
            R.color.colorDarkYellow,
            R.color.colorDarkGreen,
            R.color.colorDarkStarBlue,
            R.color.colorDarkLimeGreen,
            R.color.colorDarkGrey
    };
    public ExamAdapter(List<ExamModel> list) {
        this.list = list;
    }

    @Override
    public ExamHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.exam_time_table_card, parent, false);
        return new ExamHolder(v);
    }

    @Override
    public void onBindViewHolder(ExamHolder holder, int position) {
        ExamModel model = list.get(position);
        holder.subject.setText(model.getSubject());
        holder.date.setText(String.format("%s ( %s )",model.getDate(), model.getDay()));
        holder.time.setText(model.getTime());
        for(ExamModel em : list){
            Random random = new Random();
            holder.colorStrip.setBackgroundResource(colors[random.nextInt(9)]);
        }
    }

    @Override
    public int getItemCount() {
        return (list == null)? 0 : list.size();
    }

    class ExamHolder extends RecyclerView.ViewHolder{

        TextView date, subject, time;
        View colorStrip;
        ExamHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.tv_date);
            subject = itemView.findViewById(R.id.tv_subject);
            time = itemView.findViewById(R.id.tv_time);
            colorStrip = itemView.findViewById(R.id.color_strip);
        }
    }
}
