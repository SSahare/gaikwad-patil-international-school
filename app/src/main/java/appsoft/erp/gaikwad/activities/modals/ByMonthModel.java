package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ByMonthModel {

    @SerializedName("student_id")
    @Expose
    private String studentId;
    @SerializedName("name")
    @Expose
    private String studentName;
    @SerializedName("att")
    @Expose
    private String att;
    @SerializedName("wd")
    @Expose
    private String wd;

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getAtt() {
        return att;
    }

    public void setAtt(String att) {
        this.att = att;
    }

    public String getWd() {
        return wd;
    }

    public void setWd(String wd) {
        this.wd = wd;
    }
}
