package appsoft.erp.gaikwad.activities.activitys;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;

import java.util.List;

import appsoft.erp.gaikwad.activities.network_broadcast.ConnectionDetector;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.adapters.LeaveAdapter;
import appsoft.erp.gaikwad.activities.modals.Leave;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;

public class LeaveStatus extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager manager;
    private List<Leave> leaveList = null;
    ProgressDialog progressDoalog;
    Animation uptodown;
    private ConstraintLayout constraintLayout;
    private ImageView noDataFound;
    private SessionManagement session;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leave_status_activity);
        constraintLayout = findViewById(R.id.no_internet_layout);
        noDataFound =findViewById(R.id.no_data_found_image);
        Button tryAgain = findViewById(R.id.try_again);

        uptodown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.uptodown);
        recyclerView = findViewById(R.id.leaveRecycler);
        recyclerView.setAnimation(uptodown);
        recyclerView.setHasFixedSize(true);
        manager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(manager);
        session = new SessionManagement(this);


        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectionDetector.isConnected(getApplicationContext())){
                    viewsWhenConnected();
                    getLeaveListFromServer();
                }
            }
        });


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        if (ConnectionDetector.isConnected(getApplicationContext())){
            viewsWhenConnected();
            getLeaveListFromServer();
        }else {
            viewsWhenNotConnected();
        }

    }

    private void getLeaveListFromServer() {

        leaveStatusProgressBar();

        ApiInterface li = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<Leave>> call = li.getLeaves(session.getUserId());
        call.enqueue(new Callback<List<Leave>>() {
            @Override
            public void onResponse(Call<List<Leave>> call, Response<List<Leave>> response) {
                leaveList = response.body();
                if (leaveList == null||leaveList.size()==0){
                    viewsWhenNoDataFound();
                }
                try{

                    LeaveAdapter adapter = new LeaveAdapter(leaveList);
                    viewsWhenConnected();
                    recyclerView.setAdapter(adapter);

                }catch (NullPointerException npe){
                    Toast.makeText(LeaveStatus.this, "Empty Leaves ", Toast.LENGTH_SHORT).show();
                }
                progressDoalog.dismiss();

            }

            @Override
            public void onFailure(Call<List<Leave>> call, Throwable t) {
                viewsWhenNoDataFound();
                progressDoalog.dismiss();

            }
        });
    }

    private void viewsWhenNotConnected() {

        recyclerView.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.VISIBLE);
        noDataFound.setVisibility(View.GONE);

    }

    private void viewsWhenConnected() {

        recyclerView.setVisibility(View.VISIBLE);
        constraintLayout.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);

    }
    private void viewsWhenNoDataFound() {

        recyclerView.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.GONE);
        noDataFound.setVisibility(View.VISIBLE);

    }
    private void leaveStatusProgressBar() {

        progressDoalog = new ProgressDialog(this);
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
        progressDoalog.setMessage("Please Wait...");
//        progressDoalog.getWindow().setGravity(LinearLayout.LayoutParams.MATCH_PARENT LinearLayout.LayoutParams.MATCH_PARENT);
        progressDoalog.setIndeterminate(true);
        Circle wave=new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() ==android.R.id.home){
            finish();
            return true;
        }
        return false;
    }
}
