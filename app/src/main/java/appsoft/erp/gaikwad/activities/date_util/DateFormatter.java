package appsoft.erp.gaikwad.activities.date_util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatter {

    // This Method Changes 2018-09-27 to Sep-27-2018
    public static String getStandardDateFormat(String date){
        String outputDateString = "";
        try {
            // This is Global format
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            // This is Our format
            DateFormat outputFormat = new SimpleDateFormat("MMM-dd-yyyy");

            Date dateObj = inputFormat.parse(date);
            outputDateString = outputFormat.format(dateObj);
        }catch (Exception exc){
            return date;
        }
        return outputDateString;
    }

    public static boolean compareTwoDates(String date1, String date2){

        Date dateObj1 = null;
        Date dateObj2 = null;
        try{
            dateObj1 = new SimpleDateFormat("MMM-dd-yyyy").parse(date1);
            dateObj2 = new SimpleDateFormat("MMM-dd-yyyy").parse(date2);
        }catch (ParseException p_ex){
            p_ex.printStackTrace();
        }catch (Exception exc){
            exc.printStackTrace();
        }
        if (dateObj1 != null && dateObj2 != null){
            if (dateObj1.equals(dateObj2)){
                return true;
            }
        }
        return false;
    }
}
