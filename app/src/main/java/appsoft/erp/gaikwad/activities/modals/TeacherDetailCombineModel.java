package appsoft.erp.gaikwad.activities.modals;

public class TeacherDetailCombineModel {

    private String className;
    private String sectionName;
    private String subjectName;
    private String role;

    public TeacherDetailCombineModel(String className, String sectionName, String subjectName, String role) {
        this.className = className;
        this.sectionName = sectionName;
        this.subjectName = subjectName;
        this.role = role;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
