package appsoft.erp.gaikwad.activities.activitys;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.adapters.TimeTableAdapter;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.volley_singleton.NetworkStatus;

public class ClassTimeTable extends AppCompatActivity implements NetworkStatus{

    private Spinner classname, section, day;
    private Button ok;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mManager;
    private LinearLayout layout;
    private String classNameString = "";
    private String sectionNameString = "";
    private String dayNameString = "";
    ProgressDialog progressDoalog;

    private List<appsoft.erp.gaikwad.activities.modals.ClassTimeTable> list = null;
//    private List<ClassTimeTable> list = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_time_table);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        initialiseData();

        if (isNetworkAvailable()){
           /* setClassNames();
            setSectionNames();*/
        }else{
            final Snackbar snackbar = Snackbar.make(layout,getResources().getString(R.string.no_connection),Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(getResources().getColor(R.color.colorDarkRedBlood));
            snackbar.setAction("RETRY", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isNetworkAvailable()){
                        snackbar.show();
                        /*setClassNames();
                        setSectionNames();*/
                    }
                }
            });
            snackbar.show();
        }

        classname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position>0){
                    classNameString = (String) parent.getSelectedItem();
                    loadTimeTable();
                    progressDoalog.dismiss();
                }
                else {
                    TimeTableAdapter adapter = new TimeTableAdapter(list);
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        section.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position>0){
                    sectionNameString = (String) parent.getSelectedItem();
                    loadTimeTable();
                    progressDoalog.dismiss();
                }
                else {
                    TimeTableAdapter adapter = new TimeTableAdapter(list);
                    recyclerView.setAdapter(adapter);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        day.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0){

                    dayNameString = (String) parent.getSelectedItem();
                    loadTimeTable();
                    progressDoalog.dismiss();
                }
                else {
                    TimeTableAdapter adapter = new TimeTableAdapter(list);
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void initialiseData() {
        classname = findViewById(R.id.spinnerClass);
        section = findViewById(R.id.spinnerSection);
        day = findViewById(R.id.spinnerDay);
        recyclerView = findViewById(R.id.recyclerView);
        mManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(mManager);
//        layout = findViewById(R.id.timetableLayout);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        setClassNames();
        setSectionName();
        setDays();
    }

    private void setClassNames() {
        ArrayAdapter<CharSequence> classAdapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.list_of_class, R.layout.text_filter_view);
        classAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
        classname.setAdapter(classAdapter);
    }

    private void setSectionName(){
        ArrayAdapter<CharSequence> classAdapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.list_of_section, R.layout.text_filter_view);
        classAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
        section.setAdapter(classAdapter);
    }

    private void setDays() {

        ArrayAdapter daysAdapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.list_of_days, R.layout.text_filter_view);
        daysAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
        day.setAdapter(daysAdapter);
    }

    private void loadTimeTable(){

            timetableprogressbar();
        if (isAllFieldFilled() && isNetworkAvailable()){
            ApiInterface ttInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
            Call<List<appsoft.erp.gaikwad.activities.modals.ClassTimeTable>> call = ttInterface.getTeacherTimeTable(classNameString, sectionNameString, dayNameString);

            call.enqueue(new Callback<List<appsoft.erp.gaikwad.activities.modals.ClassTimeTable>>() {
                @Override
                public void onResponse(Call<List<appsoft.erp.gaikwad.activities.modals.ClassTimeTable>> call, retrofit2.Response<List<appsoft.erp.gaikwad.activities.modals.ClassTimeTable>> response) {
                    if (response.isSuccessful()) {
                    list = response.body();
                    try {
                        if (list.size() != 0) {
//                                Log.i("DEB", list.toString());
                            TimeTableAdapter adapter = new TimeTableAdapter(list);
                            recyclerView.setAdapter(adapter);
                        }
                        else  {
                            TimeTableAdapter adapter = new TimeTableAdapter(list);
                            recyclerView.setAdapter(adapter);
                            progressDoalog.dismiss();

                            Toast.makeText(ClassTimeTable.this, "Time Table Not Available.", Toast.LENGTH_SHORT).show();
                        }


                    } catch (Exception npe) {
                        TimeTableAdapter adapter = new TimeTableAdapter(list);
                        recyclerView.setAdapter(adapter);
                        Toast.makeText(getApplicationContext(), "Sorry, Time Table Not Found", Toast.LENGTH_LONG).show();
                    }
                        progressDoalog.dismiss();
                }



//                    }else{
//                        TimeTableAdapter adapter = new TimeTableAdapter(list);
//                        recyclerView.setAdapter(adapter);
//                        Toast.makeText(getApplicationContext(), "Sorry, Time Table Not Found", Toast.LENGTH_LONG).show();
//                    }
                }

                @Override
                public void onFailure(Call<List<appsoft.erp.gaikwad.activities.modals.ClassTimeTable>> call, Throwable t) {
                    progressDoalog.dismiss();
                    Toast.makeText(getApplicationContext(), "Something wrong with server...", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private void timetableprogressbar() {



        progressDoalog = new ProgressDialog(this);
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
        progressDoalog.setMessage("Please Wait...");
//        progressDoalog.getWindow().setGravity(LinearLayout.LayoutParams.MATCH_PARENT LinearLayout.LayoutParams.MATCH_PARENT);
        progressDoalog.setIndeterminate(true);
        Circle wave=new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();
    }

    private boolean isAllFieldFilled() {
        if (classNameString.isEmpty() || sectionNameString.isEmpty() || dayNameString.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean isNetworkAvailable() {
        ConnectivityManager conMgr =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        return netInfo != null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
           finish();
            return true;
        }else{
//            return super.onOptionsItemSelected(item);
            return false;
        }
    }


//    @Override
//    public void onBackPressed() {
//        if
//        super.onBackPressed();
//        finish();
//    }


//    @Override
//    protected void onResume() {
//        super.onResume();
//    }
}