package appsoft.erp.gaikwad.activities.fragments;


import android.app.ActionBar;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.demono.AutoScrollViewPager;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.Attendance;
import appsoft.erp.gaikwad.activities.pager_adapter.ImagePagerAdapter;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment implements  View.OnClickListener, MyAdapter.DashboardListener,OnTitleSetListener {


    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 2500; // time in milliseconds between successive task executio
    final int NUM_PAGES = 3;
    private ViewPager viewPager;
    private ImagePagerAdapter adapter;
    private LinearLayout dotsLinearLayout;
    private ImageButton ib_Next_Page;
    private TextView tv_Today_Schedule,tv_Allotment;
    private TextView[] dotText;
    private CardView homework,assignment,news,circulars,dailyDiary,attendance, teacherDetail;
    private OnCardListener mCardListener;
    private TextView totalHomework, totalAssignment, latestNews, latestCircular, totalSubjectDiary, totalWorkingDay, totalPresentDay;
    private SessionManagement session;
    private RecyclerView tr_view,recycelview_dashboard;
    private AutoScrollViewPager autoScrollViewPager;

private ActionBar toolbar;
    DashboardModel mFlowerData;
    public DashboardFragment() {}

    public static OnTitleSetListener onTitleSetListener = null;

    public static DashboardFragment getInstance(OnTitleSetListener onTitleSetListener2){
        onTitleSetListener=onTitleSetListener2;
        return new DashboardFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_fragment, container, false);
        session = new SessionManagement(getContext());

        autoScrollViewPager=view.findViewById(R.id.autoscrollview);
        adapter= new ImagePagerAdapter(getContext());
        autoScrollViewPager.setAdapter(adapter);
        autoScrollViewPager.startAutoScroll();
        autoScrollViewPager.setCycle(true);

        autoScrollViewPager.setSlideInterval(1500);
        recycelview_dashboard = view.findViewById(R.id.recycelview_dashboard);
        ib_Next_Page = view.findViewById(R.id.ib_Next_Page);
        tv_Allotment = view.findViewById(R.id.tv_Allotment);
        tv_Today_Schedule = view.findViewById(R.id.tv_Today_Schedule);
        tv_Allotment.setOnClickListener(this);
        ib_Next_Page.setOnClickListener(this);
        tv_Today_Schedule.setOnClickListener(this);
//        toolbar =  view.findViewById(R.id.toolbar);
//        ((HomeActivity) getActivity()).getSupportActionBar().hide();
        getDashbaordDetail(view);


        return view;
    }


    private void getDashbaordDetail(View view) {

        final  GridLayoutManager mGridLayoutManager = new GridLayoutManager(getActivity(), 3);
        recycelview_dashboard.setLayoutManager(mGridLayoutManager);

        final List <DashboardModel>mFlowerList = new ArrayList<>();
        mFlowerData=new DashboardModel("Time Table", R.drawable.largetimetableicon);
        mFlowerList.add(mFlowerData);
        mFlowerData=new DashboardModel("Homework", R.drawable.largehomeworkicon);
        mFlowerList.add(mFlowerData);

        mFlowerData=new DashboardModel("Assignment", R.drawable.largeassignmenticon);
        mFlowerList.add(mFlowerData);

        mFlowerData=new DashboardModel("Event", R.drawable.largeeventicon);
        mFlowerList.add(mFlowerData);

        mFlowerData=new DashboardModel("Circular", R.drawable.largecircularicon);
        mFlowerList.add(mFlowerData);

        mFlowerData=new DashboardModel("News", R.drawable.largenewsicon);
        mFlowerList.add(mFlowerData);

        MyAdapter myAdapter = new MyAdapter(getContext(), mFlowerList, this);
        recycelview_dashboard.setAdapter(myAdapter);

    }

    private void instantiateCards(View view) {
//        homework = view.findViewById(R.id.cardHomework);
//        assignment = view.findViewById(R.id.cardAssignment);
//        news = view.findViewById(R.id.cardNews);
//        circulars = view.findViewById(R.id.cardCirculars);
//        dailyDiary = view.findViewById(R.id.cardDailyDairy);
//        attendance = view.findViewById(R.id.cardAttendance);
//        teacherDetail = view.findViewById(R.id.cardTeacherDetail);
//
//        homework.setOnClickListener(this);
//        assignment.setOnClickListener(this);
//        news.setOnClickListener(this);
//        circulars.setOnClickListener(this);
//        dailyDiary.setOnClickListener(this);

//        if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER)){
//            attendance.setVisibility(View.GONE);
//        }
//        if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_PARENT)){
//            teacherDetail.setVisibility(View.GONE);
//        }
    }

//    private void getDotsIndicator(int position) {
//        try {
//            dotText = new TextView[4];
//            dotsLinearLayout.removeAllViews();
//
//            for (int i = 0; i < dotText.length; i++) {
//                dotText[i] = new TextView(getActivity().getApplicationContext());
//                dotText[i].setText(Html.fromHtml("&#8226;"));
//                dotText[i].setTextSize(40);
//                dotText[i].setTextColor(Color.parseColor("#FFFFFF"));
//                dotsLinearLayout.addView(dotText[i]);
//            }
//        }catch (NullPointerException npe){
//
//        }
//
//        if (dotText.length > 0) {
//            dotText[position].setTextColor(Color.TRANSPARENT);
//        }
//    }

//    @Override
//    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//    }

//    @Override
//    public void onPageSelected(int position) {
//        try {
//            getDotsIndicator(position);
//        }catch (Exception e){
//            Log.i("DEBUG", "Exc: "+e.toString());
//        }
//        currentPage = position;
//    }

//    @Override
//    public void onPageScrollStateChanged(int state) {
//
//    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View view) {

        if (view.getId()==R.id.tv_Allotment){

            getFragmentManager().beginTransaction().replace(R.id.frameForFragments,ViewAllotmentFragment.getInstance(this)).addToBackStack(null).commit();
        }
      if (view.getId()==R.id.tv_Today_Schedule){
          getFragmentManager().beginTransaction().replace(R.id.frameForFragments,new ViewTodaySchedule()).addToBackStack(null).commit();

      }

    }

    @Override
    public void getGridPosition(int position) {


            if (position == 0) {
                getFragmentManager().beginTransaction().replace(R.id.frameForFragments, TimeTable_Fragment.getInstance(this)).addToBackStack(null).commit();


            }
            if (position == 1) {

                getFragmentManager().beginTransaction().replace(R.id.frameForFragments, ViewHomeworkListFragment.getInstance(this)).addToBackStack(null).commit();
            }
            if (position == 2) {

                getFragmentManager().beginTransaction().replace(R.id.frameForFragments, ViewAssignmentListFragment.getInstance(this)).addToBackStack(null).commit();
            }
            if (position == 3) {
                getFragmentManager().beginTransaction().replace(R.id.frameForFragments, EventFragment.getInstance(this)).addToBackStack(null).commit();

            }
            if (position == 4) {


                getFragmentManager().beginTransaction().replace(R.id.frameForFragments, CircularFragment.getInstance(this)).addToBackStack(null).commit();

            }
            if (position == 5) {

                getFragmentManager().beginTransaction().replace(R.id.frameForFragments, NewsFragment.getInstance(this)).addToBackStack(null).commit();

            }
        }

    @Override
    public void setOnTitleText(String titleText) {

            onTitleSetListener.setOnTitleText(titleText);

    }


    public interface OnCardListener{
        void setOnCardClickListener(String cardName);
    }

//
    private void getTeacherDetail(final View v) {

    }
    private void getDashboardAttendance(){
        ApiInterface attendApi = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<Attendance> call = attendApi.getDashboardAttendance(session.getUserId());
        Log.i("DEBUG", "Dash ID: "+session.getUserId());
        call.enqueue(new Callback<Attendance>() {
            @Override
            public void onResponse(Call<Attendance> call, Response<Attendance> response) throws NullPointerException{
                if (response.isSuccessful()) { // 200 to 300
                    Attendance obj = response.body();
//                    totalWorkingDay.setText(String.valueOf(obj.getW_days()));
//                    totalPresentDay.setText(String.valueOf(obj.getP_days()));
                }
            }

            @Override
            public void onFailure(Call<Attendance> call, Throwable t) {
                Log.i("DEBUG", "Attendance Exp: "+t.toString());
            }
        });
    }

}
