package appsoft.erp.gaikwad.activities.extras;

import java.util.Random;

import appsoft.erp.gaikwad.R;

public class RandomColors {


     static    int[] darkColorNumber = {R.color.colorDarkRedBlood
                ,R.color.colorDarkPurple
                ,R.color.colorDarkIndigo
                ,R.color.colorDarkStarBlue
                ,R.color.colorDarkGreen
                ,R.color.colorDarkLime
                ,R.color.colorDarkYellow
                ,R.color.colorDarkAmber
                ,R.color.colorDarkLimeGreen
                ,R.color.colorDarkGrey};

     static    int[] lightColorNumber = {R.color.colorLightRed
                ,R.color.colorLightPurple
                ,R.color.colorLightIndigo
                ,R.color.colorLightStarBlue
                ,R.color.colorLightGreen
                ,R.color.colorLightLime
                ,R.color.colorLightYellow
                ,R.color.colorLightAmber
                ,R.color.colorLightLimeGreen
                ,R.color.colorLightGrey};

    private static int getRandomValue() {
        Random random = new Random();
        return random.nextInt(9);
    }


    public static class LightColors{

        public static int setColor(){
            int randValue = getRandomValue();
            return lightColorNumber[randValue];
        }
    }

    public static class DarkColors{

        public static int setColor(){
            int randValue = getRandomValue();
            return darkColorNumber[randValue];
        }

    }
}
