package appsoft.erp.gaikwad.activities.pager_adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import appsoft.erp.gaikwad.R;

public class ImagePagerAdapter extends PagerAdapter{

    Context context;
    ImageView ivImage;
//    String[] urls = {
//            "http://www.emax-systems.co.uk/wp-content/uploads/2018/03/erp.idge_.jpg",
//            "http://www.michellgroup.com/wp-content/uploads/2014/02/erp-mens-watches-1024x835.jpg",
//            "http://www.7seasinfra.com/images/pro03.jpg",
//            "http://techstreet.in/images/products/cms.png"
//    };
int[] urls={R.drawable.gaikwadpatil,R.drawable.kitflying,R.drawable.staffgaikwadpatil,R.drawable.playingstudent};


    public ImagePagerAdapter(Context context){
        this.context = context;
    }

    @Override
    public int getCount() {
        return urls.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.pager_adapter,container,false);
        ivImage = view.findViewById(R.id.images);

        try {
            Glide.with(context)
                    .load(urls[position])
                    .into(ivImage);
        }catch (Exception e){ e.printStackTrace();}

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager)container).removeView((View)object);
    }
}
