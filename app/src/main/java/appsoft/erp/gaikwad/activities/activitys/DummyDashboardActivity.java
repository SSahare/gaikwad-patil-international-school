//package appsoft.erp.isync.activities.activitys;
//
//import android.Manifest;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.design.widget.NavigationView;
//import android.support.v4.app.ActivityCompat;
//import android.support.v4.content.ContextCompat;
//import android.support.v4.view.GravityCompat;
//import android.support.v4.widget.DrawerLayout;
//import android.support.v7.app.ActionBarDrawerToggle;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.Toolbar;
//import android.util.Log;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.WindowManager;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.bumptech.glide.Glide;
//
//import appsoft.erp.isync.R;
//import appsoft.erp.isync.activities.extras.Constants;
//import appsoft.erp.isync.activities.fragments.CircularFragment;
//import appsoft.erp.isync.activities.fragments.DashboardFragment;
//import appsoft.erp.isync.activities.fragments.EventFragment;
//import appsoft.erp.isync.activities.fragments.FragmentActivity;
//import appsoft.erp.isync.activities.fragments.NewsFragment;
//import appsoft.erp.isync.activities.fragments.ViewAssignmentListFragment;
//import appsoft.erp.isync.activities.fragments.ViewFeedbackListFragment;
//import appsoft.erp.isync.activities.fragments.ViewHomeworkListFragment;
//import appsoft.erp.isync.activities.fragments.ViewLogFragment;
//import appsoft.erp.isync.activities.parent_activity.FeesRecordActivity;
//import appsoft.erp.isync.activities.parent_activity.ParentClassTimeTable;
//import appsoft.erp.isync.activities.parent_activity.ReportActivity;
//import appsoft.erp.isync.activities.parents_fragments.ParentAttendanceFragment;
//import appsoft.erp.isync.activities.profile.ProfilrActivity;
//import appsoft.erp.isync.activities.session.SessionManagement;
//import appsoft.erp.isync.activities.teacher_profile.TeacherProfileActivity;
//import de.hdodenhof.circleimageview.CircleImageView;
//
//import static appsoft.erp.isync.activities.extras.Constants.ATTACHMENT_BASE_URL;
//import static appsoft.erp.isync.activities.extras.Constants.STUDENT_SUB_URL;
//import static appsoft.erp.isync.activities.extras.Constants.TEACHER_SUB_URL;
//
//public class DummyDashboardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener {
//
//
//    private static final Object FragmentActivity =1 ;
//    private CircleImageView profileImage;
//    public TextView profileType, profileName;
//    private Toolbar toolbar;
//    private DashboardFragment mDashboardFragment;
//    private static final int CALENDER_PERMISSION = 100;
//    public static boolean IS_CALENDER_PERMISSION = false;
//    private SessionManagement session;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        setContentView(R.layout.home_activity);
//
//        session = new SessionManagement(getApplicationContext());
//        //session.checkLogin();
//        DrawerLayout drawer = findViewById(R.id.drawer_layout);
//
//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.addDrawerListener(toggle);
//        toggle.syncState();
//
//        toolbar.setTitle("Dashboard");
//        getSupportFragmentManager().beginTransaction().add(R.id.frameForFragments, (DashboardFragment) attachedDashboard()).addToBackStack(null).commit();
//
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        setNavigationalDrawer();
//        navigationView.setNavigationItemSelectedListener(this);
//    }
//
//    private void setNavigationalDrawer() {
//
//        String imageUrl = "";
//        NavigationView navigationView = findViewById(R.id.nav_view);
//        View headerView = navigationView.getHeaderView(0);
//
//        Intent i = getIntent();
//
//        profileName = headerView.findViewById(R.id.profileName);
//        profileImage = headerView.findViewById(R.id.profileImage);
//        profileType = headerView.findViewById(R.id.profileType);
//        LinearLayout layout = headerView.findViewById(R.id.nav_header_layout);
//
//
//        layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (session.getUserRole().equalsIgnoreCase(Constants.LOGIN_TYPE_AS_PARENT)) {
//                    Intent prIntent = new Intent(DummyDashboardActivity.this, ProfilrActivity.class);
//                    startActivity(prIntent);
//                } else if (session.getUserRole().equalsIgnoreCase(Constants.LOGIN_TYPE_AS_TEACHER)) {
//                    Intent prIntent = new Intent(DummyDashboardActivity.this, TeacherProfileActivity.class);
//                    startActivity(prIntent);
//                }
//            }
//        });
//
//        if (i != null) {
//            String fName = i.getStringExtra("KEY_FirstName");
//            String lName = i.getStringExtra("KEY_LastName");
//            String userRole = i.getStringExtra("KEY_Role");
//            int userId = i.getIntExtra("KEY_ID", 0);
//            String imagePathName = i.getStringExtra("KEY_ImagePath");
//
//            if (userRole.trim().equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
//                imageUrl = ATTACHMENT_BASE_URL + TEACHER_SUB_URL + imagePathName;
//                //setProfileImage(teacherImageUrl);
//            }
//            if (userRole.trim().equals(Constants.LOGIN_TYPE_AS_PARENT)) {
//                imageUrl = ATTACHMENT_BASE_URL + STUDENT_SUB_URL + imagePathName;
//                //setProfileImage(studentImageUrl);
//            }
//
//            session.createLoginSession(fName, lName, userId, imageUrl, userRole, imagePathName);
//
//            profileName.setText(session.getUserName());
//            profileType.setText(session.getUserRole() + "(" + session.getUserId() + ")");
//            setProfileImage(session.getImagePath());
//
//            if (session.getUserRole().trim().equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
//                navigationView.inflateMenu(R.menu.teacher_menu_list);
//            }
//            if (session.getUserRole().trim().equals(Constants.LOGIN_TYPE_AS_PARENT)) {
//                navigationView.inflateMenu(R.menu.parent_menu_list);
//            }
//        } else {
//            Toast.makeText(getApplicationContext(), "Not a valid user. Try again.", Toast.LENGTH_LONG).show();
//            finish();
//        }
//    }
//
//    private void setProfileImage(String imageUrl) {
//
//        if (session.getImageLastPath().isEmpty() || session.getImageLastPath().equals(null) || session.getImageLastPath() == "") {
//            try {
//                Glide.with(getApplicationContext())
//                        .load(getResources().getDrawable(R.drawable.person))
//                        .override(100, 100)
//                        .fitCenter()
//                        .into(profileImage);
//            } catch (Exception e) {
//            }
//        } else {
//            try {
//                Glide.with(getApplicationContext())
//                        .load(imageUrl)
//                        .override(100, 100)
//                        .fitCenter()
//                        .into(profileImage);
//            } catch (Exception e) {
//            }
//        }
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR)
//                != PackageManager.PERMISSION_GRANTED) {
//
//            if (ActivityCompat.shouldShowRequestPermissionRationale(DummyDashboardActivity.this, Manifest.permission.WRITE_CALENDAR)) {
//                AlertDialog.Builder builder = new AlertDialog.Builder(DummyDashboardActivity.this);
//                builder.setTitle("Calender Permission");
//                builder.setMessage("This app needs calender permission.");
//                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.cancel();
//                        ActivityCompat.requestPermissions(DummyDashboardActivity.this, new String[]{Manifest.permission.WRITE_CALENDAR}, CALENDER_PERMISSION);
//                    }
//                });
//                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.cancel();
//                    }
//                });
//                builder.show();
//            } else {
//                //just request the permission
//                ActivityCompat.requestPermissions(DummyDashboardActivity.this, new String[]{Manifest.permission.WRITE_CALENDAR}, CALENDER_PERMISSION);
//            }
//        } else {
//            IS_CALENDER_PERMISSION = true;
//        }
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.home, menu);
//        return true;
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == Constants.GOTO_LOGIN_PAGE_REQ) {
//            // Logout User From Here
//            Toast.makeText(this, "Logout Successfully..", Toast.LENGTH_SHORT).show();
//            finish();
//        }
//    }
//
//    @Override
//    public void onBackPressed() {
//
//        DrawerLayout drawer = findViewById(R.id.drawer_layout);
//        int count = getSupportFragmentManager().getBackStackEntryCount();
//
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        }
//        if (count >= 1) {
//            Toast.makeText(this, "Press Back One More Time To Exit", Toast.LENGTH_SHORT).show();
//            Log.i("TAG", "" + getSupportFragmentManager().getBackStackEntryCount());
//            toolbar.setTitle("Dashboard");
//            getSupportFragmentManager().popBackStack(0, 0);
//            getSupportFragmentManager().beginTransaction().add(R.id.frameForFragments, new DashboardFragment()).commit();
//        }
//        if (count == 0) {
//            finish();
//        } else {
//            super.onBackPressed();
//        }
//    }
//
//    private Object attachedDashboard() {
//        if (mDashboardFragment == null) {
//            mDashboardFragment = new DashboardFragment();
//        }
//        return mDashboardFragment;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//
//        if (id == R.id.profile) {
//            if (session.getUserRole().equalsIgnoreCase(Constants.LOGIN_TYPE_AS_PARENT)) {
//                Intent profileIntent = new Intent(DummyDashboardActivity.this, ProfilrActivity.class);
//                startActivity(profileIntent);
//            } else if (session.getUserRole().equalsIgnoreCase(Constants.LOGIN_TYPE_AS_TEACHER)) {
//                Intent profileIntent = new Intent(DummyDashboardActivity.this, TeacherProfileActivity.class);
//                startActivity(profileIntent);
//            }
//            return true;
//
//        } else if (id == R.id.logout) {
//            //session.checkLogin();
//            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
//            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivityForResult(i, Constants.GOTO_LOGIN_PAGE_REQ);
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    @SuppressWarnings("StatementWithEmptyBody")
//    @Override
//    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//        DrawerLayout drawer = findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//
//        if ((session.getUserRole()).equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
//            switch (item.getItemId()) {
//
//                case R.id.dashboard:
//                    toolbar.setTitle("Dashboard");
//                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, (DashboardFragment) attachedDashboard()).addToBackStack(null).commit();
////                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments ,(FragmentActivity) attachedDashboard()).addToBackStack(null).commit()
//                    break;
//
//                case R.id.attendance_t:
//                    Intent attendanceIntent = new Intent(DummyDashboardActivity.this, AttendenceActivity.class);
//                    startActivity(attendanceIntent);
//                    break;
//
//                case R.id.view_attendance_t:
//                    Intent viewAttendanceIntent = new Intent(DummyDashboardActivity.this, ViewAttendanceActivity.class);
//                    startActivity(viewAttendanceIntent);
//                    break;
//
//                case R.id.add_assignment:
//                    Intent addAssignIntent = new Intent(DummyDashboardActivity.this, AddAssignmentActivity.class);
//                    startActivity(addAssignIntent);
//                    return true;
//
//                case R.id.view_assignment_t:
//                    toolbar.setTitle("Assignment List");
//                    //fTransaction.replace(R.id.frameForFragments,new ViewAssignmentListFragment()).commit();
//                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, new ViewAssignmentListFragment()).addToBackStack(null).commit();
//                    break;
//
//                case R.id.add_homework:
//                    Intent addHomeworkIntent = new Intent(DummyDashboardActivity.this, AddHomeworkActivity.class);
//                    startActivity(addHomeworkIntent);
//                    break;
//
//                case R.id.view_homework_t:
//                    toolbar.setTitle("Homework List");
//                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, new ViewHomeworkListFragment()).addToBackStack(null).commit();
//                    break;
//
//                case R.id.add_daily_log:
//                    Intent addDailyLog = new Intent(DummyDashboardActivity.this, AddLogActivity.class);
//                    startActivity(addDailyLog);
//                    break;
//
//                case R.id.view_daily_log:
//                    toolbar.setTitle("Subject Dairy");
//                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, new ViewLogFragment()).addToBackStack(null).commit();
//                    break;
//
//                case R.id.class_time_table:
//                    Intent classTimetableIntent = new Intent(DummyDashboardActivity.this, ClassTimeTable.class);
//                    startActivity(classTimetableIntent);
//                    break;
//
//                case R.id.news:
//                    toolbar.setTitle("News");
//                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, new NewsFragment()).addToBackStack(null).commit();
//                    break;
//
//                case R.id.circular:
//                    toolbar.setTitle("Circulars");
//                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, new CircularFragment()).addToBackStack(null).commit();
//                    break;
//
//                case R.id.add_feedback:
//                    Intent addFeedbackIntent = new Intent(DummyDashboardActivity.this, FeedbackActivity.class);
//                    startActivity(addFeedbackIntent);
//                    break;
//
//                case R.id.view_feedback_t:
//                    toolbar.setTitle("Feedbacks");
//                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, new ViewFeedbackListFragment()).addToBackStack(null).commit();
//                    break;
//
//                case R.id.leave_manager:
//                    Intent leaveIntent = new Intent(DummyDashboardActivity.this, LeaveManagerAct.class);
//                    startActivity(leaveIntent);
//                    return true;
//
//                case R.id.event:
//                    toolbar.setTitle("Events");
//                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, new EventFragment()).addToBackStack(null).commit();
//                    break;
//            }
//            // drawer.closeDrawer(GravityCompat.START);
//        }
//        if ((session.getUserRole()).equals(Constants.LOGIN_TYPE_AS_PARENT)) {
//
//            switch (item.getItemId()) {
//
//                case R.id.dashboard:
//                    toolbar.setTitle("Dashboard");
//                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, (DashboardFragment) attachedDashboard()).addToBackStack(null).commit();
//                    break;
//
//                case R.id.attendance:
//                    toolbar.setTitle("Attendance");
//                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, new ParentAttendanceFragment()).addToBackStack(null).commit();
//                    break;
//
//                case R.id.class_time_table:
//                    Intent classTimeTable = new Intent(DummyDashboardActivity.this, ParentClassTimeTable.class);
//                    startActivity(classTimeTable);
//                    break;
//
//                case R.id.report_card:
//                    Intent repIntent = new Intent(DummyDashboardActivity.this, ReportActivity.class);
//                    startActivity(repIntent);
//                    break;
//
//                case R.id.assignment:
//                    toolbar.setTitle("Assignment List");
//                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, new ViewAssignmentListFragment()).addToBackStack(null).commit();
//                    break;
//
//                case R.id.homework:
//                    toolbar.setTitle("Homework List");
//                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, new ViewHomeworkListFragment()).addToBackStack(null).commit();
//                    break;
//
//                case R.id.sub_dairy:
//                    toolbar.setTitle("Subject Dairy");
//                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, new ViewLogFragment()).addToBackStack(null).commit();
//                    break;
//
//                case R.id.news:
//                    toolbar.setTitle("News");
//                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, new NewsFragment()).addToBackStack(null).commit();
//                    break;
//
//                case R.id.circular:
//                    toolbar.setTitle("Circulars");
//                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, new CircularFragment()).addToBackStack(null).commit();
//                    break;
//
//                case R.id.fees_record:
//                    Intent feesRecord = new Intent(DummyDashboardActivity.this, FeesRecordActivity.class);
//                    startActivity(feesRecord);
//                    break;
//
//                case R.id.feedback:
//                    toolbar.setTitle("Feedback");
//                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, new ViewFeedbackListFragment()).addToBackStack(null).commit();
//                    break;
//
//                case R.id.event:
//                    toolbar.setTitle("Events");
//                    getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments, new EventFragment()).addToBackStack(null).commit();
//                    break;
//            }
//            //drawer.closeDrawer(GravityCompat.START);
//        }
//        return true;
//    }
//
//
////    @Override
////    public void setOnCardClickListener(String cardName) {
////        switch (cardName){
////            case Constants.CARD_ASSIGNMENT:
////                toolbar.setTitle("Assignment List");
////                getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments,new ViewAssignmentListFragment()).addToBackStack(null).commit();
////                break;
////            case Constants.CARD_HOMEWORK:
////                toolbar.setTitle("Homework List");
////                getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments,new ViewHomeworkListFragment()).addToBackStack(null).commit();
////                break;
////            case Constants.CARD_NEWS:
////                toolbar.setTitle("News");
////                getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments,new NewsFragment()).addToBackStack(null).commit();
////                break;
////            case Constants.CARD_CIRCULARS:
////                toolbar.setTitle("Circulars");
////                getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments,new CircularFragment()).addToBackStack(null).commit();
////                break;
////            case Constants.CARD_DAIRY:
////                toolbar.setTitle("Daily Diary");
////                getSupportFragmentManager().beginTransaction().replace(R.id.frameForFragments,new ViewLogFragment()).addToBackStack(null).commit();
////                break;
////            default:
////                break;
////        }
////    }
//
//    @Override
//    public void onPointerCaptureChanged(boolean hasCapture) {
//
//    }
//
//    @Override
//    public void onClick(View view) {
//
//    }
//}
