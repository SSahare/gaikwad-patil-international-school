package appsoft.erp.gaikwad.activities.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.UserProfileModel;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressDetailActivity extends AppCompatActivity {
    private SessionManagement session;
    UserProfileModel model;
    TextView address,permanentaddress;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.address_activity);

        session = new SessionManagement(getApplicationContext());

        address=findViewById(R.id.tv_address);
        permanentaddress=findViewById(R.id.tv_permanent_Address);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Address Detail");
        }
        getDataFromServer();

    }

    private void getDataFromServer() {

        ApiInterface in = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<UserProfileModel> call = in.getProfileData(session.getUserId());
        call.enqueue(new Callback<UserProfileModel>() {
            @Override
            public void onResponse(Call<UserProfileModel> call, Response<UserProfileModel> response) {
                model = response.body();

                setAddressDetails();;

            }


            @Override
            public void onFailure(Call<UserProfileModel> call, Throwable t) {

                Log.i("DEBUG", "Error Ocurred");
            }
        });
    }

    private void setAddressDetails() {
        address.setText(model.getLocalAddress());
        permanentaddress.setText(model.getPermanentAddress());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
