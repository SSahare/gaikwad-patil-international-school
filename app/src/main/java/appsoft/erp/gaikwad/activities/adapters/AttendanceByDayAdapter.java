package appsoft.erp.gaikwad.activities.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.ByDayModel;

public class AttendanceByDayAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ByDayModel byDayObj;
    private List<ByDayModel> byDayModelList;

    public AttendanceByDayAdapter(List<ByDayModel> byDayModelList) {
        this.byDayModelList = byDayModelList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            View view = inflater.inflate(R.layout.by_date_card, parent, false);
            return new AttendanceViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        byDayObj = byDayModelList.get(position);

            ((AttendanceViewHolder) holder).stdName.setText(byDayObj.getStudentName());
            ((AttendanceViewHolder) holder).status.setText(byDayObj.getAtt());
        if (byDayObj.getAtt().equals("A")) {
            ((AttendanceViewHolder) holder).status.setTextColor(Color.RED);
        }
       if  (byDayObj.getAtt().equals("P"))
        {
            ((AttendanceViewHolder) holder).status.setTextColor(Color.WHITE);
        }
    }

    @Override
    public int getItemCount() {
        try{
            return byDayModelList.size();
        }catch (NullPointerException exc){
            Log.i("DEBUG", exc.toString());
            return 0;
        }
    }

    class AttendanceViewHolder extends RecyclerView.ViewHolder{

        private TextView stdName, status;

        public AttendanceViewHolder(View itemView) {
            super(itemView);
            stdName = itemView.findViewById(R.id.studentAttendanceName);
            status = itemView.findViewById(R.id.studentStatus);
        }
    }
}