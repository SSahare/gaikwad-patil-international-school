package appsoft.erp.gaikwad.activities.teacher_profile;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeacherProfileActivity extends AppCompatActivity {

    private int lastExpandedPosition = -1;
    private ExpandableListView expandableListView;
    List<String> Basic_detail;
    List<String> Educational_detail;
    List<String> Address_detail;
    private String[] heading_item;
    private TextView teacherFullName;
    private TextView  teacherId;
    //private TextView  studentClassSection;
    private CircleImageView teacherProfilePhoto;
    private SessionManagement session;
    private TeacherProfileModel profileModel;
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_profile);

        session = new SessionManagement(getApplicationContext());
        heading_item = getResources().getStringArray(R.array.teacher_profile_heading);
        expandableListView = findViewById(R.id.profileListView);
        teacherFullName = findViewById(R.id.teacherName);
        //studentClassSection = findViewById(R.id.studentClassSection);
        teacherId = findViewById(R.id.teacherId);
        teacherProfilePhoto = findViewById(R.id.teacherProfilePhoto);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        getDataFromServer();
        teacherFullName.setText(session.getUserName());
        teacherId.setText(String.valueOf(session.getUserId()));
        Log.i("DEBUG", session.getImagePath());
        if (session.getImageLastPath().isEmpty() || session.getImageLastPath().equals(null) || session.getImageLastPath() == ""){
            Glide.with(this)
                    .load("")
                    .placeholder(getResources().getDrawable(R.drawable.person))
                    .fitCenter()
                    .into(teacherProfilePhoto);
        }else {
            Glide.with(this)
                    .load(session.getImagePath())
                    .fitCenter()
                    .into(teacherProfilePhoto);
        }

        List<String> Heading = new ArrayList<String>();

        Basic_detail= new ArrayList<String>();
        Educational_detail = new ArrayList<String>();
        Address_detail = new ArrayList<String>();

        HashMap<String, List<String>> childList= new HashMap<String,List<String>>();

        for (String title: heading_item)
        {
            Heading.add(title);
        }

        childList.put(Heading.get(0),Basic_detail);
        childList.put(Heading.get(1),Educational_detail);
        childList.put(Heading.get(2),Address_detail);

        TeacherProfileAdapter profileAdapter = new TeacherProfileAdapter(Heading,childList, this);

        expandableListView.setAdapter(profileAdapter);
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    expandableListView.collapseGroup(lastExpandedPosition);

                }
                lastExpandedPosition = groupPosition;
            }
        });
    }

    private void setBasicDetails() {

        Basic_detail.add("Name: "+profileModel.getBasic().getFirstName()+" "+profileModel.getBasic().getMiddleName()+" "+profileModel.getBasic().getLastName());
        Basic_detail.add("Gender: "+profileModel.getBasic().getGender());
        Basic_detail.add("Designation: "+profileModel.getBasic().getDesignation());
        Basic_detail.add("Date of Birth: "+profileModel.getBasic().getDateOfBirth());
        Basic_detail.add("Aadhar No.: "+profileModel.getBasic().getAdharNumber());
        Basic_detail.add("Contact: "+profileModel.getBasic().getMobileNo());
        Basic_detail.add("Alternate Contact: "+profileModel.getBasic().getAlternateNo());
        Basic_detail.add("Email: "+profileModel.getBasic().getEmail());
        Basic_detail.add("Alternate Email: "+profileModel.getBasic().getAlternateEmail());
        Basic_detail.add("Appointment Date: "+profileModel.getBasic().getAppointmentDate());
        Basic_detail.add("Experience: "+profileModel.getBasic().getExperience());
        Basic_detail.add("Blood Group: "+profileModel.getBasic().getBloodGroup());
        Basic_detail.add("Medical Details: "+profileModel.getBasic().getMedicalHistory());
    }

    private void setEducationalDetails(){
        Educational_detail.add("Class: "+profileModel.getEducation().get(0).getClass_());
        Educational_detail.add("Stream: "+profileModel.getEducation().get(0).getStream());
        Educational_detail.add("Institute Name: "+profileModel.getEducation().get(0).getInstituteName());
        Educational_detail.add("Board University: "+profileModel.getEducation().get(0).getBoardUniversity());
        Educational_detail.add("Passing Year: "+profileModel.getEducation().get(0).getPassingYear());
        Educational_detail.add("Percentage: "+profileModel.getEducation().get(0).getPercentage());
    }

    private void setAddressDetails(){
        Address_detail.add("Local Address: "+profileModel.getBasic().getLocal());
        Address_detail.add("Permanent Address: "+profileModel.getBasic().getPermanent());
    }

    private void getDataFromServer(){
        ApiInterface in = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<TeacherProfileModel> call = in.getTProfileData(session.getUserId());
        call.enqueue(new Callback<TeacherProfileModel>() {
            @Override
            public void onResponse(Call<TeacherProfileModel> call, Response<TeacherProfileModel> response) {
                profileModel = response.body();

                if (profileModel.getEducation().size() != 0){
                    setEducationalDetails();
                }

                setBasicDetails();
                setAddressDetails();
            }

            @Override
            public void onFailure(Call<TeacherProfileModel> call, Throwable t) {
                Log.i("DEBUG", "Error Ocurred");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
            return true;
        }
        return false;
    }
}
