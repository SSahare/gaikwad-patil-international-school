package appsoft.erp.gaikwad.activities.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.ByMonthModel;

public class AttendanceByMonthAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ByMonthModel byMonthObj;
    private List<ByMonthModel> byMonthModelList;

    public AttendanceByMonthAdapter(List<ByMonthModel> byMonthModelList) {
        this.byMonthModelList = byMonthModelList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = inflater.inflate(R.layout.by_month_card, parent, false);
            return new AttendanceViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            byMonthObj = byMonthModelList.get(position);
        Float present =Float.parseFloat(byMonthObj.getAtt());
        Float working = Float.parseFloat(byMonthObj.getWd());
        Float percent = (present/working)*100;

            Log.i("DEBUGC", byMonthObj.getStudentName() + " " + byMonthObj.getAtt() + " " + byMonthObj.getWd());

            // Do here list fight
            ((AttendanceViewHolder) holder).studName.setText(byMonthObj.getStudentName());
            ((AttendanceViewHolder) holder).presentDays.setText(byMonthObj.getAtt());
            ((AttendanceViewHolder) holder).workingDays.setText(byMonthObj.getWd());
        ((AttendanceViewHolder) holder).studentPercent.setText(String.format("%.0f", percent));

    }

    @Override
    public int getItemCount() {
        try{
            return byMonthModelList.size();
        }catch (NullPointerException exc){
            Log.i("DEBUG", exc.toString());
            return 0;
        }
    }

    /*@Override
    public int getItemViewType(int position) {
        position -= 1;
        if (isPositionHeader(position))
            return TYPE_HEADER;
        return TYPE_ITEM;
    }*/

   /* private boolean isPositionHeader(int position) {
        return position == -1;
    }*/

    class AttendanceViewHolder extends RecyclerView.ViewHolder{

        private TextView studName, presentDays, workingDays;
        TextView studentPercent;


        public AttendanceViewHolder(View itemView) {
            super(itemView);
            studName = itemView.findViewById(R.id.studentAttendanceName);
            presentDays = itemView.findViewById(R.id.studentPresentDays);
            workingDays = itemView.findViewById(R.id.studentWorkingDays);
            studentPercent = itemView.findViewById(R.id.studentPercent);

        }
    }

    /*class AttendanceHeaderViewHolder extends RecyclerView.ViewHolder{

        private TextView studNameHeader, presentDayHeader, workingDayHeader;

        public AttendanceHeaderViewHolder(View itemView) {
            super(itemView);
            studNameHeader = itemView.findViewById(R.id.studentAttendanceNameHeader);
            presentDayHeader = itemView.findViewById(R.id.studentPresentDayHeader);
            workingDayHeader = itemView.findViewById(R.id.studentWorkingDayHeader);
        }
    }*/
}
