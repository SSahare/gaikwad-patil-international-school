package appsoft.erp.gaikwad.activities.activitys;

import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;

import com.daimajia.androidanimations.library.Techniques;
import com.viksaa.sssplash.lib.activity.AwesomeSplash;
import com.viksaa.sssplash.lib.cnst.Flags;
import com.viksaa.sssplash.lib.model.ConfigSplash;

import appsoft.erp.gaikwad.R;

public class SplaceActivity extends AwesomeSplash {

    private Handler handler;
    private View mContentView;

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);


    @Override
    public void initSplash(ConfigSplash configSplash) {
        //        ActionBar actionBar=getSupportActionBar();
//        actionBar.hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        configSplash.setBackgroundColor(R.color.colorWhite);
        configSplash.setAnimCircularRevealDuration(1000);
        configSplash.setRevealFlagX(Flags.REVEAL_LEFT);

        configSplash.setRevealFlagX(Flags.REVEAL_BOTTOM);
// set the logo
        configSplash.setLogoSplash(R.drawable.updatedlogo);
        configSplash.setAnimLogoSplashDuration(3000);
        configSplash.setAnimLogoSplashTechnique(Techniques.BounceIn);

        // set the title

        configSplash.setTitleSplash(getString(R.string.title));

        configSplash.setTitleTextColor(R.color.colorBlack);
//        configSplash.setTitleFont("values/preloaded_fonts.xml");
        configSplash.setTitleTextSize(23f);
        configSplash.setAnimTitleTechnique(Techniques.FlipInX);


    }

    @Override
    public void animationsFinished() {
        Intent newLoginIntent = new Intent(this, LoginActivity.class);
        startActivityForResult(newLoginIntent, 123);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 123) {
            finish();
        }
    }

    //        setContentView(R.layout.activity_splace);
//
//        statusBarTransparent();
//        callThread();
//        LinearLayout logoView = findViewById(R.id.logo_fade_in_layout);
//        try {
//            Animation startAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in_zoom);
//            logoView.startAnimation(startAnimation);
//        }catch (Exception exp){
//            Log.d("DEBUG", exp.getMessage());
//        }
//
//        handler = new Handler(){
//            @SuppressLint("RestrictedApi")
//            @Override
//            public void handleMessage(Message msg) {
//                super.handleMessage(msg);
//                // We find req id here
//                int requestCode = msg.arg1;
//                Intent startLogin = new Intent(SplaceActivity.this, LoginActivity.class);
//                ImageView isync_logo = findViewById(R.id.isync_logo);
//                ActivityOptionsCompat opt = ActivityOptionsCompat.makeSceneTransitionAnimation(
//                        SplaceActivity.this,
//                        isync_logo,
//                        ViewCompat.getTransitionName(isync_logo)
//                );
//              startActivityForResult(startLogin,requestCode ,opt.toBundle());
//
//            }
//        };
//    }
//
//    private void callThread(){
//        new Thread(){
//            @Override
//            public void run() {
//                try{
//                    sleep(3000);
//                    handler.sendEmptyMessage(101);
//                }catch(Exception exp){
//                    Log.i("DEBUG", "Splace Exception: "+exp.toString());
//                }
//            }
//        }.start();
//    }
//
//    private void statusBarTransparent(){
//        Window window = this.getWindow();
//        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
//            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorBackForLogo));
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == 101){
//            finish();
//        }
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        finish();
//    }
}
