package appsoft.erp.gaikwad.activities.volley_singleton;

public interface NetworkStatus {

    boolean isNetworkAvailable();
}
