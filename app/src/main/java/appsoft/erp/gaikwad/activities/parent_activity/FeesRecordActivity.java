package appsoft.erp.gaikwad.activities.parent_activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.google.gson.JsonSyntaxException;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.adapters.FeesLogAdapter;
import appsoft.erp.gaikwad.activities.modals.Fees;
import appsoft.erp.gaikwad.activities.modals.FeesRecord;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;

public class FeesRecordActivity extends AppCompatActivity {

    private TextView schoolFees;
    private TextView totalAmountPaid;
    private TextView totalAmountBal;
    private PieChartView feesChart;
    private RecyclerView mRecyclerView;
    private List<FeesRecord> feeRecordList = null;
    private SessionManagement session;
    ProgressDialog progressDoalog;

    ArrayList<String> feesTypes = new ArrayList<>();
    String[] types = {"Paid","Unpaid"};
ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fees_record_layout);
        initialiseData();

        session = new SessionManagement(getApplicationContext());
        getFeesRecordLogFromServer();

        RecyclerView.LayoutManager mManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mManager);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
//        mRecyclerView.addItemDecoration(new SimpleItemDecoration(this));
//        feesChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
//            @Override
//            public void onValueSelected(Entry e, Highlight h) {
//                Toast.makeText(FeesRecordActivity.this, ""+e.toString(), Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onNothingSelected() {
//
//            }
//        });
    }

    @SuppressLint("RestrictedApi")
    private void initialiseData() {

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

//        schoolFees = findViewById(R.id.totalFees);
        totalAmountBal = findViewById(R.id.totalFeesBalance);
        totalAmountPaid = findViewById(R.id.totalFeesPaid);
        feesChart = findViewById(R.id.feesChart);
//        progressBar=findViewById(R.id.progressbar_FeesRecord);

        mRecyclerView = findViewById(R.id.feesRecycler);

//        Description desc = new Description();
//        desc.setText("Annual School Fees in Rupees");
//        desc.setTextSize(12);
//        feesChart.setDescription(desc);
//        feesChart.setRotationEnabled(true);
//        feesChart.setHoleColor(Color.CYAN);
//        feesChart.setHoleRadius(30.0f);
//        feesChart.setCenterText("School Fees");
//        feesChart.setCenterTextSize(12.0f);
//        feesChart.setDrawHoleEnabled(false);

//        feesChart.setTransparentCircleAlpha(0);
//        feesChart.animateY(1300, Easing.EasingOption.EaseInCubic);

        for (String tp : types){
            feesTypes.add(tp);
            feesChart.setChartRotationEnabled(false);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        getFeesRecordFromServer();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
            return true;
        }else{
            return false;
//            return super.onOptionsItemSelected(item);
        }
    }

    private void getFeesRecordFromServer(){
        ApiInterface apiFees = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<Fees> call = apiFees.getStudentFeesRecord(session.getUserId());
        call.enqueue(new Callback<Fees>() {
            @Override
            public void onResponse(Call<Fees> call, @NotNull Response<Fees> response) {
                if (response.isSuccessful()){
                    try {
                        Log.i("DEBUG", "ResponseModel String: "+response.toString());
                        Fees obj = response.body();
//                       schoolFees.setText(String.valueOf(obj.getTotalSchoolFees()));
                        totalAmountPaid.setText(String.valueOf("Paid Fees : "+obj.getTotalFeesPaid()));
                        totalAmountBal.setText(String.valueOf("Balance Fees : "+obj.getBalancedFee()));
                        ArrayList<SliceValue> feeEntry = new ArrayList<>();

//                        feeEntry.add(new SliceValue(  obj.getTotalFeesPaid(), "Paid fee"));  //
//                        feeEntry.add(obj.getTotalFeesPaid(),);

                        feeEntry.add(new SliceValue((float) obj.getTotalFeesPaid(),Color.BLACK).setLabel("Paid fee\n"+obj.getTotalFeesPaid()));
                        feeEntry.add(new SliceValue((float)obj.getBalancedFee(),getResources().getColor(R.color.orange)).setLabel("Balance fee\n"+obj.getBalancedFee()));




//                        feeEntry.add(new PieChartView(float) obj.getTotalFeesPaid())
//                        feeEntry.add(new SliceValue((float) obj.getBalancedFee(), "Balance fee"));  //(float) obj.getBalancedFee()

//                          feeEntry.add(new SliceValue(100,Color.BLACK).setLabel("p"));
//                        feeEntry.add(new SliceValue(200,Color.RED).setLabel("e"));

                        setEntryToPieDataSet(feeEntry);
//                        setEntryToPieDataSet(feeEntry);


                    }catch (NullPointerException npe){ Log.i("DEBUG", "Fees NULL: "+npe.toString());
                    }catch (IllegalStateException ise){Log.i("DEBUG", "Fees Illegal"+ise.toString());
                    }catch (JsonSyntaxException jse){Log.i("DEBUG", "Fees JSON"+jse.toString());}
                }

            }

            @Override
            public void onFailure(Call<Fees> call, Throwable t) {
                Log.i("DEBUG", "Failure Exception: "+t.toString());
            }
        });
    }

    private void getfeesProgressdialog() {
        progressDoalog = new ProgressDialog(this);
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
//        progressDoalog.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.WRAP_CONTENT, RecyclerView.LayoutParams.WRAP_CONTENT));

        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
        progressDoalog.setIndeterminate(true);
        Circle wave=new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();
    }

    private void setEntryToPieDataSet(ArrayList<SliceValue> feeEntry){
        PieChartData pieChartData=new PieChartData(feeEntry);
        pieChartData.setSlicesSpacing(10);
        pieChartData.setValueLabelTextSize(12);
        pieChartData.setHasLabels(true);
//        progressBar.setVisibility(View.VISIBLE);



//        PieDataSet pieDataSet = new PieDataSet(feeEntry,"");
//        pieDataSet.setSliceSpace(1.0f);
//        pieDataSet.setValueTextSize(12);
//
//        ArrayList<Integer> colorArray = new ArrayList<>();
//        colorArray.add(getResources().getColor(R.color.black_gradient));
//        colorArray.add(getResources().getColor(R.color.orange));
//        colorArray.add(Color.BLACK);
//        colorArray.add(Color.YELLOW);







       // pieDataSet.setColors(colorArray);
//             Legend legend = new Legend();
//        legend.setForm(Legend.LegendForm.CIRCLE);
//        legend.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
        PieChartData data=new PieChartData(pieChartData);
//        PieData dataPie = new PieData(pieChartData);
//        feesChart.setData(dataPie);
        feesChart.setPieChartData(data);
        feesChart.setVisibility(View.VISIBLE);
        feesChart.invalidate();
    }

    private void getFeesRecordLogFromServer(){
        getfeesProgressdialog();
        ApiInterface logInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<FeesRecord>> call = logInterface.getStudentFeesLog(session.getUserId());
        call.enqueue(new Callback<List<FeesRecord>>() {
            @Override
            public void onResponse(Call<List<FeesRecord>> call, Response<List<FeesRecord>> response){
                feeRecordList = response.body();
                try{
                    if (feeRecordList != null){
                        FeesLogAdapter adapter = new FeesLogAdapter(getApplicationContext(), feeRecordList);
                        mRecyclerView.setAdapter(adapter);
                        progressDoalog.dismiss();

                    }

                    else{
                        Toast.makeText(getApplicationContext(), R.string.list_empty, Toast.LENGTH_SHORT).show();
                        progressDoalog.dismiss();
                    }
                    progressDoalog.dismiss();

                }catch (NullPointerException npe){
                    Log.i("DEBUG", npe.toString());
                }
                progressDoalog.dismiss();

            }

            @Override
            public void onFailure(Call<List<FeesRecord>> call, Throwable t) {
                Log.i("DEBUG", "Empty Hai");
                progressDoalog.dismiss();

            }
        });
    }
}
