package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MonthlyDetailAttendanceModel {
    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("attendance_status")
    @Expose
    private String attendanceStatus;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAttendanceStatus() {
        return attendanceStatus;
    }

    public void setAttendanceStatus(String attendanceStatus) {
        this.attendanceStatus = attendanceStatus;
    }
}
