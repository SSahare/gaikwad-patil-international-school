package appsoft.erp.gaikwad.activities.activitys;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import appsoft.erp.gaikwad.activities.helper.AddActionHelper;
import appsoft.erp.gaikwad.activities.interfaces.AddActionInterface;
import retrofit2.Call;
import retrofit2.Callback;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.extras.DatePickerFragment;
import appsoft.erp.gaikwad.activities.modals.ResponseModel;
import appsoft.erp.gaikwad.activities.modals.Subject;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import appsoft.erp.gaikwad.activities.volley_singleton.NetworkStatus;

public class AddHomeworkActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener
        ,NetworkStatus {

    private Spinner classname, section, subject;
//    private TextInputEditText description;
    private EditText description;
    private EditText title;
    private TextView submissionDate;
    private CoordinatorLayout layout;
    Subject subjectObj = null;

    private static int flag_class = 0;
    private static int flag_section = 0;
    private String classNameString = "";
    private String sectionNameString = "";
    private String subjectNameString = "";
    private SessionManagement session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_homework_layout);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        session = new SessionManagement(this);
        initialiseData();

        if (isNetworkAvailable()) {
            setClassNames();
        } else {
            final Snackbar snackbar = Snackbar.make(layout, getResources().getString(R.string.no_connection), Snackbar.LENGTH_INDEFINITE);
            snackbar.setActionTextColor(getResources().getColor(R.color.colorDarkRedBlood));
            snackbar.setAction("RETRY", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isNetworkAvailable()) {
                        snackbar.show();
                    } else {
                        setClassNames();
                    }
                }
            });
            snackbar.show();
        }

        classname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position>0){
                    classNameString = classname.getItemAtPosition(position).toString();
                    AddActionHelper.getSection(getApplicationContext(), classNameString, new AddActionInterface() {
                        @Override
                        public void getDataListener(List<String> list) {
                            ArrayAdapter<String> secAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.text_filter_view, list);
                            secAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
                            section.setAdapter(secAdapter);
                        }
                    });
                }else {
                    classNameString = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        section.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    sectionNameString = section.getItemAtPosition(position).toString();
                    AddActionHelper.getSubjects(getApplicationContext(), classNameString, sectionNameString, new AddActionInterface(){
                        @Override
                        public void getDataListener(List<String> list) {
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.text_filter_view, list);
                            adapter.setDropDownViewResource(R.layout.text_drop_down_theme);
                            subject.setAdapter(adapter);
                        }
                    });
                }else {
                    sectionNameString = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        subject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position>0){
                    subjectNameString = (String) parent.getSelectedItem();
                }else {
                    subjectNameString = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

   /* private void setArrayAdapterTosubject(List<String> subjectList){
        try{
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.text_filter_view, subjectList);
            adapter.setDropDownViewResource(R.layout.text_drop_down_theme);
            subject.setAdapter(adapter);
        }catch (NullPointerException npe){
            Log.i("DEBUG", "No subject");
        }catch (Exception exp){
            Log.i("DEBUG", "Exception");
        }
    }*/



    /*rivate void getSubjectTeacherDetail() {
        ApiInterface i = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<Subject>> call = i.getStudentDetail(new SessionManagement(getApplicationContext()).getUserId());
        call.enqueue(new Callback<List<Subject>>() {
            @Override
            public void onResponse(Call<List<Subject>> call, Response<List<Subject>> response) {
                if (response.isSuccessful()) {
                    List<Subject> myClass = response.body();
                    parseData(myClass);
                } else {
                    Toast.makeText(AddHomeworkActivity.this, "Oops, class not found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Subject>> call, Throwable t) {
                Log.i("DEBUG", "onFailure: " + t.getMessage());
                Toast.makeText(AddHomeworkActivity.this, "Server failure: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });



    }

    private void parseData(List<Subject> myClass) {
        // Filter data here
        Set<String> classes = new HashSet<>();
        Set<String> subjects = new HashSet<>();
        Set<String> sections = new HashSet<>();

        try {
            for (Subject sub : myClass) {
                classes.add(sub.getClass_());
                subjects.add(sub.getSubject());
                sections.add(sub.getSection());
            }
        }catch (NullPointerException npe){npe.printStackTrace();}

        List<String> clslist = new ArrayList<>();
        clslist.add("Select class");
        clslist.addAll(classes);
        setArrayAdapterToClasses(clslist);

        List<String> sublist = new ArrayList<>();
        sublist.add("Select subject");
        sublist.addAll(subjects);
        setArrayAdapterToSubject(sublist);

        List<String> seclist = new ArrayList<>();
        seclist.add("Select section");
        seclist.addAll(sections);
        setArrayAdapterToSection(seclist);
    }*/



    private void setArrayAdapterToClasses(List<String> classList) {/*
        try {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.text_filter_view, classList);
            adapter.setDropDownViewResource(R.layout.text_drop_down_theme);
            classname.setAdapter(adapter);

        } catch (NullPointerException npe) {
            Log.i("DEBUG", "No subject");
        } catch (Exception exp) {
            Log.i("DEBUG", "Exception");
        }
    }*/

  /*  private void setArrayAdapterToSubject(List<String> subjectList) {
        try {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.text_filter_view, subjectList);
            adapter.setDropDownViewResource(R.layout.text_drop_down_theme);
            subject.setAdapter(adapter);

        } catch (NullPointerException npe) {
            Log.i("DEBUG", "No subject");
        } catch (Exception exp) {
            Log.i("DEBUG", "Exception");
        }*/
    }

   /* private void setArrayAdapterToSection(List<String> sectionList) {
        try {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.text_filter_view, sectionList);
            adapter.setDropDownViewResource(R.layout.text_drop_down_theme);
            section.setAdapter(adapter);

        } catch (NullPointerException npe) {
            Log.i("DEBUG", "No subject");
        } catch (Exception exp) {
            Log.i("DEBUG", "Exception");
        }
    }*/





    @SuppressLint("RestrictedApi")
    private void initialiseData() {
        classname = findViewById(R.id.spinnerClass);
        section = findViewById(R.id.spinnerSection);
        subject = findViewById(R.id.spinnerSubject);
        description = findViewById(R.id.description);
        title = findViewById(R.id.assignmentTitle);
        submissionDate = findViewById(R.id.dateOfSubmission);
        layout = findViewById(R.id.homeworkLayout);
        submissionDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callDatePicker(view);
            }
        });

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        setClassNames();


    }

    private void setClassNames() {

        AddActionHelper.getClasses(getApplicationContext(), new AddActionInterface() {
            @Override
            public void getDataListener(List<String> list) {
                if (list!=null){
                    ArrayAdapter<String> arrayAdapter =new ArrayAdapter<>(getApplicationContext(),R.layout.text_filter_view,list);
                    arrayAdapter.setDropDownViewResource(R.layout.text_drop_down_theme);
                    classname.setAdapter(arrayAdapter);
                }else {
                    Toast.makeText(AddHomeworkActivity.this, "Subject is not assigned to you", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }




    private void callDatePicker(View view){
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.show(getFragmentManager(),"DatePick");
    }

    public void addHomework(View view){

        // Here we add homework data on server
        if (isAllFieldFilled()){

            if (isNetworkAvailable()){

                ApiInterface home_add_interface = ClientAPI.getRetrofit().create(ApiInterface.class);
                Call<ResponseModel> call = home_add_interface.addHomework(
                        session.getUserId(),
                        classNameString,
                        sectionNameString,
                        submissionDate.getText().toString().trim(),
                        subjectNameString,
                        title.getText().toString().trim(),
                        description.getText().toString()
                );

                call.enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call, retrofit2.Response<ResponseModel> response) {
                        try {
                            ResponseModel responses = response.body();
                            Toast toast = Toast.makeText(getApplicationContext(), ""+responses.getRes(), Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            refreshView();
                        }catch (Exception exp){
                            exp.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {
                        Toast toast = Toast.makeText(getApplicationContext(), "Oops, Homework Not Submitted", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        Log.i("DEBUG", "Exception : "+t.toString());
                        refreshView();
                    }
                });
            }

        }else{
            Toast.makeText(this, R.string.check_validations, Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isAllFieldFilled() {

        if (title.getText().toString().trim().isEmpty()){
            return false;
        }else if (description.getText().toString().trim().isEmpty()) {
            return false;
        }else if (classNameString.trim().isEmpty()){
                return false;
            }else if (sectionNameString.trim().isEmpty()){
                return false;

        }else if (subjectNameString.trim().isEmpty()){
            return false;
        }else if (submissionDate.getText().toString().trim().isEmpty()){
            return false;
        }else {
            return true;
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        Calendar calendar = new GregorianCalendar(year,month,day);
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.ENGLISH);
        submissionDate.setText(dateFormat.format(calendar.getTime()));
    }

    @Override
    public boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        return networkInfo!=null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.finish();
            return true;
        }else{
            return super.onOptionsItemSelected(item);
        }
    }

    private void refreshView(){

        description.setText("");
        title.setText("");
        submissionDate.setText("");
    }
}