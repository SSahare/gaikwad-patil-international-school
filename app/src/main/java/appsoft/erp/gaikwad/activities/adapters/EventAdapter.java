package appsoft.erp.gaikwad.activities.adapters;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.provider.CalendarContract;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.Event;
import appsoft.erp.gaikwad.activities.open_results.EventActivity;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.EventViewHolder>{

    private Context context;
    private List<Event> eventList;
    private String timeAndDateText = "From %s , %s To %s , %s";
    //private String timeText = "Event timing from %s to %s";
    Event events;

    public EventAdapter(Context context, List<Event> eventList){
        this.context = context;
        this.eventList = eventList;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view  = inflater.inflate(R.layout.event_card, parent, false);
        return new EventViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        events = eventList.get(position);

//        holder.date.setText(String.format( events.getStartDate(), events.getEndDate()));
//        holder.time.setText(String.format( events.getStartTime(), events.getEndTime()));
        holder.date.setText(String.format(timeAndDateText,events.getStartDate(),events.getStartTime(),events.getEndDate(),events.getEndTime()));

//        holder.date.setText(StringUtils.capitalize(String.format(timeAndDateText,events.getStartDate(),events.getStartTime(),events.getEndDate(),events.getEndTime())).toLowerCase().trim());
//        holder.date.setText (String.format(timeAndDateText,(StringUtils.capitalize(events.getStartDate()).toLowerCase().trim()),events.getStartTime(),events.getEndDate(),events.getEndTime()));

//        holder.date.setText(StringUtils.capitalize(events.getStartDate(),events.getStartTime(),events.getEndDate(),events.getEndTime()).toLowerCase().trim());
        holder.eventTitle.setText(events.getEventTitle());
        holder.eventTitle.setText(StringUtils.capitalize(events.getEventTitle().toLowerCase().trim()));
        holder.address.setText(StringUtils.capitalize(events.getAddress().toLowerCase().trim()));
//        holder.location.setText(events.getLocation());
//        holder.location.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        holder.location.setText(StringUtils.capitalize(events.getLocation().toLowerCase().trim()));


//        if ((eventList.size()-1) == position){
//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//            // add rule
//            //params.addRule(LinearLayout.ALIGN_BOTTOM,R.id.ll_status);
//            params.setMargins(0, 0, 0, 0);
//            holder.cardView.setLayoutParams(params);
//        }





//        holder.type.setText(events.getFeesType());
        if (events.getFeesAmount()!=null){
            holder.feeAmount.setText("INR " + events.getFeesAmount());
        }else{
            holder.feeAmount.setText("Free");
        }
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        try {
             //Y-m-d
             Date startDateInDate = format.parse(events.getStartDate());
             long timeDifferenceBetweenCurrentAndStartDate = startDateInDate.getTime() - System.currentTimeMillis();
             if (timeDifferenceBetweenCurrentAndStartDate < 0){
//                holder.register.setText("Event Registration Expired.");
//                holder.register.setEnabled(false);
//                holder.register.setBackgroundColor(context.getResources().getColor(R.color.colorLightRed));
            }
        }catch (Exception exp){ Log.i("DEBUG", "Time Exce : "+exp.toString()); }
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    public void filterList(List<Event> list){
        eventList = new ArrayList<>();
        eventList.addAll(list);
        notifyDataSetChanged();
    }

    class EventViewHolder extends RecyclerView.ViewHolder{

        private TextView date, address, time, location, type, eventTitle, feeAmount;
        private Button register;//, viewMap;
        CardView cardView;

        public EventViewHolder(View itemView) {
            super(itemView);

            date = itemView.findViewById(R.id.date);
//            time = itemView.findViewById(R.id.time);
            cardView=itemView.findViewById(R.id.card);
            address = itemView.findViewById(R.id.address);
            location = itemView.findViewById(R.id.location);
//            type = itemView.findViewById(R.id.typeOfEvent);
            eventTitle = itemView.findViewById(R.id.eventTitle);
            register = itemView.findViewById(R.id.register);
            //viewMap = itemView.findViewById(R.id.viewMap);
            feeAmount = itemView.findViewById(R.id.eventAmount);

            register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Register here for that event
                    int pos = getAdapterPosition();
                    final Event eventObj = eventList.get(pos);
                    SessionManagement session = new SessionManagement(context);
                    ApiInterface enroll = ClientAPI.getRetrofit().create(ApiInterface.class);
                    Call<String> call;
                    if (session.getUserRole().equals("parents")) {
                        call = enroll.getEnrollment(Integer.parseInt(eventObj.getEventId()), session.getUserId());
                    }else{
                        call = enroll.getEnrollmentTeacher(Integer.parseInt(eventObj.getEventId()), session.getUserId());
                    }

                    call.enqueue(new Callback<String>(){
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            String res = response.body();
                            Log.i("DEBUG", "Enroll response: "+res);
                            try {
                                if (res.equalsIgnoreCase("first")) {
                                    Toast.makeText(context, "Thanks for Enrollment!", Toast.LENGTH_SHORT).show();
                                    addEventsInCalender(eventObj);
                                } else {
                                    Toast.makeText(context, "You are already registered with this event!", Toast.LENGTH_SHORT).show();
                                }
                            }catch (Exception exp){
                                Log.i("DEBUG", "Enrollment Error : "+exp.getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            android.util.Log.i("DEBUG", "Enroll Exception: "+t.toString());
                        }
                    });
                }
            });

            /*viewMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = null;
                    Intent chooser = null;

                    i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse("geo:19.0760,72.8777"));
                    chooser = Intent.createChooser(i,"Launch Map");
                    context.startActivity(chooser);
                }
            });*/

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();
                    Event eventObj = eventList.get(pos);

                    Intent eventIntent = new Intent(context, EventActivity.class);
                    eventIntent.putExtra("title",eventObj.getEventTitle());
                    eventIntent.putExtra("address",eventObj.getAddress());
                    eventIntent.putExtra("location",eventObj.getLocation());
                    eventIntent.putExtra("entryType",eventObj.getFeesType());
                    eventIntent.putExtra("desc",eventObj.getEventDesc());
                    context.startActivity(eventIntent);
                }
            });
        }

        private void addEventsInCalender(Event eventss) {

            long calId = 3;
            long startDate = 0;
            long endDate = 0;

            ContentResolver cr = context.getContentResolver();
            ContentValues cv = new ContentValues();

            //Calendar s = Calendar.getInstance();
            //s.set(2018, 05,01, 20, 00);
            //s.set()
            //Date sDate = new Date()
            //s.setTime();

            //Calendar s = Calendar.getInstance();
            SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");

            String sDate = eventss.getStartDate() +" "+ eventss.getStartTime();
            Date sdt;

            try {
                 sdt = sFormat.parse(sDate);
                 startDate = sdt.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }

            String eDate = eventss.getEndDate() +" "+ eventss.getEndTime();
            Date edt;
            try {
                edt = sFormat.parse(eDate);
                endDate = edt.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }

            //startDate = s.getTimeInMillis();
            /*Calendar c = Calendar.getInstance();
            c.set(2018, 05, 02, 21, 36);
            endDate = c.getTimeInMillis();*/


            cv.put(CalendarContract.Events.TITLE, eventss.getEventTitle());
            cv.put(CalendarContract.Events.EVENT_LOCATION, eventss.getAddress());
            cv.put(CalendarContract.Events.DTSTART, startDate);
            cv.put(CalendarContract.Events.DTEND, endDate);
            cv.put(CalendarContract.Events.CALENDAR_ID, calId);
            cv.put(CalendarContract.Events.EVENT_TIMEZONE, "Asia/Kolkata");

            //cv.put(CalendarContract.Events.MAX_REMINDERS, 2);
            //cv.put(CalendarContract.Reminders.MINUTES, 100);

            if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR)
                    != PackageManager.PERMISSION_GRANTED) {
                android.util.Log.i("TAG", "Permission Denied");
            }else{
                cr.insert(CalendarContract.Events.CONTENT_URI, cv);
            }
        }
    }
}