package appsoft.erp.gaikwad.activities.adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.extras.Constants;
import appsoft.erp.gaikwad.activities.modals.News;
import appsoft.erp.gaikwad.activities.open_results.NewsDetailActivity;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {

    private List<News> newsList;
    private Context context;
    private News newsObj;

    public NewsAdapter(Context context, List<News> newsList) {
        this.context = context;
        this.newsList = newsList;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.news_card, parent, false);
        return new NewsViewHolder(view);
    }


    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {

        newsObj = newsList.get(position);
        holder.title.setText(newsObj.getNewsTitle());
        holder.publishDate.setText(newsObj.getDateOfPublish());
        Document docs = Jsoup.parse(newsObj.getNewsDescription());
        String description = docs.text();

        holder.description.setText(description);

    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }


    public void filteredList(List<News> nl) {
        newsList = new ArrayList<>();
        newsList.addAll(nl);
        notifyDataSetChanged();
    }

    class NewsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView description, title, publishDate;
        CardView card;
        ImageView im_News;


        public NewsViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);
            title = itemView.findViewById(R.id.newsTitle);
            description = itemView.findViewById(R.id.newsDescription);
            publishDate = itemView.findViewById(R.id.publishDate);
            card = itemView.findViewById(R.id.card);
            im_News = itemView.findViewById(R.id.im_News);


        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            News newsObj = newsList.get(pos);
//            newsListerner.getitempostion(pos);
            Intent newsIntent;
            Pair[] pair = new Pair[4];
            pair[0] = new Pair(im_News, "iv_imageshared");
            pair[1] = new Pair(publishDate, "tv_public_date_imageshared");
            pair[2] = new Pair(title, "tv_title_imageshared");
            pair[3] = new Pair(description, "tv_description_imageshared");

            newsIntent = new Intent(context, NewsDetailActivity.class);
            newsIntent.putExtra("title", newsObj.getNewsTitle());
            newsIntent.putExtra("date", newsObj.getDateOfPublish());
            newsIntent.putExtra("desc", newsObj.getNewsDescription());
            newsIntent.putExtra("flag", Constants.NEWS_FLAG);
            ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation((Activity) context, pair);
            context.startActivity(newsIntent, activityOptions.toBundle());

        }


    }
}