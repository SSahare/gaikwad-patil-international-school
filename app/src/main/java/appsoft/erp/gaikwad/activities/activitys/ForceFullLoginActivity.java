package appsoft.erp.gaikwad.activities.activitys;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import retrofit2.Callback;
import retrofit2.Response;

public class ForceFullLoginActivity  extends AppCompatActivity {
    EditText newpassword,confirmpassword;
    Button save;
    private static final int GO_TO_LOGIN_REQ = 201;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_force_full_login);
        newpassword = findViewById(R.id.et_New_Password);
        confirmpassword = findViewById(R.id.et_Confirm_New_Password);
        save = findViewById(R.id.btn_Save);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isNetworkAvailable()) {
                    if (passwordmatch()) {
                        ApiInterface apiInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
                        retrofit2.Call<Boolean> call = apiInterface.getForceFulResponse(getIntent().getStringExtra("username"),
                                newpassword.getText().toString(), confirmpassword.getText().toString());

                        save.setText("Saving...");

                        call.enqueue(new Callback<Boolean>() {
                            @Override
                            public void onResponse(retrofit2.Call<Boolean> call, Response<Boolean> response) {
                                Boolean res = response.body();

                                if (res) {
                                    save.setText("Save");
                                    Toast forcePassChangeMsg = Toast.makeText(getApplicationContext(), "Password change successfully.", Toast.LENGTH_LONG);
                                    forcePassChangeMsg.setGravity(Gravity.CENTER, 0, 0);
                                    forcePassChangeMsg.show();
                                    Intent newLogin = new Intent(getApplicationContext(), LoginActivity.class);
                                    startActivityForResult(newLogin, GO_TO_LOGIN_REQ);

                                } else {
                                    save.setText("Save");
                                    Toast passwordnotchange = Toast.makeText(getApplicationContext(), "Password not change.", Toast.LENGTH_LONG);
                                    passwordnotchange.setGravity(Gravity.CENTER, 0, 0);
                                    passwordnotchange.show();

                                }
                            }

                            @Override
                            public void onFailure(retrofit2.Call<Boolean> call, Throwable t) {

                                Toast internaleorror = Toast.makeText(getApplicationContext(), "Internal Error Occur", Toast.LENGTH_LONG);
                                internaleorror.show();

                            }
                        });
                    } else {
                        Toast.makeText(ForceFullLoginActivity.this, "Password does not match", Toast.LENGTH_LONG).show();
                        return;
                    }
                } else {
                    Toast.makeText(ForceFullLoginActivity.this, "Network unavailable or unable to access internet.", Toast.LENGTH_LONG).show();
                    return;
                }

            }

        });


    }




    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        return networkInfo != null;
    }
        private boolean passwordmatch() {

        String pass=newpassword.getText().toString();
        String confirm=confirmpassword.getText().toString();

        if (pass.equals(confirm)){
            return true;

        }else {
            return false;
        }



    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GO_TO_LOGIN_REQ);
        {
            finish();
        }

    }
}
