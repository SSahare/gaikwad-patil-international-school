package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.SerializedName;

public class ResponseModel {

    @SerializedName("response")
    private String res = "";

    public String getRes() {
        return res;
    }

    public void setRes(String res) {
        this.res = res;
    }
}
