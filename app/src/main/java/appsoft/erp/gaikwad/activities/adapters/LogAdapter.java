package appsoft.erp.gaikwad.activities.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.Log;
import appsoft.erp.gaikwad.activities.open_results.LogDetailActivity;

public class LogAdapter extends RecyclerView.Adapter<LogAdapter.LogViewHolder>{

    private String dateText = "%s\n%s\n%s";
    private List<Log> logList;
    private Context context;
    private Log logItem;
    public LogAdapter(Context context, List<Log> logList){
        this.context = context;
        this.logList = logList;
        logItem = new Log();
    }

    @Override
    public LogViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.log_card,parent,false);
        return new LogViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LogViewHolder holder, int position) {
        logItem = logList.get(position);
        String strlog=logItem.getLogDate();
        String [] arrofstringlog=strlog.split("-");
        holder.givenDateMonth.setText(arrofstringlog[0]);
        holder.givenDateDay.setText(arrofstringlog[1]);
        holder.givenDateYear.setText(arrofstringlog[2]);

        holder.subject.setText(logItem.getLogSubject());
        holder.classname.setText(("("+logItem.getClassname()+" "+","+" "+logItem.getSection())+")");

        holder.logDetails.setText(logItem.getDescription());
        if ((logList.size()-1) == position){
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            // add rule
            params.setMargins(0, 0, 0, 0);
            holder.cardLog.setLayoutParams(params);
        }
    }

    @Override
    public int getItemCount() {
        return logList.size();
    }

    public void filter(List<Log> list){
        logList = new ArrayList<>();
        logList.addAll(list);
        notifyDataSetChanged();
    }

    class LogViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView date,classname,section,logDetails,subject;
        private CardView cardLog;
        TextView  givenDateMonth, givenDateDay, givenDateYear ;

        public LogViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            givenDateMonth = itemView.findViewById(R.id.dateofGivenMonth);
            givenDateDay = itemView.findViewById(R.id.dateofGivenDay);
            givenDateYear = itemView.findViewById(R.id.dateofGivenYear);

            classname = itemView.findViewById(R.id.classname);
            section = itemView.findViewById(R.id.section);
            logDetails = itemView.findViewById(R.id.description);
            subject = itemView.findViewById(R.id.subject);
            cardLog = itemView.findViewById(R.id.cardLog);
        }

        @Override
        public void onClick(View view) {
            int postion=getAdapterPosition();
            Log log=logList.get(postion);
            Intent logIntent=new Intent(context,LogDetailActivity.class);
            logIntent.putExtra("logclass",log.getClassname());
          logIntent.putExtra("logsection",log.getSection());
            logIntent.putExtra("logdate",log.getLogDate());
            logIntent.putExtra("logdescriptiion",log.getDescription());
            logIntent.putExtra("logsubject",log.getLogSubject());
            context.startActivity(logIntent);


        }
    }
}
