package appsoft.erp.gaikwad.activities.activitys;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.SelectableStudents;
import appsoft.erp.gaikwad.activities.modals.StudentsName;

public class StudentsAttendanceAdapter extends RecyclerView.Adapter<AttendanceHolder> implements AttendanceHolder.OnItemSelectedListener{

    private boolean isMultiSelectionEnabled;
    private List<SelectableStudents> selectableStudentList = new ArrayList<>();
    AttendanceHolder.OnItemSelectedListener mListener;

    public StudentsAttendanceAdapter(AttendanceHolder.OnItemSelectedListener mListener,
                                     List<StudentsName> studentsNameList,
                                     boolean isMultiSelectionEnabled) {
        this.mListener = mListener;
        this.isMultiSelectionEnabled = isMultiSelectionEnabled;

        for (StudentsName obj: studentsNameList){
            selectableStudentList.add(new SelectableStudents(obj, true));
            Log.i("DEBUG", "SLS: "+selectableStudentList.size());
        }

        //mListener.onItemSelected()
    }

    @Override
    public AttendanceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.students_list_adapter, parent, false);
        return new AttendanceHolder(itemView, mListener);
    }

    @Override
    public void onBindViewHolder(AttendanceHolder holder, int position) {
        SelectableStudents selectableStudents = selectableStudentList.get(position);
        String name = selectableStudents.toString();
        String roll = selectableStudents.getStudentId();

        holder.studentName.setText(name);
        holder.rollno.setText(roll);

        if (isMultiSelectionEnabled) {
            TypedValue value = new TypedValue();
            holder.studentName.getContext().getTheme().resolveAttribute(android.R.attr.listChoiceIndicatorMultiple, value, true);
            int checkMarkDrawableResId = value.resourceId;
            holder.studentName.setCheckMarkDrawable(checkMarkDrawableResId);
        } else {
            TypedValue value = new TypedValue();
            holder.studentName.getContext().getTheme().resolveAttribute(android.R.attr.listChoiceIndicatorSingle, value, true);
            int checkMarkDrawableResId = value.resourceId;
            holder.studentName.setCheckMarkDrawable(checkMarkDrawableResId);
        }

        holder.mItem = selectableStudents;
        holder.setChecked(holder.mItem.isSelected());
    }

    @Override
    public int getItemCount() {
        Log.i("DEBUG", "Size of List: "+selectableStudentList.size());
        return selectableStudentList.size();
    }

    public List<StudentsName> getSelectedItems() {

        List<StudentsName> selectedItems = new ArrayList<>();
        for (SelectableStudents item : selectableStudentList) {
            if (item.isSelected()) {
                selectedItems.add(item);
            }
        }
        Log.i("DEBUG", "Selectable list: "+selectedItems.toString());
        return selectedItems;
    }

    @Override
    public int getItemViewType(int position) {
        if(isMultiSelectionEnabled){
            return AttendanceHolder.MULTI_SELECTION;
        }
        else{
            return AttendanceHolder.SINGLE_SELECTION;
        }
    }

    @Override
    public void onItemSelected(SelectableStudents item) {
        if (!isMultiSelectionEnabled) {

            for (SelectableStudents selectableItem : selectableStudentList) {
                if (!selectableItem.equals(item) && selectableItem.isSelected()) {
                    selectableItem.setSelected(false);
                } else if (selectableItem.equals(item) && item.isSelected()) {
                    selectableItem.setSelected(true);
                }
            }
            notifyDataSetChanged();
        }
        mListener.onItemSelected(item);
    }
}
