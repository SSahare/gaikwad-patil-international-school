package appsoft.erp.gaikwad.activities.fragments;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import appsoft.erp.gaikwad.R;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.FlowerViewHolder>{

    private Context mContext;
    private List mFlowerList;
    private DashboardListener mDashboardListener = null;

    public MyAdapter(Context mContext, List mFlowerList, DashboardListener mDashboardListener) {
        this.mContext = mContext;
        this.mFlowerList = mFlowerList;
        this.mDashboardListener = mDashboardListener;
    }

    public MyAdapter(List<DashboardModel> mFlowerList) {
        this.mFlowerList=mFlowerList;
    }

    @Override
    public FlowerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rowlayout, parent, false);
        return new FlowerViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(FlowerViewHolder viewHolder, int position) {

        DashboardModel flowerdata = (DashboardModel) mFlowerList.get(position);

        viewHolder.mImage.setImageResource(flowerdata.getImg_id());
        viewHolder.mTitle.setText(flowerdata.getName());


        if (position % 2 == 0){
            viewHolder.linearlaayout.setBackgroundColor(Color.parseColor("#03353E"));
        }else {
            viewHolder.linearlaayout.setBackgroundColor(Color.parseColor("#0294A5"));
        }
    }




    @Override
    public int getItemCount() {
        return mFlowerList.size();
    }


    class FlowerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout linearlaayout;
        CardView cardview;
        ImageView mImage;
        TextView mTitle;

        FlowerViewHolder(View itemView) {
            super(itemView);
            mImage = itemView.findViewById(R.id.ivImage);
            mTitle = itemView.findViewById(R.id.tvTitle);
            cardview=itemView.findViewById(R.id.cardview);
            linearlaayout=itemView.findViewById(R.id.linearlaayout);
            itemView.setOnClickListener(this);




        }

        @Override
        public void onClick(View view) {
            int position=getAdapterPosition();
            mDashboardListener.getGridPosition(position);

        }
    }

    public interface DashboardListener{
        void getGridPosition(int position);
    }



}