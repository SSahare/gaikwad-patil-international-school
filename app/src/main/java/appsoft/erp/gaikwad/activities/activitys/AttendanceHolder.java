package appsoft.erp.gaikwad.activities.activitys;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.TextView;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.SelectableStudents;

public class AttendanceHolder extends RecyclerView.ViewHolder {

    public static final int MULTI_SELECTION = 2;
    public static final int SINGLE_SELECTION = 1;

    CheckedTextView studentName;
    TextView rollno;
    SelectableStudents mItem;
    OnItemSelectedListener itemSelectedListener;

    public AttendanceHolder(View itemView, OnItemSelectedListener listener) {
        super(itemView);

        itemSelectedListener = listener;
        studentName = itemView.findViewById(R.id.studentName);
        rollno = itemView.findViewById(R.id.rollno);
        mItem = new SelectableStudents();

        studentName.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Log.i("DEBUG", mItem.toString());

            if (mItem.isSelected() && getItemViewType() == MULTI_SELECTION) {
                setChecked(false);
            } else {
                setChecked(true);
            }
            itemSelectedListener.onItemSelected(mItem);
        }
    });
}

    public void setChecked(boolean value) {
        if (value) {
            //studentName.setBackgroundColor(Color.LTGRAY);
            studentName.setTextColor(Color.WHITE);
            rollno.setTextColor(Color.WHITE);

        } else {
            studentName.setTextColor(Color.RED);
            rollno.setTextColor(Color.RED);
        }
        mItem.setSelected(value);
        studentName.setChecked(value);
    }

    public interface OnItemSelectedListener {
        void onItemSelected(SelectableStudents item);
    }
}
