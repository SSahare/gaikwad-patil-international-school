package appsoft.erp.gaikwad.activities.parent_activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.MonthlyDetailAttendanceModel;
import appsoft.erp.gaikwad.activities.parent_adapters.MontlyAttendanceAdapter;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MonthlyAttendanceActivity extends AppCompatActivity {
    TextView monthAttendance;
    RecyclerView recyclerViewAttendanceDetail;
    LinearLayoutManager layoutManager;
    private String date="";
    MontlyAttendanceAdapter montlyAttendanceAdapter;
    List<MonthlyDetailAttendanceModel> monthlyDetailAttendanceModelList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monthly_attendance);
        monthAttendance = findViewById(R.id.month_attendance);
        monthAttendance.setText(getIntent().getStringExtra("month"));
        recyclerViewAttendanceDetail = findViewById(R.id.recyclerview_attandancedetail);
        recyclerViewAttendanceDetail.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerViewAttendanceDetail.setLayoutManager(layoutManager);
        getAttendanceDetail();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(" Attendance Detail");
        }
    }
    private void getAttendanceDetail() {

        String date = getIntent().getStringExtra("month");
        int  user_id =getIntent().getIntExtra("user_id", 0);

        final ApiInterface attendance = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<MonthlyDetailAttendanceModel>> call = attendance.getAttendanceDetail(user_id, date);
        call.enqueue(new Callback<List<MonthlyDetailAttendanceModel>>() {
            @Override
            public void onResponse(Call<List<MonthlyDetailAttendanceModel>> call, Response<List<MonthlyDetailAttendanceModel>> response) {
                monthlyDetailAttendanceModelList = response.body();
                montlyAttendanceAdapter = new MontlyAttendanceAdapter(getApplicationContext(), monthlyDetailAttendanceModelList);
                recyclerViewAttendanceDetail.setAdapter(montlyAttendanceAdapter);
            }

            @Override
            public void onFailure(Call<List<MonthlyDetailAttendanceModel>> call, Throwable t) {
                Log.i("DEBUG", "Month Dash Expsn: "+t.toString());
                Toast.makeText(getApplicationContext(), "Fail to load data", Toast.LENGTH_SHORT).show();


            }
        });


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
            return true;
        }else {
            return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public boolean onSupportNavigateUp() {

        return true;
    }
}
