package appsoft.erp.gaikwad.activities.open_results;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.download.Downloader;

import static appsoft.erp.gaikwad.activities.extras.Constants.ASSIGNMENT_SUB_URL;
import static appsoft.erp.gaikwad.activities.extras.Constants.ATTACHMENT_BASE_URL;

public class ParentAssignmentDetailActivity extends AppCompatActivity {
    TextView assignmenttitle, tv_AssignmentSubmissiondate, assignmentclass, assignmentsection, assignemntdescription, tv_ParentAssignment_Subject;
    TextView downloadbutton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parent_assignment_activity);
        assignmenttitle = findViewById(R.id.tv_AssignmentTitle);
        assignemntdescription = findViewById(R.id.tv_AssignmentDescription);
        tv_AssignmentSubmissiondate = findViewById(R.id.tv_AssignmentSubmissiondate);
        tv_ParentAssignment_Subject = findViewById(R.id.tv_ParentAssignment_Subject);
        downloadbutton=findViewById(R.id.btn_Download);


        tv_ParentAssignment_Subject.setText(getIntent().getStringExtra("assignmentsubject"));
        assignmenttitle.setText(getIntent().getStringExtra("assignmenttitle"));
        assignemntdescription.setText(getIntent().getStringExtra("assignmentdescription"));
        tv_AssignmentSubmissiondate.setText(getIntent().getStringExtra("assignmentsubmissiondate"));
//        downloadbutton.setText(getIntent().getStringExtra("download"));

if (getIntent().getStringExtra("download").isEmpty()){
    downloadbutton.setVisibility(View.GONE);
}
        downloadbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String Url = new StringBuilder().append(ATTACHMENT_BASE_URL).append(ASSIGNMENT_SUB_URL).append(getIntent().getStringExtra("download")).toString();
                Toast.makeText(getApplicationContext(), "Attachment Downloading...", Toast.LENGTH_SHORT).show();
                Downloader downloader = new Downloader(Url, getApplicationContext());
                downloader.downloadTask();


            }


        });


        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

                getSupportActionBar().setTitle("Assignment Detail");
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
