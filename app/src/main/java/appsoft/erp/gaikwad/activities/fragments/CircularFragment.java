package appsoft.erp.gaikwad.activities.fragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;

import java.util.ArrayList;
import java.util.List;

import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import appsoft.erp.gaikwad.activities.network_broadcast.ConnectionDetector;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.adapters.CircularAdapter;
import appsoft.erp.gaikwad.activities.modals.Circular;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.search_package.SearchColor;

public class CircularFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager manager;
    private List<Circular> circularList;
    private SearchView searchView;
    private CircularAdapter adapter;
    Animation uptodown, downtoup;
    private ImageView dataNotFound;
    private ConstraintLayout constraintLayout;
    private static OnTitleSetListener onTitleSetListener = null;
    ProgressDialog progressDoalog;
SwipeRefreshLayout swipeRefreshLayout;
    public CircularFragment() { }

    public static CircularFragment getInstance(OnTitleSetListener onTitleSetListener1) {
        // Required empty public constructor
        onTitleSetListener = onTitleSetListener1;
        return new CircularFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onResume() {
        super.onResume();
        setTitle();
    }

    private void setTitle() {
        onTitleSetListener.setOnTitleText("Circular");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = LayoutInflater.from(getContext()).inflate(R.layout.circular_fragment, container, false);
//        int resId = R.anim.layout_animation_fall_down;
        recyclerView = view.findViewById(R.id.circularRecycler);
        uptodown = AnimationUtils.loadAnimation(getContext(), R.anim.uptodown);
        recyclerView.setAnimation(uptodown);
        recyclerView.setHasFixedSize(true);
        manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        constraintLayout = view.findViewById(R.id.no_internet_layout);
        dataNotFound = view.findViewById(R.id.no_data_found_image);
        Button tryAgain = view.findViewById(R.id.try_again);
        swipeRefreshLayout=view.findViewById(R.id.swiperefreshlayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
              getCircularFromServer();
              swipeRefreshLayout.setRefreshing(false);
            }
        });


        if (ConnectionDetector.isConnected(getContext())){
            getCircularFromServer();
            viewsWhenConnected();
            progressDoalog.dismiss();
        }else {
            viewsWhenNotConnected();
               }

        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectionDetector.isConnected(getContext())){
                    viewsWhenConnected();
                    getCircularFromServer();
                }else {
                    Toast.makeText(getActivity().getApplicationContext(), R.string.no_connection, Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }



    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search_menu, menu);
        final MenuItem searchMenuItem = menu.findItem(R.id.isync_search);
        final MenuItem  additem=menu.findItem(R.id.isync_add);


        additem.setVisible(false);
//        this.invalidateOptionsMenu();
        SearchColor.changeSearchViewColor(searchView);
        searchView = SearchColor.getSearchView(searchMenuItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()){
                    searchView.setIconified(true);
                }
                searchMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                try {
                    final List<Circular> filterModeList = filter(circularList, newText);
                    adapter.filter(filterModeList);
                }catch (NullPointerException npe){ }
                return true;
            }
        });





    }

    private List<Circular> filter(List<Circular> list, String query){

        query = query.toLowerCase();
        try {
            final List<Circular> filterList = new ArrayList();
            for (Circular obj : list) {
                final String titleText = obj.getCircularTitle().toLowerCase();
                final String dateText = obj.getDateOfPublish().toLowerCase();
                if (titleText.contains(query) || dateText.contains(query)) {
                    filterList.add(obj);
                }
            }
            return filterList;
        }catch (NullPointerException npe){
//            Log.i("DEBUG", "Khali hai Tumhara Circular");
            Log.i("DEBUG", "Circular is empty");
        }
        return null;
    }

    private void getCircularFromServer(){
        circularprogressabar();
        ApiInterface circApi = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<Circular>> call = circApi.getCircularList();
        call.enqueue(new Callback<List<Circular>>() {
            @Override
            public void onResponse(Call<List<Circular>> call, Response<List<Circular>> response){
                if (response.isSuccessful()) {


                    try {
                        circularList = response.body();
                        if (circularList == null || circularList.size() == 0){
                            viewsWhenNoDataFound();
                            progressDoalog.dismiss();
                        }
                        else {
                            adapter = new CircularAdapter(getContext(), circularList);
                            recyclerView.setAdapter(adapter);
//                            viewsWhenConnected();
                        }
//                        if (circularList != null) {
//                            adapter = new CircularAdapter(getContext(), circularList);
//                            recyclerView.setAdapter(adapter);
//                        } else {
//                            Toast.makeText(getContext(), R.string.list_empty, Toast.LENGTH_SHORT).show();
//                        }
                    }catch (NullPointerException npe){
                        Log.i("DEBUG", "Empty Circular");
                    }catch (Exception exp){  Log.i("DEBUG", "");}
                    progressDoalog.dismiss();

                }
            }

            @Override
            public void onFailure(Call<List<Circular>> call, Throwable t) {
                viewsWhenNoDataFound();
                progressDoalog.dismiss();
                Log.i("DEBUG", "Circular Dash Expsn: "+t.toString());
            }
        });
    }



    private void viewsWhenConnected() {
        recyclerView.setVisibility(View.VISIBLE);
        constraintLayout.setVisibility(View.GONE);
        dataNotFound.setVisibility(View.GONE);
    }

    private void viewsWhenNotConnected() {
        recyclerView.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.VISIBLE);
        dataNotFound.setVisibility(View.GONE);
    }


    private void viewsWhenNoDataFound() {
        recyclerView.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.GONE);
        dataNotFound.setVisibility(View.VISIBLE);
    }

    private void circularprogressabar() {
        progressDoalog = new ProgressDialog(getContext());
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.setMessage(" Please Wait....");
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
//        progressDoalog.getWindow().setGravity(LinearLayout.LayoutParams.MATCH_PARENT LinearLayout.LayoutParams.MATCH_PARENT);
        progressDoalog.setIndeterminate(true);
        Circle wave=new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();
    }
    
    
}
