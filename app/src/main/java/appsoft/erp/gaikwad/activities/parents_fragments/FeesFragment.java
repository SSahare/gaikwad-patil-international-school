package appsoft.erp.gaikwad.activities.parents_fragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;
import com.google.gson.JsonSyntaxException;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.adapters.FeesLogAdapter;
import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import appsoft.erp.gaikwad.activities.modals.Fees;
import appsoft.erp.gaikwad.activities.modals.FeesRecord;
import appsoft.erp.gaikwad.activities.network_broadcast.ConnectionDetector;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeesFragment extends Fragment {
    private TextView schoolFees;
    private TextView totalAmountPaid;
    private TextView totalAmountBal;
    private PieChartView feesChart;
    private RecyclerView mRecyclerView;
    private List<FeesRecord> feeRecordList = null;
    private SessionManagement session;
    ArrayList<String> feesTypes = new ArrayList<>();
    String[] types = {"Paid", "Unpaid"};
    ProgressBar progressBar;
    private ImageView noDataFound;
    private ConstraintLayout constraintLayout;
    ProgressDialog progressDoalog;
    LinearLayout linearLayout;
    CardView cardView;
    private static OnTitleSetListener onTitleSetListener = null;


    public FeesFragment() {
    }

    public static FeesFragment getInstance(OnTitleSetListener onTitleSetListener1) {
        onTitleSetListener = onTitleSetListener1;
        return new FeesFragment();

    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle();
    }

    private void setTitle() {
        onTitleSetListener.setOnTitleText("Fees Record");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fees_record_layout, container, false);
        totalAmountBal = view.findViewById(R.id.totalFeesBalance);
        totalAmountPaid = view.findViewById(R.id.totalFeesPaid);
        feesChart = view.findViewById(R.id.feesChart);
        linearLayout = view.findViewById(R.id.linearLayout);

        mRecyclerView=view.findViewById(R.id.feesRecycler);
        Button tryAgain = view.findViewById(R.id.try_again);
        constraintLayout = view.findViewById(R.id.no_internet_layout);
        noDataFound = view.findViewById(R.id.no_data_found_image);
        RecyclerView.LayoutManager mManager = new LinearLayoutManager(getContext());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mManager);
        session = new SessionManagement(getContext());
        if (ConnectionDetector.isConnected(getContext())){
            getFeesRecordLogFromServer();
            viewsWhenConnected();

        }else {
            viewsWhenNotConnected();
        }

        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectionDetector.isConnected(getContext())){
                    getFeesRecordLogFromServer();
                    getFeesRecordFromServer();
                    viewsWhenConnected();


                }
            }
        });



        for (String tp : types) {
            feesTypes.add(tp);
            feesChart.setChartRotationEnabled(false);
        }
        return view;

    }

    @Override
    public void onStart() {
        super.onStart();
        getFeesRecordFromServer();
    }

    private void getFeesRecordFromServer() {

        ApiInterface apiFees = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<Fees> call = apiFees.getStudentFeesRecord(session.getUserId());
        call.enqueue(new Callback<Fees>() {
            @Override
            public void onResponse(Call<Fees> call, @NotNull Response<Fees> response) {
                if (response.isSuccessful()) {
                    try {
                        Log.i("DEBUG", "ResponseModel String: " + response.toString());
                        Fees obj = response.body();
                        totalAmountPaid.setText(String.valueOf("Paid Fees :"+obj.getTotalFeesPaid()));
                        totalAmountBal.setText(String.valueOf("Balance Fees :"+obj.getBalancedFee()));
                        ArrayList<SliceValue> feeEntry = new ArrayList<>();

                        feeEntry.add(new SliceValue((float) obj.getTotalFeesPaid(), Color.BLACK).setLabel("Paid fee\n" + obj.getTotalFeesPaid()));
                        feeEntry.add(new SliceValue((float) obj.getBalancedFee(), getResources().getColor(R.color.orange)).setLabel("Balance fee\n" + obj.getBalancedFee()));

                        setEntryToPieDataSet(feeEntry);
                    } catch (NullPointerException npe) {
                        Log.i("DEBUG", "Fees NULL: " + npe.toString());
                    } catch (IllegalStateException ise) {
                        Log.i("DEBUG", "Fees Illegal" + ise.toString());
                    } catch (JsonSyntaxException jse) {
                        Log.i("DEBUG", "Fees JSON" + jse.toString());
                    }
                }

            }

            @Override
            public void onFailure(Call<Fees> call, Throwable t) {
                Log.i("DEBUG", "Failure Exception: " + t.toString());
            }
        });
    }
    private void viewsWhenNotConnected() {

        mRecyclerView.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.VISIBLE);
        noDataFound.setVisibility(View.GONE);
    }

    private void viewsWhenConnected() {

        mRecyclerView.setVisibility(View.VISIBLE);
        constraintLayout.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);

    }

    private void viewsWhenNoDataFound()
    {
        mRecyclerView.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.GONE);
        noDataFound.setVisibility(View.VISIBLE);
    }


    private void setEntryToPieDataSet(ArrayList<SliceValue> feeEntry) {

        PieChartData pieChartData = new PieChartData(feeEntry);
        pieChartData.setSlicesSpacing(10);
        pieChartData.setValueLabelTextSize(12);
        pieChartData.setHasLabels(true);
        PieChartData data = new PieChartData(pieChartData);
        feesChart.setPieChartData(data);
        feesChart.setVisibility(View.VISIBLE);
        feesChart.invalidate();

    }
    public void getFeesRecordLogFromServer() {

        getfeesProgressdialog();
        ApiInterface logInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<FeesRecord>> call = logInterface.getStudentFeesLog(session.getUserId());
        call.enqueue(new Callback<List<FeesRecord>>() {
            @Override
            public void onResponse(Call<List<FeesRecord>> call, Response<List<FeesRecord>> response){
                feeRecordList = response.body();

                if (feeRecordList == null || feeRecordList.size() == 0){
                    viewsWhenNoDataFound();

                }else {
                    linearLayout.setVisibility(View.VISIBLE);
                    viewsWhenConnected();
                    progressDoalog.dismiss();

                }
                try{
                    if (feeRecordList != null){
                        FeesLogAdapter adapter = new FeesLogAdapter(getContext(), feeRecordList);
                        mRecyclerView.setAdapter(adapter);
                        progressDoalog.dismiss();

                    }

                    else{
                        Toast.makeText(getContext(), R.string.list_empty, Toast.LENGTH_SHORT).show();
                        progressDoalog.dismiss();
                    }
                    progressDoalog.dismiss();

                }catch (NullPointerException npe){
                    Log.i("DEBUG", npe.toString());
                }
                progressDoalog.dismiss();

            }

            @Override
            public void onFailure(Call<List<FeesRecord>> call, Throwable t) {
                Log.i("DEBUG", "Empty Hai");
                progressDoalog.dismiss();

            }
        });
    }

//        getfeesProgressdialog();
//        ApiInterface logInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
//        Call<List<FeesRecord>> call = logInterface.getStudentFeesLog(session.getUserId());
//        call.enqueue(new Callback<List<FeesRecord>>() {
//            @Override
//            public void onResponse(Call<List<FeesRecord>> call, Response<List<FeesRecord>> response) {
//                feeRecordList = response.body();
//                try {
//                    if (feeRecordList != null) {
//                        FeesLogAdapter adapter = new FeesLogAdapter(getContext(), feeRecordList);
//                        mRecyclerView.setAdapter(adapter);
//                        progressDoalog.dismiss();
//
//                    } else {
//                        Toast.makeText(getContext(), R.string.list_empty, Toast.LENGTH_SHORT).show();
//                        progressDoalog.dismiss();
//                    }
//                    progressDoalog.dismiss();
//
//                } catch (NullPointerException npe) {
//                    Log.i("DEBUG", npe.toString());
//                }
//                progressDoalog.dismiss();
//
//            }
//
//            @Override
//            public void onFailure(Call<List<FeesRecord>> call, Throwable t) {
//                Log.i("DEBUG", "Empty Hai");
//                progressDoalog.dismiss();
//
//            }
//        });


    private void getfeesProgressdialog() {


        progressDoalog = new ProgressDialog(getContext());
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        progressDoalog.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.WRAP_CONTENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
        progressDoalog.setIndeterminate(true);
        Circle wave = new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();
    }
}
