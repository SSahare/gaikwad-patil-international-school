package appsoft.erp.gaikwad.activities.extras;

public class Constants {

    public static final String LOGIN_TYPE_AS_PARENT = "parents";
    public static final String LOGIN_TYPE_AS_TEACHER = "teacher";
    public static final String ATTACHMENT_BASE_URL = "https://www.gpis-erp.in/app/uploads/";
    public static final String ASSIGNMENT_SUB_URL = "assignment_files/";
    public static final String TEACHER_SUB_URL = "staff_photo/";
    public static final String STUDENT_SUB_URL = "student_photo/";
    public static final String TEMP_PDF_SUB_URL ="https://www.gpis-erp.in/app/temppdf/";
    public static final int GOTO_LOGIN_PAGE_REQ = 102;
    public static final int EXTERNAL_FILE_PERMISSION_REQUEST = 200;
    public static final String CARD_HOMEWORK = "homework";
    public static final String CARD_ASSIGNMENT = "assignment";
    public static final String CARD_DAIRY = "dairy";
    public static final String CARD_NEWS = "news";
    public static final String CARD_CIRCULARS = "circulars";

    // News and Circular Flag
    public static final int CIRCULAR_FLAG = 121;
    public static final int NEWS_FLAG = 122;
}
