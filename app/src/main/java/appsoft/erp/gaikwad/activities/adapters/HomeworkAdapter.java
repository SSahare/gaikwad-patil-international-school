package appsoft.erp.gaikwad.activities.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import appsoft.erp.gaikwad.R;

import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import appsoft.erp.gaikwad.activities.modals.Homework;

public class HomeworkAdapter extends RecyclerView.Adapter<HomeworkAdapter.HomeworkViewHolder> implements OnTitleSetListener {

    private List<Homework> homeworkList;
    private Context context;
    private String dateText = "%s\n%s\n%s";

    public HomeworkAdapter(Context context, List<Homework> homeworkList){
        this.homeworkList = homeworkList;
        this.context = context;
    }

    @Override
    public HomeworkViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.homework_card,parent,false);
        return new HomeworkViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HomeworkViewHolder holder, int position){
         Homework homework;
         homework = homeworkList.get(position);
         String dateStr = homework.getDateOfGiven();
         String[] arrOfDateStr = dateStr.split("-");

         holder.givenDateMonth.setText(arrOfDateStr[0]);
         holder.givenDateDay.setText(arrOfDateStr[1]);
         holder.givenDateYear.setText(arrOfDateStr[2]);

//         holder.submissionDate.setText("Sub. Date : "+homework.getDateOfSubmission());
         holder.title.setText(homework.getTitle());
         holder.desc.setText(homework.getDescription());
         holder.classname.setText(("("+homework.getClassname()+" "+","+" "+homework.getSection())+")");
//         holder.section.setText("Section : "+homework.getSection());
         holder.subject.setText("Subject : "+homework.getSubject());
//        if ((homeworkList.size()-1) == position){
//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//            // add rule
//            //params.addRule(LinearLayout.ALIGN_BOTTOM,R.id.ll_status);
//            params.setMargins(0, 0, 0, 0);
//            holder.card.setLayoutParams(params);
//        }
    }

    @Override
    public int getItemCount() {
        return homeworkList.size();
    }

    public void filter(List<Homework> list){

        homeworkList = new ArrayList<>();
        homeworkList.addAll(list);

        notifyDataSetChanged();
    }

    @Override
    public void setOnTitleText(String titleText) {

    }

    class HomeworkViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView givenDateMonth, givenDateDay, givenDateYear, submissionDate,title,desc,classname, section, subject;
        CardView card;

        private HomeworkViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            givenDateMonth = itemView.findViewById(R.id.dateofGivenMonth);
            givenDateDay = itemView.findViewById(R.id.dateofGivenDay);
            givenDateYear = itemView.findViewById(R.id.dateofGivenYear);
//            submissionDate = itemView.findViewById(R.id.dateOfSubmission);
            title = itemView.findViewById(R.id.assignmentTitle);
            desc = itemView.findViewById(R.id.description);
            classname = itemView.findViewById(R.id.classname);
            section = itemView.findViewById(R.id.section);
            subject = itemView.findViewById(R.id.subject);
            card=itemView.findViewById(R.id.card);
        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            Homework homework= homeworkList.get(pos);
            Intent homeworkintent=new Intent(context, appsoft.erp.gaikwad.activities.open_results.HomeWorkDetailActivity.class);
            homeworkintent.putExtra("homeworktitle",homework.getTitle());
            homeworkintent.putExtra("homeworkdescription",homework.getDescription());
            homeworkintent.putExtra("homeworksubject",homework.getSubject());
            homeworkintent.putExtra("homeworksection",homework.getSection());
            homeworkintent.putExtra("homeworkdateofsubmission",homework.getDateOfSubmission());
            homeworkintent.putExtra("homeworkclass",homework.getClassname());
            context.startActivity(homeworkintent);




        }
    }
}
