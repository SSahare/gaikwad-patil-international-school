package appsoft.erp.gaikwad.activities.fragments;


import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;

import java.util.ArrayList;
import java.util.List;

import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import appsoft.erp.gaikwad.activities.network_broadcast.ConnectionDetector;
import retrofit2.Call;
import retrofit2.Callback;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.adapters.EventAdapter;
import appsoft.erp.gaikwad.activities.modals.Event;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.search_package.SearchColor;

public class EventFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mManager;
    private List<Event> eventList;
    private SearchView searchView;
    SwipeRefreshLayout swipeRefreshLayout;
    private EventAdapter adapter;
    ProgressDialog progressDoalog;
    private ImageView dataNotFound;
    private ConstraintLayout constraintLayout;

    Animation uptodown;
    private static final int CALENDER_PERMISSION = 100;
    public static boolean IS_CALENDER_PERMISSION = false;
    private static OnTitleSetListener onTitleSetListener=null ;

    public EventFragment() { }


    public static EventFragment getInstance(OnTitleSetListener onTitleSetListener1) {
        // Required empty public constructor
       onTitleSetListener = onTitleSetListener1;
        return new EventFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    private void setTitle() {
        onTitleSetListener.setOnTitleText("Event");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.event_fragment, container, false);
        uptodown = AnimationUtils.loadAnimation(getContext(), R.anim.uptodown);
        recyclerView = view.findViewById(R.id.eventRecycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAnimation(uptodown);
        constraintLayout = view.findViewById(R.id.no_internet_layout);
        dataNotFound = view.findViewById(R.id.no_data_found_image);
        Button tryAgain = view.findViewById(R.id.try_again);
        swipeRefreshLayout =view.findViewById(R.id.swiperefreshlayout);

        mManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mManager);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getInformationFromServer();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

//        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                getInformationFromServer();
//                swipeRefreshLayout.setRefreshing(false);
//            }
//        });
//        LayoutAnimationController animation=AnimationUtils.loadLayoutAnimation(getContext(),resId);
//        recyclerView.setLayoutAnimation(animation);
        setTitle();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
//        recyclerView.addItemDecoration(new SimpleItemDecoration(getContext()));





        if (ConnectionDetector.isConnected(getContext())){
            getInformationFromServer();
            viewsWhenConnected();
        }else {
            viewsWhenNotConnected();
        }

        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectionDetector.isConnected(getContext())){
                    viewsWhenConnected();
                    getInformationFromServer();
                }else {
                    Toast.makeText(getActivity().getApplicationContext(), R.string.no_connection, Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }

    private void viewsWhenNotConnected() {
        recyclerView.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.VISIBLE);
        dataNotFound.setVisibility(View.GONE);
    }

    private void viewsWhenConnected() {
        recyclerView.setVisibility(View.VISIBLE);
        constraintLayout.setVisibility(View.GONE);
        dataNotFound.setVisibility(View.GONE);
    }

    private void viewsWhenNoDataFound() {
        recyclerView.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.GONE);
        dataNotFound.setVisibility(View.VISIBLE);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search_menu, menu);
        final MenuItem searchMenuItem = menu.findItem(R.id.isync_search);
        SearchColor.changeSearchViewColor(searchView);
        searchView = SearchColor.getSearchView(searchMenuItem);
        final MenuItem  additem=menu.findItem(R.id.isync_add);


        additem.setVisible(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()){
                    searchView.setIconified(true);
                }
                searchMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                final List<Event> filterModeList = filter(eventList, newText);
                adapter.filterList(filterModeList);
                return true;
            }
        });
    }

    private List<Event> filter(List<Event> list, String query){

        query = query.toLowerCase();
        final List<Event> filterList = new ArrayList();
        for (Event obj: list){
            final String titleText = obj.getEventTitle().toLowerCase();
            final String dateText = obj.getStartDate().toLowerCase();
            if (titleText.contains(query) || dateText.contains(query)){
                filterList.add(obj);
            }
        }
        return filterList;
    }

    private void getInformationFromServer(){
        eventprogressbar();
        ApiInterface apiInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<Event>> call = apiInterface.getEventList();
        call.enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, retrofit2.Response<List<Event>> response) {

                eventList = response.body();
                if (eventList==null ||eventList.size()==0){
                    viewsWhenNoDataFound();
                }
                else {
                    viewsWhenConnected();
                }
                try {
                    if (eventList != null) {
                        adapter = new EventAdapter(getContext(), eventList);
                        recyclerView.setAdapter(adapter);
                        progressDoalog.dismiss();

                    } else {
                        Toast.makeText(getContext(), R.string.list_empty, Toast.LENGTH_SHORT).show();
                    }
                }catch (NullPointerException npe){
                    android.util.Log.i("DEBUG", "Events are empty");
                }catch (Exception exp){
                    android.util.Log.i("DEBUG", ""+exp.toString());
                }
                progressDoalog.dismiss();

            }


            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                progressDoalog.dismiss();
                android.util.Log.i("DEBUG", "Event Failure :"+t.getMessage());
            }
        });
    }

    private void eventprogressbar() {

        progressDoalog = new ProgressDialog(getActivity());
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.setMessage("Please Wait...");
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
//        progressDoalog.getWindow().setGravity(LinearLayout.LayoutParams.MATCH_PARENT LinearLayout.LayoutParams.MATCH_PARENT);
        progressDoalog.setIndeterminate(true);
        Circle wave=new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();
    }
    
    
}
