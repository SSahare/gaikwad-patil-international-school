package appsoft.erp.gaikwad.activities.leave_manager;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.Leave;

public class Leave_History_Adapter extends RecyclerView.Adapter<Leave_History_Adapter.HistoryHolder>{

    private List<Leave> leaves;

    public Leave_History_Adapter(List<Leave> leaves){
        this.leaves = leaves;
    }
    @Override
    public HistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.leave_history_card,parent,false);
        return new HistoryHolder(view);
    }

    @Override
    public void onBindViewHolder(HistoryHolder holder, int position) {
        Leave leave = leaves.get(position);

        holder.reasonOfLeave.setText(leave.getReason());
        holder.leaveStatus.setText(leave.getStatus());
    }

    @Override
    public int getItemCount() {
        return leaves.size();
    }

    class HistoryHolder extends RecyclerView.ViewHolder{

        private TextView typeOfLeave, leaveDays,reasonOfLeave, leaveStatus;

        public HistoryHolder(View itemView) {
            super(itemView);

            typeOfLeave = itemView.findViewById(R.id.leaveType);
            leaveDays = itemView.findViewById(R.id.numberOfDays);
            reasonOfLeave = itemView.findViewById(R.id.reasonForLeave);
            leaveStatus = itemView.findViewById(R.id.leaveStatus);
        }
    }
}
