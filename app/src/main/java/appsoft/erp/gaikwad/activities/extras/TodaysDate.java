package appsoft.erp.gaikwad.activities.extras;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Locale;

public class TodaysDate {

    public static String getTodaysDate(){

        String date="";
        final Calendar calendar = Calendar.getInstance();
        int y = calendar.get(Calendar.YEAR);
        int m = calendar.get(Calendar.MONTH);
        int d = calendar.get(Calendar.DAY_OF_MONTH);

        final DateFormat myFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.ENGLISH);
        date = myFormat.format(calendar.getTime());
        return date;
    }
}
