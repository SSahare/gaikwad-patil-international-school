package appsoft.erp.gaikwad.activities.parent_adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.fragments.DashboardModel;

public class ParentDashdoardAdapter extends RecyclerView.Adapter<ParentDashdoardAdapter.ParentViewHolder>{

    private Context mContext;
    private List mFlowerList;
    private ParentDashbaordListerner parentDashbaordListerner=null ;
//     DashboardModel model = new DashboardModel("v", 22, "RED");


    public ParentDashdoardAdapter(Context mContext, List mFlowerList,ParentDashbaordListerner parentDashbaordListerner) {
        this.mContext = mContext;
        this.mFlowerList = mFlowerList;
        this.parentDashbaordListerner = parentDashbaordListerner;
    }
    public ParentDashdoardAdapter(List<DashboardModel> mFlowerList) {
        this.mFlowerList=mFlowerList;
    }


    @Override
    public ParentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.parentrowlayout, parent, false);
        return new ParentViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(ParentViewHolder holder, int position) {
        DashboardModel flowerdata = (DashboardModel) mFlowerList.get(position);
        holder.mImage.setImageResource(flowerdata.getImg_id());
        holder.mTitle.setText(flowerdata.getName());
        holder.linearlaayout.setBackgroundResource(flowerdata.getDrawable());
//        holder.mImage.setImageDrawable(flowerdata.getColor());

    }

    @Override
    public int getItemCount() {
        return mFlowerList.size();
    }


    class ParentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout linearlaayout;
        CardView cardview;
        ImageView mImage;

        TextView mTitle;

        ParentViewHolder(View itemView) {
            super(itemView);
            mImage = itemView.findViewById(R.id.ivImage);
            mTitle = itemView.findViewById(R.id.tvTitle);
            cardview=itemView.findViewById(R.id.cardview);
            linearlaayout=itemView.findViewById(R.id.linearlaayout);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position=getAdapterPosition();
            parentDashbaordListerner.getGridPosition(position);

        }
    }

    public interface ParentDashbaordListerner{
        void getGridPosition(int position);
    }
}
