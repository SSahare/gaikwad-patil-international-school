package appsoft.erp.gaikwad.activities.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

import appsoft.erp.gaikwad.activities.activitys.AddLogActivity;
import appsoft.erp.gaikwad.activities.date_util.DateFormatter;
import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import appsoft.erp.gaikwad.activities.network_broadcast.ConnectionDetector;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.adapters.LogAdapter;
import appsoft.erp.gaikwad.activities.extras.Constants;
import appsoft.erp.gaikwad.activities.modals.Log;
import appsoft.erp.gaikwad.activities.parent_adapters.ParentLogAdapter;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.search_package.SearchColor;
import appsoft.erp.gaikwad.activities.session.SessionManagement;

public class ViewLogFragment extends Fragment implements View.OnClickListener{

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager manager;
    private static List<Log> logList = null;
    private List<Log> fList = new ArrayList<>();
    private SearchView searchView;
    private static ParentLogAdapter pAdapter;
    private static LogAdapter lAdapter;
    private SessionManagement session;
    ProgressDialog progressDoalog;
    Animation uptodown, downtoup;
    private AppCompatSpinner spinner;
    private static TextView dateFilterText;
    private ImageButton closeDate;
    private ImageButton closeSubject;
    private static String userRole = "";
    SwipeRefreshLayout swipeRefreshLayout;
    ConstraintLayout constraintLayout;
    private ImageView noDataFound;
    private LinearLayout dropDownLayout,dateTextLayout;

    private static OnTitleSetListener onTitleSetListener = null;

    public ViewLogFragment() { }
    public static ViewLogFragment getInstance(OnTitleSetListener onTitleSetListener1) {
        // Required empty public constructor
        onTitleSetListener = onTitleSetListener1;
        return new ViewLogFragment();
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Override
    public void onResume() {
        super.onResume();
        setTitle();
    }
    private void setTitle() {
        onTitleSetListener.setOnTitleText("Subject Diary");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.view_log_fragment, container, false);

        session = new SessionManagement(getContext());
        userRole = session.getUserRole();
        spinner = view.findViewById(R.id.spinnerSubjectFilter);
        recyclerView = view.findViewById(R.id.logRecycler);
        recyclerView.setHasFixedSize(true);
        uptodown = AnimationUtils.loadAnimation(getContext(), R.anim.uptodown);
        manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        swipeRefreshLayout=view.findViewById(R.id.swiperefreshlayout);
        recyclerView.setAnimation(uptodown);
        closeDate = view.findViewById(R.id.close_date);
//        closeSubject = view.findViewById(R.id.close_subject);
        dateFilterText = view.findViewById(R.id.dateFilterText);
        dateTextLayout = view.findViewById(R.id.date_layout);

        constraintLayout = view.findViewById(R.id.no_internet_layout);
        dropDownLayout = view.findViewById(R.id.dropdown_layout);
        noDataFound =view.findViewById(R.id.no_data_found_image);
        Button tryAgain = view.findViewById(R.id.try_again);
        dateFilterText.setOnClickListener(this);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER)){
                    getSubjectLogFromServer();
                    swipeRefreshLayout.setRefreshing(false);
                }
                if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_PARENT)){
                    getSubjectLogFromServer();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        if (ConnectionDetector.isConnected(getContext())){
            viewsWhenConnected();
            getSubjectLogFromServer();
        }
        else {
            viewsWhenNotConnected();
        }

        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConnectionDetector.isConnected(getContext())){
                    getSubjectLogFromServer();
                    viewsWhenConnected();
                }
            }
        });




        closeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateFilterText.setText("");
                if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_PARENT)) {
                    pAdapter.filter(logList);
                }
                if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
                    lAdapter.filter(logList);
                }
            }
        });

       /* closeSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinner.setSelection(0);
                if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_PARENT)) {
                    pAdapter.filter(logList);
                }
                if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
                    lAdapter.filter(logList);
                }
            }
        });*/

        return view;
    }

    private void setFilterSpinner(List<Log> list) {
        HashSet<String> subjectSortedSet = new LinkedHashSet<>();
        subjectSortedSet.add("Subject");
        try {
            for (Log obj : list) {
                subjectSortedSet.add(obj.getLogSubject());
            }
            android.util.Log.i("DEBUG", "Subject Dairy :"+fList.size());
        }catch (NullPointerException npe){
            android.util.Log.i("DEBUG", "Subject Dairy is Empty");
        }

        if (subjectSortedSet != null) {
            List<String> subList = new ArrayList<>();
            subList.addAll(subjectSortedSet);
            final ArrayAdapter<String> subjectAdapter = new ArrayAdapter<String>(getContext(), R.layout.text_filter_view, subList);
            subjectAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(subjectAdapter);
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                dateFilterText.setText("");

                if (position != 0){
                    List<Log> finalSubList = subjectFilterList((String) parent.getSelectedItem());

                    if (userRole.equals(Constants.LOGIN_TYPE_AS_PARENT)) {
                        pAdapter.filter(finalSubList);
                    }
                    if (userRole.equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
                        lAdapter.filter(finalSubList);
                    }
                }else{
                    if (userRole.equals(Constants.LOGIN_TYPE_AS_PARENT)) {
                        pAdapter.filter(logList);
                    }
                    if (userRole.equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
                        lAdapter.filter(logList);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search_menu, menu);

        final MenuItem searchMenuItem = menu.findItem(R.id.isync_search);
        SearchColor.changeSearchViewColor(searchView);
        searchView = SearchColor.getSearchView(searchMenuItem);




        if (session.getUserRole().equals(Constants.LOGIN_TYPE_AS_TEACHER)){
            final MenuItem  additem=menu.findItem(R.id.isync_add);
            additem.setVisible(true);

            additem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                           startActivity(new Intent(getActivity(),AddLogActivity.class));
                    return true;



            }
        });
    }

        // Search view ke touch ya click per spinner default that
        // means 0 position per jana chayiye

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()){
                    searchView.setIconified(true);
                }

                searchMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                dateFilterText.setText("");
                spinner.setSelection(0);

                try {
                    final List<Log> flist = filteredList(logList, newText);
                    if (userRole.equals(Constants.LOGIN_TYPE_AS_PARENT)) {
                        pAdapter.filter(flist);
                    }
                    if (userRole.equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
                        lAdapter.filter(flist);
                    }
                }catch (NullPointerException npe){ }
                return false;
            }
        });
    }

    private List<Log> subjectFilterList(String sub){
        try {
            final List<Log> subFilterList = new ArrayList();
            for (Log lObj : logList) {
                if (lObj.getLogSubject().equals(sub)) {
                    subFilterList.add(lObj);
                }
            }
            return subFilterList;
        }catch (NullPointerException npe){}
        return null;
    }

    private List<Log> filteredList(List<Log> list, String query){
        query = query.toLowerCase();
        try {
            final List<Log> filterList = new ArrayList();
            for (Log obj : list) {
                final String titleText = obj.getLogSubject().toLowerCase();
                final String dateText = obj.getLogDate().toLowerCase();

                if (titleText.contains(query) || dateText.contains(query)) {
                    filterList.add(obj);
                }
            }
            return filterList;
        }catch (NullPointerException npe){ }
        return null;
    }

    private void getSubjectLogFromServer(){
        logprogressbar();
        ApiInterface subDairyApi = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<Log>> call;
        if (userRole.equals(Constants.LOGIN_TYPE_AS_PARENT)){
            call = subDairyApi.getStudentLogList(session.getUserId());
        }else {
            call = subDairyApi.getLogList(session.getUserId());
        }
        call.enqueue(new Callback<List<Log>>() {
            @Override
            public void onResponse(Call<List<appsoft.erp.gaikwad.activities.modals.Log>> call, Response<List<Log>> response) {
                if (response.isSuccessful()) {
                    logList = response.body();
                    if (logList==null||logList.size()==0){
                        viewsNoDataFound();
                    }
                    else {
                        viewsWhenConnected();
                    }
                    fList.addAll(logList);
                    setFilterSpinner(fList);
                    try {
                        if (userRole.equals(Constants.LOGIN_TYPE_AS_PARENT)) {
                            pAdapter = new ParentLogAdapter(getContext(),logList);
                            recyclerView.setAdapter(pAdapter);
                        }
                        if (userRole.equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
                            lAdapter = new LogAdapter(getContext(), logList);
                            recyclerView.setAdapter(lAdapter);
                        }
                    }catch (NullPointerException npe){
                        Toast.makeText(getContext(), R.string.list_empty, Toast.LENGTH_SHORT).show();
                    }

                    progressDoalog.dismiss();

                }

            }

            @Override
            public void onFailure(Call<List<appsoft.erp.gaikwad.activities.modals.Log>> call, Throwable t) {
                progressDoalog.dismiss();
                android.util.Log.i("DEBUG", "Subject Dairy Dash Expsn: "+t.toString());
            }
        });
    }



    private void viewsWhenNotConnected() {

        recyclerView.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.VISIBLE);
        noDataFound.setVisibility(View.GONE);
        dateTextLayout.setVisibility(View.GONE);
        dropDownLayout.setVisibility(View.GONE);
    }
    private  void  viewsWhenConnected(){

        recyclerView.setVisibility(View.VISIBLE);
        constraintLayout.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);
        dateTextLayout.setVisibility(View.VISIBLE);
        dropDownLayout.setVisibility(View.VISIBLE);

    }
    private void viewsNoDataFound() {
        recyclerView.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.GONE);
        noDataFound.setVisibility(View.VISIBLE);
        dateTextLayout.setVisibility(View.GONE);
        dropDownLayout.setVisibility(View.GONE);
    }



    private void logprogressbar() {

        progressDoalog = new ProgressDialog(getActivity());
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
        progressDoalog.setMessage("Please Wait...");
//        progressDoalog.getWindow().setGravity(LinearLayout.LayoutParams.MATCH_PARENT LinearLayout.LayoutParams.MATCH_PARENT);
        progressDoalog.setIndeterminate(true);
        Circle wave=new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();
    }

    private void callDatePicker(){
        NewDateDialog fragment = new NewDateDialog();
        fragment.show(getFragmentManager() , "FilterDatePick");
    }


    @Override
    public void onClick(View v) {
        searchView.setQuery("", false);
        searchView.clearFocus();
        spinner.setSelection(0);
        callDatePicker();
    }

    @SuppressLint("ValidFragment")
    public static class NewDateDialog extends DialogFragment implements  DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int y = c.get(Calendar.YEAR);
            int m = c.get(Calendar.MONTH);
            int d = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),this,y,m,d);

            return datePickerDialog;
        }

       @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

           Calendar calendar = new GregorianCalendar(year,month,dayOfMonth);
           final DateFormat dateFormat = new SimpleDateFormat("MMM-dd-yyyy");
           dateFilterText.setText(dateFormat.format(calendar.getTime()));
           List<Log> finalSubList = filterListWithDate(dateFilterText.getText().toString().trim());

           if (userRole.equals(Constants.LOGIN_TYPE_AS_PARENT)) {
               pAdapter.filter(finalSubList);
           }
           if (userRole.equals(Constants.LOGIN_TYPE_AS_TEACHER)) {
               lAdapter.filter(finalSubList);
           }


        }

        private List<Log> filterListWithDate(String dateText) {
            try {
                final List<Log> subFilterList = new ArrayList();

                for (Log obj:logList) {

                    String outputDateStr1 = obj.getLogDate();


                    if (DateFormatter.compareTwoDates(outputDateStr1, dateText)) {
                        subFilterList.add(obj);
                    }
                }
                return subFilterList;
            }catch (NullPointerException npe){}
            return null;




        }


    }
}
