package appsoft.erp.gaikwad.activities.profile;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.ybq.android.spinkit.style.Circle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.UserProfileModel;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;

public class ProfilrActivity extends AppCompatActivity implements OnTitleSetListener {

    private int lastExpandedPosition = -1;
    private ExpandableListView expandableListView;
    List<String> Basic_detail;
    List<String> Academic_detail;
    List<String> Address_detail;
    private String[] heading_item;
    private TextView  studentFullName;
    private TextView  studentId;
    ProgressDialog progressDoalog;
    private TextView  studentClassSection;
    private CircleImageView studentProfilePhoto;
    private SessionManagement session;
    UserProfileModel model;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profilr);


        session = new SessionManagement(getApplicationContext());
        heading_item = getResources().getStringArray(R.array.profile_heading);
        expandableListView = findViewById(R.id.profileListView);
        studentFullName = findViewById(R.id.studentName);
        studentClassSection = findViewById(R.id.studentClassSection);
        studentId = findViewById(R.id.studentId);
        studentProfilePhoto = findViewById(R.id.studentProfilePhoto);

//        studentProfilePhoto.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String image_url = "Your Image URL";
//                OnTouch_ZoomImage.zoomImageFromThumb(ProfilrActivity.this,studentProfilePhoto,image_url,);
//            }
//        });

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        getDataFromServer();
        studentFullName.setText(session.getUserName());
        studentId.setText(String.valueOf(session.getUserId()));
        Log.i("DEBUG", session.getImagePath());
        if (session.getImageLastPath().isEmpty() || session.getImageLastPath().equals(null) || session.getImageLastPath() == ""){
            Glide.with(this)
                    .load("")
                    .placeholder(getResources().getDrawable(R.drawable.person))
                    .fitCenter()
                    .into(studentProfilePhoto);
        }else {
            Glide.with(this)
                    .load(session.getImagePath())
                    .fitCenter()
                    .into(studentProfilePhoto);
        }
        List<String> Heading = new ArrayList<String>();

        Basic_detail= new ArrayList<String>();
        Academic_detail = new ArrayList<String>();
        Address_detail = new ArrayList<String>();

        /*List<String> l4 = new ArrayList<String>();
        List<String> l5 = new ArrayList<String>();
        List<String> l6 = new ArrayList<String>();
        List<String> l7 = new ArrayList<String>();
        List<String> l8 = new ArrayList<String>();*/

       HashMap<String, List<String>>childList= new HashMap<String,List<String>>();

       /*
        String basic_info[]=getResources().getStringArray(R.array.basic_information_list);
        String local_address[]=getResources().getStringArray(R.array.local_address);
        String permanent_address[]=getResources().getStringArray(R.array.permanent_address);
        String academic_details[]=getResources().getStringArray(R.array.academic_details);
        String student_profile[]=getResources().getStringArray(R.array.student_profile);
        String parents_profile[]=getResources().getStringArray(R.array.parent_details);
        String previous_institute[]=getResources().getStringArray(R.array.previous_institute);
        String medical_details[]=getResources().getStringArray(R.array.medical_details);*/

        for (String title: heading_item)
        {
            Heading.add(title);
        }


        /*for(String title: basic_info)
        {
            l1.add(title);
        }
        for(String title: local_address)
        {
            l2.add(title);
        }
        for(String title: permanent_address)
        {
            l3.add(title);
        }*/

        /*for(String title: academic_details)
        {
            l4.add(title);
        }
        for(String title: student_profile)
        {
            l5.add(title);
        }
        for(String title: parents_profile)
        {
            l6.add(title);
        }
        for(String title: previous_institute)
        {
            l7.add(title);
        }
        for(String title: medical_details)
        {
            l8.add(title);
        }*/

        childList.put(Heading.get(0),Basic_detail);
        childList.put(Heading.get(1),Academic_detail);
        childList.put(Heading.get(2),Address_detail);

        /*childList.put(Heading.get(3),l4);
        childList.put(Heading.get(4),l5);
        childList.put(Heading.get(5),l6);
        childList.put(Heading.get(6),l7);
        childList.put(Heading.get(7),l8);*/

        MyProfileAdapter myProfileAdapter = new MyProfileAdapter(this,Heading,childList);
        expandableListView.setAdapter(myProfileAdapter);

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    expandableListView.collapseGroup(lastExpandedPosition);

                }
                lastExpandedPosition = groupPosition;
            }
        });
    }

    private void setBasicDetails() {
        Basic_detail.add("Name: "+model.getFirstName()+" "+model.getMiddleName()+" "+model.getLastName());
        Basic_detail.add("Gender: "+model.getGender());
        Basic_detail.add("Date of Birth: "+model.getDateOfBirth());
        Basic_detail.add("Aadhar No.: "+model.getAdharNo());
        Basic_detail.add("Contact: "+model.getParentNumber());
        Basic_detail.add("Email: "+model.getParentEmail());
    }

    private void setAcademicDetails(){
        Academic_detail.add("Registration Number: "+model.getRegistrationNo());
        Academic_detail.add("Admission Number: "+model.getAdmissionNumber());
        Academic_detail.add("Admission Date: "+model.getAdmissionDate());
        Academic_detail.add("Academic Year: "+model.getAcademicYear());
    }

    private void setAddressDetails(){
        Address_detail.add("Local Address: "+model.getLocalAddress());
        Address_detail.add("Permanent Address: "+model.getPermanentAddress());
    }

    private void setClassAndSection() {
        studentClassSection.setText("("+model.getStClass()+ " , "+ model.getSection()+")");
    }

    private void getDataFromServer(){
        profileProgressBar();
        ApiInterface in = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<UserProfileModel> call = in.getProfileData(session.getUserId());
        call.enqueue(new Callback<UserProfileModel>() {
            @Override
            public void onResponse(Call<UserProfileModel> call, Response<UserProfileModel> response) {
                model = response.body();
                setBasicDetails();
                setAcademicDetails();
                setAddressDetails();
                setClassAndSection();

                progressDoalog.dismiss();

            }


            @Override
            public void onFailure(Call<UserProfileModel> call, Throwable t) {
                progressDoalog.dismiss();
                Log.i("DEBUG", "Error Ocurred");
            }
        });
    }

    private void profileProgressBar() {
        progressDoalog = new ProgressDialog(this);
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(true);
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
        progressDoalog.setMessage("Please Wait...");
//        progressDoalog.getWindow().setGravity(LinearLayout.LayoutParams.MATCH_PARENT LinearLayout.LayoutParams.MATCH_PARENT);
        progressDoalog.setIndeterminate(true);
        Circle wave=new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
            return true;
        }
        return false;
    }

    @Override
    public void setOnTitleText(String titleText) {

    }
}
