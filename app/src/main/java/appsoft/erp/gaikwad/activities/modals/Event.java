package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Event {

    @SerializedName("event_id")
    @Expose
    private String eventId;

    @SerializedName("event_name")
    @Expose
    private String eventTitle;

    @SerializedName("start_date")
    @Expose
    private String startDate;

    @SerializedName("end_date")
    @Expose
    private String endDate;

    @SerializedName("start_time")
    @Expose
    private String startTime;

    @SerializedName("end_time")
    @Expose
    private String endTime;

    @SerializedName("location")
    @Expose
    private String location;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("map")
    @Expose
    private String map;

    @SerializedName("type")
    @Expose
    private String feesType;

    @SerializedName("details")
    @Expose
    private String eventDesc;

    @SerializedName("fees")
    @Expose
    private String feesAmount;

    public Event(){}

    public Event(String eventId, String eventTitle, String startDate, String endDate, String startTime, String endTime, String location, String address, String map, String feesType, String eventDesc, String feesAmount) {
        this.eventId = eventId;
        this.eventTitle = eventTitle;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.location = location;
        this.address = address;
        this.map = map;
        this.feesType = feesType;
        this.eventDesc = eventDesc;
        this.feesAmount = feesAmount;
    }

    public String getEventId() {
        return eventId;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getLocation() {
        return location;
    }

    public String getAddress() {
        return address;
    }

    public String getMap() {
        return map;
    }

    public String getFeesType() {
        return feesType;
    }

    public String getEventDesc() {
        return eventDesc;
    }

    public String getFeesAmount() {
        return feesAmount;
    }
}
