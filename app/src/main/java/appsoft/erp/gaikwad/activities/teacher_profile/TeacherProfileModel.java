package appsoft.erp.gaikwad.activities.teacher_profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TeacherProfileModel {

    @SerializedName("basic")
    @Expose
    private Basic basic;
    @SerializedName("education")
    @Expose
    private List<Education> education = null;

    public Basic getBasic() {
        return basic;
    }

    public void setBasic(Basic basic) {
        this.basic = basic;
    }

    public List<Education> getEducation() {
        return education;
    }

    public void setEducation(List<Education> education) {
        this.education = education;
    }
}
