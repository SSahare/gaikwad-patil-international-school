package appsoft.erp.gaikwad.activities.parent_adapters;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.Assignments;
import appsoft.erp.gaikwad.activities.open_results.ParentAssignmentDetailActivity;
import appsoft.erp.gaikwad.activities.volley_singleton.NetworkStatus;

public class ParentAssignmentAdapter extends RecyclerView.Adapter<ParentAssignmentAdapter.ParentAssignmentHolder> implements NetworkStatus{

    // Parent Assignment Adapter
    private List<Assignments> assignmentsList;
    private Assignments assignments;
    private Context context;
    private String dateText = "%s\n%s\n%s";

    public ParentAssignmentAdapter(List<Assignments> assignmentsList, Context context){
        this.assignmentsList = assignmentsList;
        this.context = context;
    }

    @Override
    public ParentAssignmentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.parent_assignment_card,parent,false);
        return new ParentAssignmentHolder(view);
    }

    @Override
    public void onBindViewHolder(ParentAssignmentHolder holder, int position) {
        assignments = assignmentsList.get(position);
        String datestr=assignments.getGivenDate();
        String[] arrofdate=datestr.split("-");
        holder.givenDateMonth.setText(arrofdate[0]);
        holder.givenDateDay.setText(arrofdate[1]);
        holder.givenDateYear.setText(arrofdate[2]);
//        holder.givenDate.setText(assignments.getGivenDate());
//        holder.subDate.setText(assignments.getSubmissionDate());
        holder.subject.setText("Subject : "+assignments.getSubject());
        holder.title.setText(assignments.getTitle());
        holder.desc.setText(assignments.getDescription());

//        String attach_string = assignments.getAttachment();
//        try{
//            if (attach_string.isEmpty() || attach_string.equals(null)){
//                holder.download.setText("No attachment");
//                holder.fileIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.cancel));
//                holder.download.setEnabled(false);
//            }else{
//                holder.download.setText("Download attachment");
//                holder.download.setEnabled(true);
//                if (Pattern.matches("([^\\s]+(\\.(?i)(pdf))$)",attach_string)){
//                    holder.fileIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.pdf));
//                }else if (Pattern.matches("([^\\s]+(\\.(?i)(doc|docx))$)",attach_string)){
//                    holder.fileIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.word));
//                }else if (Pattern.matches("([^\\s]+(\\.(?i)(txt))$)",attach_string)){
//                    holder.fileIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.txt));
//                }else{
//                    holder.fileIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.image_icon));
//                }
//            }
//        }catch (NullPointerException npe){ Log.i("DEBUG", "Null pointer exp: "+npe.getMessage());}
    }

    public void filteredList(List<Assignments> list){
        assignmentsList = new ArrayList<>();
        assignmentsList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        try {
            return assignmentsList.size();
        }catch (NullPointerException npe){
            return 0;
        }
    }

    @Override
    public boolean isNetworkAvailable() {
        ConnectivityManager conMgr =  (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        return netInfo != null;
    }

    class ParentAssignmentHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView givenDate, givenDateMonth, givenDateDay, givenDateYear,subDate, title, desc, subject;
        private Button download;
        private ImageButton fileIcon;

        public ParentAssignmentHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);
            givenDateMonth = itemView.findViewById(R.id.dateofGivenMonth);
            givenDateDay = itemView.findViewById(R.id.dateofGivenDay);
            givenDateYear = itemView.findViewById(R.id.dateofGivenYear);
            title = itemView.findViewById(R.id.assignmentTitle);
            desc = itemView.findViewById(R.id.description);
            subject = itemView.findViewById(R.id.subject);
//            download = itemView.findViewById(R.id.download);

        }
        @Override
        public void onClick(View view) {
            int position=getAdapterPosition();
            Assignments assignments=assignmentsList.get(position);
            Intent assignmentIntent=new Intent(context,ParentAssignmentDetailActivity.class);
            assignmentIntent.putExtra("assignmenttitle",assignments.getTitle());
            assignmentIntent.putExtra("assignmentsubject",assignments.getSubject());
            assignmentIntent.putExtra("assignmentsubmissiondate",assignments.getSubmissionDate());
            assignmentIntent.putExtra("assignmentdescription",assignments.getDescription());
            assignmentIntent.putExtra("download",assignments.getAttachment());
            context.startActivity(assignmentIntent);
        }
    }
}