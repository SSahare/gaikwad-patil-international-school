package appsoft.erp.gaikwad.activities.modals;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StudentsName implements Parcelable{

    @SerializedName("student_id")
    @Expose
    private String studentId;

    @SerializedName("first_name")
    @Expose
    private String firstName;

    @SerializedName("middle_name")
    @Expose
    private String middleName;

    @SerializedName("last_name")
    @Expose
    private String lastName;

    @SerializedName("response")
    @Expose
    private String resp;

    public StudentsName(){}

    public StudentsName(String resp) {
        this.resp = resp;
    }

    public StudentsName(String studentId, String firstName, String middleName, String lastName) {
        this.studentId = studentId;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

    protected StudentsName(Parcel in) {
        studentId = in.readString();
        firstName = in.readString();
        middleName = in.readString();
        lastName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(studentId);
        dest.writeString(firstName);
        dest.writeString(middleName);
        dest.writeString(lastName);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null){
            return false;
        }

        StudentsName manpowerObj = (StudentsName) obj;
        if (manpowerObj.toString().equals(this.toString())){
            return true;
        }
        return false;
    }

    public static final Creator<StudentsName> CREATOR = new Creator<StudentsName>() {
        @Override
        public StudentsName createFromParcel(Parcel in) {
            return new StudentsName(in);
        }

        @Override
        public StudentsName[] newArray(int size) {
            return new StudentsName[size];
        }
    };


    public String getStudentId() {
        return studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String toString() {
        return firstName+" "+ middleName+ " " + lastName;
    }

    public String getResp() {
        return resp;
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
