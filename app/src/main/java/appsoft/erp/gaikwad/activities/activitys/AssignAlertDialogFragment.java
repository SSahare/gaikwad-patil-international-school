package appsoft.erp.gaikwad.activities.activitys;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import appsoft.erp.gaikwad.activities.modals.ResponseModel;
import appsoft.erp.gaikwad.activities.modals.StudentsName;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;


public class AssignAlertDialogFragment extends DialogFragment {

    ResponseHandler mResponseHandler;
    SessionManagement session;

    public static AssignAlertDialogFragment newInstance(){
        return new AssignAlertDialogFragment();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            mResponseHandler = (ResponseHandler) activity;
        }catch (ClassCastException cce){
            Log.i("DEBUG", "Class Cast Ex: "+cce.toString());
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        session = new SessionManagement(getContext());

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(false);
        builder.setTitle("Preview");

        final List<StudentsName> studList = getArguments().getParcelableArrayList("student_list");
        Log.i("DEBUG", ""+studList.size());
        //final Gson gson = new Gson();

        if (getArguments().getInt("size") != 0){

        builder.setMessage("Total "+ String.valueOf(getArguments().getInt("size") +" students present in Class "+getArguments().getString("className")+" "+getArguments().getString("sectionName")));
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                ApiInterface apiInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
                Call<ResponseModel> call = apiInterface.saveAttendance(
                        session.getUserId(),
                        getArguments().getString("className"),
                        getArguments().getString("sectionName"),
                        getArguments().getString("date"),
                        getManListId(studList)
                );

                call.enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                        ResponseModel mod = response.body();
                        try{
                            Log.i("DEBUG", mod.getRes());
                            mResponseHandler.getResponseString(mod.getRes());
                        }catch (Exception e){
                            mResponseHandler.getResponseString(e.toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {
                        mResponseHandler.getResponseString(t.toString());
                    }
                });
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        }else {
            builder.setMessage("All students are absent? Submit now.");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ApiInterface apiInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
                    Call<ResponseModel> call = apiInterface.saveAttendance(
                            session.getUserId(),
                            getArguments().getString("className"),
                            getArguments().getString("sectionName"),
                            getArguments().getString("date"),
                            null
                    );

                    call.enqueue(new Callback<ResponseModel>() {
                        @Override
                        public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                            ResponseModel mod = response.body();
                            try{
                                Log.i("DEBUG", mod.getRes());
                                mResponseHandler.getResponseString(mod.getRes());
                            }catch (Exception e){
                                mResponseHandler.getResponseString(e.toString());
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseModel> call, Throwable t) {
                            mResponseHandler.getResponseString("Empty attendance submitted.");
                        }
                    });
                }
            });

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }
        return builder.create();
    }

    private List<String> getManListId(List<StudentsName> studList) {

        List<String> list = new ArrayList<>();
        Iterator<StudentsName> it = studList.iterator();
        while (it.hasNext()){
            list.add(it.next().getStudentId());
        }
        return list;
    }

    public interface ResponseHandler{
        void getResponseString(String response);
    }
}
