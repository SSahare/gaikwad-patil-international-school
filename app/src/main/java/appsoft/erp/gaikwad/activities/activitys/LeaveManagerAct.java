package appsoft.erp.gaikwad.activities.activitys;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import appsoft.erp.gaikwad.activities.extras.TodaysDate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.ResponseModel;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import appsoft.erp.gaikwad.activities.volley_singleton.NetworkStatus;

public class LeaveManagerAct extends AppCompatActivity implements NetworkStatus {

    private EditText reason;
    public static TextView fromDate;
    public static TextView toDate;
    private String leaveStr = "";

    private SessionManagement session;
    private Spinner leaveType;
//    private ImageView buttonApply;
    private Button  seeLeaveStatus;
    private FloatingActionButton  apply;
    private static int toggleDateFlag = 0;


    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leave_manager_act);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        fromDate = findViewById(R.id.dateFrom);
        toDate = findViewById(R.id.dateTo);
        leaveType = findViewById(R.id.leave_type);
//        buttonApply=findViewById(R.id.buttonApply);
        apply=findViewById(R.id.fab);
        reason = findViewById(R.id.reasonForLeave);
        reason.setCursorVisible(true);
        reason.setFocusable(true);
        seeLeaveStatus = findViewById(R.id.seeLeaveStatus);

        session = new SessionManagement(getApplicationContext());
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.leave_types, R.layout.text_filter_view);
        adapter.setDropDownViewResource(R.layout.spinnerlist_leavemanager);
        leaveType.setAdapter(adapter);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        leaveType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                leaveStr = (String) parent.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        fromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callDatePickerFrom(view);
            }
        });

        toDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callDatePickerTo(view);
            }
        });
    }

    public void goToStatus(View view){
        Intent newStatus = new Intent(this, LeaveStatus.class);
        startActivity(newStatus);
    }

    public void applyForLeave(View view) throws Exception {

        Log.i("DEBUG", leaveStr);
        if (isAllFieldFilled()) {
            if (isNetworkAvailable()) {

                if (checkDateValidation()) {
                    ApiInterface leaveApi = ClientAPI.getRetrofit().create(ApiInterface.class);
                    Call<ResponseModel> call = leaveApi.applyForLeave(
                            session.getUserId(),
                            leaveStr,
                            fromDate.getText().toString(),
                            toDate.getText().toString(),
                            reason.getText().toString()
                    );

                    call.enqueue(new Callback<ResponseModel>() {
                        @Override
                        public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                            ResponseModel res = response.body();
                            try {
                                Toast t = Toast.makeText(getApplicationContext(), res.getRes(), Toast.LENGTH_LONG);
                                t.setGravity(Gravity.CENTER, 0, 0);
                                t.show();
                                refreshView();
                            } catch (Exception exp) {
                                Log.i("DEBUG", "Leave Exp: " + exp.toString());
                                refreshView();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseModel> call, Throwable t) {
                            Log.i("DEBUG", "Leave Failure: " + t.toString());
                        }
                    });
                }else {
                    Toast.makeText(this, "Please, check dates properly", Toast.LENGTH_SHORT).show();
                }
                } else {
                    Toast.makeText(this, "Check internet connection.", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Please, fill all required field.", Toast.LENGTH_SHORT).show();
            }
        }

    private void refreshView(){
        toDate.setText("");
        fromDate.setText("");
        reason.setText("");
    }

    private boolean isAllFieldFilled() {

        if (reason.getText().toString().trim().isEmpty()){
            return false;
        }else if (fromDate.getText().toString().trim().isEmpty()){
            return false;
        }else if (toDate.getText().toString().trim().isEmpty()){
            return false;
        }else if (leaveStr.trim().isEmpty()){
            return false;
        }else {
            return true;
        }
    }

    private void callDatePickerFrom(View view){
        NewDatePicker datefragment = new NewDatePicker();
        datefragment.setFlag(NewDatePicker.FLAG_START_DATE);
        datefragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void callDatePickerTo(View view){
        NewDatePicker datefragment = new NewDatePicker();
        datefragment.setFlag(NewDatePicker.FLAG_END_DATE);
        datefragment.show(getSupportFragmentManager(), "datePicker2");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
            return true;
        }
        return false;
    }

    @Override
    public boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        return networkInfo!=null;
    }

    public static class NewDatePicker extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        public static final int FLAG_START_DATE = 0;
        public static final int FLAG_END_DATE = 1;

        private int flag = 0;

        public NewDatePicker(){}

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void setFlag(int i) {
            flag = i;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            Calendar calendar = new GregorianCalendar(year,month,day);
            final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.ENGLISH);
            if (flag == FLAG_START_DATE) {
                if (!checkPreDate(dateFormat.format(calendar.getTime()))){
                    fromDate.setText(dateFormat.format(calendar.getTime()));
                }else {
                    Toast.makeText(getContext(), "Please, do not choose past date.", Toast.LENGTH_SHORT).show();
                }

            } else if (flag == FLAG_END_DATE) {
                if (!checkPreDate(dateFormat.format(calendar.getTime()))){
                    toDate.setText(dateFormat.format(calendar.getTime()));
                }else {
                    Toast.makeText(getContext(), "Please, do not choose past date.", Toast.LENGTH_SHORT).show();
                }
            }
        }

        private boolean checkPreDate(String selectedDate){
            try{
                DateFormat format = new SimpleDateFormat("MMM d, yyyy", Locale.ENGLISH);
                Date dateSelected = format.parse(selectedDate);
                Date dateCurrent = format.parse(TodaysDate.getTodaysDate());

                long valide = dateCurrent.getTime() - dateSelected.getTime();
                if (valide>0){
                    return true;
                }
            }catch (ParseException x){
                x.printStackTrace();
                return false;
            }
            return false;
        }
    }
    private boolean checkDateValidation() throws Exception{

        // How to make code robust
        String from = fromDate.getText().toString();
        String to = toDate.getText().toString();
        if (!(from.isEmpty() && to.isEmpty())){
            // We have both Dates
            DateFormat format = new SimpleDateFormat("MMM d, yyyy", Locale.ENGLISH);
            Date dateFrom = format.parse(from);
            Date dateTo = format.parse(to);
            long valide = dateTo.getTime() - dateFrom.getTime();
            if (valide >= 0){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }
}
