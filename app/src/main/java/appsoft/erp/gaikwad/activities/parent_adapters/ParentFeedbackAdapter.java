package appsoft.erp.gaikwad.activities.parent_adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.Feedback;
import appsoft.erp.gaikwad.activities.open_results.ParentDetailFeedbackActivity;

public class ParentFeedbackAdapter extends RecyclerView.Adapter<ParentFeedbackAdapter.PFeedbackHolder>{

    private List<Feedback> feedbackList;
    private Feedback feedbackObj;
    private String dateText = "%s\n%s\n%s";
    Context context;

    public ParentFeedbackAdapter( Context context,List<Feedback> feedbackList){
        this.feedbackList = feedbackList;
        this.context=context;
    }

    @Override
    public PFeedbackHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.parent_feedback_card,parent,false);
        return new PFeedbackHolder(view);
    }

    @Override
    public void onBindViewHolder(PFeedbackHolder holder, int position) {
        feedbackObj = feedbackList.get(position);
        String datestr=feedbackObj.getDate();
        String[] arrofstr=datestr.split("-");
        holder.givenDateMonth.setText(arrofstr[0]);
        holder.givenDateDay.setText(arrofstr[1]);
        holder.givenDateYear.setText(arrofstr[2]);
//        holder.date.setText(feedbackObj.getDate());
        holder.feed.setText(feedbackObj.getFeedback());
    }

    @Override
    public int getItemCount() {
        return feedbackList.size();
    }

    public void filteredList(List<Feedback> list){
        feedbackList = new ArrayList<>();
        feedbackList.addAll(list);
        notifyDataSetChanged();
    }

    class PFeedbackHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView date, feed;
        ImageView iv_icon;
        TextView  givenDateMonth, givenDateDay, givenDateYear ;

        public PFeedbackHolder(View itemView) {
            super(itemView);
                itemView.setOnClickListener(this);
            givenDateMonth = itemView.findViewById(R.id.dateofGivenMonth);
            iv_icon=itemView.findViewById(R.id.iv_icon);
            givenDateDay = itemView.findViewById(R.id.dateofGivenDay);
            givenDateYear = itemView.findViewById(R.id.dateofGivenYear);
//            date = itemView.findViewById(R.id.feedbackDate);
            feed = itemView.findViewById(R.id.studentsFeedback);
        }

        @Override
        public void onClick(View view) {
            int position= getAdapterPosition();
            Feedback feedback=feedbackList.get(position);
            Intent feedbackintent=new Intent(context,ParentDetailFeedbackActivity.class);
            feedbackintent.putExtra("feedbackdestudent",feedback.getFeedback());
            context.startActivity(feedbackintent);

        }
    }
}
