package appsoft.erp.gaikwad.activities.open_results;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;

import appsoft.erp.gaikwad.R;

public class EventActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView title, location, address, entryType;
    private TextView desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        if (getSupportActionBar() != null) {

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Event Detail");

        }

        title = findViewById(R.id.eventTitle);
        location = findViewById(R.id.location);
        address = findViewById(R.id.address);
        entryType = findViewById(R.id.entryType);
        desc = findViewById(R.id.description);

        title.setText(getIntent().getStringExtra("title"));
        location.setText(StringUtils.capitalize(getIntent().getStringExtra("location").toLowerCase().trim()));
        address.setText(StringUtils.capitalize(getIntent().getStringExtra("address").toLowerCase().trim()));
        entryType.setText(StringUtils.capitalize(getIntent().getStringExtra("entryType").toLowerCase().trim()));
        desc.setText(getIntent().getStringExtra("desc"));

        address.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.address) {

            final AlertDialog alertDialog = new AlertDialog.Builder(
                    EventActivity.this).create();

            alertDialog.setTitle("Show Address");

            alertDialog.setMessage(address.getText().toString());

//            alertDialog.getButton(alertDialog.BUTTON_NEUTRAL).setTextColor(getResources().getColor(R.color.orange));
               alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialogInterface, int i) {

                       dialogInterface.dismiss();
                   }
               });





            alertDialog.show();
    }












        }
//            AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
//            builder.setTitle("Address");
//            builder.setMessage(address.getText().toString());
//            builder.setNeutralButton("CLOSE", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    dialogInterface.dismiss();
//
//                }
//
//
//            });

//            builder.show();

//            final Dialog myDialog = new Dialog(this);
////            myDialog.setContentView(R.layout.);
//            myDialog.setTitle("Address");
//            myDialog.setCancelable(false);
//
//
//            address.setOnClickListener(new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//
//                }
//
//
//            }

//            AlertDialog dialog = builder.create();
//            dialog.show();
//        }
    }


