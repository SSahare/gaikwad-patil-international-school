package appsoft.erp.gaikwad.activities.helper;

/*
import android.content.Context;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import appsoft.erp.jagat.activities.interfaces.AddActionInterface;
import appsoft.erp.jagat.activities.modals.Class;
import appsoft.erp.jagat.activities.modals.Section;
import appsoft.erp.jagat.activities.modals.Subject;
import appsoft.erp.jagat.activities.rest_api_config.ApiInterface;
import appsoft.erp.jagat.activities.rest_api_config.ClientAPI;
import appsoft.erp.jagat.activities.session.SessionManagement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddActionHelper {
    private AddActionInterface addActionInterface;

    public static void getClasses(Context context,AddActionInterface addActionInterface){
        SessionManagement sessionManagement = new SessionManagement(context);
        getClassesFromServer(sessionManagement.getUserId(),addActionInterface);
    }

     public static void getSection(Context context,String className,AddActionInterface addActionInterface){
        SessionManagement sessionManagement = new SessionManagement(context);
        getSectionFromServer(sessionManagement.getUserId(),className,addActionInterface);
     }

     public static void getSubject(Context context,String className,String section ,AddActionInterface addActionInterface){
        SessionManagement sessionManagement = new SessionManagement(context);
        getSubjectFromServer(sessionManagement.getUserId(),className,section,addActionInterface);
     }



    private static void getClassesFromServer(int userId, final AddActionInterface addActionInterface) {
        ApiInterface apiInterface = ClientAPI.getRetrofit().create(ApiInterface.class);


        Call<List<Class>> call =apiInterface.getAssignedClasses(userId);
        call.enqueue(new Callback<List<Class>>() {
            @Override
            public void onResponse(@NotNull Call<List<Class>> call, @NotNull Response<List<Class>> response) {
                if (response.isSuccessful()){
                    try {
                        List<Class> classList = response.body();
                        List<String> list = new ArrayList<>();
                        list.add("Class");
                        if (classList !=null){
                            for (Class c : classList){
                                list.add(c.getClasses());

                            }
                        }
                        addActionInterface.getDataListerner(list);

                    }catch (Exception e){
                        Log.i("DEBUG","List error:"+e.getMessage());


                    }

                }

            }

            @Override
            public void onFailure( @NotNull Call<List<Class>> call, @NotNull Throwable t) {

            }
        });





    }


    private static void getSectionFromServer(int userId, String className, final AddActionInterface addActionInterface) {
        ApiInterface apiInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<Section>> call = apiInterface.getAssignedClassSections(userId,className);
        call.enqueue(new Callback<List<Section>>() {
            @Override
            public void onResponse( @NotNull Call<List<Section>> call, @NotNull  Response<List<Section>> response) {
                if (response.isSuccessful()){
                    try {
                        List<Section> sectionList=response.body();
                        List<String>list = new ArrayList<>();
                        list.add("Section");
                        if (sectionList != null){
                                for (Section sec:sectionList){
                                    list.add(sec.getSection());
                                }
                        }
                        addActionInterface.getDataListerner(list);

                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NotNull Call<List<Section>> call,  @NotNull Throwable t) {

            }
        });


    }


    private static void getSubjectFromServer(int userId, String className, final String section, final AddActionInterface addActionInterface) {
        ApiInterface apiInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<Subject>> call = apiInterface.getAssignedClassSubject(userId,className,section);
        call.enqueue(new Callback<List<Subject>>() {
            @Override
            public void onResponse( @NotNull Call<List<Subject>> call, @NotNull  Response<List<Subject>> response) {
                if (response.isSuccessful()){
                    try {
                        List<Subject> subjectList =response.body();
                        List<String> list = new ArrayList<>();
                        list.add("Subject");
                        if (subjectList !=null){
                            for (Subject subject :subjectList){
                                list.add(subject.getSubject());
                            }
                        }
                        addActionInterface.getDataListerner(list);

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure( @NotNull Call<List<Subject>> call,  @NotNull Throwable t) {

            }
        });



    }



}
*/












import android.content.Context;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


import appsoft.erp.gaikwad.activities.interfaces.AddActionInterface;
import appsoft.erp.gaikwad.activities.modals.Class;
import appsoft.erp.gaikwad.activities.modals.Section;
import appsoft.erp.gaikwad.activities.modals.Subject;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddActionHelper {

    private AddActionInterface actionInterface;

    public static void getClasses(Context context, AddActionInterface actionInterface){
        SessionManagement session = new SessionManagement(context);
        getClassesFromServer(session.getUserId(), actionInterface);
    }

    public static void getSection(Context context, String className, AddActionInterface actionInterface){
        SessionManagement session = new SessionManagement(context);
        getSectionFromServer(session.getUserId(), className, actionInterface);
    }

    public static void getSubjects(Context context, String className, String section, AddActionInterface actionInterface){
        SessionManagement session = new SessionManagement(context);
        getSubjectsFromServer(session.getUserId(), className, section, actionInterface);
    }

    private static void getClassesFromServer(int userId, final AddActionInterface addActionInterface) {

        ApiInterface apiInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<Class>> call = apiInterface.getAssignedClasses(userId);
        call.enqueue(new Callback<List<Class>>() {
            @Override
            public void onResponse(@NotNull Call<List<Class>> call, @NotNull Response<List<Class>> response) {
                if (response.isSuccessful()){
                    try{
                        List<Class> classList = response.body();
                        List<String> list = new ArrayList<>();
                        list.add("Class");
                        if (classList != null){
                            for (Class c : classList){
                                list.add(c.getClasses());
                            }
                        }
                        addActionInterface.getDataListener(list);
                    }catch (Exception e){
                        Log.i("DEBUG", "List error: "+e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<Class>> call, @NotNull Throwable t) {
            }
        });
    }

    private static void getSectionFromServer(int userId, String className, final AddActionInterface actionInterface) {
        ApiInterface apiInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<Section>> call = apiInterface.getAssignedClassSection(userId, className);
        call.enqueue(new Callback<List<Section>>() {
            @Override
            public void onResponse(@NotNull Call<List<Section>> call, @NotNull Response<List<Section>> response) {
                if (response.isSuccessful()){
                    try{
                        List<Section> sectionList = response.body();
                        List<String> list = new ArrayList<>();
                        list.add("Section");
                        if (sectionList != null){
                            for (Section sec : sectionList){
                                list.add(sec.getSection());
                            }
                        }
                        actionInterface.getDataListener(list);
                    }catch (Exception e){e.printStackTrace();}
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<Section>> call, @NotNull Throwable t) {
            }
        });
    }

    private static void getSubjectsFromServer(int userId, String className, String section, final AddActionInterface addActionInterface) {
        ApiInterface apiInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<List<Subject>> call = apiInterface.getAssignedSubjects(userId, className, section);
        call.enqueue(new Callback<List<Subject>>() {
            @Override
            public void onResponse(@NotNull Call<List<Subject>> call, @NotNull Response<List<Subject>> response) {
                if (response.isSuccessful()){
                    try{
                        List<Subject> subjectList = response.body();
                        List<String> list = new ArrayList<>();
                        list.add("Subject");
                        if (subjectList != null){
                            for (Subject sub : subjectList){
                                list.add(sub.getSubject());
                            }
                        }
                        addActionInterface.getDataListener(list);
                    }catch (Exception e){e.printStackTrace();}

                }
            }

            @Override
            public void onFailure(@NotNull Call<List<Subject>> call, @NotNull Throwable t) {
            }
        });
    }
}