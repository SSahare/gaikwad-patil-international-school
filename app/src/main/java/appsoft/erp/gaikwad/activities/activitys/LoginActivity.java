package appsoft.erp.gaikwad.activities.activitys;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Circle;

import retrofit2.Call;
import retrofit2.Callback;
import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.LoginModel;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;

import static appsoft.erp.gaikwad.activities.extras.Constants.LOGIN_TYPE_AS_PARENT;
import static appsoft.erp.gaikwad.activities.extras.Constants.LOGIN_TYPE_AS_TEACHER;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private TextInputEditText username, password;
    private Button loginButton;
    private TextView version;
    private static final int PARENT_REQ = 101;
    private static final int TEACHER_REQ = 102;
    ProgressDialog progressDoalog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        initialiseViews();
        loginButton.setOnClickListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progressDoalog != null){
            progressDoalog.dismiss();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (progressDoalog != null){
            progressDoalog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDoalog != null){
            progressDoalog.dismiss();
        }
    }

    @Override
    public void onClick(View view) {

        ApiInterface apiInterface = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<LoginModel> call = apiInterface.getLoginCredentials(
                username.getText().toString(),
              password.getText().toString());
                Log.i("DEBUG", username.getText().toString());


        progressDoalog = new ProgressDialog(this);
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(false);
        progressDoalog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDoalog.getWindow().setGravity(Gravity.CENTER);
        progressDoalog.setIndeterminate(true);
        Circle wave=new Circle();
        progressDoalog.setIndeterminateDrawable(wave);
        progressDoalog.show();


        call.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, retrofit2.Response<LoginModel> response) throws NullPointerException{
                Log.i("DEBUG", "On responce Called");


                if (response.isSuccessful()) {
                    Log.i("DEBUG", "onResponse: Response Successfull");
                    LoginModel model = response.body();

                    if (model.isFirstLogin()) {
                        Intent intent = new Intent(LoginActivity.this, ForceFullLoginActivity.class);
                        intent.putExtra("username", model.getUsername());
                        startActivity(intent);
                    }
                    else {
                    boolean isLogin = model.isLogged_in();
                        Log.i("DEBUG", "Login Val: " + isLogin);

                    if (isLogin) {
                        switch (model.getUser_role()) {
                            case LOGIN_TYPE_AS_PARENT:
                                Log.i("DEBUG", LOGIN_TYPE_AS_PARENT);
                                progressDoalog.dismiss();
                                Toast.makeText(getApplicationContext(), "Login Successful...", Toast.LENGTH_SHORT).show();
                                sendLoginIntent(model, PARENT_REQ);
                                break;

                            case LOGIN_TYPE_AS_TEACHER:
                                Log.i("DEBUG", LOGIN_TYPE_AS_TEACHER);
                                progressDoalog.dismiss();
                                Toast.makeText(getApplicationContext(), "Login Successful...", Toast.LENGTH_SHORT).show();
                                sendLoginIntent(model, TEACHER_REQ);
                                break;

                            default:
                                Toast toast = Toast.makeText(getApplicationContext(), "Invalid User Role", Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                                progressDoalog.dismiss();
                                break;
                        }
                    }
                    else {
                        Toast toast = Toast.makeText(getApplicationContext(), model.getError(), Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        progressDoalog.dismiss();
                    }
                }
                }else{
                    Log.i("DEBUG", response.message());
                    progressDoalog.dismiss();
                    Toast unSuccessful = Toast.makeText(getApplicationContext(),
                            "Server Internal Error or Data not found",
                            Toast.LENGTH_LONG);
                    unSuccessful.setGravity(Gravity.CENTER, 0, 0);
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                progressDoalog.dismiss();
                Toast failure = Toast.makeText(getApplicationContext(),
                        "Login Failed..",
                        Toast.LENGTH_LONG);
                Log.i("DEBUG", "onFailure: "+t.getMessage());
                failure.setGravity(Gravity.CENTER, 0, 0);
                failure.show();
            }


        });
    }


    private void sendLoginIntent(LoginModel model, int req) {
        Intent loginIntent = new Intent(getApplicationContext(), HomeActivity.class);
        int id = model.getUser_id();
        loginIntent.putExtra("KEY_Role", model.getUser_role());
        loginIntent.putExtra("KEY_ID",model.getUser_id());
        loginIntent.putExtra("KEY_FirstName", model.getFirst_name());
        loginIntent.putExtra("KEY_LastName", model.getLast_name());
        loginIntent.putExtra("KEY_ImagePath", model.getProfile_photo());
        startActivityForResult(loginIntent, req);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PARENT_REQ){
            finishLoginActivity();
        }else if(requestCode == TEACHER_REQ){
            finishLoginActivity();
        }
    }

    private void finishLoginActivity(){
        finish();
    }

    private void initialiseViews() {
        version=findViewById(R.id.tv_Version);
        username = findViewById(R.id.isync_username);
        password = findViewById(R.id.isync_password);
        loginButton = findViewById(R.id.isync_login_button);
        setStatusBarColor();

        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), 0);
            version.setText("v " + info.versionName);
            info = null;
        } catch (PackageManager.NameNotFoundException a) {
            a.printStackTrace();
        }
    }

    private void setStatusBarColor() {
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorBackForLogo));
    }
}