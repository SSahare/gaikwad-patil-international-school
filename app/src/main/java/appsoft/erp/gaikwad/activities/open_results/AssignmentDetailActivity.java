package appsoft.erp.gaikwad.activities.open_results;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.download.Downloader;

import static appsoft.erp.gaikwad.activities.extras.Constants.ASSIGNMENT_SUB_URL;
import static appsoft.erp.gaikwad.activities.extras.Constants.ATTACHMENT_BASE_URL;

public class AssignmentDetailActivity extends AppCompatActivity {
    TextView assignmenttitle, tv_AssignmentSubmissiondate, assignmentclass, assignmentsection, assignemntdescription;
    Button download;
    private static final String TAG = "assignmentclass";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment_detail);
        assignmenttitle = findViewById(R.id.tv_AssignmentTitle);
        assignmentclass = findViewById(R.id.tv_AssignmentClass);
        assignmentsection = findViewById(R.id.tv_Assignmentsection);
        assignemntdescription = findViewById(R.id.tv_AssignmentDescription);
        tv_AssignmentSubmissiondate = findViewById(R.id.tv_AssignmentSubmissiondate);
        download = findViewById(R.id.download);

        if (getIntent().getStringExtra("assignmentdownload").isEmpty()) {
            download.setVisibility(View.GONE);
        }

        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ATTACHMENT_BASE_URL.isEmpty() || ATTACHMENT_BASE_URL.isEmpty()) {
                    download.setText("No Attachment");
                } else {
                    download.setVisibility(View.VISIBLE);
                    String Url = new StringBuilder().append(ATTACHMENT_BASE_URL).append(ASSIGNMENT_SUB_URL).append(getIntent().getStringExtra("assignmentdownload")).toString();
                    Toast.makeText(getApplicationContext(), "Attachment Downloading...", Toast.LENGTH_SHORT).show();
                    Downloader downloader = new Downloader(Url, getApplicationContext());
                    downloader.downloadTask();
                }
            }
        });

        assignmenttitle.setText(getIntent().getStringExtra("assignmenttitle"));
        assignmentclass.setText("Class :" + getIntent().getStringExtra("assignmentclass"));
        Log.d(TAG, "no output");
        assignmentsection.setText("Section :" + getIntent().getStringExtra("assignmentsection"));
        assignemntdescription.setText(getIntent().getStringExtra("assignmentdescription"));
        tv_AssignmentSubmissiondate.setText("Submission Date :" + getIntent().getStringExtra("assignmentsubmissiondate"));

        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            getSupportActionBar().setTitle(getIntent().getStringExtra("assignmentsubject"));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}


