package appsoft.erp.gaikwad.activities.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.interfaces.OnTitleSetListener;
import appsoft.erp.gaikwad.activities.modals.UserProfileModel;
import appsoft.erp.gaikwad.activities.rest_api_config.ApiInterface;
import appsoft.erp.gaikwad.activities.rest_api_config.ClientAPI;
import appsoft.erp.gaikwad.activities.session.SessionManagement;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends Fragment  implements OnTitleSetListener {

    private int lastExpandedPosition = -1;
    private ExpandableListView expandableListView;
    List<String> Basic_detail;
    List<String> Academic_detail;
    List<String> Address_detail;
    private String[] heading_item;
    private TextView studentFullName;
    private TextView  studentId;
    private TextView  studentClassSection;
    private CircleImageView studentProfilePhoto;
    private SessionManagement session;
    UserProfileModel model;
    LinearLayout basicdetail,educationaldetail,addressdetail;
    private   static  OnTitleSetListener onTitleSetListener = null ;

    public ProfileFragment(){}

    public static ProfileFragment  getInstance(OnTitleSetListener onTitleSetListener1){
        onTitleSetListener = onTitleSetListener1;
        return new ProfileFragment();
    }


    @Override
    public void onResume() {
        super.onResume();
        settitle();
    }

    private void settitle() {

         onTitleSetListener.setOnTitleText("Profile");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.profile_activity, container, false);

        session = new SessionManagement(getContext());

        basicdetail=view.findViewById(R.id.linearlayout_basic_detail);
        educationaldetail=view.findViewById(R.id.linearlayout_educational_detail);
        addressdetail=view.findViewById(R.id.linearlayout_address_detail);
        studentFullName=view.findViewById(R.id.studentName);
        studentClassSection = view.findViewById(R.id.studentClassSection);
        studentId = view.findViewById(R.id.studentId);
        studentProfilePhoto = view.findViewById(R.id.studentProfilePhoto);
        studentFullName.setText(session.getUserName());
        studentId.setText(String.valueOf(session.getUserId()));
        getDataFromServer();


        Log.i("DEBUG", session.getImagePath());
        if (session.getImageLastPath().isEmpty() || session.getImageLastPath().equals(null) || session.getImageLastPath() == ""){
            Glide.with(this)
                    .load("")
                    .placeholder(getResources().getDrawable(R.drawable.person))
                    .fitCenter()
                    .into(studentProfilePhoto);
        }else {
            Glide.with(this)
                    .load(session.getImagePath())
                    .fitCenter()
                    .into(studentProfilePhoto);
        }




        basicdetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),BasicDetailActivity.class);
                startActivity(intent);
            }
        });

        educationaldetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(getContext(),EducationalActivity.class);
                startActivity(intent);
            }
        });

        addressdetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 Intent intent = new Intent(getContext(),AddressDetailActivity.class);
                 startActivity(intent);

            }
        });
        return view;
    }

    private void getDataFromServer() {

        ApiInterface in = ClientAPI.getRetrofit().create(ApiInterface.class);
        Call<UserProfileModel> call = in.getProfileData(session.getUserId());
        call.enqueue(new Callback<UserProfileModel>() {
            @Override
            public void onResponse(Call<UserProfileModel> call, Response<UserProfileModel> response) {
                model = response.body();

                setClassAndSection();

            }

            @Override
            public void onFailure(Call<UserProfileModel> call, Throwable t) {

                Log.i("DEBUG", "Error Ocurred");

            }
        });
    }

    private void setClassAndSection() {

        studentClassSection.setText("("+model.getStClass()+ " , "+ model.getSection()+")");

    }





    @Override
    public void setOnTitleText(String titleText) {

        onTitleSetListener.setOnTitleText("Profile");
    }
}

