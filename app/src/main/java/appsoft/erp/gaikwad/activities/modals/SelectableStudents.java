package appsoft.erp.gaikwad.activities.modals;

public class SelectableStudents extends StudentsName{

    private boolean isSelect = false;

    public SelectableStudents(StudentsName studentsName, boolean isSelected) {
        super(studentsName.getStudentId(), studentsName.getFirstName(), studentsName.getMiddleName(), studentsName.getLastName());
        this.isSelect = isSelected;
    }

    public SelectableStudents(){}

    public boolean isSelected() {
        return isSelect;
    }

    public void setSelected(boolean selected) {
        isSelect = selected;
    }
}
