package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReportModel {

    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("marks_obtained")
    @Expose
    private String marksObtained;
    @SerializedName("out_of_marks")
    @Expose
    private String outOfMarks;
    // it will change iin future
    @SerializedName("result")
    @Expose
    private String result;


    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMarksObtained() {
        return marksObtained;
    }

    public void setMarksObtained(String marksObtained) {
        this.marksObtained = marksObtained;
    }

    public String getOutOfMarks() {
        return outOfMarks;
    }

    public void setOutOfMarks(String outOfMarks) {
        this.outOfMarks = outOfMarks;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
