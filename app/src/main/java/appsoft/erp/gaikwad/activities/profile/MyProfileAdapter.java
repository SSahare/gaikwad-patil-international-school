package appsoft.erp.gaikwad.activities.profile;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import appsoft.erp.gaikwad.R;

public class MyProfileAdapter extends BaseExpandableListAdapter{
    private List<String> heading_titles;
    private HashMap<String,List<String>> header_contents;
    private Context ctx;

    MyProfileAdapter(Context ctx, List<String> heading_titles, HashMap<String,List<String>> header_contents)
    {
        this.ctx=ctx;
        this.header_contents=header_contents;
        this.heading_titles=heading_titles;
    }

    @Override
    public int getGroupCount() {
        return heading_titles.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return header_contents.get(heading_titles.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return heading_titles.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return header_contents.get(heading_titles.get(i)).get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        String title = (String)this.getGroup(i);
        if(view == null )
        {
            LayoutInflater layoutInflater=(LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=layoutInflater.inflate(R.layout.profile_list_view_layout,null);
        }
        TextView textView= (TextView) view.findViewById(R.id.headingItem);
        textView.setTypeface(null, Typeface.BOLD);
        textView.setText(title);
        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        String title=(String)this.getChild(i,i1);
        if(view == null)
        {
            LayoutInflater layoutInflater=(LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.my_profile_list_content,null);
        }
        TextView textView= (TextView) view.findViewById(R.id.childItem);
        textView.setText(title);

        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
