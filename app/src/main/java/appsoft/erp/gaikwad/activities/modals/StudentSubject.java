package appsoft.erp.gaikwad.activities.modals;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StudentSubject {
    @SerializedName("m_nursery_to_kg2_c")
    @Expose
    List<String> m_nursery_to_kg2 = new ArrayList<>();

    @SerializedName("g_nursery_to_kg2_c")
    @Expose
    List<String> g_nursery_to_kg2 = new ArrayList<>();

    @SerializedName("m_1_c")
    @Expose
    List<String> m_1 = new ArrayList<>();

    @SerializedName("g_1_to_4_c")
    @Expose
    List<String> g_1_to_4 = new ArrayList<>();

    @SerializedName("m_2_to_3_c")
    @Expose
    List<String> m_2_to_3 = new ArrayList<>();

    @SerializedName("m_4_c")
    @Expose
    List<String> m_4 = new ArrayList<>();

    @SerializedName("m_5_c")
    @Expose
    List<String> m_5 = new ArrayList<>();

    @SerializedName("g_5_to_7_c")
    @Expose
    List<String> g_5_to_7 = new ArrayList<>();

    @SerializedName("m_6_to_7_c")
    @Expose
    List<String> m_6_to_7 = new ArrayList<>();

    @SerializedName("m_8_to_9_c")
    @Expose
    List<String> m_8_to_9 = new ArrayList<>();

    @SerializedName("g_8_c")
    @Expose
    List<String> g_8 = new ArrayList<>();

    @SerializedName("g_9_c")
    @Expose
    List<String> g_9 = new ArrayList<>();

    @SerializedName("m_10_c")
    @Expose
    List<String> m_10 = new ArrayList<>();

    @SerializedName("g_10_c")
    @Expose
    List<String> g_10 = new ArrayList<>();

    public List<String> getM_nursery_to_kg2() {
        return m_nursery_to_kg2;
    }

    public void setM_nursery_to_kg2(List<String> m_nursery_to_kg2) {
        this.m_nursery_to_kg2 = m_nursery_to_kg2;
    }

    public List<String> getG_nursery_to_kg2() {
        return g_nursery_to_kg2;
    }

    public void setG_nursery_to_kg2(List<String> g_nursery_to_kg2) {
        this.g_nursery_to_kg2 = g_nursery_to_kg2;
    }

    public List<String> getM_1() {
        return m_1;
    }

    public void setM_1(List<String> m_1) {
        this.m_1 = m_1;
    }

    public List<String> getG_1_to_4() {
        return g_1_to_4;
    }

    public void setG_1_to_4(List<String> g_1_to_4) {
        this.g_1_to_4 = g_1_to_4;
    }

    public List<String> getM_2_to_3() {
        return m_2_to_3;
    }

    public void setM_2_to_3(List<String> m_2_to_3) {
        this.m_2_to_3 = m_2_to_3;
    }

    public List<String> getM_4() {
        return m_4;
    }

    public void setM_4(List<String> m_4) {
        this.m_4 = m_4;
    }

    public List<String> getM_5() {
        return m_5;
    }

    public void setM_5(List<String> m_5) {
        this.m_5 = m_5;
    }

    public List<String> getG_5_to_7() {
        return g_5_to_7;
    }

    public void setG_5_to_7(List<String> g_5_to_7) {
        this.g_5_to_7 = g_5_to_7;
    }

    public List<String> getM_6_to_7() {
        return m_6_to_7;
    }

    public void setM_6_to_7(List<String> m_6_to_7) {
        this.m_6_to_7 = m_6_to_7;
    }

    public List<String> getM_8_to_9() {
        return m_8_to_9;
    }

    public void setM_8_to_9(List<String> m_8_to_9) {
        this.m_8_to_9 = m_8_to_9;
    }

    public List<String> getG_8() {
        return g_8;
    }

    public void setG_8(List<String> g_8) {
        this.g_8 = g_8;
    }

    public List<String> getG_9() {
        return g_9;
    }

    public void setG_9(List<String> g_9) {
        this.g_9 = g_9;
    }

    public List<String> getM_10() {
        return m_10;
    }

    public void setM_10(List<String> m_10) {
        this.m_10 = m_10;
    }

    public List<String> getG_10() {
        return g_10;
    }

    public void setG_10(List<String> g_10) {
        this.g_10 = g_10;
    }
}