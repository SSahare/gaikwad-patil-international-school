package appsoft.erp.gaikwad.activities.modals;

import com.google.gson.annotations.SerializedName;

public class ClassTimeTable {

    @SerializedName("time")
    private String time;

    @SerializedName("subject")
    private String subject;

    public ClassTimeTable(String time, String subject) {
        this.time = time;
        this.subject = subject;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTime() {
        return time;
    }

    public String getSubject() {
        return subject;
    }
}
