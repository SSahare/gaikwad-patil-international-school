package appsoft.erp.gaikwad.activities.parent_adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import appsoft.erp.gaikwad.R;
import appsoft.erp.gaikwad.activities.modals.Log;
import appsoft.erp.gaikwad.activities.open_results.ParentLogDetailActivity;

public class ParentLogAdapter extends RecyclerView.Adapter<ParentLogAdapter.ParentLogHolder>{

    private List<Log> logList;
    private Log logObj;
    private String dateText = "%s\n%s\n%s";
     Context context;

     public ParentLogAdapter(Context context, List<Log> logList){
        this.logList = logList;
        this.context=context;
    }


    @Override
    public ParentLogHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
//        LayoutInflater inflater=LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.parent_log_card,parent,false);
        return new ParentLogHolder(view);
    }

    @Override
    public void onBindViewHolder(ParentLogHolder holder, int position) {
        logObj = logList.get(position);

        String dateStr = logObj.getLogDate();
        String[] arrOfDateStr = dateStr.split("-");
        holder.givenDateMonth.setText(arrOfDateStr[0]);
        holder.givenDateDay.setText(arrOfDateStr[1]);
        holder.givenDateYear.setText(arrOfDateStr[2]);
        holder.subject.setText("Subject : "+logObj.getLogSubject());
        holder.description.setText(logObj.getDescription());
//        holder.date.setText("Date : "+logObj.getLogDate());
    }

    @Override
    public int getItemCount() {
        return logList.size();
    }

    public void filter(List<Log> list){
        logList = new ArrayList<>();
        logList.addAll(list);
        notifyDataSetChanged();
    }

    class ParentLogHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView subject, description,givenDateMonth, givenDateDay, givenDateYear;

        public ParentLogHolder(View itemView) {
            super(itemView);
                itemView.setOnClickListener(this);
            subject = itemView.findViewById(R.id.subject);
//            date = itemView.findViewById(R.id.logDate);
            description = itemView.findViewById(R.id.description);
            givenDateMonth = itemView.findViewById(R.id.dateofGivenMonth);
            givenDateDay = itemView.findViewById(R.id.dateofGivenDay);
            givenDateYear = itemView.findViewById(R.id.dateofGivenYear);
        }

        @Override
        public void onClick(View view) {
            int postion=getAdapterPosition();
            Log log=logList.get(postion);
            Intent logIntent=new Intent(context,ParentLogDetailActivity.class);
//            logIntent.putExtra("logclass",log.getClassname());
//            logIntent.putExtra("logsection",log.getSection());
//            logIntent.putExtra("logdate",log.getLogDate());
            logIntent.putExtra("logdescriptiion",log.getDescription());
            logIntent.putExtra("logsubject",log.getLogSubject());
            context.startActivity(logIntent);

        }
    }
}
