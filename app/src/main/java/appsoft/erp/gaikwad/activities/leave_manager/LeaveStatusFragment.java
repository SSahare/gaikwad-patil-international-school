package appsoft.erp.gaikwad.activities.leave_manager;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import appsoft.erp.gaikwad.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LeaveStatusFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mManager;

    public LeaveStatusFragment() {    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.leaves_history, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        mManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mManager);

        getInfoToList();
        return view;
    }

    private void getInfoToList() {
        // get data here
    }
}
